package codexo.neonclasses;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.player.IframeplayerActivity;

public class Splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    AppCompatActivity activity;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        setContentView(R.layout.activity_splash);
        activity = Splash.this;
        Constant.imageLoader_ = ImageLoader.getInstance();
        Constant.imageLoader_.getInstance().init(Constant.getImageLoaderConfig(getApplicationContext()));
     ///   fullScreen();
        session = new SessionManager(getApplicationContext());
        Constant.device_info = " " + android.os.Build.VERSION.RELEASE + " " + android.os.Build.BRAND;
        Constant.device_model = Build.MODEL;
        Constant.device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Log.w("", "getInstanceId failed", task.getException());
                            return;
                        }

                        Constant.fcm_token = task.getResult().getToken();
                        Constant.device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//                        Log.d("demofcm", "TokenOld: "+task.getResult().getToken());
                        /*if (!Constant.adbEnabled(activity)){
                           Constant.checkAdb(activity);
                        }else {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    session.checkLogin();
                                    finish();

                                }
                            }, SPLASH_TIME_OUT);
                        }*/
                    }

                });

/*        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        Constant.fcm_token_new = token;
                        Log.d("demofcm", "TokenNew: "+Constant.fcm_token_new+" model "+Constant.device_model+"  id "+Constant.device_id+"  info  "+Constant.device_info);
//                        Toast.makeText(Splash.this, token, Toast.LENGTH_SHORT).show();
                    }
                });*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                session.checkLogin();
                finish();

            }
        }, SPLASH_TIME_OUT);
    }

    private void fullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    protected void onResume() {
        super.onResume();
      /*  if (!Constant.adbEnabled(activity)){
            Constant.checkAdb(activity);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    session.checkLogin();
                    finish();

                }
            }, SPLASH_TIME_OUT);
        }*/
    }
}