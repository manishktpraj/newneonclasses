package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class Register extends AppCompatActivity {
    AppCompatActivity activity;
    TextView welcomtoneon,welcomdesc,loginbutton,registerbutton;
    ImageView passwordeye;
    String passwordtext="hide";
    TextInputLayout username,password,refferalcode,mobile,email;
    TextInputEditText username_edit,password_edit,refferal_edit_text,mobile_edit,email_edit;
    CheckBox refferalstatus;
    String refferal_status="no";
    ProgressDialog pDialog;
    String username_string = "",password_string = "",mobile_string ="",
            email_string = "",refferal_edit_string = "";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Register.this;
        Constant.setToolbar(activity, toolbar);
        passwordeye =findViewById(R.id.passwordeye);
        welcomtoneon =findViewById(R.id.welcomtoneon);
        welcomdesc =findViewById(R.id.welcomdesc);
        loginbutton =findViewById(R.id.loginbutton);
          registerbutton=findViewById(R.id.registerbutton);
        refferalcode =findViewById(R.id.refferalcode);
        refferal_edit_text =findViewById(R.id.refferal_edit_text);
        refferalstatus =findViewById(R.id.refferalstatus);

        Constant.adbEnabled(Register.this);
        Constant.checkAdb(Register.this);
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
        welcomdesc.setTypeface(Constant.getFonts(getApplicationContext()));
        loginbutton.setTypeface(Constant.getsemiFonts(getApplicationContext()));
          registerbutton.setTypeface(Constant.getFontsBold(getApplicationContext()));
        refferal_edit_text.setTypeface(Constant.getFonts(getApplicationContext()));
        refferalstatus.setTypeface(Constant.getFonts(getApplicationContext()));

        pDialog = Constant.getProgressBar(Register.this);
        username= findViewById(R.id.username);
        username_edit =findViewById(R.id.username_edit);

        password= findViewById(R.id.password);
        password_edit =findViewById(R.id.password_edit);

        username_edit.setTypeface(Constant.getFonts(getApplicationContext()));
        username_edit.setEnabled(true);
        username_edit.setTextIsSelectable(true);
        username_edit.setFocusable(true);
        username_edit.setFocusableInTouchMode(true);
        username_edit.requestFocus();

        mobile= findViewById(R.id.mobile);
        mobile_edit =findViewById(R.id.mobile_edit);

        email= findViewById(R.id.email);
        email_edit =findViewById(R.id.email_edit);

        passwordeye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(passwordtext.equals("hide"))
                {
                    passwordtext ="show";
                    password_edit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordeye.setImageResource(R.drawable.passwordeyeclose);

                }else{
                    passwordtext ="hide";
                    password_edit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordeye.setImageResource(R.drawable.passwordeye);
                }
            }
        });

        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), Login.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
                finish();
            }
        });
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent iLogin = new Intent(getApplicationContext(), Otp.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);*/
                String go_next = "yes";
                username_string = username_edit.getText().toString();
                password_string = password_edit.getText().toString();
                mobile_string = mobile_edit.getText().toString();
                email_string = email_edit.getText().toString();
                refferal_edit_string = refferal_edit_text.getText().toString();
                if(username_string.equals(""))
                {
                    username_edit.setError("Please Enter Username");
                    go_next ="no";
                }
                if(mobile_string.equals(""))
                {
                    mobile_edit.setError("Please Enter Mobile number");
                    go_next ="no";
                }
                if(mobile_string.length()<10)
                {
                    mobile_edit.setError("Please Enter Valid Mobile number");
                    go_next ="no";
                }
                if(email_string.equals(""))
                {
                    email_edit.setError("Please Enter Email Id");
                    go_next ="no";
                }
                if (!email_string.matches(emailPattern) || email_string.length() <= 0)
                {
                    email_edit.setError("Please Enter Valid Email Id");
                    go_next ="no";
                }
                    if(password_string.equals(""))
                {
                    password_edit.setError("Please Enter password");
                    go_next ="no";
                }
                if(refferal_status.equals("yes"))
                {
                    if(refferal_edit_string.equals(""))
                    {
                        refferal_edit_text.setError("Please Enter Valid Refferal Code");
                        go_next ="no";
                    }
                }

                if(go_next.equals("yes")) {
                    doRegister(username_string,password_string,mobile_string,email_string,refferal_edit_string);
                }
            }
        });
        refferalstatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    refferal_status ="yes";
                    refferalcode.setVisibility(View.VISIBLE);
                }else{
                    refferal_status ="no";
                    refferalcode.setVisibility(View.GONE);
                }
            }
        });
    }
    public void customalert(String notification) {
        final Dialog builder = new  Dialog(Register.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.custome_alert, null);
        TextView txt_dia =  dialogLayout.findViewById(R.id.txt_dia);
        TextView txt_alert =  dialogLayout.findViewById(R.id.txt_alert);
        txt_dia.setText(notification);
        txt_alert.setText("Registration Failed");
        TextView btn_yes =  dialogLayout.findViewById(R.id.btn_yes);
        TextView btn_no =  dialogLayout.findViewById(R.id.btn_no);
        builder.setContentView(dialogLayout);
        builder.setCancelable(true);
        builder.show();
        btn_yes.setText("Retry");

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister(username_string,password_string,mobile_string,email_string,refferal_edit_string);
                 builder.dismiss();
            }
        });
    }

    private void doRegister(final String username_string, final String password_string,final String mobile_string,
                            final  String email_string,final String refferal_edit_string) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().doRegister(username_string,password_string,mobile_string,email_string,refferal_edit_string,Constant.fcm_token, Constant.device_id,Constant.device_info);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");

                            Toast.makeText(activity, notification, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(activity, Otp.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("results",localJSONObject2+"");
                            i.putExtra("type","Register");
                            activity.startActivity(i);
                            finish();
                        } else {
                            Constant.setToast(Register.this,notification);
                          ///  customalert(notification);
                         }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                doRegister(username_string, password_string,mobile_string,email_string,refferal_edit_string);
            }
        });
    }

    @Override
    public void onBackPressed(){
        startActivity(new Intent(getApplicationContext(),Intro.class));
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Register.this);
        Constant.checkAdb(Register.this);
    }
}