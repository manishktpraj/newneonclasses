package codexo.neonclasses.helperclasses;

import static codexo.neonclasses.Constant.UNANSWERED;
import static codexo.neonclasses.Constant.btnNext;
import static codexo.neonclasses.Constant.btnPrevious;
import static codexo.neonclasses.Constant.cardOptA;
import static codexo.neonclasses.Constant.cardOptB;
import static codexo.neonclasses.Constant.cardOptC;
import static codexo.neonclasses.Constant.cardOptD;
import static codexo.neonclasses.Constant.imgOptionA;
import static codexo.neonclasses.Constant.imgOptionB;
import static codexo.neonclasses.Constant.imgOptionC;
import static codexo.neonclasses.Constant.imgOptionD;
import static codexo.neonclasses.Constant.imgOptionE;
import static codexo.neonclasses.Constant.llIndexA;
import static codexo.neonclasses.Constant.llIndexB;
import static codexo.neonclasses.Constant.llIndexC;
import static codexo.neonclasses.Constant.llIndexD;
import static codexo.neonclasses.Constant.llIndexE;
import static codexo.neonclasses.Constant.llOptionA;
import static codexo.neonclasses.Constant.llOptionB;
import static codexo.neonclasses.Constant.llOptionC;
import static codexo.neonclasses.Constant.llOptionD;
import static codexo.neonclasses.Constant.llOptionE;
import static codexo.neonclasses.Constant.llReview;
import static codexo.neonclasses.Constant.llReview1;
import static codexo.neonclasses.Constant.optCardE;
import static codexo.neonclasses.Constant.solutionCardView;
import static codexo.neonclasses.Constant.tvquestion_no;
import static codexo.neonclasses.Constant.txtOptionA;
import static codexo.neonclasses.Constant.txtOptionB;
import static codexo.neonclasses.Constant.txtOptionC;
import static codexo.neonclasses.Constant.txtOptionD;
import static codexo.neonclasses.Constant.txtOptionE;
import static codexo.neonclasses.Constant.webQuestion;
import static codexo.neonclasses.Constant.webSolution;
import static codexo.neonclasses.Constant.imgSolution;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.model.UstQuesAnswerModel;
import codexo.neonclasses.ui.test.ExamActivity;

public class HelperFile {
    Cursor getnextprevdataCursor, fetchfromUstQueAnswerData;
    SqlDatabase db;
    Context context;


    public int tlq_counterid, langStatus, examType;
    public int tlq_id1;
    public int que_test_id1;
    public int tlq_question_id;
    public String tlq_unique_id;
    public int tlq_question_type;
    public String tlq_options;
    public String tlq_option1_attachments;
    public String tlq_option2_attachments;
    public String tlq_option3_attachments;
    public String tlq_option4_attachments;
    public String tlq_option5_attachments;
    public String tlq_lang_id;
    public String tlq_question_text;
    public String tlq_question_text_hindi;
    public String tlq_options_hindi;
    public String tlq_question_attachments;
    public String tlq_option1_attachments_hindi;
    public String tlq_option2_attachments_hindi;
    public String tlq_option3_attachments_hindi;
    public String tlq_option4_attachments_hindi;
    public String tlq_option5_attachments_hindi;
    public String tlq_question_attachments_hindi;
    public String tlq_english_instruction;
    public String tlq_hindi_instruction;
    public String tlq_hindi_solution;
    public String tlq_english_solution;
    public String tlq_hindi_solution_attach;
    public String tlq_english_solution_attach;
    public String tlq_option_attach;
    public int tlq_question_order;
    public int tlq_user_id;
    public String tlq_option_hindi_attach, ustqa_correct, ustqa_mark, ustqa_user_id;
    public int tlq_sub_id, tlq_correct_option, user_que_status, UserSelectedAns, tlq_id, ustqa_counterid, ustqa_id, ustqa_que_id, ustqa_user_ans, ustqa_test_id, ustqa_sub, ustqa_ust_id, ustqa_status, ustqa_review;
    public boolean tlq_question_type_hindi, tlq_question_status;
    List<QuestionModel> questionModels = new ArrayList<>();
    ArrayList<String> checkoption;
    ArrayList<String> checkoption1;
    ArrayList<String> checkoptionhindi;
    ArrayList<String> checkoptionhindi1;

    public HelperFile(SqlDatabase db, Context context) {
        this.db = db;
        this.context = context;
    }

    public void ExamQuestions(String Exam_id, int counters) {
        getnextprevdataCursor = db.getallquestiondataofnewxtprev(Integer.parseInt(Exam_id), counters);

        fetchfromUstQueAnswerData = db.getallUstQueAnswerData(Integer.parseInt(Exam_id), counters);

        getnextprevdataCursor.moveToNext();
        fetchfromUstQueAnswerData.moveToNext();

        tlq_id1 = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_id"));
        tlq_counterid = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_counterid"));
        que_test_id1 = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("que_test_id"));
        tlq_question_id = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_question_id"));
        tlq_unique_id = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_unique_id"));
        tlq_question_type = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_question_type"));
        tlq_options = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_options"));
        tlq_option1_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option1_attachments"));
        tlq_option2_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option2_attachments"));
        tlq_option3_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option3_attachments"));
        tlq_option4_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option4_attachments"));
        tlq_option5_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option5_attachments"));
        tlq_lang_id = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_lang_id"));
        tlq_question_text = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_question_text"));
        tlq_question_text_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_question_text_hindi"));
        tlq_options_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_options_hindi"));
        tlq_question_attachments = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_question_attachments"));
        tlq_option1_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option1_attachments_hindi"));
        tlq_option2_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option2_attachments_hindi"));
        tlq_option3_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option3_attachments_hindi"));
        tlq_option4_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option4_attachments_hindi"));
        tlq_option5_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option5_attachments_hindi"));
        tlq_question_type_hindi = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_question_type_hindi")) == 0;
        tlq_question_attachments_hindi = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_question_attachments_hindi"));
        tlq_english_instruction = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_english_instruction"));
        tlq_hindi_instruction = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_hindi_instruction"));
        tlq_hindi_solution = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_hindi_solution"));
        tlq_english_solution = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_english_solution"));
        tlq_hindi_solution_attach = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_hindi_solution_attach"));
        tlq_english_solution_attach = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_english_solution_attach"));
        tlq_option_attach = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option_attach"));
        tlq_question_status = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_question_status")) == 0;
        tlq_question_order = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_question_order"));
        tlq_user_id = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_user_id"));
        tlq_option_hindi_attach = getnextprevdataCursor.getString(getnextprevdataCursor.getColumnIndex("tlq_option_hindi_attach"));
        tlq_sub_id = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_sub_id"));
        tlq_correct_option = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("tlq_correct_option"));
        user_que_status = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("user_que_status"));
        UserSelectedAns = getnextprevdataCursor.getInt(getnextprevdataCursor.getColumnIndex("UserSelectedAns"));

        //fetch from UstQueAnswerData Table

        tlq_id = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("tlq_id"));
        ustqa_counterid = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("tlq_counterid"));
        ustqa_id = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_id"));
        ustqa_que_id = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_que_id"));
        ustqa_correct = fetchfromUstQueAnswerData.getString(fetchfromUstQueAnswerData.getColumnIndex("ustqa_correct"));
        ustqa_user_ans = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_user_ans"));
        ustqa_mark = fetchfromUstQueAnswerData.getString(fetchfromUstQueAnswerData.getColumnIndex("ustqa_mark"));
        ustqa_test_id = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_test_id"));
        ustqa_sub = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_sub"));
        ustqa_ust_id = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_ust_id"));
        ustqa_user_id = fetchfromUstQueAnswerData.getString(fetchfromUstQueAnswerData.getColumnIndex("ustqa_user_id"));
        ustqa_status = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_status"));
        ustqa_review = fetchfromUstQueAnswerData.getInt(fetchfromUstQueAnswerData.getColumnIndex("ustqa_review"));

        Constant.tlqUiniId = tlq_id;
        updateQuestionTableonNextPre(tlq_id, Constant.UNANSWERED);
        if (tlq_counterid == 1) {
            btnPrevious.setVisibility(View.GONE);
        } else {
            btnPrevious.setVisibility(View.VISIBLE);
        }
        if (Constant.questionModelList.size() == tlq_counterid) {
            btnNext.setVisibility(View.GONE);
        } else {
            btnNext.setVisibility(View.VISIBLE);
        }
        setAllQData(Constant.exLang);
        selectedOption(Constant.examTyped);
    }

    private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }

    public void setAllQData(int lang) {

        tvquestion_no.setText("Q. " + tlq_counterid);

        ArrayList<String> questionOption = new ArrayList<String>(Arrays.asList(tlq_options.split("###OPT###")));
        ArrayList<String> questionOptionHindi = new ArrayList<String>(Arrays.asList(tlq_options_hindi.split("###OPT###")));
        ArrayList<String> questionOptionHindiAttach = new ArrayList<String>(Arrays.asList(tlq_option_hindi_attach.split("###OPT###")));
        ArrayList<String> questionOptionEnglishAttach = new ArrayList<String>(Arrays.asList(tlq_option_attach.split("###OPT###")));
        Constant.logPrint("all_opt_lenth", "Opt En text :- " + questionOption.size() + "  Opt Hi text :- " + questionOptionHindi.size() + "  Opt Hi img :- " + questionOptionHindiAttach.size() + "  Opt Eng img :- " + questionOptionEnglishAttach.size());

        if (lang == 0) {
            if (!questionOption.isEmpty() || !questionOptionEnglishAttach.isEmpty()) {
                checkoption = questionOption;
                checkoption1 = questionOptionEnglishAttach;
                if (!questionOption.isEmpty()) {
                    for (int i = 0; i <= checkoption.size(); i++) {
                        if (checkoption.size() == 5) {
                            cardOptA.setVisibility(View.VISIBLE);
                            cardOptB.setVisibility(View.VISIBLE);
                            cardOptC.setVisibility(View.VISIBLE);
                            cardOptD.setVisibility(View.VISIBLE);
                            optCardE.setVisibility(View.VISIBLE);
                            if (checkoption.get(0).isEmpty()) {
                                txtOptionA.setVisibility(View.GONE);
                            } else {
                                imgOptionA.setVisibility(View.GONE);
                                txtOptionA.setVisibility(View.VISIBLE);
                                txtOptionA.setText(checkoption.get(0));
                            }
                            if (checkoption.get(1).isEmpty()) {
                                txtOptionB.setVisibility(View.GONE);
                            } else {
                                imgOptionB.setVisibility(View.GONE);
                                txtOptionB.setVisibility(View.VISIBLE);
                                txtOptionB.setText(checkoption.get(1));
                            }
                            if (checkoption.get(2).isEmpty()) {
                                txtOptionC.setVisibility(View.GONE);
                            } else {
                                imgOptionC.setVisibility(View.GONE);
                                txtOptionC.setVisibility(View.VISIBLE);
                                txtOptionC.setText(checkoption.get(2));
                            }
                            if (checkoption.get(3).isEmpty()) {
                                txtOptionD.setVisibility(View.GONE);
                            } else {
                                imgOptionD.setVisibility(View.GONE);
                                txtOptionD.setVisibility(View.VISIBLE);
                                txtOptionD.setText(checkoption.get(3));
                            }
                            if (checkoption.get(4).isEmpty()) {
                                txtOptionE.setVisibility(View.GONE);
                            } else {
                                imgOptionE.setVisibility(View.GONE);
                                txtOptionE.setVisibility(View.VISIBLE);
                                txtOptionE.setText(checkoption.get(4));
                            }
                        }
                        else if (checkoption.size() == 4) {
                            cardOptA.setVisibility(View.VISIBLE);
                            cardOptB.setVisibility(View.VISIBLE);
                            cardOptC.setVisibility(View.VISIBLE);
                            cardOptD.setVisibility(View.VISIBLE);
                            optCardE.setVisibility(View.GONE);
                            if (checkoption.get(0).isEmpty()) {
                                txtOptionA.setVisibility(View.GONE);
                            } else {
                                imgOptionA.setVisibility(View.GONE);
                                txtOptionA.setVisibility(View.VISIBLE);
                                txtOptionA.setText(checkoption.get(0));
                            }
                            if (checkoption.get(1).isEmpty()) {
                                txtOptionB.setVisibility(View.GONE);
                            } else {
                                imgOptionB.setVisibility(View.GONE);
                                txtOptionB.setVisibility(View.VISIBLE);
                                txtOptionB.setText(checkoption.get(1));
                            }
                            if (checkoption.get(2).isEmpty()) {
                                txtOptionC.setVisibility(View.GONE);
                            } else {
                                imgOptionC.setVisibility(View.GONE);
                                txtOptionC.setVisibility(View.VISIBLE);
                                txtOptionC.setText(checkoption.get(2));
                            }
                            if (checkoption.get(3).isEmpty()) {
                                txtOptionD.setVisibility(View.GONE);
                            } else {
                                imgOptionD.setVisibility(View.GONE);
                                txtOptionD.setVisibility(View.VISIBLE);
                                txtOptionD.setText(checkoption.get(3));
                            }
                        }
                        else if (checkoption.size() == 3) {
                            if (checkoption.size() == 3 && checkoption1.size() == 3) {
                                cardOptA.setVisibility(View.VISIBLE);
                                cardOptB.setVisibility(View.VISIBLE);
                                cardOptC.setVisibility(View.VISIBLE);
                                cardOptD.setVisibility(View.GONE);
                                optCardE.setVisibility(View.GONE);
                            } else {
                                cardOptA.setVisibility(View.VISIBLE);
                                cardOptB.setVisibility(View.VISIBLE);
                                cardOptC.setVisibility(View.VISIBLE);
                                cardOptD.setVisibility(View.VISIBLE);
                                optCardE.setVisibility(View.GONE);
                            }
                            if (checkoption.get(0).isEmpty()) {
                                txtOptionA.setVisibility(View.GONE);
                            } else {
                                imgOptionA.setVisibility(View.GONE);
                                txtOptionA.setVisibility(View.VISIBLE);
                                txtOptionA.setText(checkoption.get(0));
                            }
                            if (checkoption.get(1).isEmpty()) {
                                txtOptionB.setVisibility(View.GONE);
                            } else {
                                imgOptionB.setVisibility(View.GONE);
                                txtOptionB.setVisibility(View.VISIBLE);
                                txtOptionB.setText(checkoption.get(1));
                            }
                            if (checkoption.get(2).isEmpty()) {
                                txtOptionC.setVisibility(View.GONE);
                            } else {
                                imgOptionC.setVisibility(View.GONE);
                                txtOptionC.setVisibility(View.VISIBLE);
                                txtOptionC.setText(checkoption.get(2));
                            }
                        }
                        else if (checkoption.size() == 2) {
                            if (checkoption.get(0).isEmpty()) {
                                txtOptionA.setVisibility(View.GONE);
                            } else {
                                txtOptionA.setVisibility(View.VISIBLE);
                                txtOptionA.setText(checkoption.get(0));
                            }
                            if (checkoption.get(1).isEmpty()) {
                                txtOptionB.setVisibility(View.GONE);
                            } else {
                                txtOptionB.setVisibility(View.VISIBLE);
                                txtOptionB.setText(checkoption.get(1));
                            }
                        }
                        else if (checkoption.size() == 1) {
                            if (checkoption.get(0).isEmpty()) {
                                txtOptionA.setVisibility(View.GONE);
                            } else {
                                txtOptionA.setVisibility(View.VISIBLE);
                                txtOptionA.setText(checkoption.get(0));
                            }
                        }
                        else {
                            txtOptionA.setVisibility(View.GONE);
                            txtOptionB.setVisibility(View.GONE);
                            txtOptionC.setVisibility(View.GONE);
                            txtOptionD.setVisibility(View.GONE);
                            txtOptionE.setVisibility(View.GONE);
                        }
                    }
                }
                if (!questionOptionEnglishAttach.isEmpty()) {
                    for (int j = 0; j <= checkoption1.size(); j++) {
                        if (checkoption1.size() == 5) {
                            cardOptA.setVisibility(View.VISIBLE);
                            cardOptB.setVisibility(View.VISIBLE);
                            cardOptC.setVisibility(View.VISIBLE);
                            cardOptD.setVisibility(View.VISIBLE);
                            optCardE.setVisibility(View.VISIBLE);
                            if (checkoption1.get(0).isEmpty()) {
                                imgOptionA.setVisibility(View.GONE);
                            } else {
                                txtOptionA.setVisibility(View.GONE);
                                imgOptionA.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                            }
                            if (checkoption1.get(1).isEmpty()) {
                                imgOptionB.setVisibility(View.GONE);
                            } else {
                                txtOptionB.setVisibility(View.GONE);
                                imgOptionB.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                            }
                            if (checkoption1.get(2).isEmpty()) {
                                imgOptionC.setVisibility(View.GONE);
                            } else {
                                txtOptionC.setVisibility(View.GONE);
                                imgOptionC.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                            }
                            if (checkoption1.get(3).isEmpty()) {
                                imgOptionD.setVisibility(View.GONE);
                            } else {
                                txtOptionD.setVisibility(View.GONE);
                                imgOptionD.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                            }
                            if (checkoption1.get(4).isEmpty()) {
                                imgOptionE.setVisibility(View.GONE);
                            } else {
                                txtOptionE.setVisibility(View.GONE);
                                imgOptionE.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(4), imgOptionE, context);
                            }
                        } else if (checkoption1.size() == 4) {
                            cardOptA.setVisibility(View.VISIBLE);
                            cardOptB.setVisibility(View.VISIBLE);
                            cardOptC.setVisibility(View.VISIBLE);
                            cardOptD.setVisibility(View.VISIBLE);
                            optCardE.setVisibility(View.GONE);
                            if (checkoption1.get(0).isEmpty()) {
                                imgOptionA.setVisibility(View.GONE);
                            } else {
                                txtOptionA.setVisibility(View.GONE);
                                imgOptionA.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                            }
                            if (checkoption1.get(1).isEmpty()) {
                                imgOptionB.setVisibility(View.GONE);
                            } else {
                                txtOptionB.setVisibility(View.GONE);
                                imgOptionB.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                            }
                            if (checkoption1.get(2).isEmpty()) {
                                imgOptionC.setVisibility(View.GONE);
                            } else {
                                txtOptionC.setVisibility(View.GONE);
                                imgOptionC.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                            }
                            if (checkoption1.get(3).isEmpty()) {
                                imgOptionD.setVisibility(View.GONE);
                            } else {
                                txtOptionD.setVisibility(View.GONE);
                                imgOptionD.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                            }
                        }
                        else if (checkoption1.size() == 3) {
                            if (checkoption.size() == 3 && checkoption1.size() == 3) {
                                cardOptA.setVisibility(View.VISIBLE);
                                cardOptB.setVisibility(View.VISIBLE);
                                cardOptC.setVisibility(View.VISIBLE);
                                cardOptD.setVisibility(View.GONE);
                                optCardE.setVisibility(View.GONE);
                            } else {
                                cardOptA.setVisibility(View.VISIBLE);
                                cardOptB.setVisibility(View.VISIBLE);
                                cardOptC.setVisibility(View.VISIBLE);
                                cardOptD.setVisibility(View.VISIBLE);
                                optCardE.setVisibility(View.GONE);
                            }
                            if (checkoption1.get(0).isEmpty()) {
                                imgOptionA.setVisibility(View.GONE);
                            } else {
                                txtOptionA.setVisibility(View.GONE);
                                imgOptionA.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                            }
                            if (checkoption1.get(1).isEmpty()) {
                                imgOptionB.setVisibility(View.GONE);
                            } else {
                                txtOptionB.setVisibility(View.GONE);
                                imgOptionB.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                            }
                            if (checkoption1.get(2).isEmpty()) {
                                imgOptionC.setVisibility(View.GONE);
                            } else {
                                txtOptionC.setVisibility(View.GONE);
                                imgOptionC.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                            }
                        } else if (checkoption1.size() == 2) {
                            if (checkoption1.get(0).isEmpty()) {
                                imgOptionA.setVisibility(View.GONE);
                            } else {
                                imgOptionA.setVisibility(View.GONE);
                                txtOptionA.setVisibility(View.GONE);
                                imgOptionA.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                            }
                            if (checkoption1.get(1).isEmpty()) {
                                imgOptionB.setVisibility(View.GONE);
                            } else {
                                imgOptionB.setVisibility(View.GONE);
                                txtOptionB.setVisibility(View.GONE);
                                imgOptionB.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                            }
                        } else if (checkoption1.size() == 1) {
                            if (checkoption1.get(0).isEmpty()) {
                                imgOptionA.setVisibility(View.GONE);
                            } else {
                                imgOptionA.setVisibility(View.GONE);
                                txtOptionA.setVisibility(View.GONE);
                                imgOptionA.setVisibility(View.VISIBLE);
                                Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                            }
                        } else {
                            imgOptionA.setVisibility(View.GONE);
                            imgOptionB.setVisibility(View.GONE);
                            imgOptionC.setVisibility(View.GONE);
                            imgOptionD.setVisibility(View.GONE);
                            imgOptionE.setVisibility(View.GONE);
                        }
                    }
                }
            }
            webQuestion.getSettings().setBuiltInZoomControls(true);
            webQuestion.getSettings().setJavaScriptEnabled(true);
            webQuestion.setHorizontalScrollBarEnabled(true);
            webQuestion.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            String instruction_English = "";
            if (!tlq_english_instruction.equals("") && !tlq_english_instruction.equals("<p><br></p>")) {
                instruction_English = tlq_english_instruction;
            }

            String url = instruction_English + tlq_question_text;
            String urlStr = "https://neonclasses.com/";
            String mimeType = "text/html";
            String encoding = null;

            webQuestion.loadDataWithBaseURL(urlStr, getHtmlData(url), mimeType, encoding, urlStr);


            if (!tlq_english_solution.equals("") && !tlq_english_solution.equals("<p><br></p>")) {
                webSolution.getSettings().setBuiltInZoomControls(true);
                webSolution.getSettings().setJavaScriptEnabled(true);
                webSolution.setHorizontalScrollBarEnabled(true);
                webSolution.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                String tlq_english_solution1 = "";
                if (!tlq_english_solution.equals("") && !tlq_english_solution.equals("<p><br></p>")) {
                    tlq_english_solution1 = tlq_english_solution;
                }

                String urlS = tlq_english_solution1;
                String urlStrS = "https://neonclasses.com/";
                String mimeTypeS = "text/html";
                String encodingS = null;
                webSolution.loadDataWithBaseURL(urlStrS, getHtmlData(urlS), mimeTypeS, encodingS, urlStrS);
            } else if (!tlq_english_solution_attach.equals("")) {
                webSolution.setVisibility(View.GONE);
                imgSolution.setVisibility(View.VISIBLE);
                Constant.setImage(Constant.inExamImgUrl + tlq_english_solution_attach, imgSolution, context);
            } else {
                solutionCardView.setVisibility(View.GONE);
            }
        }
        else if (lang == 1) {

           if (questionOptionHindi.size() >0 || questionOptionHindiAttach.size() >0) {
               checkoptionhindi = questionOptionHindi;
               checkoptionhindi1 = questionOptionHindiAttach;
               if (questionOptionHindi.size() > 0) {
                   for (int i = 0; i <= checkoptionhindi.size(); i++) {
                       if (checkoptionhindi.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoptionhindi.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoptionhindi.get(0));
                           }
                           if (checkoptionhindi.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoptionhindi.get(1));
                           }
                           if (checkoptionhindi.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoptionhindi.get(2));
                           }
                           if (checkoptionhindi.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoptionhindi.get(3));
                           }
                           if (checkoptionhindi.get(4).isEmpty()) {
                               txtOptionE.setVisibility(View.GONE);
                           } else {
                               imgOptionE.setVisibility(View.GONE);
                               txtOptionE.setVisibility(View.VISIBLE);
                               txtOptionE.setText(checkoption.get(4));
                           }
                       }
                       else if (checkoptionhindi.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoptionhindi.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoptionhindi.get(0));
                           }
                           if (checkoptionhindi.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoptionhindi.get(1));
                           }
                           if (checkoptionhindi.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoptionhindi.get(2));
                           }
                           if (checkoptionhindi.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoptionhindi.get(3));
                           }
                       }
                       else if (checkoptionhindi.size() == 3) {
                           if (checkoptionhindi.size() == 3 && checkoptionhindi1.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoptionhindi.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoptionhindi.get(0));
                           }
                           if (checkoptionhindi.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoptionhindi.get(1));
                           }
                           if (checkoptionhindi.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoptionhindi.get(2));
                           }
                       }
                       else if (checkoptionhindi.size() == 2) {
                           if (checkoptionhindi.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoptionhindi.get(0));
                           }
                           if (checkoptionhindi.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoptionhindi.get(1));
                           }
                       }
                       else if (checkoptionhindi.size() == 1) {
                           if (checkoptionhindi.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoptionhindi.get(0));
                           }
                       }
                       else {
                           txtOptionA.setVisibility(View.GONE);
                           txtOptionB.setVisibility(View.GONE);
                           txtOptionC.setVisibility(View.GONE);
                           txtOptionD.setVisibility(View.GONE);
                           txtOptionE.setVisibility(View.GONE);
                       }
                   }
               } else if (questionOptionHindi.size() <= 0 && questionOption.size() > 0) {
                   ArrayList<String> checkoption11 = questionOption;
                   for (int i = 0; i <= checkoption11.size(); i++) {
                       if (checkoption11.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoption11.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption11.get(0));
                           }
                           if (checkoption11.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption11.get(1));
                           }
                           if (checkoption11.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption11.get(2));
                           }
                           if (checkoption11.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoption11.get(3));
                           }
                           if (checkoption11.get(4).isEmpty()) {
                               txtOptionE.setVisibility(View.GONE);
                           } else {
                               imgOptionE.setVisibility(View.GONE);
                               txtOptionE.setVisibility(View.VISIBLE);
                               txtOptionE.setText(checkoption11.get(4));
                           }
                       }
                       else if (checkoption11.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoption11.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption11.get(0));
                           }
                           if (checkoption11.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption11.get(1));
                           }
                           if (checkoption11.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption11.get(2));
                           }
                           if (checkoption11.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoption11.get(3));
                           }
                       }
                       else if (checkoption11.size() == 3) {
                           if (checkoption11.size() == 3 && checkoption1.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoption11.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption11.get(0));
                           }
                           if (checkoption11.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption11.get(1));
                           }
                           if (checkoption11.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption11.get(2));
                           }
                       }
                       else if (checkoption11.size() == 2) {
                           if (checkoption11.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption11.get(0));
                           }
                           if (checkoption11.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption11.get(1));
                           }
                       }
                       else if (checkoption11.size() == 1) {
                           if (checkoption11.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption11.get(0));
                           }
                       }
                       else {
                           txtOptionA.setVisibility(View.GONE);
                           txtOptionB.setVisibility(View.GONE);
                           txtOptionC.setVisibility(View.GONE);
                           txtOptionD.setVisibility(View.GONE);
                           txtOptionE.setVisibility(View.GONE);
                       }
                   }
               } else {
               }

               if (questionOptionHindiAttach.size() >0){
                   for (int j = 0; j <= checkoptionhindi1.size(); j++) {
                       if (checkoptionhindi1.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoptionhindi1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(0), imgOptionA, context);
                           }
                           if (checkoptionhindi1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(1), imgOptionB, context);
                           }
                           if (checkoptionhindi1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(2), imgOptionC, context);
                           }
                           if (checkoptionhindi1.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(3), imgOptionD, context);
                           }
                           if (checkoptionhindi1.get(4).isEmpty()) {
                               imgOptionE.setVisibility(View.GONE);
                           } else {
                               txtOptionE.setVisibility(View.GONE);
                               imgOptionE.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(4), imgOptionE, context);
                           }
                       }
                       else if (checkoptionhindi1.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoptionhindi1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(0), imgOptionA, context);
                           }
                           if (checkoptionhindi1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(1), imgOptionB, context);
                           }
                           if (checkoptionhindi1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(2), imgOptionC, context);
                           }
                           if (checkoptionhindi1.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(3), imgOptionD, context);
                           }
                       }
                       else if (checkoptionhindi1.size() == 3) {
                           if (checkoptionhindi.size() == 3 && checkoptionhindi1.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoptionhindi1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(0), imgOptionA, context);
                           }
                           if (checkoptionhindi1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(1), imgOptionB, context);
                           }
                           if (checkoptionhindi1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(2), imgOptionC, context);
                           }
                       }
                       else if (checkoptionhindi1.size() == 2) {
                           if (checkoptionhindi1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(0), imgOptionA, context);
                           }
                           if (checkoptionhindi1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(1), imgOptionB, context);
                           }
                       }
                       else if (checkoptionhindi1.size() == 1) {
                           if (checkoptionhindi1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoptionhindi1.get(0), imgOptionA, context);
                           }
                       }
                       else {
                           imgOptionA.setVisibility(View.GONE);
                           imgOptionB.setVisibility(View.GONE);
                           imgOptionC.setVisibility(View.GONE);
                           imgOptionD.setVisibility(View.GONE);
                           imgOptionE.setVisibility(View.GONE);
                       }
                   }
               }
               else if (questionOptionHindiAttach.size() <=0 && questionOptionEnglishAttach.size() >0){
                  ArrayList<String> checkoption12 = questionOptionEnglishAttach;
                   for (int j = 0; j <= questionOptionEnglishAttach.size(); j++) {
                       if (checkoption12.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoption12.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(0), imgOptionA, context);
                           }
                           if (checkoption12.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(1), imgOptionB, context);
                           }
                           if (checkoption12.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(2), imgOptionC, context);
                           }
                           if (checkoption12.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(3), imgOptionD, context);
                           }
                           if (checkoption12.get(4).isEmpty()) {
                               imgOptionE.setVisibility(View.GONE);
                           } else {
                               txtOptionE.setVisibility(View.GONE);
                               imgOptionE.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(4), imgOptionE, context);
                           }
                       }
                       else if (checkoption12.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoption12.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(0), imgOptionA, context);
                           }
                           if (checkoption12.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(1), imgOptionB, context);
                           }
                           if (checkoption12.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(2), imgOptionC, context);
                           }
                           if (checkoption12.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(3), imgOptionD, context);
                           }
                       }
                       else if (checkoption12.size() == 3) {
                           if (checkoptionhindi.size() == 3 && checkoption12.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoption12.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(0), imgOptionA, context);
                           }
                           if (checkoption12.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(1), imgOptionB, context);
                           }
                           if (checkoption12.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(2), imgOptionC, context);
                           }
                       }
                       else if (checkoption12.size() == 2) {
                           if (checkoption12.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(0), imgOptionA, context);
                           }
                           if (checkoption12.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(1), imgOptionB, context);
                           }
                       }
                       else if (checkoption12.size() == 1) {
                           if (checkoption12.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption12.get(0), imgOptionA, context);
                           }
                       } else {
                           imgOptionA.setVisibility(View.GONE);
                           imgOptionB.setVisibility(View.GONE);
                           imgOptionC.setVisibility(View.GONE);
                           imgOptionD.setVisibility(View.GONE);
                           imgOptionE.setVisibility(View.GONE);
                       }
                   }
               }
              /* else {
                   checkoption = questionOption;
                   checkoption1 = questionOptionEnglishAttach;
                   if (!questionOption.isEmpty()) {
                       for (int i = 0; i <= checkoption.size(); i++) {
                           if (checkoption.size() == 5) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.VISIBLE);
                               if (checkoption.get(0).isEmpty()) {
                                   txtOptionA.setVisibility(View.GONE);
                               } else {
                                   imgOptionA.setVisibility(View.GONE);
                                   txtOptionA.setVisibility(View.VISIBLE);
                                   txtOptionA.setText(checkoption.get(0));
                               }
                               if (checkoption.get(1).isEmpty()) {
                                   txtOptionB.setVisibility(View.GONE);
                               } else {
                                   imgOptionB.setVisibility(View.GONE);
                                   txtOptionB.setVisibility(View.VISIBLE);
                                   txtOptionB.setText(checkoption.get(1));
                               }
                               if (checkoption.get(2).isEmpty()) {
                                   txtOptionC.setVisibility(View.GONE);
                               } else {
                                   imgOptionC.setVisibility(View.GONE);
                                   txtOptionC.setVisibility(View.VISIBLE);
                                   txtOptionC.setText(checkoption.get(2));
                               }
                               if (checkoption.get(3).isEmpty()) {
                                   txtOptionD.setVisibility(View.GONE);
                               } else {
                                   imgOptionD.setVisibility(View.GONE);
                                   txtOptionD.setVisibility(View.VISIBLE);
                                   txtOptionD.setText(checkoption.get(3));
                               }
                               if (checkoption.get(4).isEmpty()) {
                                   txtOptionE.setVisibility(View.GONE);
                               } else {
                                   imgOptionE.setVisibility(View.GONE);
                                   txtOptionE.setVisibility(View.VISIBLE);
                                   txtOptionE.setText(checkoption.get(4));
                               }
                           }
                           else if (checkoption.size() == 4) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                               if (checkoption.get(0).isEmpty()) {
                                   txtOptionA.setVisibility(View.GONE);
                               } else {
                                   imgOptionA.setVisibility(View.GONE);
                                   txtOptionA.setVisibility(View.VISIBLE);
                                   txtOptionA.setText(checkoption.get(0));
                               }
                               if (checkoption.get(1).isEmpty()) {
                                   txtOptionB.setVisibility(View.GONE);
                               } else {
                                   imgOptionB.setVisibility(View.GONE);
                                   txtOptionB.setVisibility(View.VISIBLE);
                                   txtOptionB.setText(checkoption.get(1));
                               }
                               if (checkoption.get(2).isEmpty()) {
                                   txtOptionC.setVisibility(View.GONE);
                               } else {
                                   imgOptionC.setVisibility(View.GONE);
                                   txtOptionC.setVisibility(View.VISIBLE);
                                   txtOptionC.setText(checkoption.get(2));
                               }
                               if (checkoption.get(3).isEmpty()) {
                                   txtOptionD.setVisibility(View.GONE);
                               } else {
                                   imgOptionD.setVisibility(View.GONE);
                                   txtOptionD.setVisibility(View.VISIBLE);
                                   txtOptionD.setText(checkoption.get(3));
                               }
                           }
                           else if (checkoption.size() == 3) {
                               if (checkoption.size() == 3 && checkoption1.size() == 3) {
                                   cardOptA.setVisibility(View.VISIBLE);
                                   cardOptB.setVisibility(View.VISIBLE);
                                   cardOptC.setVisibility(View.VISIBLE);
                                   cardOptD.setVisibility(View.GONE);
                                   optCardE.setVisibility(View.GONE);
                               } else {
                                   cardOptA.setVisibility(View.VISIBLE);
                                   cardOptB.setVisibility(View.VISIBLE);
                                   cardOptC.setVisibility(View.VISIBLE);
                                   cardOptD.setVisibility(View.VISIBLE);
                                   optCardE.setVisibility(View.GONE);
                               }
                               if (checkoption.get(0).isEmpty()) {
                                   txtOptionA.setVisibility(View.GONE);
                               } else {
                                   imgOptionA.setVisibility(View.GONE);
                                   txtOptionA.setVisibility(View.VISIBLE);
                                   txtOptionA.setText(checkoption.get(0));
                               }
                               if (checkoption.get(1).isEmpty()) {
                                   txtOptionB.setVisibility(View.GONE);
                               } else {
                                   imgOptionB.setVisibility(View.GONE);
                                   txtOptionB.setVisibility(View.VISIBLE);
                                   txtOptionB.setText(checkoption.get(1));
                               }
                               if (checkoption.get(2).isEmpty()) {
                                   txtOptionC.setVisibility(View.GONE);
                               } else {
                                   imgOptionC.setVisibility(View.GONE);
                                   txtOptionC.setVisibility(View.VISIBLE);
                                   txtOptionC.setText(checkoption.get(2));
                               }
                           }
                           else if (checkoption.size() == 2) {
                               if (checkoption.get(0).isEmpty()) {
                                   txtOptionA.setVisibility(View.GONE);
                               } else {
                                   txtOptionA.setVisibility(View.VISIBLE);
                                   txtOptionA.setText(checkoption.get(0));
                               }
                               if (checkoption.get(1).isEmpty()) {
                                   txtOptionB.setVisibility(View.GONE);
                               } else {
                                   txtOptionB.setVisibility(View.VISIBLE);
                                   txtOptionB.setText(checkoption.get(1));
                               }
                           }
                           else if (checkoption.size() == 1) {
                               if (checkoption.get(0).isEmpty()) {
                                   txtOptionA.setVisibility(View.GONE);
                               } else {
                                   txtOptionA.setVisibility(View.VISIBLE);
                                   txtOptionA.setText(checkoption.get(0));
                               }
                           }
                           else {
                               txtOptionA.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.GONE);
                               txtOptionE.setVisibility(View.GONE);
                           }
                       }
                   }
                   if (!questionOptionEnglishAttach.isEmpty()) {
                       for (int j = 0; j <= checkoption1.size(); j++) {
                           if (checkoption1.size() == 5) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.VISIBLE);
                               if (checkoption1.get(0).isEmpty()) {
                                   imgOptionA.setVisibility(View.GONE);
                               } else {
                                   txtOptionA.setVisibility(View.GONE);
                                   imgOptionA.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                               }
                               if (checkoption1.get(1).isEmpty()) {
                                   imgOptionB.setVisibility(View.GONE);
                               } else {
                                   txtOptionB.setVisibility(View.GONE);
                                   imgOptionB.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                               }
                               if (checkoption1.get(2).isEmpty()) {
                                   imgOptionC.setVisibility(View.GONE);
                               } else {
                                   txtOptionC.setVisibility(View.GONE);
                                   imgOptionC.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                               }
                               if (checkoption1.get(3).isEmpty()) {
                                   imgOptionD.setVisibility(View.GONE);
                               } else {
                                   txtOptionD.setVisibility(View.GONE);
                                   imgOptionD.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                               }
                               if (checkoption1.get(4).isEmpty()) {
                                   imgOptionE.setVisibility(View.GONE);
                               } else {
                                   txtOptionE.setVisibility(View.GONE);
                                   imgOptionE.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(4), imgOptionE, context);
                               }
                           } else if (checkoption1.size() == 4) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                               if (checkoption1.get(0).isEmpty()) {
                                   imgOptionA.setVisibility(View.GONE);
                               } else {
                                   txtOptionA.setVisibility(View.GONE);
                                   imgOptionA.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                               }
                               if (checkoption1.get(1).isEmpty()) {
                                   imgOptionB.setVisibility(View.GONE);
                               } else {
                                   txtOptionB.setVisibility(View.GONE);
                                   imgOptionB.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                               }
                               if (checkoption1.get(2).isEmpty()) {
                                   imgOptionC.setVisibility(View.GONE);
                               } else {
                                   txtOptionC.setVisibility(View.GONE);
                                   imgOptionC.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                               }
                               if (checkoption1.get(3).isEmpty()) {
                                   imgOptionD.setVisibility(View.GONE);
                               } else {
                                   txtOptionD.setVisibility(View.GONE);
                                   imgOptionD.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                               }
                           }
                           else if (checkoption1.size() == 3) {
                               if (checkoption.size() == 3 && checkoption1.size() == 3) {
                                   cardOptA.setVisibility(View.VISIBLE);
                                   cardOptB.setVisibility(View.VISIBLE);
                                   cardOptC.setVisibility(View.VISIBLE);
                                   cardOptD.setVisibility(View.GONE);
                                   optCardE.setVisibility(View.GONE);
                               } else {
                                   cardOptA.setVisibility(View.VISIBLE);
                                   cardOptB.setVisibility(View.VISIBLE);
                                   cardOptC.setVisibility(View.VISIBLE);
                                   cardOptD.setVisibility(View.VISIBLE);
                                   optCardE.setVisibility(View.GONE);
                               }
                               if (checkoption1.get(0).isEmpty()) {
                                   imgOptionA.setVisibility(View.GONE);
                               } else {
                                   txtOptionA.setVisibility(View.GONE);
                                   imgOptionA.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                               }
                               if (checkoption1.get(1).isEmpty()) {
                                   imgOptionB.setVisibility(View.GONE);
                               } else {
                                   txtOptionB.setVisibility(View.GONE);
                                   imgOptionB.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                               }
                               if (checkoption1.get(2).isEmpty()) {
                                   imgOptionC.setVisibility(View.GONE);
                               } else {
                                   txtOptionC.setVisibility(View.GONE);
                                   imgOptionC.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                               }
                           } else if (checkoption1.size() == 2) {
                               if (checkoption1.get(0).isEmpty()) {
                                   imgOptionA.setVisibility(View.GONE);
                               } else {
                                   imgOptionA.setVisibility(View.GONE);
                                   txtOptionA.setVisibility(View.GONE);
                                   imgOptionA.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                               }
                               if (checkoption1.get(1).isEmpty()) {
                                   imgOptionB.setVisibility(View.GONE);
                               } else {
                                   imgOptionB.setVisibility(View.GONE);
                                   txtOptionB.setVisibility(View.GONE);
                                   imgOptionB.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                               }
                           } else if (checkoption1.size() == 1) {
                               if (checkoption1.get(0).isEmpty()) {
                                   imgOptionA.setVisibility(View.GONE);
                               } else {
                                   imgOptionA.setVisibility(View.GONE);
                                   txtOptionA.setVisibility(View.GONE);
                                   imgOptionA.setVisibility(View.VISIBLE);
                                   Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                               }
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.GONE);
                               imgOptionE.setVisibility(View.GONE);
                           }
                       }
                   }
               }*/
           }
           else {
               checkoption = questionOption;
               checkoption1 = questionOptionEnglishAttach;
               if (!questionOption.isEmpty()) {
                   for (int i = 0; i <= checkoption.size(); i++) {
                       if (checkoption.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoption.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption.get(0));
                           }
                           if (checkoption.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption.get(1));
                           }
                           if (checkoption.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption.get(2));
                           }
                           if (checkoption.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoption.get(3));
                           }
                           if (checkoption.get(4).isEmpty()) {
                               txtOptionE.setVisibility(View.GONE);
                           } else {
                               imgOptionE.setVisibility(View.GONE);
                               txtOptionE.setVisibility(View.VISIBLE);
                               txtOptionE.setText(checkoption.get(4));
                           }
                       }
                       else if (checkoption.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoption.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption.get(0));
                           }
                           if (checkoption.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption.get(1));
                           }
                           if (checkoption.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption.get(2));
                           }
                           if (checkoption.get(3).isEmpty()) {
                               txtOptionD.setVisibility(View.GONE);
                           } else {
                               imgOptionD.setVisibility(View.GONE);
                               txtOptionD.setVisibility(View.VISIBLE);
                               txtOptionD.setText(checkoption.get(3));
                           }
                       }
                       else if (checkoption.size() == 3) {
                           if (checkoption.size() == 3 && checkoption1.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoption.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption.get(0));
                           }
                           if (checkoption.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption.get(1));
                           }
                           if (checkoption.get(2).isEmpty()) {
                               txtOptionC.setVisibility(View.GONE);
                           } else {
                               imgOptionC.setVisibility(View.GONE);
                               txtOptionC.setVisibility(View.VISIBLE);
                               txtOptionC.setText(checkoption.get(2));
                           }
                       }
                       else if (checkoption.size() == 2) {
                           if (checkoption.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption.get(0));
                           }
                           if (checkoption.get(1).isEmpty()) {
                               txtOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.VISIBLE);
                               txtOptionB.setText(checkoption.get(1));
                           }
                       }
                       else if (checkoption.size() == 1) {
                           if (checkoption.get(0).isEmpty()) {
                               txtOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.VISIBLE);
                               txtOptionA.setText(checkoption.get(0));
                           }
                       }
                       else {
                           txtOptionA.setVisibility(View.GONE);
                           txtOptionB.setVisibility(View.GONE);
                           txtOptionC.setVisibility(View.GONE);
                           txtOptionD.setVisibility(View.GONE);
                           txtOptionE.setVisibility(View.GONE);
                       }
                   }
               }
               if (!questionOptionEnglishAttach.isEmpty()) {
                   for (int j = 0; j <= checkoption1.size(); j++) {
                       if (checkoption1.size() == 5) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.VISIBLE);
                           if (checkoption1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                           }
                           if (checkoption1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                           }
                           if (checkoption1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                           }
                           if (checkoption1.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                           }
                           if (checkoption1.get(4).isEmpty()) {
                               imgOptionE.setVisibility(View.GONE);
                           } else {
                               txtOptionE.setVisibility(View.GONE);
                               imgOptionE.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(4), imgOptionE, context);
                           }
                       } else if (checkoption1.size() == 4) {
                           cardOptA.setVisibility(View.VISIBLE);
                           cardOptB.setVisibility(View.VISIBLE);
                           cardOptC.setVisibility(View.VISIBLE);
                           cardOptD.setVisibility(View.VISIBLE);
                           optCardE.setVisibility(View.GONE);
                           if (checkoption1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                           }
                           if (checkoption1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                           }
                           if (checkoption1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                           }
                           if (checkoption1.get(3).isEmpty()) {
                               imgOptionD.setVisibility(View.GONE);
                           } else {
                               txtOptionD.setVisibility(View.GONE);
                               imgOptionD.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(3), imgOptionD, context);
                           }
                       }
                       else if (checkoption1.size() == 3) {
                           if (checkoption.size() == 3 && checkoption1.size() == 3) {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.GONE);
                               optCardE.setVisibility(View.GONE);
                           } else {
                               cardOptA.setVisibility(View.VISIBLE);
                               cardOptB.setVisibility(View.VISIBLE);
                               cardOptC.setVisibility(View.VISIBLE);
                               cardOptD.setVisibility(View.VISIBLE);
                               optCardE.setVisibility(View.GONE);
                           }
                           if (checkoption1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                           }
                           if (checkoption1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                           }
                           if (checkoption1.get(2).isEmpty()) {
                               imgOptionC.setVisibility(View.GONE);
                           } else {
                               txtOptionC.setVisibility(View.GONE);
                               imgOptionC.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(2), imgOptionC, context);
                           }
                       } else if (checkoption1.size() == 2) {
                           if (checkoption1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                           }
                           if (checkoption1.get(1).isEmpty()) {
                               imgOptionB.setVisibility(View.GONE);
                           } else {
                               imgOptionB.setVisibility(View.GONE);
                               txtOptionB.setVisibility(View.GONE);
                               imgOptionB.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(1), imgOptionB, context);
                           }
                       } else if (checkoption1.size() == 1) {
                           if (checkoption1.get(0).isEmpty()) {
                               imgOptionA.setVisibility(View.GONE);
                           } else {
                               imgOptionA.setVisibility(View.GONE);
                               txtOptionA.setVisibility(View.GONE);
                               imgOptionA.setVisibility(View.VISIBLE);
                               Constant.setImage(Constant.inExamImgUrl + checkoption1.get(0), imgOptionA, context);
                           }
                       } else {
                           imgOptionA.setVisibility(View.GONE);
                           imgOptionB.setVisibility(View.GONE);
                           imgOptionC.setVisibility(View.GONE);
                           imgOptionD.setVisibility(View.GONE);
                           imgOptionE.setVisibility(View.GONE);
                       }
                   }
               }
           }


            webQuestion.getSettings().setBuiltInZoomControls(true);
            webQuestion.getSettings().setJavaScriptEnabled(true);
            webQuestion.setHorizontalScrollBarEnabled(true);
            webQuestion.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            String instruction_hindi = "";
            String question_text_text = "";
            if (!tlq_hindi_instruction.equals("") && !tlq_hindi_instruction.equals("<p><br></p>")) {
                instruction_hindi = tlq_hindi_instruction;
            } else if (!tlq_english_instruction.equals("") && !tlq_english_instruction.equals("<p><br></p>")) {
                instruction_hindi = tlq_english_instruction;
            } else {
            }

            if (!tlq_question_text_hindi.equals("") && !tlq_question_text_hindi.equals("<p><br></p>")) {
                question_text_text = tlq_question_text_hindi;
            } else if (!tlq_question_text.equals("")) {
                question_text_text = tlq_question_text;
            } else {
            }
            String url = instruction_hindi + question_text_text;
            String urlStr = "https://neonclasses.com/";
            String mimeType = "text/html";
            String encoding = null;
            webQuestion.loadDataWithBaseURL(urlStr, getHtmlData(url), mimeType, encoding, urlStr);


            if (!tlq_hindi_solution.equals("") && !tlq_hindi_solution.equals("<p><br></p>") || !tlq_english_solution.equals("") && !tlq_english_solution.equals("<p><br></p>")) {
                webSolution.getSettings().setBuiltInZoomControls(true);
                webSolution.getSettings().setJavaScriptEnabled(true);
                webSolution.setHorizontalScrollBarEnabled(true);
                webSolution.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                String tlq_hindi_solution1 = "";
                if (!tlq_hindi_solution.equals("") && !tlq_hindi_solution.equals("<p><br></p>")) {
                    tlq_hindi_solution1 = tlq_hindi_solution;
                } else if (!tlq_english_solution.equals("") && !tlq_english_solution.equals("<p><br></p>")) {
                    tlq_hindi_solution1 = tlq_english_solution;
                } else {
                }

                String urlS = tlq_hindi_solution1;
                String urlStrS = "https://neonclasses.com/";
                String mimeTypeS = "text/html";
                String encodingS = null;

                webSolution.loadDataWithBaseURL(urlStrS, getHtmlData(urlS), mimeTypeS, encodingS, urlStrS);
            } else if (!tlq_hindi_solution_attach.equals("")) {
                webSolution.setVisibility(View.GONE);
                imgSolution.setVisibility(View.VISIBLE);
                Constant.setImage(Constant.inExamImgUrl + tlq_hindi_solution_attach, imgSolution, context);
            } else if (tlq_hindi_solution_attach.equals("") && !tlq_english_solution_attach.equals("")) {
                webSolution.setVisibility(View.GONE);
                imgSolution.setVisibility(View.VISIBLE);
                Constant.setImage(Constant.inExamImgUrl + tlq_english_solution_attach, imgSolution, context);
            } else {
                solutionCardView.setVisibility(View.GONE);
            }

        } else {

        }
    }

    public void selectedOption(int exType) {
        if (exType == 0 || exType == 1) {

            if (ustqa_review == 1) {
                llReview.setVisibility(View.GONE);
                llReview1.setVisibility(View.VISIBLE);
            } else {
                llReview.setVisibility(View.VISIBLE);
                llReview1.setVisibility(View.GONE);
            }
            if (ustqa_user_ans == 1) {
//                Log.e("Check", "Check=" + 1);
                llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.green_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
            } else if (ustqa_user_ans == 2) {
//                Log.e("Check", "Check=" + 2);
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.green_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
            } else if (ustqa_user_ans == 3) {
//                Log.e("Check", "Check=" + 3);
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.green_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
            } else if (ustqa_user_ans == 4) {
//                Log.e("Check", "Check=" + 4);
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.green_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
            } else if (ustqa_user_ans == 5) {
//                Log.e("Check", "Check=" + 5);
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.green_circle);
            } else {
//                Log.e("Check", "Check=" + 6);
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }

            //Section for selectiong options
            llOptionA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(tlq_id, 1, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            llOptionB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(tlq_id, 2, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            llOptionC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(tlq_id, 3, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            llOptionD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(tlq_id, 4, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            llOptionE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(tlq_id, 5, Constant.ANSWERED);
                }
            });
            llReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    llReview.setVisibility(View.GONE);
                    llReview1.setVisibility(View.VISIBLE);
                    updateQuestionTableWithReview(tlq_id, 0, Constant.REVIEW);
                }
            });
            llReview1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    llReview1.setVisibility(View.GONE);
                    llReview.setVisibility(View.VISIBLE);
                    updateQuestionTableWithReview(tlq_id, 0, Constant.UNANSWERED);
                }
            });
        } else if (exType == 2) {
            if (ustqa_correct.equals("1")) {
                if (ustqa_user_ans == 0) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 1) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 2) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.red_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 3) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.red_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 4) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.red_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 5) {
                    llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.green_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            } else if (ustqa_correct.equals("2")) {
                if (ustqa_user_ans == 0) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 1) {
                    llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.red_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 2) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 3) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.red_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 4) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.red_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 5) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.green_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            } else if (ustqa_correct.equals("3")) {
                if (ustqa_user_ans == 0) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 1) {
                    llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.red_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 2) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.red_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 3) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 4) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.red_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 5) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.green_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            } else if (ustqa_correct.equals("4")) {
                if (ustqa_user_ans == 0) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 1) {
                    llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.red_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 2) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.red_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 3) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.red_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 4) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqa_user_ans == 5) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.green_circle);
                    llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            } else if (ustqa_correct.equals("5")) {
                if (ustqa_user_ans == 0) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqa_user_ans == 1) {
                    llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.red_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqa_user_ans == 2) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.red_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqa_user_ans == 3) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.red_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqa_user_ans == 4) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.red_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqa_user_ans == 5) {
                    llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    llIndexE.setBackgroundResource(R.drawable.green_circle);
                }
            }


        }
    }

    public void updateQuestionTable(int questionId, int selectedAnswer, int questionStatus) {
        Boolean checkupdatedata = db.updateQuestionTable(questionId, selectedAnswer, questionStatus);
//        Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_status"));
        @SuppressLint("Range") int tlqid = res.getInt(res.getColumnIndex("tlq_id"));
        changecolor(selectedans);
    }

    public void updateQuestionTableonNextPre(int questionId, int questionStatus) {
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_status"));
        @SuppressLint("Range") int tlqid = res.getInt(res.getColumnIndex("tlq_id"));
        if (selectedans != 0) {

            Boolean checkupdatedata = db.updateQuestionTableonNextPre(questionId, 1);
        } else {
            Boolean checkupdatedata = db.updateQuestionTableonNextPre(questionId, 0);
        }

//        Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
    }

    public void updateClearQuestionTable(int questionId, int selectedAnswer, int questionStatus) {
//        Log.e("getDatass", "else= Status : " + questionStatus + " Answer : " + selectedAnswer + " QuestionId : " + questionId);
        Boolean checkupdatedata = db.updateQuestionTable(questionId, selectedAnswer, questionStatus);
        Boolean checkupdatedata1 = db.updateQuestionReviewTable(questionId, selectedAnswer, questionStatus);
        if (checkupdatedata && checkupdatedata1) {
//            Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(context, "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        }

        changecolor(selectedAnswer);
    }

    // Option select Color change
    public void changecolor(int selectedAnswer) {
//        Log.e("checkss", selectedAnswer + "");
        switch (selectedAnswer) {
            case 1:
                llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.green_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                Log.e("Checks", "Check=" + 1);
                break;
            case 2:
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.green_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                Log.e("Checks", "Check=" + 2);
                break;
            case 3:
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.green_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                Log.e("Checks", "Check=" + 3);
                break;
            case 4:
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.green_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                Log.e("Checks", "Check=" + 4);
                break;
            case 5:
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.green_circle);
//                Log.e("Checks", "Check=" + 5);
                break;
            default:
                llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                llIndexA.setBackgroundResource(R.drawable.grey_circle);
                llIndexB.setBackgroundResource(R.drawable.grey_circle);
                llIndexC.setBackgroundResource(R.drawable.grey_circle);
                llIndexD.setBackgroundResource(R.drawable.grey_circle);
                llIndexE.setBackgroundResource(R.drawable.grey_circle);
                llReview1.setVisibility(View.GONE);
                llReview.setVisibility(View.VISIBLE);
                break;
        }

    }

    public void updateQuestionTableWithReview(int questionId, int selectedAnswer, int questionStatus) {
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_review"));
        UstQuesAnswerModel quedata = new UstQuesAnswerModel();
        quedata.ustqa_review = quesSta;
        quedata.ustqa_user_ans = selectedans;
        if (selectedans != 0) {
            db.updateQuestionReviewTable(questionId, quedata.ustqa_user_ans, questionStatus);
//            Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        } else {
            db.updateQuestionReviewTable(questionId, selectedAnswer, questionStatus);
//            Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        }

    }
}
