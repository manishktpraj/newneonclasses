package codexo.neonclasses;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.load.engine.Resource;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import java.util.List;

import codexo.neonclasses.databinding.ActivityMainBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.download.DownloadsActivity;
import codexo.neonclasses.ui.help.HelpActivity;
import codexo.neonclasses.ui.home.HomeFragment;
import codexo.neonclasses.ui.news.NewsActivity;
import codexo.neonclasses.ui.offer.OfferActivity;
import codexo.neonclasses.ui.profile.MyProfileActivity;
import codexo.neonclasses.ui.profile.ProfileFragment;
import codexo.neonclasses.ui.purchase.MyPurchaseActivity;
import codexo.neonclasses.ui.share.ShareActivity;
import codexo.neonclasses.ui.study.StudyMaterialFragment;
import codexo.neonclasses.ui.test.TestSeriesFragment;
import codexo.neonclasses.ui.videos.VideosFragment;

public class MainActivity extends AppCompatActivity {

    private static ActivityMainBinding binding;
    SessionManager session;
    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;
    private static final int FLEXIBLE_APP_UPDATE_REQ_CODE = 123;
    public static void openDrawer() {
        binding.drawer.openDrawer(GravityCompat.START);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Settings.Global.putInt(getContentResolver(), Settings.Global.ADB_ENABLED, 0);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(MainActivity.this);
        Constant.checkAdb(MainActivity.this);
        openFragment(new HomeFragment());
        session = new SessionManager(getApplicationContext());
        Constant.setStatusBar(MainActivity.this);
        binding.bottomNavigation.setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    openFragment(new HomeFragment());
                    return true;
                case R.id.navigation_test:
                    openFragment(new TestSeriesFragment());
                    return true;
                case R.id.navigation_video:
                    openFragment(new VideosFragment());
                    return true;
                case R.id.navigation_study:
                    openFragment(new StudyMaterialFragment());
                    return true;
                case R.id.navigation_user:
                    openFragment(new ProfileFragment());
                    return true;
            }
            return false;
        });

//        Constant.logPrint("FCMFCMFCMFCMFCMFCMFCMFCMFCMFCMFCMFCMFCM", Constant.fcm_token);

        binding.drawerLayout.txtPurchase.setOnClickListener(view -> startActivity(new Intent(this, MyPurchaseActivity.class)));
        binding.drawerLayout.txtHome.setOnClickListener(view -> {
            binding.drawer.close();
            binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(R.id.navigation_home).getItemId());
        });
        binding.drawerLayout.txtName.setText(session.getFirstName());
        binding.drawerLayout.txtName.setTypeface(Constant.getFontsBold(getApplicationContext()));
//        Constant.logPrint(session.getUserPhone(),"session.getUserPhone()");
        binding.drawerLayout.txtNumber.setText(session.getUserPhone());
        binding.drawerLayout.txtNumber.setTypeface(Constant.getFontsBold(getApplicationContext()));

        binding.drawerLayout.txtTest.setOnClickListener(view -> {
            binding.drawer.close();
            binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(R.id.navigation_test).getItemId());
        });
        binding.drawerLayout.txtStudy.setOnClickListener(view -> {
            binding.drawer.close();
            binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(R.id.navigation_study).getItemId());
        });
        binding.drawerLayout.txtVideo.setOnClickListener(view -> {
            binding.drawer.close();
            binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(R.id.navigation_video).getItemId());
        });
        binding.drawerLayout.txtRate.setOnClickListener(view -> {
            binding.drawer.close();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=codexo.neonclasses"));
            startActivity(browserIntent);
        });
        binding.drawerLayout.txtOffer.setOnClickListener(view -> {
            binding.drawer.close();
            Intent intent = new Intent(getApplicationContext(), OfferActivity.class);
            intent.putExtra("type", "Home");
            intent.putExtra("amount", "amount");
            intent.putExtra("apply_on", "apply_on");
            intent.putExtra("apply_on_id", "apply_on_id");
            startActivity(intent);
        });


        binding.drawerLayout.txtHelp.setOnClickListener(view -> startActivity(new Intent(this, HelpActivity.class)));
        binding.drawerLayout.txtNumber.setOnClickListener(view -> startActivity(new Intent(this, MyProfileActivity.class)));
        binding.drawerLayout.txtDownloads.setOnClickListener(view -> startActivity(new Intent(this, DownloadsActivity.class)));
        binding.drawerLayout.txtNews.setOnClickListener(view -> startActivity(new Intent(this, NewsActivity.class)));
        binding.drawerLayout.txtCurrent.setOnClickListener(view -> startActivity(new Intent(this, NewsActivity.class)));
        binding.drawerLayout.txtShare.setOnClickListener(view -> startActivity(new Intent(this, ShareActivity.class)));
        binding.drawerLayout.txtVersion.setText("Version "+ BuildConfig.VERSION_NAME);

        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        installStateUpdatedListener = state -> {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                removeInstallStateUpdateListener();
            } else {
                Toast.makeText(getApplicationContext(), "InstallStateUpdatedListener: state: " + state.installStatus(), Toast.LENGTH_LONG).show();
            }
        };
        appUpdateManager.registerListener(installStateUpdatedListener);
        checkUpdate();

    }
    @Override
    protected void onStop() {
        super.onStop();
        removeInstallStateUpdateListener();
    }
    private void checkUpdate() {

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                startUpdateFlow(appUpdateInfo);
//                Constant.logPrint(appUpdateInfoTask+"","appUpdateInfoTaskappUpdateInfoTask");
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate();
//                Constant.logPrint(appUpdateInfoTask+"","appUpdateInfoTaskappUpdateInfoTask");

            }
        });
    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, this, FLEXIBLE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void popupSnackBarForCompleteUpdate() {
      /*  Snackbar snackbar =   Snackbar.make(findViewById(android.R.id.content).getRootView(), "New app is ready!", Snackbar.LENGTH_INDEFINITE);



        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)snackbarLayout.getLayoutParams();
        layoutParams.setMargins(15, 20, 15, 32);
        snackbarLayout.setLayoutParams(layoutParams);*/
        binding.customToastContainer.setVisibility(View.VISIBLE);
        binding.installbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appUpdateManager != null) {
                    appUpdateManager.completeUpdate();
                }
            }
        });
      /*  Snackbar.make(findViewById(android.R.id.content).getRootView(), "New app is ready!", Snackbar.LENGTH_INDEFINITE).setAction("Install", view -> {
                    if (appUpdateManager != null) {
                        appUpdateManager.completeUpdate();
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.purple_500) )
                .show();*/
    }

    private void removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
            binding.customToastContainer.setVisibility(View.GONE);
        }
    }
    public void setselected(int id) {
        binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(id).getItemId());

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FLEXIBLE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Update canceled by user! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Update success! Result Code: " + resultCode, Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getApplicationContext(), "Update Failed! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                checkUpdate();
            }
        }
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private Fragment getVisibleFragment() {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }
    @Override
    public void onBackPressed() {
        if (getVisibleFragment() instanceof HomeFragment) {
            new AlertDialog.Builder(this)
                    .setTitle("Confirm exit")
                    .setMessage("Are you sure you want to exit app?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }else{

            openFragment(new HomeFragment());
            binding.bottomNavigation.setSelectedItemId(binding.bottomNavigation.getMenu().findItem(R.id.navigation_home).getItemId());

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Constant.adbEnabled(MainActivity.this)){
            Constant.checkAdb(MainActivity.this);
        }
    }

}