package codexo.neonclasses.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.Splash;
import codexo.neonclasses.session.SessionManager;

import static android.content.Context.NOTIFICATION_SERVICE;
public class MyNotificationManager {
    private Context mCtx;
    private static MyNotificationManager mInstance;

    static SessionManager sessionManager;
    
    private MyNotificationManager(Context context) {
        mCtx = context;
    }
    
    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
            
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            
            sessionManager=new SessionManager(context);
        }
        return mInstance;
    }
    
    
    public void displayNotification(String id, String title, String message,String youtubelink) {
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
//        Log.d("Random", "" + m);
        
        Constant.fcm_call = "Yes";
        Constant.token = readTokenFromFile();
        Intent resultIntent = null;

        if(message.equals("multiple login")) {
            Constant.home="0";
            sessionManager.logoutUser();
        }
        if(youtubelink=="") {

//            Constant.logPrint("DataDataData",message);
            if(message.equals("multiple login")) {
                        sessionManager.logoutUser();
            }else {
                switch (id) {
                    case "1":
                        if (sessionManager.isLoggedIn()) {
                            resultIntent = new Intent(mCtx, MainActivity.class);
                        } else {
                            resultIntent = new Intent(mCtx, Splash.class);
                        }

                        break;

                    case "5":
                        if (sessionManager.isLoggedIn()) {
                            resultIntent = new Intent(mCtx, MainActivity.class);
                        } else {
                            resultIntent = new Intent(mCtx, Splash.class);
                        }
                        break;
                    default:
                        if (sessionManager.isLoggedIn()) {
                            resultIntent = new Intent(mCtx, MainActivity.class);
                        } else {
                            resultIntent = new Intent(mCtx, Splash.class);
                        }

                        break;
                }
            }
        }else{
            resultIntent = new Intent(Intent.ACTION_VIEW);
            resultIntent.setData(Uri.parse(youtubelink));
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mCtx, Constant.CHANNEL_ID)
                        .setSmallIcon(R.drawable.unnamed)
                       // .setLargeIcon(R.drawable.avtar)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(false)
                        .setColor(mCtx.getResources().getColor(R.color.red))
                        .setFullScreenIntent(pendingIntent ,false )
                        .setStyle(new NotificationCompat.BigTextStyle());
//        Log.d("message123", message);
        NotificationManager mNotifyMgr =
                (NotificationManager) mCtx.getSystemService(NOTIFICATION_SERVICE);

        if (mNotifyMgr != null) {
            mNotifyMgr.notify(0, mBuilder.build());
        }
    }
    
    public void displayNotificationImage(String id, String title, String message, Bitmap bitmap,String youtubelink) {
        
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
//        Log.d("Random", "" + m);
        
        Constant.fcm_call = "Yes";
        Constant.token = readTokenFromFile();
        Intent resultIntent = null;

        if(youtubelink.equals("")) {
             if(message.equals("multiple login")) {

//                 Constant.logPrint("DataDataData",message);
                    Constant.home="0";
                    sessionManager.logoutUser();
            }else {
                 switch (id) {
                     case "1":
                         if (sessionManager.isLoggedIn()) {
                             resultIntent = new Intent(mCtx, MainActivity.class);
                         } else {
                             resultIntent = new Intent(mCtx, Splash.class);
                         }
                         break;
                        default:
                         if (sessionManager.isLoggedIn()) {
                             resultIntent = new Intent(mCtx, MainActivity.class);
                         } else {
                             resultIntent = new Intent(mCtx, Splash.class);
                         }
                         break;
                 }
             }
        }else{
            resultIntent = new Intent(Intent.ACTION_VIEW);
            resultIntent.setData(Uri.parse(youtubelink));
         }
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mCtx, Constant.CHANNEL_ID)
                        .setSmallIcon(R.drawable.unnamed)
                        .setLargeIcon(bitmap)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(false)
                        .setColor(mCtx.getResources().getColor(R.color.red))
                        .setPriority(Notification.PRIORITY_MAX)
                        .setFullScreenIntent(pendingIntent ,true )
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(bitmap));
        
        NotificationManager mNotifyMgr =
                (NotificationManager) mCtx.getSystemService(NOTIFICATION_SERVICE);
        
        if (mNotifyMgr != null) {
            mNotifyMgr.notify(0, mBuilder.build());
        }
    }
    
    public String readTokenFromFile() {
        String ret = "";
        try {
            InputStream inputStream = mCtx.openFileInput("token.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
//            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
//            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }
    
}