package codexo.neonclasses.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;


public class MyFireBaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    String title = "";
    String id = "";
    String message = "";
    String image_url = "";
    String image = "";
    String youtubelink="";
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
//        Log.e("NEW_TOKEN",s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        
//        Log.d(TAG, "remoteMessage99999: " + remoteMessage);
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "data: " + remoteMessage.getData());
        
        
        if (remoteMessage.getNotification() == null) {
            try {
                JSONObject data_obj = new JSONObject(remoteMessage.getData());
                title = data_obj.getString("title");
                message = data_obj.getString("body");
                image = data_obj.getString("image");
                image_url = "";
                youtubelink = data_obj.getString("youtubelink");
//                Constant.logPrint(youtubelink,"youtubelinkyoutubelink");
                id = "0";
                if (title == null || title.equals("")) {
                    title = getResources().getString(R.string.app_name);
                }
    
                if (image == null || image.equals("")) {
                    // generateNotification(this, id, title, message, blog_id, news_id);
        
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(Constant.CHANNEL_ID, Constant.CHANNEL_NAME, importance);
                        mChannel.setDescription(Constant.CHANNEL_DESCRIPTION);
                        mChannel.enableLights(true);
                        mChannel.setLightColor(Color.RED);
                        mChannel.enableVibration(true);
                        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                        mNotificationManager.createNotificationChannel(mChannel);
                    }
        
                    MyNotificationManager.getInstance(this).displayNotification(id, title, message,youtubelink);
        
//                    Log.d("from webservice..", "webservice");
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
//                            Log.d("from webservice..", "webservice_img");
                            new sendNotification(getApplicationContext()).execute(id, title, message, image, image_url,youtubelink);
                        }
                    }).start();
                }
                
            } catch (Exception ex) {
//                Log.d("ex..", ex.toString());
            }
        }
        
    }
    
    private class sendNotification extends AsyncTask<String, Void, Bitmap> {
        
        Context ctx;
        String title = "";
        String id = "";
        String image = "";
        String message = "";
        boolean is_correct_image_path = false;
        String youtubelink="";
        public sendNotification(Context ctx) {
            super();
            this.ctx = ctx;
        }
        
        @Override
        protected Bitmap doInBackground(String... params) {
            
            InputStream in;
            id = params[0];
            title = params[1];
            message = params[2];
            image = params[3];
            youtubelink =params[5];
//            Constant.logPrint(youtubelink,"youtubelinkyoutubelink");
            if (image.equals("")) {
            
            } else {
                try {
                    image = image.replaceAll(" ", "%20");
                    URL url = new URL(image);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                            in = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(in);
                    is_correct_image_path = true;
                    return myBitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException ee) {
                    ee.printStackTrace();
                }
                
            }
            return null;
        }
        
        
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(Constant.CHANNEL_ID, Constant.CHANNEL_NAME, importance);
                mChannel.setDescription(Constant.CHANNEL_DESCRIPTION);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.RED);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mNotificationManager.createNotificationChannel(mChannel);
            }
            MyNotificationManager.getInstance(ctx.getApplicationContext()).displayNotificationImage(id, title, message, result,youtubelink);

        }
    }
    
}
