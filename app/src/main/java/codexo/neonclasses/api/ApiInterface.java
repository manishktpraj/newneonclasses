package codexo.neonclasses.api;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.model.ExamResponse;
import codexo.neonclasses.model.SubjectModel;
import codexo.neonclasses.model.SubmitModel.SubmitModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
//
//    @FormUrlEncoded
//    @POST("register")
//    Call<User> performRegistration(@Field("name") String name,
//                                   @Field("email") String email,
//                                   @Field("number") String number,
//                                   @Field("password") String password,
//                                   @Field("confirm_password") String confirm_password,
//                                   @Field("refer_code") String refer_code,
//                                   @Field("gender") String gender);

    @FormUrlEncoded
    @POST("exam/examwebservicesnew")
    Call<ExamResponse> examdetails(@Field("student_id") String student_id,
                                         @Field("test_id") String test_id);

    @FormUrlEncoded
    @POST("exam/submitexamwebservice2")
    Call<SubmitModel> submitExamDetails(@Field("all_quesion") JSONArray all_quesion,
                                        @Field("sub_data") JSONArray sub_data,
                                        @Field("test_id") String test_id,
                                        @Field("timer") String timer,
                                        @Field("ustid") JSONObject ustid);

    @FormUrlEncoded
    @POST("exam/submitexamwebservicesolution")
    Call<SubmitModel> resumeExamDetails(@Field("all_quesion") JSONArray all_quesion,
                                        @Field("sub_data") JSONArray sub_data,
                                        @Field("test_id") String test_id,
                                        @Field("timer") String timer,
                                        @Field("ustid") JSONObject ustid);




}
