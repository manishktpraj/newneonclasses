package codexo.neonclasses.api;

import codexo.neonclasses.Constant;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetResponse {


    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.LOGIN)
    Call<String> doLogin(
            @Field("user_email") String user_email,
            @Field("user_password") String user_password,
            @Field("device_id") String device_id,
            @Field("device_info") String device_info);

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.REGISTER)
    Call<String> doRegister(
            @Field("student_first_name") String student_first_name,
            @Field("student_password") String student_password,
            @Field("student_phone") String student_phone,
            @Field("student_email") String student_email,
            @Field("student_ref_id") String student_ref_id,
            @Field("userFirebaseToken") String userFirebaseToken,
            @Field("userDeviceId") String userDeviceId,
            @Field("userdeviceos") String userdeviceos
            );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.RESENDOTP)
    Call<String> resendotp(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.VERIFYOTP)
    Call<String> verifyotp(
            @Field("student_id") String student_id,
             @Field("student_otp") String otp
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.LOGINVIAOTP)
    Call<String> loginviaotp(
            @Field("student_phone") String student_phone
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FORGOT)
    Call<String> forgotpassword(
            @Field("student_phone") String student_phone
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CHANGEPASSWORD)
    Call<String> changepassword(
            @Field("student_id") String student_id,
            @Field("student_password") String student_password
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.DASHBOARD)
    Call<String> getdashboard(
            @Field("student_id") String student_id,
               @Field("userFirebaseToken") String userFirebaseToken,
                 @Field("userDeviceId") String userDeviceId,
                   @Field("userdeviceos") String userdeviceos
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.FREEVIDEO)
    Call<String> freevideolist(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.BOOKS)
    Call<String> neonbooks(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.POPULARCOURSE)
    Call<String> popularcourse(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.UPDATEPROFILE)
    Call<String> updateprofile(
            @Field("student_id") String student_id,
            @Field("student_first_name") String student_first_name
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.NEONPLUS)
    Call<String> neonplus(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.STUDY_MATERIAL_FREE)
    Call<String> getfreestudymaterial(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.NEWS)
    Call<String> getnewsserver(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.NOTIFICATION)
    Call<String> getnotification(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.VIDEOS)
    Call<String> getvideos(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.VIDEOSUBCATEGORY)
    Call<String> getvideosubcategory(
            @Field("student_id") String student_id,
            @Field("parent_id") String parent_id

    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CATEGORYWISEVIDEO)
    Call<String> categorywisevideo(
            @Field("student_id") String student_id,
            @Field("parent_id") String parent_id

    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.STUDYSUBCATEGORY)
    Call<String> gtestudysubcategory(
            @Field("student_id") String student_id,
            @Field("parent_id") String parent_id

    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.CATEGORYWISESTUDY)
    Call<String> categorywisestudy(
            @Field("student_id") String student_id,
            @Field("parent_id") String parent_id

    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.SUBSCRIPTION)
    Call<String> getsubscription(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.PURCHASELIST)
    Call<String> getpurchase(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.SHARE_CONTENT)
    Call<String> getsharecontent(
            @Field("student_id") String student_id
    );
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.weburl)
    Call<String> getweburl(
            @Field("grand_total_amount") String grand_total_amount,
            @Field("user_id") String user_id,
            @Field("refferal_off") String refferal_off,
            @Field("refferal_user_id") String refferal_user_id,
            @Field("coupon_id") String coupon_id,
            @Field("sub_pack_id") String sub_pack_id,
          @Field("discount_amount") String discount_amount,
            @Field("redeem_amount") String redeem_amount);
            ;



    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.OFFERS)
    Call<String> getoffers(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.GETCOUPONDETAIL)
    Call<String> getcoupondetail(
            @Field("student_id") String student_id,
            @Field("coupon_code") String coupon_code,
            @Field("apply_on_id") String apply_on_id,
            @Field("coupon_apply_on") String coupon_apply_on,
            @Field("amount") String amount
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ISSUE)
    Call<String> uploadTicket(
            @Field("user_message") String user_message,
            @Field("changeproffimage") String changeproffimage,
            @Field("user_id") String user_id,
            @Field("user_name") String user_name,
            @Field("user_phone") String user_phone);


    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.ISSUEHISTORY)
    Call<String> getTicketHistory(
            @Field("user_id") String user_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.EXAM)
    Call<String> exam(
            @Field("student_id") String student_id
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.DEPARTMENT)
    Call<String> department(
            @Field("user_id") String user_id,
            @Field("type") String type
    );

    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.GETCOURSEDETAIL)
    Call<String> getcoursedetail(
            @Field("user_id") String user_id,
            @Field("pack_id") String pack_id
    );
}
