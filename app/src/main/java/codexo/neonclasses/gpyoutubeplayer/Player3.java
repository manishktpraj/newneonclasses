package codexo.neonclasses.gpyoutubeplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityPlayer3Binding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.player.ExoUtilCustom;
import codexo.neonclasses.ui.player.WebuiPlayer;

import static codexo.neonclasses.gpyoutubeplayer.Constance.UrlConstance.FULL_SCREEN_OFF;

public class Player3 extends AppCompatActivity {
    private ActivityPlayer3Binding binding;

    protected SimpleExoPlayer player;
    private String YOUTUBE_VIDEO_ID = "";
    private String url = "";
    DatabaseReference messagesRef;
    String vid_id = "",vid_url="";
    JSONArray jsonarayquality = new JSONArray();
    JSONObject video_detail = new JSONObject();
    JSONArray arrayListchat = new JSONArray();
    private static LinearLayoutManager mLayoutManager;
    private Context context;
    AppCompatActivity activity;
    SessionManager session;
    String student_id = "0";
    private FirebaseDatabase mDatabase;
    private DataSource.Factory dataSourceFactory;
    private ValueEventListener mSearchedLocationReferenceListener;

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int idx = 0; idx < group.getChildCount(); idx++) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        binding=ActivityPlayer3Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        session = new SessionManager(getApplicationContext());
        activity = Player3.this;
        student_id = session.getUserId();
        dataSourceFactory = ExoUtilCustom.getDataSourceFactory(this);
        mDatabase = FirebaseDatabase.getInstance();
        //Constant.fullScreen(Player3.this);


        try {
            video_detail = new JSONObject(getIntent().getStringExtra("video_detail"));
//            Constant.logPrint("video_detail",video_detail+"");

            vid_url = video_detail.getString("vid_url");
//            Constant.logPrint("vid_urlurlurlrulurlurl",vid_url);
            //YOUTUBE_VIDEO_ID = extractVideoIdFromUrl(video_detail.getString("vid_url"));
            vid_id = video_detail.getString("vid_id");
            String custommg = video_detail.getString("vid_off_message");
            if (!custommg.equals("")) {
                binding.customMessageData.setText(custommg);
                binding.custommessage.setVisibility(View.VISIBLE);
            }
//            Constant.logPrint("video_detail", video_detail + "");
            binding.txtTitle.setText(video_detail.getString("vid_title"));
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
//            Constant.logPrint(YOUTUBE_VIDEO_ID, "sdsdddddddddddddd");
            //YOUTUBE_VIDEO_ID ="LDvq9TM0snc"; E1Ameff6F44
            //mYoutubeLink = BASE_URL + "/watch?v=" + YOUTUBE_VIDEO_ID;
            String MESSAGE_CHANNEL = "/neonmain/" + video_detail.getString("vid_id") + "/";
//            Constant.logPrint(MESSAGE_CHANNEL, "MESSAGE_CHANNEL");
            messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);
            context = this;
            /*binding.playerView.setErrorMessageProvider(new PlayerActivity.PlayerErrorMessageProvider());
            binding.playerView.requestFocus();*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        binding.YoutubeActivity.initialize(vid_url+"?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" + "&amp;" + FULL_SCREEN_OFF);
        //binding.YoutubeActivity.initialize(vid_url+"?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" + "&amp;" + FULL_SCREEN_OFF);
        binding.YoutubeActivity.setOnLongClickListener(null);

        enableDisableView(binding.YoutubeActivity, false);

        binding.YoutubeActivity.setLongClickable(false);
        binding.YoutubeActivity.setHapticFeedbackEnabled(false);
        binding.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.overLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.overLay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.back.setOnClickListener(v -> {
            finish();
        });

        binding.username.setText(session.getUserShareCode());

        binding.editChatBox.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String go_next = "yes";
                    String username_string = binding.editChatBox.getText().toString();
                    if (username_string.equals("")) {
                        int duration = Toast.LENGTH_SHORT;
                        binding.editChatBox.setError("Please Enter Your Question");
                        go_next = "no";
                    }

                    if (go_next.equals("yes")) {
                        Long daedat = new Date().getTime();
                        String userid = session.getUserId();
                        Map<String, String> formData = new HashMap<String, String>();
                        formData.put("user_id", userid);
                        formData.put("doubts_user_name", session.getUserName());
                        formData.put("doubts_user_email", session.getUserEmail());
                        formData.put("doubts_user_phone", session.getUserPhone());
                        formData.put("video_chat_reply", "");
                        formData.put("video_reply_time", "");
                        formData.put("videochat_created", daedat + "");
                        formData.put("videochat_message", username_string);
//                        Constant.logPrint(formData + "", "dddddddddddddddd");
//                        Constant.logPrint(daedat + "", "daedatdaedatdaedatdaedatdaedat" + formData + "");
                        // messagesRef.child(daedat+"").setValue(formData);
                        messagesRef.push().setValue(formData);
                        binding.editChatBox.setText("");
                        Toast.makeText(activity, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                    }
                    hideSoftKeyboard(Player3.this);
                    return true;
                }
                return false;
            }
        });
        binding.imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = binding.editChatBox.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    binding.editChatBox.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();
                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("video_chat_reply", "");
                    formData.put("video_reply_time", "");
                    formData.put("videochat_created", formattedDate + "");
                    formData.put("videochat_message", username_string);
//                    Constant.logPrint(formData + "", "dddddddddddddddd");
//                    Constant.logPrint(daedat + "", "daedatdaedatdaedatdaedatdaedat" + formData + "");
                    // messagesRef.child(daedat+"").setValue(formData);
                    messagesRef.push().setValue(formData);
                    binding.editChatBox.setText("");
                    Toast.makeText(activity, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
                    Log.d("Locations updated", "location: " + vid_id + location);

                    if (locationSnapshot != null) {
                        try {
                            JSONObject objd = new JSONObject();
                            objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                            objd.put("videochat_created", locationSnapshot.child("videochat_created").getValue().toString());
                            objd.put("videochat_message", locationSnapshot.child("videochat_message").getValue().toString());
                            objd.put("video_chat_reply", locationSnapshot.child("video_chat_reply").getValue().toString());
                            objd.put("video_reply_time", locationSnapshot.child("video_reply_time").getValue().toString());
                            objd.put("doubts_user_email", locationSnapshot.child("doubts_user_email").getValue().toString());
                            objd.put("doubts_user_name", locationSnapshot.child("doubts_user_name").getValue().toString());
                            objd.put("doubts_user_phone", locationSnapshot.child("doubts_user_phone").getValue().toString());

                            ///       Log.d("updatedupdatedupdatedupdatedupdated", "location: " +  objd);
                            if (objd.getString("user_id").equals(session.getUserId())) {
                                arrayListchat.put(objd);
//                            Constant.logPrint("locationSnapshot",arrayListchat.toString());
                            }

                            if (!binding.recyclerView.equals(null)) {
                                final CustomAdapterCat adapter = new CustomAdapterCat(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                binding.recyclerView.setLayoutManager(mLayoutManager);
                                binding.recyclerView.setHasFixedSize(true);
                                binding.recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Constant.adbEnabled(Player3.this)){
            Constant.checkAdb(Player3.this);
        }
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            binding.lay.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
            ViewGroup.LayoutParams parms = binding.YoutubeActivity.getLayoutParams();
            parms.height = (int) getResources().getDimension(R.dimen.height);
        } else {
            binding.lay.setBackgroundColor(ContextCompat.getColor(this, R.color.trans));
        }

        binding.viewInFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    int orientation = getResources().getConfiguration().orientation;
                    if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        ViewGroup.LayoutParams parms = binding.YoutubeActivity.getLayoutParams();
                        parms.height = ViewGroup.LayoutParams.MATCH_PARENT; // LayoutParams: android.view.ViewGroup.LayoutParams
                        //((ViewGroup.MarginLayoutParams)parms).topMargin  = (int) getResources().getDimension(R.dimen.margin_top);

                        //parms.height =    resources.getDimension(R.dimen.height_full).toInt() // LayoutParams: android.view.ViewGroup.LayoutParams
                        binding.YoutubeActivity.requestLayout();
                        binding.lay.setVisibility(View.VISIBLE);
                        binding.lay.setBackgroundColor(ContextCompat.getColor(Player3.this, R.color.trans));
                        binding.txtTitle.setVisibility(View.GONE);
                        binding.overLay1.setVisibility(View.GONE);
                    } else {
                        setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        ViewGroup.LayoutParams parms = binding.YoutubeActivity.getLayoutParams();
                        parms.height = (int) getResources().getDimension(R.dimen.height); // LayoutParams: android.view.ViewGroup.LayoutParams
                        ((ViewGroup.MarginLayoutParams)parms).topMargin = (int) getResources().getDimension(R.dimen.margin_top_prt);
                        binding.YoutubeActivity.requestLayout();
                        binding.lay.setVisibility(View.VISIBLE);
                        binding.lay.setBackgroundColor(ContextCompat.getColor(Player3.this, R.color.red));
                        binding.txtTitle.setVisibility(View.VISIBLE);
                        binding.overLay1.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(),
                    0
            );
        }
    }

    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            CustomAdapterCat.MainListHolder listHolder = new CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final CustomAdapterCat.MainListHolder mainHolder = (CustomAdapterCat.MainListHolder) holder;


            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("videochat_message"));
                String comment = arrayListchat.getJSONObject(position).getString("videochat_message");
                String reply = arrayListchat.getJSONObject(position).getString("video_chat_reply");
                if (!comment.equals("")) {
                    mainHolder.admincomment.setText(comment);
                }

                if (!reply.equals("")) {
                    mainHolder.usercomment.setText(reply);
                } else {
                    mainHolder.usercomment.setVisibility(View.GONE);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title, admincomment, usercomment;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.txt_des);
                usercomment = view.findViewById(R.id.txt_reply);
            }
        }
    }
    /*final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public void onLongPress(MotionEvent e) {
            Log.e("Longpress detected", "Longpress detected");
            Toast.makeText(Player3.this, "Your Pressed", Toast.LENGTH_SHORT).show();
        }
    });

    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
        //return false;
    }*/
}