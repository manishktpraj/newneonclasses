package codexo.neonclasses.gpyoutubeplayer.Constance;

public class UrlConstance {
    public static String REMOVE_VIDEO_ICON="modestbranding=1";
    public static String YOUTUBE_CONTROLLER="controls=1";
    public static String REMOVE_INFO="showinfo=0";
    public static String FULL_SCREEN_OFF="fs=0";
    public static String FULL_SCREEN_ON="fs=1";
    public static String YOUTUBE_URL="https://www.youtube-nocookie.com/embed/";
}
