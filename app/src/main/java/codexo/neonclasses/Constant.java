package codexo.neonclasses;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import codexo.neonclasses.api.GetResponse;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.ui.videos.VideosubcategoryActivity;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static java.lang.Integer.parseInt;

public class Constant {

    public static final String YOUR_NEW_API_URL_LIVE = "https://neonclasses.com/testapi/";
    public static final String YOUR_TEST_SERIES_API_URL = "https://www.neonclasses.com/webapi/";
    public static final String CHAT_URL = "http://neonclasses.co.in/portal/chat/index.html";


    public static final String weburl = "https://neonclasses.com/webapi/orders/createTransactionOnCartIdpackagenew";


    public static final String INSTRUCTION = "https://www.neonclasses.com/webapi/exam/instruction/262";
    public static final String ANALYTICSURL = "https://neonclasses.com/webapi/exam/report/";

    public static final String LOGIN = "register/loginWithpassword";
    public static final String REGISTER = "register/finalregister";
    public static final String RESENDOTP = "register/resendotp";
    public static final String VERIFYOTP = "register/verifyotp";
    public static final String SHARE_CONTENT = "register/sharetext";
    public static final String LOGINVIAOTP = "register/loginWithMobile";
    public static final String FORGOT = "register/forgot";
    public static final String CHANGEPASSWORD = "register/changepassword";
    public static final String UPDATEPROFILE = "register/updateuserprofile";
    public static final String DASHBOARD = "dashboard/";
    public static final String FREEVIDEO = "video/freevideo";
    public static final String BOOKS = "dashboard/neonbooks";
    public static final String POPULARCOURSE = "dashboard/popularcourse";
    public static final String NEONPLUS = "dashboard/getWalletHistory";
    public static final String STUDY_MATERIAL_FREE = "dashboard/getstudymaterialfree";
    public static final String NEWS = "dashboard/getnews";
    public static final String OFFERS = "dashboard/getoffers";
    public static final String GETCOUPONDETAIL = "dashboard/getcoupondetail";
    public static final String NOTIFICATION = "dashboard/notification";
//    public static final String VIDEOS = "video";
    public static final String VIDEOS = "video/testing";
    public static final String Email_TO = "neonclasses007@gmail.com";
    public static final String VIDEOSUBCATEGORY = "video/videosubcategory";
    public static final String CATEGORYWISEVIDEO = "video/videolist";
    public static final String STUDYSUBCATEGORY = "video/studysubcategory";
    public static final String CATEGORYWISESTUDY = "video/categorywisestudy";
    public static final String SUBSCRIPTION = "dashboard/subscriptionlist";
    public static final String PURCHASELIST = "dashboard/purchaselist";
    public static final String ISSUE = "issue/uploadwork";
    public static final String ISSUEHISTORY = "issue";
    public static String qItemClick="";
    public static String lastqItemClick="";
    public static int exLang=0;
    public static int clickedQue=0;
    public static int tlqUiniId=0;

    public static TextView tvquestion_no,txtOptionA,txtOptionB,txtOptionC,txtOptionD,txtOptionE;
    public static LinearLayout llReview,llReview1,llOptionA,llOptionB,llOptionC,llOptionD,llOptionE,llIndexA,llIndexB,llIndexC,llIndexD,llIndexE;
    public static WebView webQuestion,webSolution;
    public static CardView cardOptA,cardOptB,cardOptC,cardOptD,optCardE,btnPrevious,btnNext,solutionCardView;
    public static ImageView imgOptionA,imgOptionB,imgOptionC,imgOptionD,imgOptionE,imgSolution;
    public static List<QuestionModel> questionModelList;
    public static int examTyped,submitBtnStatus=0;
    public static String submitTest="";
    public static String inExamImgUrl="https://www.neonclasses.com/uploads/question_images/";
    public static int subSkippe = 0;
    public static int subCorrect = 0;
    public static int subWorng = 0;
    public static int childViewLl = 0;
//    public static String inExamImgUrl="https://neonclasses.com/uploads/subscribe_image/1651659690EnglishMainsBatchadmissionopenbannersquare.jpg";

    ///// Test Series

    SharedPreferences sharedPreferences;
    Context context;

    public static final int NOT_VISITED = ' ';
    public static final int UNANSWERED = 0;
    public static final int ANSWERED = 1;
    public static final int REVIEW = 1;
    public static int quePosition =0;

    public static final String EXAM = "exam/";
    public static final String DEPARTMENT = "package/department";
    public static final String GETCOURSEDETAIL = "dashboard/getcoursedetail";

    public static final String BLOGDETAIL = YOUR_NEW_API_URL_LIVE + "blog/getblogdetail/";
    public static String YTID = "";
    public static String PSC_URL = "https://telegram.org/neonclasses";
    public static String PSC_TEXT = "Go to Telegram";

    public static String home = "0";
    public static JSONObject alldashboarditem = new JSONObject();
    public static String player_setting = "0";

    public static String couponDiscount = "";
    public static String amountAfterCoupon ="";

    //////Webpages

    public static final String FAQ = YOUR_NEW_API_URL_LIVE + "pages/faq";


    public static String token = "";
    public static String fcm_call = "";

    public static String login_from = "";
    public static String skip = "";
    public static String device_id = "";
    public static String device_info = "";
    public static String device_model = "";
    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Neeonclasses Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.neonclasses.com";

    public static String fragments = "home";

    public static String CUSTOM_MESSAGE = "";


    public Constant(Context context) {
        this.context = context;
        this.sharedPreferences  =  context.getSharedPreferences(context.getString(R.string.pref_file), Context.MODE_PRIVATE);
    }


    public void writeLoginStatus(boolean status){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getString(R.string.pref_login_status),status);
        editor.apply();
    }

    public boolean readLoginStatus(){
        return sharedPreferences.getBoolean(context.getString(R.string.pref_login_status),false);
    }
    public void writeInsertedData(boolean inserted){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getString(R.string.pref_inserted),inserted);
        editor.apply();
    }

    public boolean readInsertedData(){
        return sharedPreferences.getBoolean(context.getString(R.string.pref_inserted),false);
    }


    public static String getCalculatedDate(String date, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date1 = s.parse(date);
            cal.setTime(date1);
            cal.add(Calendar.DAY_OF_MONTH, days);
            Date newDate = cal.getTime();
            return s.format(newDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static final int API = Build.VERSION.SDK_INT;
    public static final int ENABLED=1, DISABLED=0;

    public static boolean adbEnabled(final AppCompatActivity activity){
        if(API>16)
            return ENABLED == Settings.Global.getInt(activity.getContentResolver(), Settings.Global.ADB_ENABLED,ENABLED);
        else
            return ENABLED == Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.ADB_ENABLED,ENABLED);
    }

        public static void checkAdb(final AppCompatActivity activity){
        //if ADB disabled
       /* if(!adbEnabled(activity)){
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setIcon(R.drawable.icon).setTitle("Security Alert");
            builder.setMessage("Please off USB debugging");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //open developer options settings
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);

                }
            });
            *//*builder.setNegativeButton("QUIT APP", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            activity.finishAffinity();
                            System.exit(0);
                        }
                    }, 000);
                    dialogInterface.cancel();
                }
            });*//*
            AlertDialog dialog = builder.create();
            dialog.show();

//            Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
//            nbutton.setTextColor(Color.parseColor("#D6483F"));

            Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.parseColor("#008000"));

        }*/
    }

    public static void hideError(TextInputEditText editText, final TextInputLayout layout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                layout.setError(null);
                layout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public static void setBlinkAnim(final View view){
        Animation anim = new AlphaAnimation(0.0f, 5.0f);
        anim.setDuration(200); //You can manage the blinking time with this parameter
        anim.setStartOffset(50);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        view.startAnimation(anim);
    }

    public static GetResponse getUrl() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(codexo.neonclasses.Constant.YOUR_NEW_API_URL_LIVE)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        return retrofit.create(GetResponse.class);
    }

    public static GetResponse getTestSeriesUrl() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.YOUR_TEST_SERIES_API_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        return retrofit.create(GetResponse.class);
    }

    public static String getCalculatedDateOther(String date, int days) {
        Calendar cal = Calendar.getInstance();

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date1 = s.parse(date);
            cal.setTime(date1);
            cal.add(Calendar.DAY_OF_MONTH, days);
            Date newDate = cal.getTime();

            SimpleDateFormat new_format = new SimpleDateFormat("dd MMMM", Locale.getDefault());

            return new_format.format(newDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static SimpleDateFormat serverDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public static Date changedateformat(String date) throws ParseException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.CANADA);
        return df.parse(date);

    }

    public static ArrayList<String> snack_array = new ArrayList<>();


    public static boolean isNetworkConnectionAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
//            Log.d("Network", "Connected to internet");
            return true;
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("No internet Connection");
            builder.setMessage("Please turn on internet connection to continue");
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return false;
        }
    }


    public static void fullScreen(AppCompatActivity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

    }

    public static void setToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void callBack(final AppCompatActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        activity.finishAffinity();
                        System.exit(0);
                    }
                }, 000);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.parseColor("#D6483F"));

        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#008000"));

    }

    public static void logPrint(String title, String message) {

        Log.d(title, message + "");

    }

    public static void hideKeyBoard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
    public static void showkeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInputFromInputMethod(editText.getWindowToken(), 0);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }

    public static void setStatusBar(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.white, activity.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.white));
        }
    }

    @SuppressLint("RestrictedApi")
    public static void setToolbar(final AppCompatActivity activity, Toolbar toolbar) {
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });
            toolbar.getNavigationIcon().setColorFilter(activity.getResources().getColor(R.color.purple_700), PorterDuff.Mode.SRC_ATOP);
            activity.setTitle("");
        }
    }

    public static void printHashKey(AppCompatActivity activity) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
//            Log.e("TAG", "printHashKey()", e);
        } catch (Exception e) {
//            Log.e("TAG", "printHashKey()", e);
        }
    }

    public static ColorStateList getColorState() {

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{

                        Color.parseColor("#666666") //disabled
                        , Color.parseColor("#fdbc0a") //enabled

                }
        );
        return colorStateList;
    }

    public static ColorStateList getColorStatePaytm() {

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{

                        Color.parseColor("#666666") //disabled
                        , Color.parseColor("#00b9f5") //enabled

                }
        );
        return colorStateList;
    }

    public static ProgressDialog getProgressBar(Context context) {
        ProgressDialog loading = new ProgressDialog(context);
        loading.setMessage("Please wait... For a while");
        loading.setIndeterminate(true);
        loading.setCancelable(false);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.loader));
        return loading;
    }

    public static AlertDialog alertDialog(Context context, ViewGroup view, String correctQ, String wrongQ, String skippedQ){
//        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.loading, view, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        TextView correctedQ, WrongedQ, SkipQ;
        correctedQ = dialogView.findViewById(R.id.correctQ);
        WrongedQ = dialogView.findViewById(R.id.wrongQ);
        SkipQ = dialogView.findViewById(R.id.skippedQ);
        correctedQ.setText(String.valueOf(correctQ));
        WrongedQ.setText(String.valueOf(wrongQ));
        SkipQ.setText(String.valueOf(skippedQ));
        builder.setView(dialogView);
        builder.setCancelable(false);
        AlertDialog alertDialogLoader = builder.create();
        return alertDialogLoader;
    }


    public static ProgressDialog getProgressBar1(Context context) {
        ProgressDialog loading = new ProgressDialog(context);
        loading.setMessage("Please wait... For a while");
        loading.setIndeterminate(true);
        loading.setCancelable(false);
//        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.loader));
        loading.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        return loading;
    }

    public static Typeface getFonts(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public static Typeface getsemiFonts(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");
    }

    public static Typeface getFontsBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
    }

    public static Typeface getFontsExtraBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
    }

    public static String fcm_token = "";
    public static String fcm_token_new = "";

    public static void printHashKey() {
    }

    public void writeTokenToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("token.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readTokenFromFile(AppCompatActivity activity) {
        String ret = "";
        try {
            InputStream inputStream = activity.openFileInput("token.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
//            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
//            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    public static ImageLoader imageLoader_;

    public static ImageLoaderConfiguration getImageLoaderConfig(Context context) {
        DisplayImageOptions options_dis = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_img)
                .showImageForEmptyUri(R.drawable.no_img)
                .showImageOnFail(R.drawable.no_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        return new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options_dis)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
    }

    private static DisplayImageOptions getImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_img)
                .showImageForEmptyUri(R.drawable.no_img)
                .showImageOnFail(R.drawable.no_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public static void setImage(String url, final ImageView imageView, final Context context) {

        if (url.equals("")) {
            imageView.setImageResource(R.drawable.no_img);
        } else {

            String filename = url.substring(url.lastIndexOf("/") + 1);
            if (filename.equals("")) {
                imageView.setImageResource(R.drawable.no_img);
            } else {
                RequestOptions requestOption = new RequestOptions();
                Glide.with(context).load(url)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .thumbnail(Glide.with(context)
                                .load(url)
                                .apply(requestOption))
                        .apply(requestOption)
                        .into(imageView);

               /* imageLoader_.displayImage(url, imageView, Constant.getImageOptions(), new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                imageView.setImageResource(R.drawable.no_img);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                imageView.setImageResource(R.drawable.no_img);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                imageView.setImageBitmap(loadedImage);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                imageView.setImageResource(R.drawable.no_img);
                            }
                        }
                );*/
            }

            /* */
        }

    }

    public static final int MULTIPLE_PERMISSIONS = 100;

    public static boolean checkPermissions(AppCompatActivity activity, String[] permissions) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    public static void writeSkipToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("skip.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readSkipFromFile(AppCompatActivity activity) {
        String ret = "no";
        try {
            InputStream inputStream = activity.openFileInput("skip.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
//            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
//            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    public static String getdiscount(String actualprice, String newprice) {
        String discount = "0";
        Double actualpriced = Double.parseDouble(actualprice);
        Double newpriced = Double.parseDouble(newprice);

        Double difference = actualpriced - newpriced;
        logPrint(difference + "", "differencedifferencedifferencedifferencedifferencedifference" + actualpriced);
        if (difference > 0) {
            Double differeperc = (difference / actualpriced) * 100;
            logPrint(differeperc + "", "differepercdifferepercdifferepercdiffereperc");
            discount = differeperc + "";
        }
        return String.format("%.2f", Double.parseDouble(discount));
    }

    public static String getdiscountPrice(String actualprice, String newprice) {
        String discount = "0";
        int actualpriced = (int) parseInt(actualprice);
        int newpriced = (int) parseInt(newprice);

        int difference = actualpriced - newpriced;
        logPrint(difference + "", "differencedifferencedifferencedifferencedifferencedifference" + actualpriced);
        if (difference > 0) {
            int differeperc = actualpriced - newpriced;
            logPrint(differeperc + "", "differepercdifferepercdifferepercdiffereperc");
            discount = differeperc + "";
        }
        return discount;
    }


}
