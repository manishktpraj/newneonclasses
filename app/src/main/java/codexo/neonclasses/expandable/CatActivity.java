package codexo.neonclasses.expandable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCatBinding;
import codexo.neonclasses.databinding.ItemCategoryHorizontalBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.videos.CategoryVideosActivity;
import codexo.neonclasses.ui.videos.PscFragment;
import codexo.neonclasses.ui.videos.StudyMaterialTabFragment;
import codexo.neonclasses.ui.videos.VideoTabFragment;
import codexo.neonclasses.ui.videos.VideosFragment;
import codexo.neonclasses.ui.videos.VideosubcategoryActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class CatActivity extends AppCompatActivity {
    ActivityCatBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray free_video =new JSONArray();
    JSONArray recent_video =new JSONArray();
    JSONArray live_video =new JSONArray();
    JSONArray video_course =new JSONArray();
    JSONArray video_category =new JSONArray();
    JSONArray video_child_category =new JSONArray();
    String category_url="",package_url="";
    JSONArray study_category_new = new JSONArray();
    String allow_test="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCatBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        session = new SessionManager(CatActivity.this);
        pDialog = Constant.getProgressBar(CatActivity.this);
        getvideos();
    }

    private void getvideos() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getvideos(session.getUserId());
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
                 Constant.logPrint("response_video", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            String dashboard_message = jsonObject.getString("dashboard_message");
                            video_course =jsonObject.getJSONArray("video_course");
                            free_video =jsonObject.getJSONArray("free");
                            recent_video =jsonObject.getJSONArray("recent");
                            live_video =jsonObject.getJSONArray("live");
                            study_category_new =jsonObject.getJSONArray("prepration");
                            category_url =jsonObject.getString("category_url");
                            package_url =jsonObject.getString("package_url");
                            video_category =jsonObject.getJSONArray("results");
                            allow_test =jsonObject.getString("allow_test");
                          /*  listDataHeader = new ArrayList<String>();
                            listDataChild = new HashMap<String, List<String>>();
                            List<String> childList = new ArrayList<String>();
                            for (int i=0;i<video_category.length();i++){
                                JSONObject d = video_category.getJSONObject(i);
                                // Adding Header data


                                listDataHeader.add(d.getString("bcat_name"));

                                // Adding child data for lease offer

                               JSONArray child = d.getJSONArray("child");
                               if (child.length()>0) {
                                   for (int j = 0; j < child.length(); j++) {
                                       JSONObject dd = child.getJSONObject(j);
                                       childList.add(dd.getString("bcat_name"));
                                   }
                               }
                                listDataChild.put(listDataHeader.get(i), childList);
//                                Constant.logPrint("123445", d.getString("bcat_name")+"\n"+child);
                                Constant.logPrint("123445", listDataChild.toString());
                            }
*/
                            RecyclerAdapter7 recyclerAdapter7 = new RecyclerAdapter7(getApplicationContext(), video_category);
                            LinearLayoutManager layoutManager7 = new LinearLayoutManager(getApplicationContext());
                            layoutManager7.setOrientation(RecyclerView.VERTICAL);
                            binding.videoCategoriesRecycler.setLayoutManager(layoutManager7);
                            binding.videoCategoriesRecycler.setAdapter(recyclerAdapter7);
                        } else {
                            Constant.setToast(getApplicationContext(),"Server Not Responding");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getvideos();
            }
        });
    }

    public class RecyclerAdapter7 extends RecyclerView.Adapter<RecyclerAdapter7.RecyclerViewHolder> {
        ItemCategoryHorizontalBinding binding_data;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter7(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter7.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding_data = ItemCategoryHorizontalBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter7.RecyclerViewHolder holder = new RecyclerAdapter7.RecyclerViewHolder(binding_data);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter7.RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {

           /* if (position == 0) {
//                binding_data.topspace.setVisibility(View.VISIBLE);
            } else {
                binding_data.topspace.setVisibility(View.GONE);
            }*/
            try {
                JSONObject item = listItem.getJSONObject(position);
                Constant.logPrint("chiddata1", position+" = "+item.toString());
                String img = item.getString("bcat_image");
                Constant.setImage(img, binding_data.image, getApplicationContext());
                binding_data.txtTitle.setText(item.getString("bcat_name"));
                binding_data.txtTitle.setTypeface(Constant.getFontsBold(context.getApplicationContext()));
                binding_data.txtDes.setText(item.getString("bcat_description"));
                binding_data.txtDes.setTypeface(Constant.getFonts(context.getApplicationContext()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

            binding_data.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = listItem.getJSONObject(position);
                        JSONArray child = item.getJSONArray("child");
                        if (child.length() > 0) {
                            View vd = null;
                            vd = binding.videoCategoriesRecycler.getChildAt(position).findViewById(R.id.childViewLl);
                            vd.setVisibility((vd.getVisibility() == View.VISIBLE)
                                    ? View.GONE : View.VISIBLE);
                            Constant.logPrint("ppppppppp",position+"");
                            String parent_id = item.getString("bcat_id");
//                            getvideosubcategory(binding_data.childRv,parent_id,position);

                                notifyItemChanged(position);
                                Intent vidcategory = new Intent(getApplicationContext(), VideosubcategoryActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                vidcategory.putExtra("course_url", package_url + "");
                                startActivity(vidcategory);
                        }
                        else {
                            Intent vidcategory = new Intent(getApplicationContext(), CategoryVideosActivity.class);
                            vidcategory.putExtra("parent_category", item + "");
                            startActivity(vidcategory);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            public RecyclerViewHolder(@NonNull ItemCategoryHorizontalBinding itemView) {
                super(itemView.getRoot());


            }


        }
    }

    private void getvideosubcategory(RecyclerView recyclerView, String parent_id,int pp) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getvideosubcategory(session.getUserId(),parent_id);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();



                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            Constant.logPrint("responseChild", parent_id+" == "+pp+" = "+response);
                            video_child_category =jsonObject.getJSONArray("results");
                            RecyclerAdapter7 recyclerAdapter4 = new RecyclerAdapter7(CatActivity.this, video_child_category);
                            LinearLayoutManager layoutManager4 = new LinearLayoutManager(CatActivity.this);
                            layoutManager4.setOrientation(RecyclerView.VERTICAL);
                            recyclerView.setLayoutManager(layoutManager4);
                            recyclerView.setAdapter(recyclerAdapter4);
                        } else {
                            Constant.setToast(CatActivity.this,"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
//                getvideosubcategory(recyclerView,parent_id);
            }
        });
    }
}