package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.smslistener.SmsListener;
import codexo.neonclasses.smslistener.SmsReceiver;
import retrofit2.Call;
import retrofit2.Callback;

public class Otp extends AppCompatActivity {
    AppCompatActivity activity;
    TextView firsttext,secondtext,sign_up,thirdtext,loginviaotp,resendotp;
    TextInputLayout password;
    TextInputEditText default_edit_text;
    EditText otp_textbox_one, otp_textbox_two, otp_textbox_three, otp_textbox_four,otp_textbox_five,otp_textbox_six;
    JSONObject student_data;
    ProgressDialog pDialog;
    String student_id,type;
    long remainingmil;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Otp.this;
        Constant.adbEnabled(Otp.this);
        Constant.checkAdb(Otp.this);
        Constant.setToolbar(activity, toolbar);
        firsttext =findViewById(R.id.firsttext);
        secondtext =findViewById(R.id.secondtext);
        sign_up =findViewById(R.id.sign_up);
        password= findViewById(R.id.password);
        thirdtext= findViewById(R.id.thirdtext);
        loginviaotp= findViewById(R.id.loginviaotp);
        resendotp= findViewById(R.id.resendotp);
        pDialog = Constant.getProgressBar(Otp.this);
        session = new SessionManager(getApplicationContext());

        firsttext.setTypeface(Constant.getFontsBold(getApplicationContext()));
        secondtext.setTypeface(Constant.getFonts(getApplicationContext()));
        thirdtext.setTypeface(Constant.getFonts(getApplicationContext()));
        sign_up.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        loginviaotp.setTypeface(Constant.getFontsBold(getApplicationContext()));
        resendotp.setTypeface(Constant.getFonts(getApplicationContext()));

        try {
            type =getIntent().getStringExtra("type");
            student_data = new JSONObject(getIntent().getStringExtra("results"));
            student_id = student_data.getString("student_id");
            if(type.equals("Register"))
            {
                secondtext.setText("Verify your mobile number "+student_data.getString("student_phone")+" \nto complete registration");

            }

            if(type.equals("LOGINOTP"))
            {
                secondtext.setText("Verify your mobile number "+student_data.getString("student_phone")+" \nto login");
            }
            if(type.equals("FORGOT"))
            {
                secondtext.setText("Verify your mobile number "+student_data.getString("student_phone")+" \nto Reset your password");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        otp_textbox_one = findViewById(R.id.otp_edit_box1);
        otp_textbox_two = findViewById(R.id.otp_edit_box2);
        otp_textbox_three = findViewById(R.id.otp_edit_box3);
        otp_textbox_four = findViewById(R.id.otp_edit_box4);

        EditText[] edit = {otp_textbox_one, otp_textbox_two, otp_textbox_three, otp_textbox_four};
        otp_textbox_one.addTextChangedListener(new GenericTextWatcher(otp_textbox_one, edit));
        otp_textbox_two.addTextChangedListener(new GenericTextWatcher(otp_textbox_two, edit));
        otp_textbox_three.addTextChangedListener(new GenericTextWatcher(otp_textbox_three, edit));
        otp_textbox_four.addTextChangedListener(new GenericTextWatcher(otp_textbox_four, edit));
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String message, String type) {

                if (type.equals("otp")) {

                    String  new_message = message.replaceAll("[^a-zA-Z]", "");
//                    Log.d("new_message", new_message + " ");

                            //new_message.equals("YouroneTimePasswordOTPforregistrationtransactionisDONOTSHAREWITHANYBODYNEONCS")
                    if (new_message.equals("YouroneTimePasswordOTPforregistrationtransactionisDONOTSHAREWITHANYBODY")) {
                        String numberOnly;
                        numberOnly = message.replaceAll("[^0-9]", "");

//                        Log.d("Text", message + " " + numberOnly);
                    if(numberOnly.length()==4)
                    {
                         otp_textbox_one.setText(numberOnly.charAt(0)+"");
                        otp_textbox_two.setText(numberOnly.charAt(1)+"");
                        otp_textbox_three.setText(numberOnly.charAt(2)+"");
                        otp_textbox_four.setText(numberOnly.charAt(3)+"");

                        String otp1 = otp_textbox_one.getText().toString();
                        String otp2 = otp_textbox_two.getText().toString();
                        String otp3 = otp_textbox_three.getText().toString();
                        String otp4 = otp_textbox_four.getText().toString();
                        String otp = otp1+""+otp2+""+otp3+""+otp4+"";
                        verifyotp(otp);
                    }
                   ///     pinView.setText(numberOnly);
                    ///    verifyotp();
                    }
                }
            }
        }, "otp");
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ////Intent iLogin = new Intent(getApplicationContext(), Changepassword.class);
               //// iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              ////  iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             ////   getApplicationContext().startActivity(iLogin);
               // otp_textbox_one.
               String otp1 = otp_textbox_one.getText().toString();
                String otp2 = otp_textbox_two.getText().toString();
                String otp3 = otp_textbox_three.getText().toString();
                String otp4 = otp_textbox_four.getText().toString();

                if(otp1.equals(""))
                {
                    otp_textbox_one.requestFocus();
                    Constant.setToast(Otp.this,"Please Enter Valid OTP");


                    return;
                }
                if(otp2.equals(""))
                {
                    otp_textbox_two.requestFocus();
                    Constant.setToast(Otp.this,"Please Enter Valid OTP");
                    return;
                }
                if(otp3.equals(""))
                {
                    otp_textbox_three.requestFocus();
                    Constant.setToast(Otp.this,"Please Enter Valid OTP");
                    return;
                }
                if(otp4.equals(""))
                {
                    otp_textbox_four.requestFocus();
                    Constant.setToast(Otp.this,"Please Enter Valid OTP");
                    return;
                }
                String otp = otp1+""+otp2+""+otp3+""+otp4+"";
                verifyotp(otp);
            }
        });

        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(remainingmil<=0) {
                    resendotp();
                }
            }
        });
    }
    private void verifyotp(String otp) {
        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().verifyotp(student_id,otp);
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {
                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");
                            if(type.equals("Register")) {
                                session.createLoginSession(localJSONObject2);
                                Intent i = new Intent(activity, MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                activity.startActivity(i);
                                finish();
                            }

                            if(type.equals("LOGINOTP"))
                            {
                                session.createLoginSession(localJSONObject2);
                                Intent i = new Intent(activity, MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                activity.startActivity(i);
                                finish();

                            }

                            if(type.equals("FORGOT"))
                            {
                              ///  session.createLoginSession(localJSONObject2);
                                Intent i = new Intent(activity, Changepassword.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("results",localJSONObject2+"");
                                activity.startActivity(i);
                                finish();

                            }
                            Constant.setToast(Otp.this,notification);

                        } else {
                            Constant.setToast(Otp.this,notification);
                            ///  customalert(notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                verifyotp(otp);
            }
        });
    }
    private void resendotp() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().resendotp(student_id);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                            Constant.setToast(Otp.this,notification);
                            new CountDownTimer(60000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    remainingmil = millisUntilFinished/1000;
                                    resendotp.setText(remainingmil+"");
                                 }

                                public void onFinish() {
                                    remainingmil=0;
                                    resendotp.setText("Resend Code");
                                }
                            }.start();
                        } else {
                            Constant.setToast(Otp.this,notification);
                            ///  customalert(notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                resendotp();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Otp.this);
        Constant.checkAdb(Otp.this);
    }
}