package codexo.neonclasses.examDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;


import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.model.OtQuestionModel;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.model.RowUserSectionTestModel;
import codexo.neonclasses.model.SubjectInfoModel;
import codexo.neonclasses.model.SubjectModel;
import codexo.neonclasses.model.UstQuesAnswerModel;

public class SqlDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "myDB";
    // below int is our database version
    private static final int DB_VERSION = 1;
    //table name
//    public static final String RowInfo_TABLE_NAME = "RawExamInfoTable";
//    public static final String Question_TABLE_NAME = "QuestionTable";
//    public static final String Subject_TABLE_NAME = "SubjectTable";
//
//    public static final String test_admin_id = "test_admin_id";
//    public static final String test_duration = "test_duration";
//    public static final String test_free_status = "test_free_status";
//    public static final String test_dpt_id = "test_dpt_id";
//    public static final String test_solution_file = "test_solution_file";
//    public static final String test_datetime = "test_datetime";
//    public static final String test_total_marks = "test_total_marks";
//    public static final String product_in_stock = "product_in_stock";
//    public static final String test_marks = "test_marks";
//    public static final String test_series_id = "test_series_id";
//    public static final String test_title = "test_title";
//    public static final String test_ts_id = "test_ts_id";
//    public static final String test_section_type = "test_section_type";
//    public static final String test_et_id = "test_et_id";
//    public static final String test_paper_file = "test_paper_file";
//    public static final String test_sub_question = "test_sub_question";
//    public static final String test_id = "testid";
//    public static final String test_sectional_status = "test_sectional_status";
//    public static final String test_status = "test_status";
//    public static final String test_total_question = "test_total_question";
//    public static final String test_start_date = "test_start_date";
//    public static final String ot_test_solution_datetime = "ot_test_solution_datetime";
//    public static final String test_subject_stope_time = "test_subject_stope_time";
//    public static final String test_live_status = "test_live_status";
//    public static final String test_sub_id = "test_sub_id";
//    public static final String test_comming_date = "test_comming_date";
//    public static final String test_type = "test_type";
//    public static final String test_end_date = "test_end_date";
//    public static final String test_negative_marks = "test_negative_marks";
//    public static final String test_syllabus_info = "test_syllabus_info";
//    public static final String ot_user_test_subject_view = "ot_user_test_subject_view";
//    public static final String product_publish_date = "product_publish_date";
//    public static final String test_number = "test_number";
//
//    //Question tables strings
//    public static final String tlqId = "tlqId";
//    public static final String tlq_question_id = "tlq_question_id";
//    public static final String tlq_unique_id = "tlq_unique_id";
//    public static final String tlq_question_type = "tlq_question_type";
//    public static final String tlq_options = "tlq_options";
//    public static final String tlq_option1_attachments = "tlq_option1_attachments";
//    public static final String tlq_option2_attachments = "tlq_option2_attachments";
//    public static final String tlq_option3_attachments = "tlq_option3_attachments";
//    public static final String tlq_option4_attachments = "tlq_option4_attachments";
//    public static final String tlq_option5_attachments = "tlq_option5_attachments";
//    public static final String tlq_lang_id = "tlq_lang_id";
//    public static final String tlq_question_text = "tlq_question_text";
//    public static final String tlq_question_text_hindi = "tlq_question_text_hindi";
//    public static final String tlq_options_hindi = "tlq_options_hindi";
//    public static final String tlq_question_attachments = "tlq_question_attachments";
//    public static final String tlq_option1_attachments_hindi = "tlq_option1_attachments_hindi";
//    public static final String tlq_option2_attachments_hindi = "tlq_option2_attachments_hindi";
//    public static final String tlq_option3_attachments_hindi = "tlq_option3_attachments_hindi";
//    public static final String tlq_option4_attachments_hindi = "tlq_option4_attachments_hindi";
//    public static final String tlq_option5_attachments_hindi = "tlq_option5_attachments_hindi";
//    public static final String tlq_question_type_hindi = "tlq_question_type_hindi";
//    public static final String tlq_question_attachments_hindi = "tlq_question_attachments_hindi";
//    public static final String tlq_english_instruction = "tlq_english_instruction";
//    public static final String tlq_hindi_instruction = "tlq_hindi_instruction";
//    public static final String tlq_hindi_solution = "tlq_hindi_solution";
//    public static final String tlq_english_solution = "tlq_english_solution";
//    public static final String tlq_hindi_solution_attach = "tlq_hindi_solution_attach";
//    public static final String tlq_english_solution_attach = "tlq_english_solution_attach";
//    public static final String tlq_option_attach = "tlq_option_attach";
//    public static final String tlq_question_status = "tlq_question_status";
//    public static final String tlq_question_order = "tlq_question_order";
//    public static final String tlq_user_id = "tlq_user_id";
//    public static final String tlq_option_hindi_attach = "tlq_option_hindi_attach";
//    public static final String tlq_sub_id = "tlq_sub_id";
//    public static final String tlq_correct_option = "tlq_correct_option";
//    public static final String UserSelectedAns = "UserSelectedAns";
//    public static final String user_que_status = "user_que_status";
//    public static final String user_que_review = "user_que_review";
//
//    //Subject Table Strings
//    public static final String testId = "testId";
//    public static final String tsd_subject_id = "tsd_subject_id";
//    public static final String tsd_negative_marks = "tsd_negative_marks";
//    public static final String tsd_stop_time = "tsd_stop_time";
//    public static final String tsd_test_id = "tsd_test_id";
//    public static final String sub_id = "sub_id";
//    public static final String sub_status = "sub_status";
//    public static final String sub_parent_id = "sub_parent_id";
//    public static final String tsd_total_question = "tsd_total_question";
//    public static final String tsd_total_marks = "tsd_total_marks";
//    public static final String sub_name = "sub_name";


    public SqlDatabase(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    //    @Override
//    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        // TODO Auto-generated method stub
//        super.onDowngrade(db, oldVersion, newVersion);
//    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create Table SubjectDetails(test_id int,test_language int,test_admin_id int,test_duration Text,test_pause_on_que_num int,test_free_status int DEFAULT 0, test_dpt_id int, test_solution_file Text, test_datetime Text, test_title Text, " +
                "test_total_marks int, product_in_stock int, test_marks int,test_series_id int, test_ts_id int, test_section_type int, test_et_id int, test_paper_file Text, test_sub_question Text, test_sectional_status int," +
                "test_status int,test_total_question int, test_start_date Text, ot_test_solution_datetime Text, test_subject_stope_time Text, test_live_status int, test_sub_id Text,test_comming_date Text," +
                "test_type int, test_end_date Text,test_negative_marks real, test_syllabus_info Text, product_publish_date Text,test_number int)");

        db.execSQL("create Table SubjectQuestionDetails(test_id INTEGER PRIMARY KEY AUTOINCREMENT, tsd_subject_id Text,tsd_negative_marks real,tsd_stop_time Text, tsd_test_id int, sub_id int, sub_status int, sub_parent_id int, " +
                "tsd_total_question Text, tsd_total_marks Text, sub_name Text)");

        db.execSQL("create Table QuestionsDetails(tlq_id INTEGER PRIMARY KEY AUTOINCREMENT,tlq_counterid int,que_test_id int,tlq_question_id int,tlq_unique_id Text,tlq_question_type int, tlq_options Text, tlq_option1_attachments Text, tlq_option2_attachments Text, " +
                "tlq_option3_attachments Text, tlq_option4_attachments Text, tlq_option5_attachments Text, tlq_lang_id Text, tlq_question_text Text, tlq_question_text_hindi Text," +
                " tlq_options_hindi Text,tlq_question_attachments Text, tlq_option1_attachments_hindi Text, tlq_option2_attachments_hindi Text, tlq_option3_attachments_hindi Text," +
                " tlq_option4_attachments_hindi Text, tlq_option5_attachments_hindi Text, tlq_question_type_hindi int DEFAULT 0, tlq_question_attachments_hindi Text, tlq_english_instruction Text," +
                "tlq_hindi_instruction Text, tlq_hindi_solution Text, tlq_english_solution Text, tlq_hindi_solution_attach Text, tlq_english_solution_attach Text, tlq_option_attach Text," +
                "tlq_question_status int DEFAULT 0, tlq_question_order int, tlq_user_id int, tlq_option_hindi_attach Text, tlq_sub_id int, tlq_correct_option int,UserSelectedAns int," +
                "user_que_status int, user_que_review int DEFAULT 0)");

        db.execSQL("create Table OtQuestionDetailsTable(que_test_id Text, que_order int, que_correct_option Text, que_area int, que_correct_option_hindi Text, que_id int," +
                "que_sub_id Text, que_tlq_id int, ot_test_language_question Text, ot_matchword_ques Text)");

        db.execSQL("create Table UstQuesAnswerTable(tlq_id INTEGER PRIMARY KEY AUTOINCREMENT,tlq_counterid int,ustqa_id int, ustqa_que_id int, ustqa_correct Text, ustqa_user_ans int, " +
                "ustqa_mark Text, ustqa_test_id int," +
                "ustqa_sub int, ustqa_ust_id int, ustqa_user_id Text, ustqa_status int, ustqa_review int)");

        db.execSQL("create Table rowUserSectionTestTable(ust_id int, ust_test_id int, ust_user_id int, ust_created Text, ust_remainingtime Text, ust_status int," +
                "ust_question_mark Text, ust_wrong_question Text, ust_correct_question Text, ust_dob Text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("drop Table if exists SubjectDetails");
//        db.execSQL("drop Table if exists CREATE_Question_TABLE");
    }

    //Inserting data to SubjectDetails table
    public Boolean addRawExam(int test_id,int test_language,int test_admin_id, String test_duration,int test_pause_on_que_num,boolean test_free_status, int test_dpt_id, String test_solution_file, String test_datetime, String test_title, int test_total_marks, int product_in_stock,
                              int test_marks,int test_series_id, int test_ts_id, int test_section_type, int test_et_id, String test_paper_file, String test_sub_question, int test_sectional_status, int test_status,
                              int test_total_question, String test_start_date, String ot_test_solution_datetime, String test_subject_stope_time, int test_live_status, String test_sub_id, String test_comming_date,
                              int test_type,String test_end_date, double test_negative_marks, String test_syllabus_info, String product_publish_date, int test_number){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("test_id",test_id);
        values.put("test_language",test_language);
        values.put("test_admin_id",test_admin_id);
        values.put("test_duration",test_duration);
        values.put("test_pause_on_que_num",test_pause_on_que_num);
        values.put("test_free_status", test_free_status);
        values.put("test_dpt_id", test_dpt_id);
        values.put("test_solution_file", test_solution_file);
        values.put("test_datetime", test_datetime);
        values.put("test_title", test_title);
        values.put("test_total_marks", test_total_marks);
        values.put("product_in_stock", product_in_stock);
        values.put("test_marks", test_marks);
        values.put("test_series_id", test_series_id);
        values.put("test_ts_id", test_ts_id);
        values.put("test_section_type", test_section_type);
        values.put("test_et_id", test_et_id);
        values.put("test_paper_file", test_paper_file);
        values.put("test_sub_question", test_sub_question);
        values.put("test_sectional_status", test_sectional_status);
        values.put("test_status", test_status);
        values.put("test_total_question", test_total_question);
        values.put("test_start_date", test_start_date);
        values.put("ot_test_solution_datetime", ot_test_solution_datetime);
        values.put("test_subject_stope_time", test_subject_stope_time);
        values.put("test_live_status", test_live_status);
        values.put("test_sub_id", test_sub_id);
        values.put("test_comming_date", test_comming_date);
        values.put("test_type", test_type);
        values.put("test_end_date",test_end_date);
        values.put("test_negative_marks", test_negative_marks);
        values.put("test_syllabus_info", test_syllabus_info);
        values.put("product_publish_date", product_publish_date);
        values.put("test_number", test_number);
        long result = DB.insert("SubjectDetails",null,values);
        if (result>0){
            return true;
        }
        else{
            return false;
        }
    }

    //fetching data from SubjectDetails table
    public List<SubjectInfoModel> getRawExamInfo(int testId){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from SubjectDetails WHERE test_id == " + testId,null);
        List<SubjectInfoModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int test_id=cursor.getInt(cursor.getColumnIndex("test_id"));
            @SuppressLint("Range") int test_language=cursor.getInt(cursor.getColumnIndex("test_language"));
            @SuppressLint("Range") int test_admin_id=cursor.getInt(cursor.getColumnIndex("test_admin_id"));
            @SuppressLint("Range") String test_duration = cursor.getString(cursor.getColumnIndex("test_duration"));
            @SuppressLint("Range") int test_pause_on_que_num = cursor.getInt(cursor.getColumnIndex("test_pause_on_que_num"));
            @SuppressLint("Range") boolean test_free_status = cursor.getInt(cursor.getColumnIndex("test_free_status"))== 0;
            @SuppressLint("Range") int test_dpt_id = cursor.getInt(cursor.getColumnIndex("test_dpt_id"));
            @SuppressLint("Range") String test_solution_file = cursor.getString(cursor.getColumnIndex("test_solution_file"));
            @SuppressLint("Range") String test_datetime = cursor.getString(cursor.getColumnIndex("test_datetime"));
            @SuppressLint("Range") String test_title = cursor.getString(cursor.getColumnIndex("test_title"));
            @SuppressLint("Range") int test_total_marks = cursor.getInt(cursor.getColumnIndex("test_total_marks"));
            @SuppressLint("Range") int product_in_stock = cursor.getInt(cursor.getColumnIndex("product_in_stock"));
            @SuppressLint("Range") int test_marks = cursor.getInt(cursor.getColumnIndex("test_marks"));
            @SuppressLint("Range") int test_series_id = cursor.getInt(cursor.getColumnIndex("test_series_id"));
            @SuppressLint("Range") int test_ts_id = cursor.getInt(cursor.getColumnIndex("test_ts_id"));
            @SuppressLint("Range") int test_section_type = cursor.getInt(cursor.getColumnIndex("test_section_type"));
            @SuppressLint("Range") int test_et_id = cursor.getInt(cursor.getColumnIndex("test_et_id"));
            @SuppressLint("Range") String test_paper_file = cursor.getString(cursor.getColumnIndex("test_paper_file"));
            @SuppressLint("Range") String test_sub_question = cursor.getString(cursor.getColumnIndex("test_sub_question"));
            @SuppressLint("Range") int test_sectional_status = cursor.getInt(cursor.getColumnIndex("test_sectional_status"));
            @SuppressLint("Range") int test_status = cursor.getInt(cursor.getColumnIndex("test_status"));
            @SuppressLint("Range") int test_total_question = cursor.getInt(cursor.getColumnIndex("test_total_question"));
            @SuppressLint("Range") String test_start_date = cursor.getString(cursor.getColumnIndex("test_start_date"));
            @SuppressLint("Range") String ot_test_solution_datetime = cursor.getString(cursor.getColumnIndex("ot_test_solution_datetime"));
            @SuppressLint("Range") String test_subject_stope_time = cursor.getString(cursor.getColumnIndex("test_subject_stope_time"));
            @SuppressLint("Range") int test_live_status = cursor.getInt(cursor.getColumnIndex("test_live_status"));
            @SuppressLint("Range") String test_sub_id = cursor.getString(cursor.getColumnIndex("test_sub_id"));
            @SuppressLint("Range") String test_comming_date = cursor.getString(cursor.getColumnIndex("test_comming_date"));
            @SuppressLint("Range") int test_type = cursor.getInt(cursor.getColumnIndex("test_type"));
            @SuppressLint("Range") String test_end_date = cursor.getString(cursor.getColumnIndex("test_end_date"));
            @SuppressLint("Range") double test_negative_marks = cursor.getDouble(cursor.getColumnIndex("test_negative_marks"));
            @SuppressLint("Range") String test_syllabus_info = cursor.getString(cursor.getColumnIndex("test_syllabus_info"));
            @SuppressLint("Range") int test_number = cursor.getInt(cursor.getColumnIndex("test_number"));

            SubjectInfoModel rawdata = new SubjectInfoModel();
            rawdata.test_id=test_id;
            rawdata.test_language=test_language;
            rawdata.test_admin_id=test_admin_id;
            rawdata.test_duration=test_duration;
            rawdata.test_pause_on_que_num=test_pause_on_que_num;
            rawdata.test_free_status=test_free_status;
            rawdata.test_dpt_id=test_dpt_id;
            rawdata.test_solution_file=test_solution_file;
            rawdata.test_datetime=test_datetime;
            rawdata.test_title=test_title;
            rawdata.test_total_marks=test_total_marks;
            rawdata.product_in_stock=product_in_stock;
            rawdata.test_marks=test_marks;
            rawdata.test_series_id=test_series_id;
            rawdata.test_ts_id=test_ts_id;
            rawdata.test_section_type=test_section_type;
            rawdata.test_et_id=test_et_id;
            rawdata.test_paper_file=test_paper_file;
            rawdata.test_sub_question=test_sub_question;
            rawdata.test_sectional_status=test_sectional_status;
            rawdata.test_status=test_status;
            rawdata.test_total_question=test_total_question;
            rawdata.test_start_date=test_start_date;
            rawdata.ot_test_solution_datetime=ot_test_solution_datetime;
            rawdata.test_subject_stope_time=test_subject_stope_time;
            rawdata.test_live_status=test_live_status;
            rawdata.test_sub_id=test_sub_id;
            rawdata.test_comming_date=test_comming_date;
            rawdata.test_type=test_type;
            rawdata.test_end_date=test_end_date;
            rawdata.test_negative_marks=test_negative_marks;
            rawdata.test_syllabus_info=test_syllabus_info;
            rawdata.test_number=test_number;
            data.add(rawdata);
        }

        return data;
//        return cursor;
    }

    //Inserting data to SubjectQuestionDetails Table
    public Boolean addSubject(String tsd_subject_id, double tsd_negative_marks,String tsd_stop_time, int tsd_test_id, int sub_id, int sub_status, int sub_parent_id, String tsd_total_question, String tsd_total_marks,
                              String sub_name, int size){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor mCount= DB.rawQuery("Select * from SubjectQuestionDetails", null);

//        if(mCount.getCount()  != size){
            ContentValues values = new ContentValues();
            values.put("tsd_subject_id",tsd_subject_id);
            values.put("tsd_negative_marks",tsd_negative_marks);
            values.put("tsd_stop_time", tsd_stop_time);
            values.put("tsd_test_id", tsd_test_id);
            values.put("sub_id", sub_id);
            values.put("sub_status", sub_status);
            values.put("sub_parent_id", sub_parent_id);
            values.put("tsd_total_question", tsd_total_question);
            values.put("tsd_total_marks", tsd_total_marks);
            values.put("sub_name", sub_name);
            long result = DB.insert("SubjectQuestionDetails",null,values);
//            if (result>0){
                return true;
//            }
//            else{
//                return false;
//            }
//        }else{
//            mCount.close();
//            return false;
//        }
    }
    // Fetching data from SubjectQuestionDetails Table
    public List<SubjectModel> getOtUserTestSubjectViewItem(int testId){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from SubjectQuestionDetails WHERE tsd_test_id == " + testId, null);
        List<SubjectModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int test_id=cursor.getInt(cursor.getColumnIndex("test_id"));
            @SuppressLint("Range") String tsd_subject_id=cursor.getString(cursor.getColumnIndex("tsd_subject_id"));
            @SuppressLint("Range") double tsd_negative_marks = cursor.getDouble(cursor.getColumnIndex("tsd_negative_marks"));
            @SuppressLint("Range") String tsd_stop_time = cursor.getString(cursor.getColumnIndex("tsd_stop_time"));
            @SuppressLint("Range") int tsd_test_id = cursor.getInt(cursor.getColumnIndex("tsd_test_id"));
            @SuppressLint("Range") int sub_id = cursor.getInt(cursor.getColumnIndex("sub_id"));
            @SuppressLint("Range") int sub_status = cursor.getInt(cursor.getColumnIndex("sub_status"));
            @SuppressLint("Range") int sub_parent_id = cursor.getInt(cursor.getColumnIndex("sub_parent_id"));
            @SuppressLint("Range") String tsd_total_question = cursor.getString(cursor.getColumnIndex("tsd_total_question"));
            @SuppressLint("Range") String tsd_total_marks = cursor.getString(cursor.getColumnIndex("tsd_total_marks"));
            @SuppressLint("Range") String sub_name = cursor.getString(cursor.getColumnIndex("sub_name"));

            SubjectModel subdata = new SubjectModel();
            subdata.test_id=test_id;
            subdata.tsd_subject_id=tsd_subject_id;
            subdata.tsd_negative_marks=tsd_negative_marks;
            subdata.tsd_stop_time=tsd_stop_time;
            subdata.tsd_test_id=tsd_test_id;
            subdata.sub_id=sub_id;
            subdata.sub_status=sub_status;
            subdata.sub_parent_id=sub_parent_id;
            subdata.tsd_total_question=tsd_total_question;
            subdata.tsd_total_marks=tsd_total_marks;
            subdata.sub_name=sub_name;
            data.add(subdata);
        }

        return data;
    }

    //Inserting data to QuestionsDetails Table
    public Boolean addQuestions(int tlq_counterid,int tlq_question_id, int que_test_id, String tlq_unique_id, int tlq_question_type, String tlq_options, String tlq_option1_attachments, String tlq_option2_attachments,
                                String tlq_option3_attachments, String tlq_option4_attachments, String tlq_option5_attachments, String tlq_lang_id,String tlq_question_text, String tlq_question_text_hindi,
                                String tlq_options_hindi, String tlq_question_attachments, String tlq_option1_attachments_hindi, String tlq_option2_attachments_hindi,
                                String tlq_option3_attachments_hindi, String tlq_option4_attachments_hindi, String tlq_option5_attachments_hindi, boolean tlq_question_type_hindi,
                                String tlq_question_attachments_hindi, String tlq_english_instruction, String tlq_hindi_instruction, String tlq_hindi_solution, String tlq_english_solution,
                                String tlq_hindi_solution_attach, String tlq_english_solution_attach, String tlq_option_attach, boolean tlq_question_status, int tlq_question_order,
                                int tlq_user_id, String tlq_option_hindi_attach, int tlq_sub_id, int tlq_correct_option, int UserSelectedAns, int user_que_status){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor mCount= DB.rawQuery("Select * from QuestionsDetails", null);

//        if(mCount.getCount() == 0){
            ContentValues values = new ContentValues();
            values.put("tlq_counterid",tlq_counterid);
            values.put("tlq_question_id",tlq_question_id);
            values.put("que_test_id",que_test_id);
            values.put("tlq_unique_id",tlq_unique_id);
            values.put("tlq_question_type",tlq_question_type);
            values.put("tlq_options",tlq_options);
            values.put("tlq_option1_attachments",tlq_option1_attachments);
            values.put("tlq_option2_attachments",tlq_option2_attachments);
            values.put("tlq_option3_attachments",tlq_option3_attachments);
            values.put("tlq_option4_attachments",tlq_option4_attachments);
            values.put("tlq_option5_attachments",tlq_option5_attachments);
            values.put("tlq_lang_id",tlq_lang_id);
            values.put("tlq_question_text",tlq_question_text);
            values.put("tlq_question_text_hindi",tlq_question_text_hindi);
            values.put("tlq_options_hindi",tlq_options_hindi);
            values.put("tlq_question_attachments",tlq_question_attachments);
            values.put("tlq_option1_attachments_hindi",tlq_option1_attachments_hindi);
            values.put("tlq_option2_attachments_hindi",tlq_option2_attachments_hindi);
            values.put("tlq_option3_attachments_hindi",tlq_option3_attachments_hindi);
            values.put("tlq_option4_attachments_hindi",tlq_option4_attachments_hindi);
            values.put("tlq_option5_attachments_hindi",tlq_option5_attachments_hindi);
            values.put("tlq_question_type_hindi",tlq_question_type_hindi);
            values.put("tlq_question_attachments_hindi",tlq_question_attachments_hindi);
            values.put("tlq_english_instruction",tlq_english_instruction);
            values.put("tlq_hindi_instruction",tlq_hindi_instruction);
            values.put("tlq_hindi_solution",tlq_hindi_solution);
            values.put("tlq_english_solution",tlq_english_solution);
            values.put("tlq_hindi_solution_attach",tlq_hindi_solution_attach);
            values.put("tlq_english_solution_attach",tlq_english_solution_attach);
            values.put("tlq_option_attach",tlq_option_attach);
            values.put("tlq_question_status",tlq_question_status);
            values.put("tlq_question_order",tlq_question_order);
            values.put("tlq_user_id",tlq_user_id);
            values.put("tlq_option_hindi_attach",tlq_option_hindi_attach);
            values.put("tlq_sub_id",tlq_sub_id);
            values.put("tlq_correct_option",tlq_correct_option);
            values.put("UserSelectedAns",UserSelectedAns);
            values.put("user_que_status",user_que_status);
//        values.put("user_que_review",user_que_review);
            long result = DB.insert("QuestionsDetails",null,values);
//            if (result>0){
                return true;
//            }
//            else{
//                return false;
//            }
//        }else{
//            mCount.close();
//            return false;
//        }
    }
    // Fetching data from QuestionsDetails
    public List<QuestionModel> getQuestionData(int subject_id, int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where tlq_sub_id="+ subject_id +" and que_test_id="+ qtest_id,null);

        List<QuestionModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int tlq_id=cursor.getInt(cursor.getColumnIndex("tlq_id"));
            @SuppressLint("Range") int tlq_counterid=cursor.getInt(cursor.getColumnIndex("tlq_counterid"));
            @SuppressLint("Range") int que_test_id=cursor.getInt(cursor.getColumnIndex("que_test_id"));
            @SuppressLint("Range") int tlq_question_id=cursor.getInt(cursor.getColumnIndex("tlq_question_id"));
            @SuppressLint("Range") String tlq_unique_id = cursor.getString(cursor.getColumnIndex("tlq_unique_id"));
            @SuppressLint("Range") int tlq_question_type = cursor.getInt(cursor.getColumnIndex("tlq_question_type"));
            @SuppressLint("Range") String tlq_options = cursor.getString(cursor.getColumnIndex("tlq_options"));
            @SuppressLint("Range") String tlq_option1_attachments = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments"));
            @SuppressLint("Range") String tlq_option2_attachments = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments"));
            @SuppressLint("Range") String tlq_option3_attachments = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments"));
            @SuppressLint("Range") String tlq_option4_attachments = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments"));
            @SuppressLint("Range") String tlq_option5_attachments = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments"));
            @SuppressLint("Range") String tlq_lang_id = cursor.getString(cursor.getColumnIndex("tlq_lang_id"));
            @SuppressLint("Range") String tlq_question_text = cursor.getString(cursor.getColumnIndex("tlq_question_text"));
            @SuppressLint("Range") String tlq_question_text_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_text_hindi"));
            @SuppressLint("Range") String tlq_options_hindi = cursor.getString(cursor.getColumnIndex("tlq_options_hindi"));
            @SuppressLint("Range") String tlq_question_attachments = cursor.getString(cursor.getColumnIndex("tlq_question_attachments"));
            @SuppressLint("Range") String tlq_option1_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments_hindi"));
            @SuppressLint("Range") String tlq_option2_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments_hindi"));
            @SuppressLint("Range") String tlq_option3_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments_hindi"));
            @SuppressLint("Range") String tlq_option4_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments_hindi"));
            @SuppressLint("Range") String tlq_option5_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments_hindi"));
            @SuppressLint("Range") boolean tlq_question_type_hindi = cursor.getInt(cursor.getColumnIndex("tlq_question_type_hindi"))==0;
            @SuppressLint("Range") String tlq_question_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_attachments_hindi"));
            @SuppressLint("Range") String tlq_english_instruction = cursor.getString(cursor.getColumnIndex("tlq_english_instruction"));
            @SuppressLint("Range") String tlq_hindi_instruction = cursor.getString(cursor.getColumnIndex("tlq_hindi_instruction"));
            @SuppressLint("Range") String tlq_hindi_solution = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution"));
            @SuppressLint("Range") String tlq_english_solution = cursor.getString(cursor.getColumnIndex("tlq_english_solution"));
            @SuppressLint("Range") String tlq_hindi_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution_attach"));
            @SuppressLint("Range") String tlq_english_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_english_solution_attach"));
            @SuppressLint("Range") String tlq_option_attach = cursor.getString(cursor.getColumnIndex("tlq_option_attach"));
            @SuppressLint("Range") boolean tlq_question_status = cursor.getInt(cursor.getColumnIndex("tlq_question_status"))==0;
            @SuppressLint("Range") int tlq_question_order = cursor.getInt(cursor.getColumnIndex("tlq_question_order"));
            @SuppressLint("Range") int tlq_user_id = cursor.getInt(cursor.getColumnIndex("tlq_user_id"));
            @SuppressLint("Range") String tlq_option_hindi_attach = cursor.getString(cursor.getColumnIndex("tlq_option_hindi_attach"));
            @SuppressLint("Range") int tlq_sub_id = cursor.getInt(cursor.getColumnIndex("tlq_sub_id"));
            @SuppressLint("Range") int tlq_correct_option = cursor.getInt(cursor.getColumnIndex("tlq_correct_option"));
            @SuppressLint("Range") int user_que_status = cursor.getInt(cursor.getColumnIndex("user_que_status"));

            QuestionModel quedata = new QuestionModel();
            quedata.tlq_id=tlq_id;
            quedata.tlq_counterid=tlq_counterid;
            quedata.que_test_id=que_test_id;
            quedata.tlq_question_id=tlq_question_id;
            quedata.tlq_unique_id=tlq_unique_id;
            quedata.tlq_question_type=tlq_question_type;
            quedata.tlq_options=tlq_options;
            quedata.tlq_option1_attachments=tlq_option1_attachments;
            quedata.tlq_option2_attachments=tlq_option2_attachments;
            quedata.tlq_option3_attachments=tlq_option3_attachments;
            quedata.tlq_option4_attachments=tlq_option4_attachments;
            quedata.tlq_option5_attachments=tlq_option5_attachments;
            quedata.tlq_lang_id=tlq_lang_id;
            quedata.tlq_question_text=tlq_question_text;
            quedata.tlq_question_text_hindi=tlq_question_text_hindi;
            quedata.tlq_options_hindi=tlq_options_hindi;
            quedata.tlq_question_attachments=tlq_question_attachments;
            quedata.tlq_option1_attachments_hindi=tlq_option1_attachments_hindi;
            quedata.tlq_option2_attachments_hindi=tlq_option2_attachments_hindi;
            quedata.tlq_option3_attachments_hindi=tlq_option3_attachments_hindi;
            quedata.tlq_option4_attachments_hindi=tlq_option4_attachments_hindi;
            quedata.tlq_option5_attachments_hindi=tlq_option5_attachments_hindi;
            quedata.tlq_question_type_hindi=tlq_question_type_hindi;
            quedata.tlq_question_attachments_hindi=tlq_question_attachments_hindi;
            quedata.tlq_english_instruction=tlq_english_instruction;
            quedata.tlq_hindi_instruction=tlq_hindi_instruction;
            quedata.tlq_hindi_solution=tlq_hindi_solution;
            quedata.tlq_english_solution=tlq_english_solution;
            quedata.tlq_hindi_solution_attach=tlq_hindi_solution_attach;
            quedata.tlq_english_solution_attach=tlq_english_solution_attach;
            quedata.tlq_option_attach=tlq_option_attach;
            quedata.tlq_question_status=tlq_question_status;
            quedata.tlq_question_order=tlq_question_order;
            quedata.tlq_user_id=tlq_user_id;
            quedata.tlq_option_hindi_attach=tlq_option_hindi_attach;
            quedata.tlq_sub_id=tlq_sub_id;
            quedata.tlq_correct_option=tlq_correct_option;
            quedata.user_que_status=user_que_status;
            data.add(quedata);
        }

        return data;
    }


    // Fetching data from QuestionsDetails
    public List<QuestionModel> getAllQuestionData(int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where que_test_id="+ qtest_id,null);

        List<QuestionModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int tlq_id=cursor.getInt(cursor.getColumnIndex("tlq_id"));
            @SuppressLint("Range") int tlq_counterid=cursor.getInt(cursor.getColumnIndex("tlq_counterid"));
            @SuppressLint("Range") int que_test_id=cursor.getInt(cursor.getColumnIndex("que_test_id"));
            @SuppressLint("Range") int tlq_question_id=cursor.getInt(cursor.getColumnIndex("tlq_question_id"));
            @SuppressLint("Range") String tlq_unique_id = cursor.getString(cursor.getColumnIndex("tlq_unique_id"));
            @SuppressLint("Range") int tlq_question_type = cursor.getInt(cursor.getColumnIndex("tlq_question_type"));
            @SuppressLint("Range") String tlq_options = cursor.getString(cursor.getColumnIndex("tlq_options"));
            @SuppressLint("Range") String tlq_option1_attachments = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments"));
            @SuppressLint("Range") String tlq_option2_attachments = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments"));
            @SuppressLint("Range") String tlq_option3_attachments = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments"));
            @SuppressLint("Range") String tlq_option4_attachments = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments"));
            @SuppressLint("Range") String tlq_option5_attachments = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments"));
            @SuppressLint("Range") String tlq_lang_id = cursor.getString(cursor.getColumnIndex("tlq_lang_id"));
            @SuppressLint("Range") String tlq_question_text = cursor.getString(cursor.getColumnIndex("tlq_question_text"));
            @SuppressLint("Range") String tlq_question_text_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_text_hindi"));
            @SuppressLint("Range") String tlq_options_hindi = cursor.getString(cursor.getColumnIndex("tlq_options_hindi"));
            @SuppressLint("Range") String tlq_question_attachments = cursor.getString(cursor.getColumnIndex("tlq_question_attachments"));
            @SuppressLint("Range") String tlq_option1_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments_hindi"));
            @SuppressLint("Range") String tlq_option2_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments_hindi"));
            @SuppressLint("Range") String tlq_option3_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments_hindi"));
            @SuppressLint("Range") String tlq_option4_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments_hindi"));
            @SuppressLint("Range") String tlq_option5_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments_hindi"));
            @SuppressLint("Range") boolean tlq_question_type_hindi = cursor.getInt(cursor.getColumnIndex("tlq_question_type_hindi"))==0;
            @SuppressLint("Range") String tlq_question_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_attachments_hindi"));
            @SuppressLint("Range") String tlq_english_instruction = cursor.getString(cursor.getColumnIndex("tlq_english_instruction"));
            @SuppressLint("Range") String tlq_hindi_instruction = cursor.getString(cursor.getColumnIndex("tlq_hindi_instruction"));
            @SuppressLint("Range") String tlq_hindi_solution = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution"));
            @SuppressLint("Range") String tlq_english_solution = cursor.getString(cursor.getColumnIndex("tlq_english_solution"));
            @SuppressLint("Range") String tlq_hindi_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution_attach"));
            @SuppressLint("Range") String tlq_english_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_english_solution_attach"));
            @SuppressLint("Range") String tlq_option_attach = cursor.getString(cursor.getColumnIndex("tlq_option_attach"));
            @SuppressLint("Range") boolean tlq_question_status = cursor.getInt(cursor.getColumnIndex("tlq_question_status"))==0;
            @SuppressLint("Range") int tlq_question_order = cursor.getInt(cursor.getColumnIndex("tlq_question_order"));
            @SuppressLint("Range") int tlq_user_id = cursor.getInt(cursor.getColumnIndex("tlq_user_id"));
            @SuppressLint("Range") String tlq_option_hindi_attach = cursor.getString(cursor.getColumnIndex("tlq_option_hindi_attach"));
            @SuppressLint("Range") int tlq_sub_id = cursor.getInt(cursor.getColumnIndex("tlq_sub_id"));
            @SuppressLint("Range") int tlq_correct_option = cursor.getInt(cursor.getColumnIndex("tlq_correct_option"));
            @SuppressLint("Range") int user_que_status = cursor.getInt(cursor.getColumnIndex("user_que_status"));
            @SuppressLint("Range") int UserSelectedAns = cursor.getInt(cursor.getColumnIndex("UserSelectedAns"));

            QuestionModel quedata = new QuestionModel();
            quedata.tlq_id=tlq_id;
            quedata.tlq_counterid=tlq_counterid;
            quedata.que_test_id=que_test_id;
            quedata.tlq_question_id=tlq_question_id;
            quedata.tlq_unique_id=tlq_unique_id;
            quedata.tlq_question_type=tlq_question_type;
            quedata.tlq_options=tlq_options;
            quedata.tlq_option1_attachments=tlq_option1_attachments;
            quedata.tlq_option2_attachments=tlq_option2_attachments;
            quedata.tlq_option3_attachments=tlq_option3_attachments;
            quedata.tlq_option4_attachments=tlq_option4_attachments;
            quedata.tlq_option5_attachments=tlq_option5_attachments;
            quedata.tlq_lang_id=tlq_lang_id;
            quedata.tlq_question_text=tlq_question_text;
            quedata.tlq_question_text_hindi=tlq_question_text_hindi;
            quedata.tlq_options_hindi=tlq_options_hindi;
            quedata.tlq_question_attachments=tlq_question_attachments;
            quedata.tlq_option1_attachments_hindi=tlq_option1_attachments_hindi;
            quedata.tlq_option2_attachments_hindi=tlq_option2_attachments_hindi;
            quedata.tlq_option3_attachments_hindi=tlq_option3_attachments_hindi;
            quedata.tlq_option4_attachments_hindi=tlq_option4_attachments_hindi;
            quedata.tlq_option5_attachments_hindi=tlq_option5_attachments_hindi;
            quedata.tlq_question_type_hindi=tlq_question_type_hindi;
            quedata.tlq_question_attachments_hindi=tlq_question_attachments_hindi;
            quedata.tlq_english_instruction=tlq_english_instruction;
            quedata.tlq_hindi_instruction=tlq_hindi_instruction;
            quedata.tlq_hindi_solution=tlq_hindi_solution;
            quedata.tlq_english_solution=tlq_english_solution;
            quedata.tlq_hindi_solution_attach=tlq_hindi_solution_attach;
            quedata.tlq_english_solution_attach=tlq_english_solution_attach;
            quedata.tlq_option_attach=tlq_option_attach;
            quedata.tlq_question_status=tlq_question_status;
            quedata.tlq_question_order=tlq_question_order;
            quedata.tlq_user_id=tlq_user_id;
            quedata.tlq_option_hindi_attach=tlq_option_hindi_attach;
            quedata.tlq_sub_id=tlq_sub_id;
            quedata.tlq_correct_option=tlq_correct_option;
            quedata.user_que_status=user_que_status;
            quedata.UserSelectedAns=UserSelectedAns;
            data.add(quedata);
        }

        return data;
    }

    public Cursor getallquestiondataofnewxtprev(int qtest_id, int qId){
        SQLiteDatabase DB = this.getReadableDatabase();
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where que_test_id="+ qtest_id+" and tlq_counterid="+ qId,null);
        return cursor;
    }

    public Cursor getallUstQueAnswerData(int qtest_id, int qId){
        SQLiteDatabase DB = this.getReadableDatabase();
        Cursor cursor = DB.rawQuery("Select * from UstQuesAnswerTable where ustqa_test_id="+ qtest_id+" and tlq_counterid="+ qId,null);
        return cursor;
    }
//
//    public List<QuestionModel> getAllQuestionData(int qtest_id,int btnclickid){
//        SQLiteDatabase DB = this.getWritableDatabase();
//        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where que_test_id="+ qtest_id+" and tlq_counterid="+ btnclickid,null);
//
//        List<QuestionModel> data=new ArrayList<>();
//        while (cursor.moveToNext()){
//            @SuppressLint("Range") int tlq_id=cursor.getInt(cursor.getColumnIndex("tlq_id"));
//            @SuppressLint("Range") int tlq_counterid=cursor.getInt(cursor.getColumnIndex("tlq_counterid"));
//            @SuppressLint("Range") int que_test_id=cursor.getInt(cursor.getColumnIndex("que_test_id"));
//            @SuppressLint("Range") int tlq_question_id=cursor.getInt(cursor.getColumnIndex("tlq_question_id"));
//            @SuppressLint("Range") String tlq_unique_id = cursor.getString(cursor.getColumnIndex("tlq_unique_id"));
//            @SuppressLint("Range") int tlq_question_type = cursor.getInt(cursor.getColumnIndex("tlq_question_type"));
//            @SuppressLint("Range") String tlq_options = cursor.getString(cursor.getColumnIndex("tlq_options"));
//            @SuppressLint("Range") String tlq_option1_attachments = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments"));
//            @SuppressLint("Range") String tlq_option2_attachments = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments"));
//            @SuppressLint("Range") String tlq_option3_attachments = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments"));
//            @SuppressLint("Range") String tlq_option4_attachments = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments"));
//            @SuppressLint("Range") String tlq_option5_attachments = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments"));
//            @SuppressLint("Range") String tlq_lang_id = cursor.getString(cursor.getColumnIndex("tlq_lang_id"));
//            @SuppressLint("Range") String tlq_question_text = cursor.getString(cursor.getColumnIndex("tlq_question_text"));
//            @SuppressLint("Range") String tlq_question_text_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_text_hindi"));
//            @SuppressLint("Range") String tlq_options_hindi = cursor.getString(cursor.getColumnIndex("tlq_options_hindi"));
//            @SuppressLint("Range") String tlq_question_attachments = cursor.getString(cursor.getColumnIndex("tlq_question_attachments"));
//            @SuppressLint("Range") String tlq_option1_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option1_attachments_hindi"));
//            @SuppressLint("Range") String tlq_option2_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option2_attachments_hindi"));
//            @SuppressLint("Range") String tlq_option3_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option3_attachments_hindi"));
//            @SuppressLint("Range") String tlq_option4_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option4_attachments_hindi"));
//            @SuppressLint("Range") String tlq_option5_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_option5_attachments_hindi"));
//            @SuppressLint("Range") boolean tlq_question_type_hindi = cursor.getInt(cursor.getColumnIndex("tlq_question_type_hindi"))==0;
//            @SuppressLint("Range") String tlq_question_attachments_hindi = cursor.getString(cursor.getColumnIndex("tlq_question_attachments_hindi"));
//            @SuppressLint("Range") String tlq_english_instruction = cursor.getString(cursor.getColumnIndex("tlq_english_instruction"));
//            @SuppressLint("Range") String tlq_hindi_instruction = cursor.getString(cursor.getColumnIndex("tlq_hindi_instruction"));
//            @SuppressLint("Range") String tlq_hindi_solution = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution"));
//            @SuppressLint("Range") String tlq_english_solution = cursor.getString(cursor.getColumnIndex("tlq_english_solution"));
//            @SuppressLint("Range") String tlq_hindi_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_hindi_solution_attach"));
//            @SuppressLint("Range") String tlq_english_solution_attach = cursor.getString(cursor.getColumnIndex("tlq_english_solution_attach"));
//            @SuppressLint("Range") String tlq_option_attach = cursor.getString(cursor.getColumnIndex("tlq_option_attach"));
//            @SuppressLint("Range") boolean tlq_question_status = cursor.getInt(cursor.getColumnIndex("tlq_question_status"))==0;
//            @SuppressLint("Range") int tlq_question_order = cursor.getInt(cursor.getColumnIndex("tlq_question_order"));
//            @SuppressLint("Range") int tlq_user_id = cursor.getInt(cursor.getColumnIndex("tlq_user_id"));
//            @SuppressLint("Range") String tlq_option_hindi_attach = cursor.getString(cursor.getColumnIndex("tlq_option_hindi_attach"));
//            @SuppressLint("Range") int tlq_sub_id = cursor.getInt(cursor.getColumnIndex("tlq_sub_id"));
//            @SuppressLint("Range") int tlq_correct_option = cursor.getInt(cursor.getColumnIndex("tlq_correct_option"));
//            @SuppressLint("Range") int user_que_status = cursor.getInt(cursor.getColumnIndex("user_que_status"));
//            @SuppressLint("Range") int UserSelectedAns = cursor.getInt(cursor.getColumnIndex("UserSelectedAns"));
//
//            QuestionModel quedata = new QuestionModel();
//            quedata.tlq_id=tlq_id;
//            quedata.tlq_counterid=tlq_counterid;
//            quedata.que_test_id=que_test_id;
//            quedata.tlq_question_id=tlq_question_id;
//            quedata.tlq_unique_id=tlq_unique_id;
//            quedata.tlq_question_type=tlq_question_type;
//            quedata.tlq_options=tlq_options;
//            quedata.tlq_option1_attachments=tlq_option1_attachments;
//            quedata.tlq_option2_attachments=tlq_option2_attachments;
//            quedata.tlq_option3_attachments=tlq_option3_attachments;
//            quedata.tlq_option4_attachments=tlq_option4_attachments;
//            quedata.tlq_option5_attachments=tlq_option5_attachments;
//            quedata.tlq_lang_id=tlq_lang_id;
//            quedata.tlq_question_text=tlq_question_text;
//            quedata.tlq_question_text_hindi=tlq_question_text_hindi;
//            quedata.tlq_options_hindi=tlq_options_hindi;
//            quedata.tlq_question_attachments=tlq_question_attachments;
//            quedata.tlq_option1_attachments_hindi=tlq_option1_attachments_hindi;
//            quedata.tlq_option2_attachments_hindi=tlq_option2_attachments_hindi;
//            quedata.tlq_option3_attachments_hindi=tlq_option3_attachments_hindi;
//            quedata.tlq_option4_attachments_hindi=tlq_option4_attachments_hindi;
//            quedata.tlq_option5_attachments_hindi=tlq_option5_attachments_hindi;
//            quedata.tlq_question_type_hindi=tlq_question_type_hindi;
//            quedata.tlq_question_attachments_hindi=tlq_question_attachments_hindi;
//            quedata.tlq_english_instruction=tlq_english_instruction;
//            quedata.tlq_hindi_instruction=tlq_hindi_instruction;
//            quedata.tlq_hindi_solution=tlq_hindi_solution;
//            quedata.tlq_english_solution=tlq_english_solution;
//            quedata.tlq_hindi_solution_attach=tlq_hindi_solution_attach;
//            quedata.tlq_english_solution_attach=tlq_english_solution_attach;
//            quedata.tlq_option_attach=tlq_option_attach;
//            quedata.tlq_question_status=tlq_question_status;
//            quedata.tlq_question_order=tlq_question_order;
//            quedata.tlq_user_id=tlq_user_id;
//            quedata.tlq_option_hindi_attach=tlq_option_hindi_attach;
//            quedata.tlq_sub_id=tlq_sub_id;
//            quedata.tlq_correct_option=tlq_correct_option;
//            quedata.user_que_status=user_que_status;
//            quedata.UserSelectedAns=UserSelectedAns;
//            data.add(quedata);
//        }
//
//        return data;
//    }


    //Inserting data to OtQuestionDetailsTable Table
    public Boolean addOtQuestionData(String que_test_id, int que_order, String que_correct_option,
                                     int que_area, String que_correct_option_hindi, int que_id, String que_sub_id, int que_tlq_id,
                                     String ot_test_language_question,String ot_matchword_ques){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor mCount= DB.rawQuery("Select * from OtQuestionDetailsTable", null);
        ContentValues values = new ContentValues();
        values.put("que_test_id",que_test_id);
        values.put("que_order",que_order);
        values.put("que_correct_option",que_correct_option);
        values.put("que_area",que_area);
        values.put("que_correct_option_hindi",que_correct_option_hindi);
        values.put("que_id",que_id);
        values.put("que_sub_id",que_sub_id);
        values.put("que_tlq_id",que_tlq_id);
        values.put("ot_test_language_question",ot_test_language_question);
        values.put("ot_matchword_ques",ot_matchword_ques);

        long result = DB.insert("OtQuestionDetailsTable",null,values);
//            if (result>0){
        return true;
//            }
    }



    // Fetching data from OtQuestionDetailsTable
    public List<OtQuestionModel> getOtQuestionData(int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from OtQuestionDetailsTable where que_test_id="+ qtest_id,null);

        List<OtQuestionModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") String que_test_id = cursor.getString(cursor.getColumnIndex("que_test_id"));
            @SuppressLint("Range") int que_order=cursor.getInt(cursor.getColumnIndex("que_order"));
            @SuppressLint("Range") String que_correct_option = cursor.getString(cursor.getColumnIndex("que_correct_option"));
            @SuppressLint("Range") int que_area=cursor.getInt(cursor.getColumnIndex("que_area"));
            @SuppressLint("Range") String que_correct_option_hindi = cursor.getString(cursor.getColumnIndex("que_correct_option_hindi"));
            @SuppressLint("Range") int que_id=cursor.getInt(cursor.getColumnIndex("que_id"));
            @SuppressLint("Range") String que_sub_id = cursor.getString(cursor.getColumnIndex("que_sub_id"));
            @SuppressLint("Range") int que_tlq_id=cursor.getInt(cursor.getColumnIndex("que_tlq_id"));
            @SuppressLint("Range") String ot_test_language_question=cursor.getString(cursor.getColumnIndex("ot_test_language_question"));
            @SuppressLint("Range") String ot_matchword_ques=cursor.getString(cursor.getColumnIndex("ot_matchword_ques"));

            OtQuestionModel quedata = new OtQuestionModel();
            quedata.que_test_id=que_test_id;
            quedata.que_order=que_order;
            quedata.que_correct_option=que_correct_option;
            quedata.que_area=que_area;
            quedata.que_correct_option_hindi=que_correct_option_hindi;
            quedata.que_id=que_id;
            quedata.que_sub_id=que_sub_id;
            quedata.que_tlq_id=que_tlq_id;
            quedata.ot_test_language_question=ot_test_language_question;
            quedata.ot_matchword_ques=ot_matchword_ques;

            data.add(quedata);
        }

        return data;
    }


    //Inserting data to UstQuesAnswerTable Table
    public Boolean addUstQuesAnswerTable(int tlq_counterid,int ustqa_id, int ustqa_que_id, String ustqa_correct,int ustqa_user_ans, String ustqa_mark, int ustqa_test_id,
                                         int ustqa_sub, int ustqa_ust_id, String ustqa_user_id, int ustqa_status, int ustqa_review){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor mCount= DB.rawQuery("Select * from UstQuesAnswerTable", null);
        ContentValues values = new ContentValues();
        values.put("tlq_counterid",tlq_counterid);
        values.put("ustqa_id",ustqa_id);
        values.put("ustqa_que_id",ustqa_que_id);
        values.put("ustqa_correct",ustqa_correct);
        values.put("ustqa_user_ans",ustqa_user_ans);
        values.put("ustqa_mark",ustqa_mark);
        values.put("ustqa_test_id",ustqa_test_id);
        values.put("ustqa_sub",ustqa_sub);
        values.put("ustqa_ust_id",ustqa_ust_id);
        values.put("ustqa_user_id",ustqa_user_id);
        values.put("ustqa_status",ustqa_status);
        values.put("ustqa_review",ustqa_review);

        long result = DB.insert("UstQuesAnswerTable",null,values);
//            if (result>0){
        return true;
//            }
    }

    // Fetching data from UstQuesAnswerTable
    public List<UstQuesAnswerModel> getUstQuesAnswerTable(int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from UstQuesAnswerTable where ustqa_test_id="+ qtest_id,null);

        List<UstQuesAnswerModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int tlq_id=cursor.getInt(cursor.getColumnIndex("tlq_id"));
            @SuppressLint("Range") int tlq_counterid=cursor.getInt(cursor.getColumnIndex("tlq_counterid"));
            @SuppressLint("Range") int ustqa_id=cursor.getInt(cursor.getColumnIndex("ustqa_id"));
            @SuppressLint("Range") int ustqa_que_id=cursor.getInt(cursor.getColumnIndex("ustqa_que_id"));
            @SuppressLint("Range") String ustqa_correct=cursor.getString(cursor.getColumnIndex("ustqa_correct"));
            @SuppressLint("Range") int ustqa_user_ans=cursor.getInt(cursor.getColumnIndex("ustqa_user_ans"));
            @SuppressLint("Range") String ustqa_mark=cursor.getString(cursor.getColumnIndex("ustqa_mark"));
            @SuppressLint("Range") int ustqa_test_id=cursor.getInt(cursor.getColumnIndex("ustqa_test_id"));
            @SuppressLint("Range") int ustqa_sub=cursor.getInt(cursor.getColumnIndex("ustqa_sub"));
            @SuppressLint("Range") int ustqa_ust_id=cursor.getInt(cursor.getColumnIndex("ustqa_ust_id"));
            @SuppressLint("Range") String ustqa_user_id=cursor.getString(cursor.getColumnIndex("ustqa_user_id"));
            @SuppressLint("Range") int ustqa_status=cursor.getInt(cursor.getColumnIndex("ustqa_status"));
            @SuppressLint("Range") int ustqa_review=cursor.getInt(cursor.getColumnIndex("ustqa_review"));

            UstQuesAnswerModel quedata = new UstQuesAnswerModel();
            quedata.tlq_id=tlq_id;
            quedata.tlq_counterid=tlq_counterid;
            quedata.ustqa_id=ustqa_id;
            quedata.ustqa_que_id=ustqa_que_id;
            quedata.ustqa_correct=ustqa_correct;
            quedata.ustqa_user_ans=ustqa_user_ans;
            quedata.ustqa_mark=ustqa_mark;
            quedata.ustqa_test_id=ustqa_test_id;
            quedata.ustqa_sub=ustqa_sub;
            quedata.ustqa_ust_id=ustqa_ust_id;
            quedata.ustqa_user_id=ustqa_user_id;
            quedata.ustqa_status=ustqa_status;
            quedata.ustqa_review=ustqa_review;

            data.add(quedata);
        }

        return data;
    }


    // Inserting data to rowUserSectionTestTable Table
    public Boolean addrowUserSectionTestTable(int ust_id, int ust_test_id, int ust_user_id, String ust_created, String ust_remainingtime, int ust_status,
                                         String ust_question_mark, String ust_wrong_question, String ust_correct_question, String ust_dob){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor mCount= DB.rawQuery("Select * from rowUserSectionTestTable", null);
        ContentValues values = new ContentValues();
        values.put("ust_id",ust_id);
        values.put("ust_test_id",ust_test_id);
        values.put("ust_user_id",ust_user_id);
        values.put("ust_created",ust_created);
        values.put("ust_remainingtime",ust_remainingtime);
        values.put("ust_status",ust_status);
        values.put("ust_question_mark",ust_question_mark);
        values.put("ust_wrong_question",ust_wrong_question);
        values.put("ust_correct_question",ust_correct_question);
        values.put("ust_dob",ust_dob);

        long result = DB.insert("rowUserSectionTestTable",null,values);
//            if (result>0){
        return true;
//            }
    }

    // Fetching data from UstQuesAnswerTable
    public List<RowUserSectionTestModel> getrowUserSectionTestTable(int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from rowUserSectionTestTable where ust_test_id="+ qtest_id,null);

        List<RowUserSectionTestModel> data=new ArrayList<>();
        while (cursor.moveToNext()){
            @SuppressLint("Range") int ust_id=cursor.getInt(cursor.getColumnIndex("ust_id"));
            @SuppressLint("Range") int ust_test_id=cursor.getInt(cursor.getColumnIndex("ust_test_id"));
            @SuppressLint("Range") int ust_user_id=cursor.getInt(cursor.getColumnIndex("ust_user_id"));
            @SuppressLint("Range") String ust_created=cursor.getString(cursor.getColumnIndex("ust_created"));
            @SuppressLint("Range") String ust_remainingtime=cursor.getString(cursor.getColumnIndex("ust_remainingtime"));
            @SuppressLint("Range") int ust_status=cursor.getInt(cursor.getColumnIndex("ust_status"));
            @SuppressLint("Range") String ust_question_mark=cursor.getString(cursor.getColumnIndex("ust_question_mark"));
            @SuppressLint("Range") String ust_wrong_question=cursor.getString(cursor.getColumnIndex("ust_wrong_question"));
            @SuppressLint("Range") String ust_correct_question=cursor.getString(cursor.getColumnIndex("ust_correct_question"));
            @SuppressLint("Range") String ust_dob=cursor.getString(cursor.getColumnIndex("ust_dob"));

            RowUserSectionTestModel quedata = new RowUserSectionTestModel();
            quedata.ust_id=ust_id;
            quedata.ust_test_id=ust_test_id;
            quedata.ust_user_id=ust_user_id;
            quedata.ust_created=ust_created;
            quedata.ust_remainingtime=ust_remainingtime;
            quedata.ust_status=ust_status;
            quedata.ust_question_mark=ust_question_mark;
            quedata.ust_wrong_question=ust_wrong_question;
            quedata.ust_correct_question=ust_correct_question;
            quedata.ust_dob=ust_dob;

            data.add(quedata);
        }

        return data;
    }



    public boolean checkDataExistOrNot(String columnName, String value) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = "SELECT * FROM" + " SubjectDetails" + " WHERE " + columnName + " = " + value;
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;  // return false if value not exists in database
        }
        cursor.close();
        return true;  // return true if value exists in database
    }


    // Fetching data from QuestionsDetails
    public Cursor getQuestionIdData1(int qtest_id){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where tlq_question_id="+ qtest_id,null);

        return cursor;
    }

    // get Question data for submit fun;
    public Cursor getQuestionIdData11(int tlqid){
        SQLiteDatabase DB = this.getWritableDatabase();
        List<QuestionModel> list = new ArrayList<>();
//        Log.e("abccc","tlg="+tlqid);
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where tlq_question_id ="+ tlqid,null);
        return cursor;
    }

    // get data from UstQuesAnswer Table for submit fun;
    public Cursor getUstQuesAnswerTableSubmit(String tlqid){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from UstQuesAnswerTable where ustqa_que_id ="+ tlqid,null);
//        Log.e("abcccId","tlg="+qtest_id);

        return cursor;
    }

    // get SubjectDetails Table Data
    public Cursor getSubjectDetailsInfoData(int tlqid){
        SQLiteDatabase DB = this.getWritableDatabase();
        List<SubjectInfoModel> list = new ArrayList<>();
//        Log.e("abccc","tlg="+tlqid);
        Cursor cursor = DB.rawQuery("Select * from SubjectDetails where test_id ="+ tlqid,null);
        return cursor;
    }

    public Cursor getquestiondata(int tlqid){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from UstQuesAnswerTable where tlq_id ="+ tlqid,null);
        return cursor;
    }


    public Cursor getSubjectDetailsData(int tastId){
        SQLiteDatabase DB = this.getWritableDatabase();
        List<QuestionModel> list = new ArrayList<>();
//        Log.e("abccc","tlg="+tastId);
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where tlq_id ="+ tastId,null);
        return cursor;
    }




    //Update Question table
    public Boolean updateQuestionTable(int tlqid,int selectedAnswer,int questionStatus){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues1= new ContentValues();
        contentValues1.put("ustqa_user_ans",selectedAnswer);
        contentValues1.put("ustqa_status",questionStatus);
        long result = DB.update("UstQuesAnswerTable",contentValues1,"tlq_id=?",new String[]{String.valueOf(tlqid)});
//        Log.e("DBQData ", "Q = "+tlqid+" Ans = "+selectedAnswer+" Q Status = "+questionStatus);

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    //Update Question table
    public Boolean updateQuestionTableonNextPre(int tlqid, int questionStatus){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues1= new ContentValues();
        contentValues1.put("ustqa_status",questionStatus);
        long result = DB.update("UstQuesAnswerTable",contentValues1,"tlq_id=?",new String[]{String.valueOf(tlqid)});

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    //Update Question table
    public Boolean updateQuestionReviewTable(int tlqid,int selectedAnswer,int questionReview){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues1= new ContentValues();
        contentValues1.put("ustqa_user_ans",selectedAnswer);
        contentValues1.put("ustqa_review",questionReview);
        long result = DB.update("UstQuesAnswerTable",contentValues1,"tlq_id=?",new String[]{String.valueOf(tlqid)});
//        Log.e("DBQData", "Q = "+tlqid+" Ans = "+selectedAnswer+" Review = "+questionReview);

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    //Update SubjectDetails table
    public Boolean updateSubjectDetailTable(int tlqid,int selectedLanguage){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("test_language",selectedLanguage);
        long result = DB.update("SubjectDetails",contentValues,"test_id=?",new String[]{String.valueOf(tlqid)});

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    //Update SubjectDetails table Time
    public Boolean updateTimerSubjectDetailTable(int tlqid,String timer){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ust_remainingtime",timer);
        long result = DB.update("rowUserSectionTestTable",contentValues,"ust_test_id=?",new String[]{String.valueOf(tlqid)});

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    //Update SubjectDetails table Time
    public Boolean updateQuestionNumberSubjectDetailTable(int tlqid,int queNo){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("test_pause_on_que_num",queNo);
        long result = DB.update("SubjectDetails",contentValues,"test_id=?",new String[]{String.valueOf(tlqid)});

        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }

    public Boolean updateQuestionTableWithReview(int tlqid,int selectedAnswer,int questionStatus){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("UserSelectedAns",selectedAnswer);
        contentValues.put("user_que_status",questionStatus);
        long result = DB.update("QuestionsDetails",contentValues,"tlq_id=?",new String[]{String.valueOf(tlqid)});
        if (result == 1){
            return false;
        }
        else{
            return true;
        }
    }




    //Delete SubjectDetails
    public Boolean deleteSubjectDetails(){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from SubjectDetails",null);
        if (cursor.getCount() > 0){
            long result = DB.delete("SubjectDetails",null,null);
            DB.close();
            if(result==-1){
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
    //Delete SubjectQuestionDetails
    public Boolean deleteSubjectQuestionDetails(){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from SubjectQuestionDetails",null);
        if (cursor.getCount() > 0){
            long result = DB.delete("SubjectQuestionDetails",null,null);
            DB.close();
            if(result==-1){
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
    //Delete QuestionsDetails
    public Boolean deleteQuestionsDetails(int tlqid){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from QuestionsDetails where tlq_id="+ tlqid,null);
        if (cursor.getCount() > 0){
            long result = DB.delete("QuestionsDetails","tlq_id=?",new String[]{String.valueOf(tlqid)});
            DB.close();
            if(result==-1){
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }



}
