package codexo.neonclasses.examDatabase;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import codexo.neonclasses.model.QuestionTable;
import codexo.neonclasses.model.RawExamInfoTable;
import codexo.neonclasses.model.SubjectDataTable;


public class DatabaseRepo {
    private Context context;
    public CartAppDatabase database;
    private CartDatabaseDAO dao;

    public DatabaseRepo(Context context) {
        if (database == null) {
            database = CartAppDatabase.getInstance(context);
            dao = database.getCartDAO();
        }
    }

    //Livedata
    public LiveData<List<QuestionTable>> getQuestionTable(String subjectId) {
        return dao.getQuestionTable(subjectId);
    }

    public LiveData<List<SubjectDataTable>> getSubjectDetail(int testId) {
        return dao.getRawSubjectData(testId);
    }
//
//    public LiveData<RawExamInfoTable> getRawExamInfo() {
//        return dao.getRawTestInfo();
//    }

    public void Insert(RawExamInfoTable table) {
        AsyncTask.execute(() -> dao.Insert(table));
    }

    public void Update(RawExamInfoTable table) {
        AsyncTask.execute(() -> dao.Update(table));
    }

//    public void UpdateMultipleData(RawExamInfoTable table) {
//        AsyncTask.execute(() -> dao.UpdateDataInRawExamInfo(table.getSolutionPublishDate(), table.getTestTitle(), table.getTestId()));
//    }

    public void Delete(RawExamInfoTable table) {
        AsyncTask.execute(() -> dao.Delete(table));
    }

    public void DeleteRawExamInfoTable(RawExamInfoTable table) {
        AsyncTask.execute(() -> dao.DeleteRawExamTable());
    }

    public void Insert(SubjectDataTable table) {
        AsyncTask.execute(() -> dao.Insert(table));
    }

    public void Delete(SubjectDataTable table) {
        AsyncTask.execute(() -> dao.Delete(table));
    }

    public void Update(SubjectDataTable table) {
        AsyncTask.execute(() -> dao.Update(table));
    }

    public void Insert(QuestionTable table) {
        AsyncTask.execute(() -> dao.Insert(table));
    }

    public void Delete(QuestionTable table) {
        AsyncTask.execute(() -> dao.Delete(table));
    }

    public void Update(QuestionTable table) {
        AsyncTask.execute(() -> dao.Update(table));
    }

    public void DeleteAll(){
        AsyncTask.execute(() ->{
            dao.DeleteSubjectDataTable();
            dao.DeleteRawExamTable();
            dao.DeleteQuestionTable();
        });
    }


}
