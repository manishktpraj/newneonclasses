package codexo.neonclasses.examDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


import java.util.List;

import codexo.neonclasses.model.QuestionTable;
import codexo.neonclasses.model.RawExamInfoTable;
import codexo.neonclasses.model.SubjectDataTable;

@Dao
public interface CartDatabaseDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void Insert(RawExamInfoTable contact);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void Insert(SubjectDataTable contact);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void Insert(QuestionTable contact);

    @Update
    public void Update(RawExamInfoTable contact);
    @Update
    public void Update(SubjectDataTable contact);
    @Update
    public void Update(QuestionTable contact);

    @Delete
    public void Delete(RawExamInfoTable contact);
    @Delete
    public void Delete(SubjectDataTable contact);
    @Delete
    public void Delete(QuestionTable contact);

    @Query("UPDATE RawExamInfoTable SET product_publish_date=:publishDate and test_title=:title WHERE testId = :testId" )
    public void UpdateDataInRawExamInfo(String publishDate , String title,int testId);


    @Query("select * from RawExamInfoTable")
    public LiveData<RawExamInfoTable> getRawTestInfo();

    @Query("select * from SubjectDataTable where testId=:testId")
    public LiveData<List<SubjectDataTable>> getRawSubjectData(int testId);

    @Query("select * from QuestionTable where tlq_sub_id=:subjectId")
    public LiveData<List<QuestionTable>> getQuestionTable(String subjectId);


    @Query("DELETE FROM RawExamInfoTable")
    public void DeleteRawExamTable();

    @Query("DELETE FROM SubjectDataTable")
    public void DeleteSubjectDataTable();

    @Query("DELETE FROM QuestionTable")
    public void DeleteQuestionTable();

}
