package codexo.neonclasses.examDatabase;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import codexo.neonclasses.model.QuestionTable;
import codexo.neonclasses.model.RawExamInfoTable;
import codexo.neonclasses.model.SubjectDataTable;


@Database(entities = {RawExamInfoTable.class, SubjectDataTable.class, QuestionTable.class},version = 1,exportSchema = false)
public abstract class  CartAppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "Gp_Database";
    public abstract CartDatabaseDAO getCartDAO();
    private static CartAppDatabase instance;
    private static Callback roomcallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };

    public static synchronized CartAppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), CartAppDatabase.class, DATABASE_NAME)
                    .setJournalMode(JournalMode.AUTOMATIC).allowMainThreadQueries()
                    .addCallback(roomcallback).fallbackToDestructiveMigration().build();
        }
        return instance;
    }

}
