package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class Forgot extends AppCompatActivity {
    AppCompatActivity activity;
    TextView welcomtoneon,welcomdesc,loginbutton,registerbutton,forgotpass,loginviaotp;
    ImageView passwordeye;
    TextInputLayout username;
    TextInputEditText username_edit;
    String username_string;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Forgot.this;
        Constant.setToolbar(activity, toolbar);
         welcomtoneon =findViewById(R.id.welcomtoneon);
        welcomdesc =findViewById(R.id.welcomdesc);
        loginbutton =findViewById(R.id.loginbutton);
        pDialog = Constant.getProgressBar(Forgot.this);
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
        welcomdesc.setTypeface(Constant.getFonts(getApplicationContext()));
        loginbutton.setTypeface(Constant.getFonts(getApplicationContext()));
        username= findViewById(R.id.username);
        username_edit =findViewById(R.id.username_edit);
        username_edit.setTypeface(Constant.getFonts(getApplicationContext()));
        username_edit.setEnabled(true);
        username_edit.setTextIsSelectable(true);
        username_edit.setFocusable(true);
        username_edit.setFocusableInTouchMode(true);
        username_edit.requestFocus();


        Constant.adbEnabled(Forgot.this);
        Constant.checkAdb(Forgot.this);

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String go_next = "yes";
                username_string = username_edit.getText().toString();
                if(username_string.equals(""))
                {
                    username_edit.setError("Please Enter Mobile number");
                    go_next ="no";
                }
                if(username_string.length()<10)
                {
                    username_edit.setError("Please Enter Valid Mobile number");
                    go_next ="no";
                }
                if(go_next.equals("yes")) {
                    forgotpassword(username_string);
                }

            }
        });

    }

    private void forgotpassword(final String username_string) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().forgotpassword(username_string);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");
                            Constant.setToast(Forgot.this,notification);
                            Intent i = new Intent(activity, Otp.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("results",localJSONObject2+"");
                            i.putExtra("type","FORGOT");
                            activity.startActivity(i);
                            finish();
                        } else {
                            Constant.setToast(Forgot.this,notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                forgotpassword(username_string);
            }
        });
    }
    @Override
    public void onBackPressed(){
        startActivity(new Intent(getApplicationContext(),Intro.class));
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Forgot.this);
        Constant.checkAdb(Forgot.this);
    }
}