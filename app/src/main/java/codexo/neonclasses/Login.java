package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import codexo.neonclasses.session.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;

public class Login extends AppCompatActivity {
    AppCompatActivity activity;
    TextView welcomtoneon,welcomdesc,loginbutton,registerbutton,forgotpass,loginviaotp;
    ImageView passwordeye;
    String passwordtext="hide";
    TextInputLayout username,password;
    TextInputEditText username_edit,password_edit;
    SessionManager session;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Login.this;
        Constant.setToolbar(activity, toolbar);
        passwordeye =findViewById(R.id.passwordeye);
        welcomtoneon =findViewById(R.id.welcomtoneon);
        welcomdesc =findViewById(R.id.welcomdesc);
        loginbutton =findViewById(R.id.loginbutton);
        forgotpass =findViewById(R.id.forgotpass);
        loginviaotp=findViewById(R.id.loginviaotp);
        registerbutton=findViewById(R.id.registerbutton);
        Constant.adbEnabled(Login.this);
        Constant.checkAdb(Login.this);
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
        welcomdesc.setTypeface(Constant.getFonts(getApplicationContext()));
        loginbutton.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        forgotpass.setTypeface(Constant.getFontsBold(getApplicationContext()));
        loginviaotp.setTypeface(Constant.getFontsBold(getApplicationContext()));
        registerbutton.setTypeface(Constant.getFontsBold(getApplicationContext()));


        username= findViewById(R.id.username);
        username_edit =findViewById(R.id.username_edit);

        password= findViewById(R.id.password);
        password_edit =findViewById(R.id.password_edit);

        username_edit.setTypeface(Constant.getFonts(getApplicationContext()));
        username_edit.setEnabled(true);
        username_edit.setTextIsSelectable(true);
        username_edit.setFocusable(true);
        username_edit.setFocusableInTouchMode(true);
        username_edit.requestFocus();

        pDialog = Constant.getProgressBar(Login.this);
        session = new SessionManager(getApplicationContext());
       /// fullScreen();
        passwordeye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(passwordtext.equals("hide"))
                {
                    passwordtext ="show";
                    password_edit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordeye.setImageResource(R.drawable.passwordeyeclose);

                }else{
                    passwordtext ="hide";
                    password_edit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordeye.setImageResource(R.drawable.passwordeye);
                }
            }
        });

        loginviaotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), Loginviaotp.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
                finish();
             }
        });
        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), Forgot.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
                finish();
            }
        });
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String go_next = "yes";
                String username_string = username_edit.getText().toString();
                String password_string = password_edit.getText().toString();
                if(username_string.equals(""))
                {
                    username_edit.setError("Please Enter Username");
                    go_next ="no";
                }
                if(password_string.equals(""))
                {
                    password_edit.setError("Please Enter password");
                    go_next ="no";
                }
                if(go_next.equals("yes")) {

                    doLogin(username_string,password_string);
                }
            }
        });
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), Register.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
                finish();
            }
        });
    }



    private void doLogin(final String username_string, final String password_string) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().doLogin(username_string,password_string,Constant.device_id,Constant.device_info);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
               // Constant.logPrint("response", response);
                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            Constant.skip="yes";
                            Constant.fragments="main";

                            Toast.makeText(activity, "Login successfully", Toast.LENGTH_SHORT).show();

                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");

                            session.createLoginSession(localJSONObject2);
                            Intent i = new Intent(activity, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            activity.startActivity(i);
                            finish();
                        } else {
                            Constant.setToast(Login.this,"Login failed! Please enter correct username or password.");

                           /* new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("Login failed! Please enter correct username or password.")
                                    .setNegativeButton("ok", null).show();*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                doLogin(username_string, password_string);
            }
        });
    }
    @Override
    public void onBackPressed(){
        startActivity(new Intent(getApplicationContext(),Intro.class));
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Login.this);
        Constant.checkAdb(Login.this);
    }
}