package codexo.neonclasses.model;

public class SubjectInfoModel {
    public int test_id;
    public int test_language;
    public int test_admin_id;
    public String test_duration;
    public int test_pause_on_que_num;
    public boolean test_free_status;
    public int test_dpt_id;
    public String test_solution_file;
    public String test_datetime;
    public String test_title;
    public int test_total_marks;
    public int product_in_stock;
    public int test_marks;
    public int test_series_id;
    public int test_ts_id =0;
    public int test_section_type;
    public int test_et_id;
    public String test_paper_file;
    public String test_sub_question;
    public int test_sectional_status;
    public int test_status;
    public int test_total_question;
    public String test_start_date;
    public String ot_test_solution_datetime;
    public String test_subject_stope_time;
    public int test_live_status;
    public String test_sub_id;
    public String test_comming_date;
    public int test_type;
    public String test_end_date;
    public double test_negative_marks;
    public String test_syllabus_info;
    public String product_publish_date;
    public int test_number;
    public String ot_user_test_subject_view;
}
