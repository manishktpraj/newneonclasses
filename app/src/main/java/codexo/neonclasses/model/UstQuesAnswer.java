package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

public class UstQuesAnswer {
    @SerializedName("ustqa_id")
    private int ustqaId;

    @SerializedName("ustqa_que_id")
    private int ustqaQueId;

    @SerializedName("ustqa_correct")
    private String ustqaCorrect;

    @SerializedName("ustqa_user_ans")
    private int ustqaUserAns;

    @SerializedName("ustqa_mark")
    private String ustqaMark;

    @SerializedName("ustqa_test_id")
    private int ustqaTestId;

    @SerializedName("ustqa_ust_id")
    private int ustqaUstId;

    @SerializedName("ustqa_user_id")
    private String ustqaUserId;

    @SerializedName("ustqa_status")
    private int ustqaStatus;


    public int getUstqaId() {
        return ustqaId;
    }

    public void setUstqaId(int ustqaId) {
        this.ustqaId = ustqaId;
    }

    public int getUstqaQueId() {
        return ustqaQueId;
    }

    public void setUstqaQueId(int ustqaQueId) {
        this.ustqaQueId = ustqaQueId;
    }

    public String getUstqaCorrect() {
        return ustqaCorrect;
    }

    public void setUstqaCorrect(String ustqaCorrect) {
        this.ustqaCorrect = ustqaCorrect;
    }

    public int getUstqaUserAns() {
        return ustqaUserAns;
    }

    public void setUstqaUserAns(int ustqaUserAns) {
        this.ustqaUserAns = ustqaUserAns;
    }

    public String getUstqaMark() {
        return ustqaMark;
    }

    public void setUstqaMark(String ustqaMark) {
        this.ustqaMark = ustqaMark;
    }

    public int getUstqaTestId() {
        return ustqaTestId;
    }

    public void setUstqaTestId(int ustqaTestId) {
        this.ustqaTestId = ustqaTestId;
    }

    public int getUstqaUstId() {
        return ustqaUstId;
    }

    public void setUstqaUstId(int ustqaUstId) {
        this.ustqaUstId = ustqaUstId;
    }

    public String getUstqaUserId() {
        return ustqaUserId;
    }

    public void setUstqaUserId(String ustqaUserId) {
        this.ustqaUserId = ustqaUserId;
    }

    public int getUstqaStatus() {
        return ustqaStatus;
    }

    public void setUstqaStatus(int ustqaStatus) {
        this.ustqaStatus = ustqaStatus;
    }
}
