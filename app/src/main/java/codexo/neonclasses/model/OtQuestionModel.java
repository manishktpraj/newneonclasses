package codexo.neonclasses.model;

public class OtQuestionModel {
    public String que_test_id;
    public int que_order;
    public String que_correct_option;
    public int que_area;
    public String que_correct_option_hindi;
    public int que_id;
    public String que_sub_id;
    public int que_tlq_id;
    public String ust_ques_answer;
    public String ot_test_language_question;
    public String ot_matchword_ques;
}
