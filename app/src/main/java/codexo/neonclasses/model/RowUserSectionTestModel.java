package codexo.neonclasses.model;

public class RowUserSectionTestModel {
    public int ust_id;
    public int ust_test_id;
    public int ust_user_id;
    public String ust_created;
    public String ust_remainingtime;
    public int ust_status;
    public String ust_question_mark;
    public String ust_wrong_question;
    public String ust_correct_question;
    public String ust_dob;
}
