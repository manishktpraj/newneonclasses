package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

public class results_freeexam {

    @SerializedName("sub_pack_id")
    private int sub_pack_id;

    @SerializedName("sub_pack_title")
    private String sub_pack_title;

    @SerializedName("sub_pack_price")
    private String sub_pack_price;

    @SerializedName("sub_pack_no_test")
    private String sub_pack_no_test;

    @SerializedName("sub_pack_validity")
    private String sub_pack_validity;

    @SerializedName("sub_pack_created_date")
    private int sub_pack_created_date;

    @SerializedName("test_status")
    private String test_status;

    @SerializedName("test_admin_id")
    private String test_admin_id;

    @SerializedName("test_series_id")
    private String test_series_id;

    @SerializedName("test_paper_file")
    private String test_paper_file;

    @SerializedName("test_solution_file")
    private int test_solution_file;

    @SerializedName("test_free_status")
    private String test_free_status;

    @SerializedName("product_in_stock")
    private String product_in_stock;

    @SerializedName("test_sectional_status")
    private String test_sectional_status;

    @SerializedName("test_live_status")
    private String test_live_status;

    @SerializedName("test_end_date")
    private int test_end_date;


}
