package codexo.neonclasses.model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;


import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.ui.test.ExamActivity;

public class Viewmodel extends AndroidViewModel {
    private SqlDatabase repository;
    ExamActivity examData;
    RawExamInfoTable rawExamInfoTable;

    public Viewmodel(@NonNull Application application, SqlDatabase repository) {
        super(application);
        this.repository = repository;
        examData = new ExamActivity();
    }

//    public Viewmodel(@NonNull Application application) {
//        super(application);
//        repository = new SqlDatabase(application);
//        examData = new MainActivity();
//    }

//    public LiveData<List<RawExamInfoTable>> getExamData() {
//        return repository.getRawExamInfo();
//    }

//    public LiveData<List<SubjectDataTable>> getSubjectata(int testid) {
//        return repository.getSubjectDetail(testid);
//    }
//
//    public LiveData<List<QuestionTable>> getQuestiontata(String subjectId) {
//        return repository.getQuestionTable(subjectId);
//    }

    public void GetExamDataFromApi(String jsondata) {
        Type type = new TypeToken<ExamResponse>() {
        }.getType();
        Gson gson = new Gson();
        ExamResponse response = gson.fromJson(jsondata, type);
        Log.e("response123",response.toString());





//        repository.addRawExam(new RawExamInfoTable());
//        Log.d("ldldldll", String.valueOf(response.getRowExamInfo().getOtUserTestSubjectView().size()));

//        for (int i = 0; i < response.getRowExamInfo().getOtUserTestSubjectView().size(); i++) {
//            repository.Insert(new SubjectDataTable(response.getRowExamInfo().getTestId(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdSubjectId(),
//                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdNegativeMarks(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdStopTime(),
//                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTestId(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
//                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubStatus(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubParentId(),
//                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalQuestion(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalMarks(),
//                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubName()));
//
//            for (int j=0; j <response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size(); j++){
//                repository.Insert(new QuestionTable(response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUniqueId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionType(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptions(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1Attachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2Attachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3Attachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4Attachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5Attachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqLangId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionText(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionTextHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachments(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1AttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2AttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3AttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4AttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5AttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionTypeHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachmentsHindi(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishInstruction(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiInstruction(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolution(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolution(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolutionAttach(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolutionAttach(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionAttach(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionStatus(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionOrder(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUserId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionHindiAttach(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
//                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqCorrectOption()));
//                Log.d("ldldldll",response.getRowExamInfo().getOtUserTestSubjectView().size()+"++"+i+"++"+response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size()
//                +"+++"+j+"+++"+response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqCorrectOption());
//
//            }
//        }

        /*for (int i = 0; i < response.getRowExamInfo().getOtUserTestSubjectView().size(); i++) {
            repository.Insert(new SubjectDataTable());
        }
        repository.Insert(new QuestionTable());*/
        //Call api here
        //Insert data into database
        //from repository.insert(rawExaminfoObject)
    }

//    public void DeleteAll() {
//        repository.DeleteAll();
//    }
//
//    public void updateQuestionTable(QuestionTable clickedItem) {
//        repository.Insert(clickedItem);
//    }
}
