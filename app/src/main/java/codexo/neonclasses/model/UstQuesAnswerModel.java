package codexo.neonclasses.model;

public class UstQuesAnswerModel {
    public int tlq_id;
    public int tlq_counterid;
    public int ustqa_id;
    public int ustqa_que_id;
    public String ustqa_correct;
    public int ustqa_user_ans;
    public String ustqa_mark;
    public int ustqa_test_id;
    public int ustqa_sub;
    public int ustqa_ust_id;
    public String ustqa_user_id;
    public int ustqa_status;
    public int ustqa_review;

}
