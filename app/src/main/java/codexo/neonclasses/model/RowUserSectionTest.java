package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

public class RowUserSectionTest {

	@SerializedName("ust_id")
	private int ustId;

	@SerializedName("ust_test_id")
	private int ustTestId;

	@SerializedName("ust_user_id")
	private int ustUserId;

	@SerializedName("ust_created")
	private String ustCreated;

	@SerializedName("ust_remainingtime")
	private String ustRemainingtime;

	@SerializedName("ust_status")
	private int ustStatus;

	@SerializedName("ust_question_mark")
	private String ustQuestionMark;

	@SerializedName("ust_wrong_question")
	private String ustWrongQuestion;

	@SerializedName("ust_correct_question")
	private String ustCorrectQuestion;

	@SerializedName("ust_dob")
	private String ustDob;

	public int getUstId() {
		return ustId;
	}

	public void setUstId(int ustId) {
		this.ustId = ustId;
	}

	public int getUstTestId() {
		return ustTestId;
	}

	public void setUstTestId(int ustTestId) {
		this.ustTestId = ustTestId;
	}

	public int getUstUserId() {
		return ustUserId;
	}

	public void setUstUserId(int ustUserId) {
		this.ustUserId = ustUserId;
	}

	public String getUstCreated() {
		return ustCreated;
	}

	public void setUstCreated(String ustCreated) {
		this.ustCreated = ustCreated;
	}

	public String getUstRemainingtime() {
		return ustRemainingtime;
	}

	public void setUstRemainingtime(String ustRemainingtime) {
		this.ustRemainingtime = ustRemainingtime;
	}

	public int getUstStatus() {
		return ustStatus;
	}

	public void setUstStatus(int ustStatus) {
		this.ustStatus = ustStatus;
	}

	public String getUstQuestionMark() {
		return ustQuestionMark;
	}

	public void setUstQuestionMark(String ustQuestionMark) {
		this.ustQuestionMark = ustQuestionMark;
	}

	public String getUstWrongQuestion() {
		return ustWrongQuestion;
	}

	public void setUstWrongQuestion(String ustWrongQuestion) {
		this.ustWrongQuestion = ustWrongQuestion;
	}

	public String getUstCorrectQuestion() {
		return ustCorrectQuestion;
	}

	public void setUstCorrectQuestion(String ustCorrectQuestion) {
		this.ustCorrectQuestion = ustCorrectQuestion;
	}

	public String getUstDob() {
		return ustDob;
	}

	public void setUstDob(String ustDob) {
		this.ustDob = ustDob;
	}
}