package codexo.neonclasses.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity()
public class RawExamInfoTable {
//     @PrimaryKey
//     @SerializedName("id") int testId;
//     @SerializedName("test_duration") String testDuration;
//     @SerializedName("test_ts_id") int tsId=0;
//     @SerializedName("test_dpt_id") int dpt_id;
//     @SerializedName("test_et_id") int etId;
//     @SerializedName("test_number") int testNumber;
//     @SerializedName("test_sub_id") String subId;
//     @SerializedName("test_marks") int testMarks;
//     @SerializedName("test_negative_marks") double negativeMarks;
//     @SerializedName("test_sub_question") String subQuestion;
//     @SerializedName("test_datetime") String dateTime;
//     @SerializedName("test_status") int testStatus;
//     @SerializedName("test_admin_id") int testAdminId;
//     @SerializedName("test_series_id") int testSeriesId;
//     @SerializedName("test_paper_file") String paperFile;
//     @SerializedName("test_solution_file") String solutionFile;
//     @SerializedName("test_free_status") boolean testFreeStatus=false;
//     @SerializedName("product_in_stock") int productInStock;
//     @SerializedName("test_sectional_status") int testSelectionStatus;
//     @SerializedName("test_live_status") int testLiveStatus;
//     @SerializedName("test_end_date") String testEndDate;
//     @SerializedName("test_start_date") String testStartDate;
//     @SerializedName("ot_test_solution_datetime") String testSolutionDateTime;
//     @SerializedName("test_subject_stope_time") String subjectStopTime;
//     @SerializedName("test_syllabus_info") String syllabusInfo;
//     @SerializedName("test_comming_date") String testCommingDate;
//     @SerializedName("test_title") String testTitle;
//     @SerializedName("test_total_marks") int totalMarks;
//     @SerializedName("test_total_question") int totalQuestion;
//     @SerializedName("product_publish_date") String solutionPublishDate;
//     @SerializedName("test_section_type") int selectionType;
//     @SerializedName("test_type") int testType;

     @PrimaryKey
     private int testId;
     @ColumnInfo(name ="test_duration")
     private String testDuration;
     @ColumnInfo(name ="test_ts_id")
     private int tsId=0;
     @ColumnInfo(name ="test_dpt_id")
     private int dpt_id;
     @ColumnInfo(name ="test_et_id")
     private int etId;
     @ColumnInfo(name ="test_number")
     private int testNumber;
     @ColumnInfo(name ="test_sub_id")
     private String subId;
     @ColumnInfo(name ="test_marks")
     private int testMarks;
     @ColumnInfo(name = "test_negative_marks")
     private double negativeMarks;
     @ColumnInfo(name ="test_sub_question")
     private String subQuestion;
     @ColumnInfo(name ="test_datetime")
     private String dateTime;
     @ColumnInfo(name ="test_status")
     private int testStatus;
     @ColumnInfo(name ="test_admin_id")
     private int testAdminId;
     @ColumnInfo(name ="test_series_id")
     private int testSeriesId;
     @ColumnInfo(name ="test_paper_file")
     private String paperFile;
     @ColumnInfo(name ="test_solution_file")
     private String solutionFile;
     @ColumnInfo(name ="test_free_status")
     private boolean testFreeStatus=false;
     @ColumnInfo(name ="product_in_stock")
     private int productInStock;
     @ColumnInfo(name ="test_sectional_status")
     private int testSelectionStatus;
     @ColumnInfo(name ="test_live_status")
     private int testLiveStatus;
     @ColumnInfo(name ="test_end_date")
     private String testEndDate;
     @ColumnInfo(name ="test_start_date")
     private String testStartDate;
     @ColumnInfo(name ="ot_test_solution_datetime")
     private String testSolutionDateTime;
     @ColumnInfo(name ="test_subject_stope_time")
     private String subjectStopTime;
     @ColumnInfo(name ="test_syllabus_info")
     private String syllabusInfo;
     @ColumnInfo(name ="test_comming_date")
     private String testCommingDate;
     @ColumnInfo(name ="test_title")
     private String testTitle;
     @ColumnInfo(name = "test_total_marks")
     private int totalMarks;
     @ColumnInfo(name ="test_total_question")
     private int totalQuestion;
     @ColumnInfo(name = "product_publish_date")
     private String solutionPublishDate;
     @ColumnInfo(name ="test_section_type")
     private int selectionType;
     @ColumnInfo(name ="test_type")
     private int testType;

     public RawExamInfoTable(@NonNull int testId, String testDuration, int tsId, int dpt_id, int etId, int testNumber, String subId, int testMarks, double negativeMarks,
                             String subQuestion, String dateTime, int testStatus, int testAdminId, int testSeriesId, String paperFile, String solutionFile, boolean testFreeStatus,
                             int productInStock, int testSelectionStatus, int testLiveStatus, String testEndDate, String testStartDate, String testSolutionDateTime,
                             String subjectStopTime, String syllabusInfo, String testCommingDate, String testTitle, int totalMarks, int totalQuestion, String solutionPublishDate, int selectionType, int testType) {
          this.testId = testId;
          this.testDuration = testDuration;
          this.tsId = tsId;
          this.dpt_id = dpt_id;
          this.etId = etId;
          this.testNumber = testNumber;
          this.subId = subId;
          this.testMarks = testMarks;
          this.negativeMarks = negativeMarks;
          this.subQuestion = subQuestion;
          this.dateTime = dateTime;
          this.testStatus = testStatus;
          this.testAdminId = testAdminId;
          this.testSeriesId = testSeriesId;
          this.paperFile = paperFile;
          this.solutionFile = solutionFile;
          this.testFreeStatus = testFreeStatus;
          this.productInStock = productInStock;
          this.testSelectionStatus = testSelectionStatus;
          this.testLiveStatus = testLiveStatus;
          this.testEndDate = testEndDate;
          this.testStartDate = testStartDate;
          this.testSolutionDateTime = testSolutionDateTime;
          this.subjectStopTime = subjectStopTime;
          this.syllabusInfo = syllabusInfo;
          this.testCommingDate = testCommingDate;
          this.testTitle = testTitle;
          this.totalMarks = totalMarks;
          this.totalQuestion = totalQuestion;
          this.solutionPublishDate = solutionPublishDate;
          this.selectionType = selectionType;
          this.testType = testType;
     }

     @Ignore
     public RawExamInfoTable() {
     }

     @NonNull
     public int getTestId() {
          return testId;
     }

     public void setTestId(@NonNull int testId) {
          this.testId = testId;
     }

     public String getTestDuration() {
          return testDuration;
     }

     public void setTestDuration(String testDuration) {
          this.testDuration = testDuration;
     }

     public int getTsId() {
          return tsId;
     }

     public void setTsId(int tsId) {
          this.tsId = tsId;
     }

     public int getDpt_id() {
          return dpt_id;
     }

     public void setDpt_id(int dpt_id) {
          this.dpt_id = dpt_id;
     }

     public int getEtId() {
          return etId;
     }

     public void setEtId(int etId) {
          this.etId = etId;
     }

     public int getTestNumber() {
          return testNumber;
     }

     public void setTestNumber(int testNumber) {
          this.testNumber = testNumber;
     }

     public String getSubId() {
          return subId;
     }

     public void setSubId(String subId) {
          this.subId = subId;
     }

     public int getTestMarks() {
          return testMarks;
     }

     public void setTestMarks(int testMarks) {
          this.testMarks = testMarks;
     }

     public double getNegativeMarks() {
          return negativeMarks;
     }

     public void setNegativeMarks(double negativeMarks) {
          this.negativeMarks = negativeMarks;
     }

     public String getSubQuestion() {
          return subQuestion;
     }

     public void setSubQuestion(String subQuestion) {
          this.subQuestion = subQuestion;
     }

     public String getDateTime() {
          return dateTime;
     }

     public void setDateTime(String dateTime) {
          this.dateTime = dateTime;
     }

     public int getTestStatus() {
          return testStatus;
     }

     public void setTestStatus(int testStatus) {
          this.testStatus = testStatus;
     }

     public int getTestAdminId() {
          return testAdminId;
     }

     public void setTestAdminId(int testAdminId) {
          this.testAdminId = testAdminId;
     }

     public int getTestSeriesId() {
          return testSeriesId;
     }

     public void setTestSeriesId(int testSeriesId) {
          this.testSeriesId = testSeriesId;
     }

     public String getPaperFile() {
          return paperFile;
     }

     public void setPaperFile(String paperFile) {
          this.paperFile = paperFile;
     }

     public String getSolutionFile() {
          return solutionFile;
     }

     public void setSolutionFile(String solutionFile) {
          this.solutionFile = solutionFile;
     }

     public boolean isTestFreeStatus() {
          return testFreeStatus;
     }

     public void setTestFreeStatus(boolean testFreeStatus) {
          this.testFreeStatus = testFreeStatus;
     }

     public int getProductInStock() {
          return productInStock;
     }

     public void setProductInStock(int productInStock) {
          this.productInStock = productInStock;
     }

     public int getTestSelectionStatus() {
          return testSelectionStatus;
     }

     public void setTestSelectionStatus(int testSelectionStatus) {
          this.testSelectionStatus = testSelectionStatus;
     }

     public int getTestLiveStatus() {
          return testLiveStatus;
     }

     public void setTestLiveStatus(int testLiveStatus) {
          this.testLiveStatus = testLiveStatus;
     }

     public String getTestEndDate() {
          return testEndDate;
     }

     public void setTestEndDate(String testEndDate) {
          this.testEndDate = testEndDate;
     }

     public String getTestStartDate() {
          return testStartDate;
     }

     public void setTestStartDate(String testStartDate) {
          this.testStartDate = testStartDate;
     }

     public String getTestSolutionDateTime() {
          return testSolutionDateTime;
     }

     public void setTestSolutionDateTime(String testSolutionDateTime) {
          this.testSolutionDateTime = testSolutionDateTime;
     }

     public String getSubjectStopTime() {
          return subjectStopTime;
     }

     public void setSubjectStopTime(String subjectStopTime) {
          this.subjectStopTime = subjectStopTime;
     }

     public String getSyllabusInfo() {
          return syllabusInfo;
     }

     public void setSyllabusInfo(String syllabusInfo) {
          this.syllabusInfo = syllabusInfo;
     }

     public String getTestCommingDate() {
          return testCommingDate;
     }

     public void setTestCommingDate(String testCommingDate) {
          this.testCommingDate = testCommingDate;
     }

     public String getTestTitle() {
          return testTitle;
     }

     public void setTestTitle(String testTitle) {
          this.testTitle = testTitle;
     }

     public int getTotalMarks() {
          return totalMarks;
     }

     public void setTotalMarks(int totalMarks) {
          this.totalMarks = totalMarks;
     }

     public int getTotalQuestion() {
          return totalQuestion;
     }

     public void setTotalQuestion(int totalQuestion) {
          this.totalQuestion = totalQuestion;
     }

     public String getSolutionPublishDate() {
          return solutionPublishDate;
     }

     public void setSolutionPublishDate(String solutionPublishDate) {
          this.solutionPublishDate = solutionPublishDate;
     }

     public int getSelectionType() {
          return selectionType;
     }

     public void setSelectionType(int selectionType) {
          this.selectionType = selectionType;
     }

     public int getTestType() {
          return testType;
     }

     public void setTestType(int testType) {
          this.testType = testType;
     }
}
