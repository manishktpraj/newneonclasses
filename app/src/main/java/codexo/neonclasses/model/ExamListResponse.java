package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamListResponse {
    @SerializedName("results")
    private ExamListresults examListresults;

    @SerializedName("results_freeexam")
    private List<results_freeexam> results_freeexam;

    @SerializedName("results_mocexam")
    private List<results_mocexam> results_mocexam;

    @SerializedName("message")
    private String message;

    @SerializedName("student_id")
    private String student_id;

    @SerializedName("test_id")
    private String test_id;

    public ExamListresults getExamListresults() {
        return examListresults;
    }

    public void setExamListresults(ExamListresults examListresults) {
        this.examListresults = examListresults;
    }

    public List<codexo.neonclasses.model.results_freeexam> getResults_freeexam() {
        return results_freeexam;
    }

    public void setResults_freeexam(List<codexo.neonclasses.model.results_freeexam> results_freeexam) {
        this.results_freeexam = results_freeexam;
    }

    public List<codexo.neonclasses.model.results_mocexam> getResults_mocexam() {
        return results_mocexam;
    }

    public void setResults_mocexam(List<codexo.neonclasses.model.results_mocexam> results_mocexam) {
        this.results_mocexam = results_mocexam;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }
}
