package codexo.neonclasses.model.SubmitModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import codexo.neonclasses.model.OtQuestionItem;
import codexo.neonclasses.model.OtUserTestSubjectViewItem;
import codexo.neonclasses.model.RowUserSectionTest;

public class SubmitModel {
    @SerializedName("test_id")
    private int testId;

    @SerializedName("test_duration")
    private String testDuration;

    @SerializedName("test_admin_id")
    private int testAdminId;



    @SerializedName("test_free_status")
    private boolean testFreeStatus;

    @SerializedName("test_dpt_id")
    private int testDptId;

    @SerializedName("test_solution_file")
    private String testSolutionFile;

    @SerializedName("test_datetime")
    private String testDatetime;

    @SerializedName("test_total_marks")
    private int testTotalMarks;

    @SerializedName("product_in_stock")
    private int productInStock;

    @SerializedName("test_marks")
    private int testMarks;

    @SerializedName("test_series_id")
    private int testSeriesId;

    @SerializedName("test_title")
    private String testTitle;

    @SerializedName("test_ts_id")
    private int testTsId;

    @SerializedName("test_section_type")
    private int testSectionType;

    @SerializedName("test_et_id")
    private int testEtId;

    @SerializedName("test_paper_file")
    private String testPaperFile;

    @SerializedName("test_sub_question")
    private String testSubQuestion;



    @SerializedName("test_sectional_status")
    private int testSectionalStatus;

    @SerializedName("test_status")
    private int testStatus;

    @SerializedName("test_total_question")
    private int testTotalQuestion;

    @SerializedName("test_start_date")
    private String testStartDate;

    @SerializedName("ot_test_solution_datetime")
    private String otTestSolutionDatetime;

    @SerializedName("test_subject_stope_time")
    private String testSubjectStopeTime;

    @SerializedName("test_live_status")
    private int testLiveStatus;

    @SerializedName("test_sub_id")
    private String testSubId;

    @SerializedName("test_comming_date")
    private String testCommingDate;

    @SerializedName("test_type")
    private int testType;

    @SerializedName("test_end_date")
    private String testEndDate;

    @SerializedName("test_negative_marks")
    private double testNegativeMarks;

    @SerializedName("test_syllabus_info")
    private String testSyllabusInfo;

    @SerializedName("message")
    private String message;

   /* @SerializedName("ot_user_test_subject_view")
    private List<OtUserTestSubjectViewItem> otUserTestSubjectView;*/

    @SerializedName("product_publish_date")
    private String productPublishDate;

    @SerializedName("test_number")
    private int testNumber;

    private List<OtQuestionItem> all_quesion;


    private List<OtUserTestSubjectViewItem> sub_data;

    private RowUserSectionTest ustid;

    public SubmitModel(int testId, String testDuration, int testAdminId, boolean testFreeStatus, int testDptId, String testSolutionFile, String testDatetime, int testTotalMarks, int productInStock, int testMarks, int testSeriesId, String testTitle, int testTsId, int testSectionType, int testEtId, String testPaperFile, String testSubQuestion, int testSectionalStatus, int testStatus, int testTotalQuestion, String testStartDate, String otTestSolutionDatetime, String testSubjectStopeTime, int testLiveStatus, String testSubId, String testCommingDate, int testType, String testEndDate, double testNegativeMarks, String testSyllabusInfo, String message, String productPublishDate, int testNumber, List<OtQuestionItem> all_quesion, List<OtUserTestSubjectViewItem> sub_data, RowUserSectionTest ustid) {
        this.testId = testId;
        this.testDuration = testDuration;
        this.testAdminId = testAdminId;
        this.testFreeStatus = testFreeStatus;
        this.testDptId = testDptId;
        this.testSolutionFile = testSolutionFile;
        this.testDatetime = testDatetime;
        this.testTotalMarks = testTotalMarks;
        this.productInStock = productInStock;
        this.testMarks = testMarks;
        this.testSeriesId = testSeriesId;
        this.testTitle = testTitle;
        this.testTsId = testTsId;
        this.testSectionType = testSectionType;
        this.testEtId = testEtId;
        this.testPaperFile = testPaperFile;
        this.testSubQuestion = testSubQuestion;
        this.testSectionalStatus = testSectionalStatus;
        this.testStatus = testStatus;
        this.testTotalQuestion = testTotalQuestion;
        this.testStartDate = testStartDate;
        this.otTestSolutionDatetime = otTestSolutionDatetime;
        this.testSubjectStopeTime = testSubjectStopeTime;
        this.testLiveStatus = testLiveStatus;
        this.testSubId = testSubId;
        this.testCommingDate = testCommingDate;
        this.testType = testType;
        this.testEndDate = testEndDate;
        this.testNegativeMarks = testNegativeMarks;
        this.testSyllabusInfo = testSyllabusInfo;
        this.message = message;
        this.productPublishDate = productPublishDate;
        this.testNumber = testNumber;
        this.all_quesion = all_quesion;
        this.sub_data = sub_data;
        this.ustid = ustid;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(String testDuration) {
        this.testDuration = testDuration;
    }

    public int getTestAdminId() {
        return testAdminId;
    }

    public void setTestAdminId(int testAdminId) {
        this.testAdminId = testAdminId;
    }

    public boolean isTestFreeStatus() {
        return testFreeStatus;
    }

    public void setTestFreeStatus(boolean testFreeStatus) {
        this.testFreeStatus = testFreeStatus;
    }

    public int getTestDptId() {
        return testDptId;
    }

    public void setTestDptId(int testDptId) {
        this.testDptId = testDptId;
    }

    public String getTestSolutionFile() {
        return testSolutionFile;
    }

    public void setTestSolutionFile(String testSolutionFile) {
        this.testSolutionFile = testSolutionFile;
    }

    public String getTestDatetime() {
        return testDatetime;
    }

    public void setTestDatetime(String testDatetime) {
        this.testDatetime = testDatetime;
    }

    public int getTestTotalMarks() {
        return testTotalMarks;
    }

    public void setTestTotalMarks(int testTotalMarks) {
        this.testTotalMarks = testTotalMarks;
    }

    public int getProductInStock() {
        return productInStock;
    }

    public void setProductInStock(int productInStock) {
        this.productInStock = productInStock;
    }

    public int getTestMarks() {
        return testMarks;
    }

    public void setTestMarks(int testMarks) {
        this.testMarks = testMarks;
    }

    public int getTestSeriesId() {
        return testSeriesId;
    }

    public void setTestSeriesId(int testSeriesId) {
        this.testSeriesId = testSeriesId;
    }

    public String getTestTitle() {
        return testTitle;
    }

    public void setTestTitle(String testTitle) {
        this.testTitle = testTitle;
    }

    public int getTestTsId() {
        return testTsId;
    }

    public void setTestTsId(int testTsId) {
        this.testTsId = testTsId;
    }

    public int getTestSectionType() {
        return testSectionType;
    }

    public void setTestSectionType(int testSectionType) {
        this.testSectionType = testSectionType;
    }

    public int getTestEtId() {
        return testEtId;
    }

    public void setTestEtId(int testEtId) {
        this.testEtId = testEtId;
    }

    public String getTestPaperFile() {
        return testPaperFile;
    }

    public void setTestPaperFile(String testPaperFile) {
        this.testPaperFile = testPaperFile;
    }

    public String getTestSubQuestion() {
        return testSubQuestion;
    }

    public void setTestSubQuestion(String testSubQuestion) {
        this.testSubQuestion = testSubQuestion;
    }

    public int getTestSectionalStatus() {
        return testSectionalStatus;
    }

    public void setTestSectionalStatus(int testSectionalStatus) {
        this.testSectionalStatus = testSectionalStatus;
    }

    public int getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(int testStatus) {
        this.testStatus = testStatus;
    }

    public int getTestTotalQuestion() {
        return testTotalQuestion;
    }

    public void setTestTotalQuestion(int testTotalQuestion) {
        this.testTotalQuestion = testTotalQuestion;
    }

    public String getTestStartDate() {
        return testStartDate;
    }

    public void setTestStartDate(String testStartDate) {
        this.testStartDate = testStartDate;
    }

    public String getOtTestSolutionDatetime() {
        return otTestSolutionDatetime;
    }

    public void setOtTestSolutionDatetime(String otTestSolutionDatetime) {
        this.otTestSolutionDatetime = otTestSolutionDatetime;
    }

    public String getTestSubjectStopeTime() {
        return testSubjectStopeTime;
    }

    public void setTestSubjectStopeTime(String testSubjectStopeTime) {
        this.testSubjectStopeTime = testSubjectStopeTime;
    }

    public int getTestLiveStatus() {
        return testLiveStatus;
    }

    public void setTestLiveStatus(int testLiveStatus) {
        this.testLiveStatus = testLiveStatus;
    }

    public String getTestSubId() {
        return testSubId;
    }

    public void setTestSubId(String testSubId) {
        this.testSubId = testSubId;
    }

    public String getTestCommingDate() {
        return testCommingDate;
    }

    public void setTestCommingDate(String testCommingDate) {
        this.testCommingDate = testCommingDate;
    }

    public int getTestType() {
        return testType;
    }

    public void setTestType(int testType) {
        this.testType = testType;
    }

    public String getTestEndDate() {
        return testEndDate;
    }

    public void setTestEndDate(String testEndDate) {
        this.testEndDate = testEndDate;
    }

    public double getTestNegativeMarks() {
        return testNegativeMarks;
    }

    public void setTestNegativeMarks(double testNegativeMarks) {
        this.testNegativeMarks = testNegativeMarks;
    }

    public String getTestSyllabusInfo() {
        return testSyllabusInfo;
    }

    public void setTestSyllabusInfo(String testSyllabusInfo) {
        this.testSyllabusInfo = testSyllabusInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProductPublishDate() {
        return productPublishDate;
    }

    public void setProductPublishDate(String productPublishDate) {
        this.productPublishDate = productPublishDate;
    }

    public int getTestNumber() {
        return testNumber;
    }

    public void setTestNumber(int testNumber) {
        this.testNumber = testNumber;
    }

    public List<OtQuestionItem> getAll_quesion() {
        return all_quesion;
    }

    public void setAll_quesion(List<OtQuestionItem> all_quesion) {
        this.all_quesion = all_quesion;
    }

    public List<OtUserTestSubjectViewItem> getSub_data() {
        return sub_data;
    }

    public void setSub_data(List<OtUserTestSubjectViewItem> sub_data) {
        this.sub_data = sub_data;
    }

    public RowUserSectionTest getUstid() {
        return ustid;
    }

    public void setUstid(RowUserSectionTest ustid) {
        this.ustid = ustid;
    }
}
