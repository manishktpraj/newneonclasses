package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamModel {
    @SerializedName("rowUserSectionTest")
    private RowUserSectionTest rowUserSectionTest;

    @SerializedName("rowExamInfo")
    private List<String> RowExamInfo;

    @SerializedName("message")
    private String message;

    public ExamModel(RowUserSectionTest rowUserSectionTest, List<String> rowExamInfo, String message) {
        this.rowUserSectionTest = rowUserSectionTest;
        RowExamInfo = rowExamInfo;
        this.message = message;
    }

    public RowUserSectionTest getRowUserSectionTest() {
        return rowUserSectionTest;
    }

    public void setRowUserSectionTest(RowUserSectionTest rowUserSectionTest) {
        this.rowUserSectionTest = rowUserSectionTest;
    }

    public List<String> getRowExamInfo() {
        return RowExamInfo;
    }

    public void setRowExamInfo(List<String> rowExamInfo) {
        RowExamInfo = rowExamInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
