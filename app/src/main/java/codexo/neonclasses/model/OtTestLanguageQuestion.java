package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

public class OtTestLanguageQuestion {

	@SerializedName("tlq_lang_id")
	private String tlqLangId;

	@SerializedName("tlq_option4_attachments_hindi")
	private String tlqOption4AttachmentsHindi;

	@SerializedName("tlq_option1_attachments_hindi")
	private String tlqOption1AttachmentsHindi;

	@SerializedName("tlq_question_text_hindi")
	private String tlqQuestionTextHindi;

	@SerializedName("tlq_question_type_hindi")
	private boolean tlqQuestionTypeHindi;

	@SerializedName("tlq_hindi_instruction")
	private String tlqHindiInstruction;

	@SerializedName("tlq_hindi_solution")
	private String tlqHindiSolution;

	@SerializedName("tlq_english_solution_attach")
	private String tlqEnglishSolutionAttach;

	@SerializedName("tlq_option2_attachments_hindi")
	private String tlqOption2AttachmentsHindi;

	@SerializedName("tlq_english_solution")
	private String tlqEnglishSolution;

	@SerializedName("tlq_user_id")
	private int tlqUserId;

	@SerializedName("tlq_option2_attachments")
	private String tlqOption2Attachments;

	@SerializedName("tlq_sub_id")
	private int tlqSubId;

	@SerializedName("tlq_option5_attachments_hindi")
	private String tlqOption5AttachmentsHindi;

	@SerializedName("tlq_question_attachments")
	private String tlqQuestionAttachments;

	@SerializedName("tlq_question_status")
	private boolean tlqQuestionStatus;

	@SerializedName("tlq_hindi_solution_attach")
	private String tlqHindiSolutionAttach;

	@SerializedName("tlq_option1_attachments")
	private String tlqOption1Attachments;

	@SerializedName("tlq_question_text")
	private String tlqQuestionText;

	@SerializedName("tlq_option_hindi_attach")
	private String tlqOptionHindiAttach;

	@SerializedName("tlq_option_attach")
	private String tlqOptionAttach;

	@SerializedName("tlq_option3_attachments")
	private String tlqOption3Attachments;

	@SerializedName("tlq_option3_attachments_hindi")
	private String tlqOption3AttachmentsHindi;

	@SerializedName("tlq_option5_attachments")
	private String tlqOption5Attachments;

	@SerializedName("tlq_unique_id")
	private String tlqUniqueId;

	@SerializedName("tlq_question_type")
	private int tlqQuestionType;

	@SerializedName("tlq_option4_attachments")
	private String tlqOption4Attachments;

	@SerializedName("tlq_question_id")
	private String tlqQuestionId;

	@SerializedName("tlq_options_hindi")
	private String tlqOptionsHindi;

	@SerializedName("tlq_id")
	private int tlqId;

	@SerializedName("tlq_english_instruction")
	private String tlqEnglishInstruction;

	@SerializedName("tlq_question_attachments_hindi")
	private String tlqQuestionAttachmentsHindi;

	@SerializedName("tlq_correct_option")
	private int tlqCorrectOption;

	@SerializedName("tlq_options")
	private String tlqOptions;

	@SerializedName("tlq_question_order")
	private int tlqQuestionOrder;

	public void setTlqLangId(String tlqLangId){
		this.tlqLangId = tlqLangId;
	}

	public String getTlqLangId(){
		return tlqLangId;
	}

	public void setTlqOption4AttachmentsHindi(String tlqOption4AttachmentsHindi){
		this.tlqOption4AttachmentsHindi = tlqOption4AttachmentsHindi;
	}

	public String getTlqOption4AttachmentsHindi(){
		return tlqOption4AttachmentsHindi;
	}

	public void setTlqOption1AttachmentsHindi(String tlqOption1AttachmentsHindi){
		this.tlqOption1AttachmentsHindi = tlqOption1AttachmentsHindi;
	}

	public String getTlqOption1AttachmentsHindi(){
		return tlqOption1AttachmentsHindi;
	}

	public void setTlqQuestionTextHindi(String tlqQuestionTextHindi){
		this.tlqQuestionTextHindi = tlqQuestionTextHindi;
	}

	public String getTlqQuestionTextHindi(){
		return tlqQuestionTextHindi;
	}

	public void setTlqQuestionTypeHindi(boolean tlqQuestionTypeHindi){
		this.tlqQuestionTypeHindi = tlqQuestionTypeHindi;
	}

	public boolean isTlqQuestionTypeHindi(){
		return tlqQuestionTypeHindi;
	}

	public void setTlqHindiInstruction(String tlqHindiInstruction){
		this.tlqHindiInstruction = tlqHindiInstruction;
	}

	public String getTlqHindiInstruction(){
		return tlqHindiInstruction;
	}

	public void setTlqHindiSolution(String tlqHindiSolution){
		this.tlqHindiSolution = tlqHindiSolution;
	}

	public String getTlqHindiSolution(){
		return tlqHindiSolution;
	}

	public void setTlqEnglishSolutionAttach(String tlqEnglishSolutionAttach){
		this.tlqEnglishSolutionAttach = tlqEnglishSolutionAttach;
	}

	public String getTlqEnglishSolutionAttach(){
		return tlqEnglishSolutionAttach;
	}

	public void setTlqOption2AttachmentsHindi(String tlqOption2AttachmentsHindi){
		this.tlqOption2AttachmentsHindi = tlqOption2AttachmentsHindi;
	}

	public String getTlqOption2AttachmentsHindi(){
		return tlqOption2AttachmentsHindi;
	}

	public void setTlqEnglishSolution(String tlqEnglishSolution){
		this.tlqEnglishSolution = tlqEnglishSolution;
	}

	public String getTlqEnglishSolution(){
		return tlqEnglishSolution;
	}

	public void setTlqUserId(int tlqUserId){
		this.tlqUserId = tlqUserId;
	}

	public int getTlqUserId(){
		return tlqUserId;
	}

	public void setTlqOption2Attachments(String tlqOption2Attachments){
		this.tlqOption2Attachments = tlqOption2Attachments;
	}

	public String getTlqOption2Attachments(){
		return tlqOption2Attachments;
	}

	public void setTlqSubId(int tlqSubId){
		this.tlqSubId = tlqSubId;
	}

	public int getTlqSubId(){
		return tlqSubId;
	}

	public void setTlqOption5AttachmentsHindi(String tlqOption5AttachmentsHindi){
		this.tlqOption5AttachmentsHindi = tlqOption5AttachmentsHindi;
	}

	public String getTlqOption5AttachmentsHindi(){
		return tlqOption5AttachmentsHindi;
	}

	public void setTlqQuestionAttachments(String tlqQuestionAttachments){
		this.tlqQuestionAttachments = tlqQuestionAttachments;
	}

	public String getTlqQuestionAttachments(){
		return tlqQuestionAttachments;
	}

	public void setTlqQuestionStatus(boolean tlqQuestionStatus){
		this.tlqQuestionStatus = tlqQuestionStatus;
	}

	public boolean isTlqQuestionStatus(){
		return tlqQuestionStatus;
	}

	public void setTlqHindiSolutionAttach(String tlqHindiSolutionAttach){
		this.tlqHindiSolutionAttach = tlqHindiSolutionAttach;
	}

	public String getTlqHindiSolutionAttach(){
		return tlqHindiSolutionAttach;
	}

	public void setTlqOption1Attachments(String tlqOption1Attachments){
		this.tlqOption1Attachments = tlqOption1Attachments;
	}

	public String getTlqOption1Attachments(){
		return tlqOption1Attachments;
	}

	public void setTlqQuestionText(String tlqQuestionText){
		this.tlqQuestionText = tlqQuestionText;
	}

	public String getTlqQuestionText(){
		return tlqQuestionText;
	}

	public void setTlqOptionHindiAttach(String tlqOptionHindiAttach){
		this.tlqOptionHindiAttach = tlqOptionHindiAttach;
	}

	public String getTlqOptionHindiAttach(){
		return tlqOptionHindiAttach;
	}

	public void setTlqOptionAttach(String tlqOptionAttach){
		this.tlqOptionAttach = tlqOptionAttach;
	}

	public String getTlqOptionAttach(){
		return tlqOptionAttach;
	}

	public void setTlqOption3Attachments(String tlqOption3Attachments){
		this.tlqOption3Attachments = tlqOption3Attachments;
	}

	public String getTlqOption3Attachments(){
		return tlqOption3Attachments;
	}

	public void setTlqOption3AttachmentsHindi(String tlqOption3AttachmentsHindi){
		this.tlqOption3AttachmentsHindi = tlqOption3AttachmentsHindi;
	}

	public String getTlqOption3AttachmentsHindi(){
		return tlqOption3AttachmentsHindi;
	}

	public void setTlqOption5Attachments(String tlqOption5Attachments){
		this.tlqOption5Attachments = tlqOption5Attachments;
	}

	public String getTlqOption5Attachments(){
		return tlqOption5Attachments;
	}

	public void setTlqUniqueId(String tlqUniqueId){
		this.tlqUniqueId = tlqUniqueId;
	}

	public String getTlqUniqueId(){
		return tlqUniqueId;
	}

	public void setTlqQuestionType(int tlqQuestionType){
		this.tlqQuestionType = tlqQuestionType;
	}

	public int getTlqQuestionType(){
		return tlqQuestionType;
	}

	public void setTlqOption4Attachments(String tlqOption4Attachments){
		this.tlqOption4Attachments = tlqOption4Attachments;
	}

	public String getTlqOption4Attachments(){
		return tlqOption4Attachments;
	}

	public void setTlqQuestionId(String tlqQuestionId){
		this.tlqQuestionId = tlqQuestionId;
	}

	public String getTlqQuestionId(){
		return tlqQuestionId;
	}

	public void setTlqOptionsHindi(String tlqOptionsHindi){
		this.tlqOptionsHindi = tlqOptionsHindi;
	}

	public String getTlqOptionsHindi(){
		return tlqOptionsHindi;
	}

	public void setTlqId(int tlqId){
		this.tlqId = tlqId;
	}

	public int getTlqId(){
		return tlqId;
	}

	public void setTlqEnglishInstruction(String tlqEnglishInstruction){
		this.tlqEnglishInstruction = tlqEnglishInstruction;
	}

	public String getTlqEnglishInstruction(){
		return tlqEnglishInstruction;
	}

	public void setTlqQuestionAttachmentsHindi(String tlqQuestionAttachmentsHindi){
		this.tlqQuestionAttachmentsHindi = tlqQuestionAttachmentsHindi;
	}

	public String getTlqQuestionAttachmentsHindi(){
		return tlqQuestionAttachmentsHindi;
	}

	public void setTlqCorrectOption(int tlqCorrectOption){
		this.tlqCorrectOption = tlqCorrectOption;
	}

	public int getTlqCorrectOption(){
		return tlqCorrectOption;
	}

	public void setTlqOptions(String tlqOptions){
		this.tlqOptions = tlqOptions;
	}

	public String getTlqOptions(){
		return tlqOptions;
	}

	public void setTlqQuestionOrder(int tlqQuestionOrder){
		this.tlqQuestionOrder = tlqQuestionOrder;
	}

	public int getTlqQuestionOrder(){
		return tlqQuestionOrder;
	}
}