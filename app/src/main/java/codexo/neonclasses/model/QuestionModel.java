package codexo.neonclasses.model;

public class QuestionModel {
    public int tlq_id;
    public int tlq_counterid;
    public int que_test_id;
    public int tlq_question_id;
    public String tlq_unique_id;
    public int tlq_question_type;
    public String tlq_options;
    public String tlq_option1_attachments;
    public String tlq_option2_attachments;
    public String tlq_option3_attachments;
    public String tlq_option4_attachments;
    public String tlq_option5_attachments;
    public String tlq_lang_id;
    public String tlq_question_text;
    public String tlq_question_text_hindi;
    public String tlq_options_hindi;
    public String tlq_question_attachments;
    public String tlq_option1_attachments_hindi;
    public String tlq_option2_attachments_hindi;
    public String tlq_option3_attachments_hindi;
    public String tlq_option4_attachments_hindi;
    public String tlq_option5_attachments_hindi;
    public boolean tlq_question_type_hindi;
    public String tlq_question_attachments_hindi;
    public String tlq_english_instruction;
    public String tlq_hindi_instruction;
    public String tlq_hindi_solution;
    public String tlq_english_solution;
    public String tlq_hindi_solution_attach;
    public String tlq_english_solution_attach;
    public String tlq_option_attach;
    public boolean tlq_question_status;
    public int tlq_question_order;
    public int tlq_user_id;
    public String tlq_option_hindi_attach;
    public int tlq_sub_id;
    public int tlq_correct_option;
    public int UserSelectedAns;
    public int user_que_status=0;
    public boolean user_que_review;
    public int test_id;
}
