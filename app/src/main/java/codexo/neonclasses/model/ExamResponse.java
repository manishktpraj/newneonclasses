package codexo.neonclasses.model;

import com.google.gson.annotations.SerializedName;

public class ExamResponse {

	@SerializedName("rowUserSectionTest")
	private RowUserSectionTest rowUserSectionTest;

	@SerializedName("rowExamInfo")
	private RowExamInfo rowExamInfo;

	@SerializedName("message")
	private String message;

	@SerializedName("student_id")
	private String student_id;

	@SerializedName("test_id")
	private String test_id;

	public void setRowUserSectionTest(RowUserSectionTest rowUserSectionTest){
		this.rowUserSectionTest = rowUserSectionTest;
	}

	public RowUserSectionTest getRowUserSectionTest(){
		return rowUserSectionTest;
	}

	public void setRowExamInfo(RowExamInfo rowExamInfo){
		this.rowExamInfo = rowExamInfo;
	}

	public RowExamInfo getRowExamInfo(){
		return rowExamInfo;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}