package codexo.neonclasses;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.gpyoutubeplayer.Player3;

public class Intro extends AppCompatActivity {
    TextView welcomtoneon, welcomdesc, loginbutton, registerbutton;
    AppCompatActivity activity;
    String[] permissions = new String[]{
            android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_SECURE_SETTINGS,
            Manifest.permission.ACCESS_FINE_LOCATION};
    String osName = "";
    public static final int MULTIPLE_PERMISSIONS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        activity = Intro.this;
        Constant.adbEnabled(Intro.this);
        Constant.checkAdb(Intro.this);
        welcomtoneon = findViewById(R.id.welcomtoneon);
        welcomdesc = findViewById(R.id.welcomdesc);
        loginbutton = findViewById(R.id.loginbutton);
        registerbutton = findViewById(R.id.registerbutton);
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
        welcomdesc.setTypeface(Constant.getFonts(getApplicationContext()));
        loginbutton.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        registerbutton.setTypeface(Constant.getsemiFonts(getApplicationContext()));

        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissions();
        }
        Constant.device_info = Build.VERSION.BASE_OS + " " + android.os.Build.VERSION.RELEASE + " " + android.os.Build.BRAND;
        Constant.device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        ///fullScreen();

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(activity, Login.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(iLogin);
                finish();
            }
        });

        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(activity, Register.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(iLogin);
                finish();
            }
        });
    }

    private void checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(Intro.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Intro.this);
        Constant.checkAdb(Intro.this);
    }
}