package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class Loginviaotp extends AppCompatActivity {
    AppCompatActivity activity;
    TextView welcomtoneon,welcomdesc,loginbutton,registerbutton,forgotpass,loginviaotp;
    ImageView passwordeye;
     TextInputLayout username;
    TextInputEditText username_edit;
    String username_string;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginviaotp);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Loginviaotp.this;
        Constant.setToolbar(activity, toolbar);
         welcomtoneon =findViewById(R.id.welcomtoneon);
        welcomdesc =findViewById(R.id.welcomdesc);
        loginbutton =findViewById(R.id.loginbutton);
         loginviaotp=findViewById(R.id.loginviaotp);
        registerbutton=findViewById(R.id.registerbutton);
        Constant.adbEnabled(Loginviaotp.this);
        Constant.checkAdb(Loginviaotp.this);
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
        welcomdesc.setTypeface(Constant.getFonts(getApplicationContext()));
        loginbutton.setTypeface(Constant.getFonts(getApplicationContext()));
         loginviaotp.setTypeface(Constant.getFontsBold(getApplicationContext()));
        registerbutton.setTypeface(Constant.getFontsBold(getApplicationContext()));

        pDialog = Constant.getProgressBar(Loginviaotp.this);

        username= findViewById(R.id.username);
        username_edit =findViewById(R.id.username_edit);


        username_edit.setTypeface(Constant.getFonts(getApplicationContext()));
        username_edit.setEnabled(true);
        username_edit.setTextIsSelectable(true);
        username_edit.setFocusable(true);
        username_edit.setFocusableInTouchMode(true);
        username_edit.requestFocus();



        loginviaotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent iLogin = new Intent(getApplicationContext(), Login.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin); 


             }
        });
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String go_next = "yes";
                username_string = username_edit.getText().toString();
                if(username_string.equals(""))
                {
                    username_edit.setError("Please Enter Mobile number");
                    go_next ="no";
                }
                if(username_string.length()<10)
                {
                    username_edit.setError("Please Enter Valid Mobile number");
                    go_next ="no";
                }
                if(go_next.equals("yes")) {
                    loginviaotp(username_string);
                }

            }
        });
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), Register.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
            }
        });
    }

    private void loginviaotp(final String username_string) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().loginviaotp(username_string);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                           JSONObject localJSONObject2 = jsonObject.getJSONObject("results");
                            Constant.setToast(Loginviaotp.this,notification);
                            Intent i = new Intent(activity, Otp.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("results",localJSONObject2+"");
                            i.putExtra("type","LOGINOTP");
                            activity.startActivity(i);
//                            finish();?
                        } else {
                            Constant.setToast(Loginviaotp.this,notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                loginviaotp(username_string);
            }
        });
    }
    @Override
    public void onBackPressed(){
        startActivity(new Intent(getApplicationContext(),Intro.class));
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Loginviaotp.this);
        Constant.checkAdb(Loginviaotp.this);
    }
}