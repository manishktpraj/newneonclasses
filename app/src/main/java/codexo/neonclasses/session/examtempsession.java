package codexo.neonclasses.session;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;

public class examtempsession {

    SharedPreferences pref, autocompletepref;
    SharedPreferences.Editor editor, autocompeditor;
    static Context _context;
    int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "EXAMPref";
    public static final String AUTO_PREF_NAME = "EXAMPref";
    public static final String CART_ARRAY_STRING = "CART_ARRAY_STRING";

    public static JSONArray CART_ARRAY = new JSONArray();
    private Activity activity;
    public static final String SHOWCASE = "showcase";
    @SuppressLint("CommitPrefEdits")
    public examtempsession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public examtempsession(Activity activity) {
        this.activity = activity;
        this._context = activity.getApplicationContext();
        pref = activity.getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public SharedPreferences.Editor getSessionEditor() {
        return editor;
    }

    public void setShowcase(String showcase) {
        editor.putString(SHOWCASE, showcase).commit();
    }



    public String getexamsessionjsonarray() {
        return pref.getString(CART_ARRAY_STRING, null);
    }


    public void removeexamsessionitems(JSONArray cartarrray,String id,String type) throws JSONException {
        JSONArray newcart =new JSONArray();
        for(Integer i=0;i<cartarrray.length();i++)
        {
            JSONObject ned = cartarrray.getJSONObject(i);
            if(ned.getString("cart_id").equals(id) && ned.getString("cart_type").equals(type))
            {

            }else{
                newcart.put(ned);
            }

        }
        createExamSession(newcart);
    }
    public Boolean isInExamSession(String id,String type) throws JSONException {
        Boolean status =false;
        for(Integer i=0;i<CART_ARRAY.length();i++)
        {
            JSONObject ned = CART_ARRAY.getJSONObject(i);
            if(ned.getString("cart_id").equals(id) && ned.getString("cart_type").equals(type))
            {
                status =true;
            }
        }
        return status;
    }

    public void createExamSession(JSONArray responseJSONObject) {
        try {
            CART_ARRAY =responseJSONObject;
            editor.putString(CART_ARRAY_STRING,  CART_ARRAY+"");
            editor.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    public void clearcart() {
        editor.clear();
        editor.commit();
        Constant.fragments="main";
        /*Intent i1 = new Intent(_context, MainActivity.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i1);*/
    }

}
