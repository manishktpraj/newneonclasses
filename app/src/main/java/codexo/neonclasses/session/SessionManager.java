package codexo.neonclasses.session;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import codexo.neonclasses.Constant;
import codexo.neonclasses.Intro;
import codexo.neonclasses.Login;
import codexo.neonclasses.MainActivity;

public class SessionManager {
    SharedPreferences pref, autocompletepref;
    SharedPreferences.Editor editor, autocompeditor;
    static Context _context;
    int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "BQCSPref";
    public static final String AUTO_PREF_NAME = "BQCSPref";
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_SHARE_CODE = "student_registration_id";
    public static final String SHOWCASE = "showcase";
    private Activity activity;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public SessionManager(Activity activity) {
        this.activity = activity;
        this._context = activity.getApplicationContext();
        pref = activity.getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public SharedPreferences.Editor getSessionEditor() {
        return editor;
    }

    public void setShowcase(String showcase) {
        editor.putString(SHOWCASE, showcase).commit();
    }

    public String getIsLogin() {
        return pref.getString(IS_LOGIN, null);
    }

    public String getUserId() {
        return pref.getString(USER_ID, null);
    }

    public String getUserName() {
        return pref.getString(USER_NAME, null);
    }

    public String getFirstName() {
        return pref.getString(FIRST_NAME, null);
    }

    public String getLastName() {
        return pref.getString(LAST_NAME, null);
    }

    public String getUserPhone() {
        return pref.getString(USER_PHONE, null);
    }

    public String getUserEmail() {
        return pref.getString(USER_EMAIL, null);
    }
    public String getUserShareCode() {
        return pref.getString(USER_SHARE_CODE, null);
    }

    public void createLoginSession(JSONObject responseJSONObject) {
        try {
            String user_first_name = responseJSONObject.getString("student_first_name");
            String user_last_name = responseJSONObject.getString("student_last_name");
            String user_phone = responseJSONObject.getString("student_phone");
            String user_email = responseJSONObject.getString("student_email");
            String user_id = responseJSONObject.getString("student_id");
            String registration_id = responseJSONObject.getString("student_registration_id");

            editor.putBoolean(IS_LOGIN, true);
            editor.putString(USER_ID, user_id);
            String user_name = user_first_name + " " + user_last_name;
            if (user_name.equals(" ")) {
                editor.putString(USER_NAME, "Hello Buddy");
                editor.putString(FIRST_NAME, "");
                editor.putString(LAST_NAME, "");
            } else {
                editor.putString(USER_NAME, user_name);
                editor.putString(FIRST_NAME, user_first_name);
                editor.putString(LAST_NAME, user_last_name);
            }
            editor.putString(USER_PHONE, user_phone);
            editor.putString(USER_EMAIL, user_email);
            editor.putString(USER_SHARE_CODE, registration_id);
            editor.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
/*
    public void updatePackageDetails(final HashMap<String, String> updateDataHashMap){
        editor.putString(PACKAGE_NAME, updateDataHashMap.get(PACKAGE_NAME));
        editor.putString(PACKAGE_ID, updateDataHashMap.get(PACKAGE_ID));
        editor.putString(PACKAGE_REGULAR, updateDataHashMap.get(PACKAGE_REGULAR));
        editor.putString(PACKAGE_DISCOUNT, updateDataHashMap.get(PACKAGE_DISCOUNT));
        editor.putString(PACKAGE_STATUS, updateDataHashMap.get(PACKAGE_STATUS));
        editor.putString(PACKAGE_COLOR, updateDataHashMap.get(PACKAGE_COLOR));
        editor.commit();
    }*/

    public void updateUserDetails(final HashMap<String, String> updateDataHashMap) {
        editor.putString(USER_NAME, updateDataHashMap.get(USER_NAME));
        editor.putString(FIRST_NAME, updateDataHashMap.get(FIRST_NAME));
        editor.putString(LAST_NAME, updateDataHashMap.get(LAST_NAME));
        editor.putString(USER_PHONE, updateDataHashMap.get(USER_PHONE));
        editor.putString(USER_EMAIL, updateDataHashMap.get(USER_EMAIL));
        editor.commit();
    }

    public void checkLogin() {

        if (isLoggedIn()) {
            Intent iLogin = new Intent(_context, MainActivity.class);
            iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(iLogin);
        } else {
            if (Constant.skip.equals("no")) {
                Intent i = null;
                i = new Intent(_context, Intro.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                _context.startActivity(i);
            } else {
                Intent iLogin = new Intent(_context, Intro.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                _context.startActivity(iLogin);
            }
        }
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
        Constant.fragments="main";
        Intent i1 = new Intent(_context, Intro.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i1);

    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
