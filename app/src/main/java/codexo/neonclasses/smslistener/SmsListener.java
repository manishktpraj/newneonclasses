package codexo.neonclasses.smslistener;

/**
 * Created by admin on 25-05-2017.
 */
public interface SmsListener {
    public void messageReceived(String messageText, String type);
}