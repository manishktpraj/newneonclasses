package codexo.neonclasses.smslistener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by admin on 25-05-2017.
 */
public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener mListener;
    private static String type_;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();

//        Log.d("data",data+"     data");

        Object[] pdus = (Object[]) data.get("pdus");

        try {

            for (int i = 0; i < pdus.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

                String sender = smsMessage.getDisplayOriginatingAddress();
                //You must check here if the sender is your provider and not another one with same text.

                String messageBody = smsMessage.getMessageBody();

                //Pass on the text to our listener.
                mListener.messageReceived(messageBody,type_);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void bindListener(SmsListener listener,String type) {
        mListener = listener;
        type_=type;
    }
}
