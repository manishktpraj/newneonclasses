package codexo.neonclasses.gpexoplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityNewVideoPlayerBinding;
import codexo.neonclasses.databinding.VideoPxItemLayoutBinding;
import codexo.neonclasses.gpyoutubeplayer.Player3;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.player.PlayerActivity;
import codexo.neonclasses.ui.videos.CategoryVideosActivity;
import codexo.neonclasses.ui.youtube.YoutubeActivity2;

public class NewVideoPlayer extends AppCompatActivity {
    ActivityNewVideoPlayerBinding binding;
    PlayerView playerView;
    SimpleExoPlayer player;
    boolean playWhenReady = true;
    int currentWindow = 0;
    long playbackPosition = 0;
    ProgressDialog pDialog;
    JSONArray viddata = new JSONArray();
    JSONArray audioData = new JSONArray();
    AlertDialog dialog;
    Button playback, qualitybtn;
    private Float speed = 1f;
    private int mCurrentWindow = 0;
    long playerPosition = 0;
    private long mPlaybackPosition = 0;
    private String YOUTUBE_VIDEO_ID = "";
    private String BASE_URL = "https://www.youtube.com";
    private String mYoutubeLink = "";
    private View decorView;
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_SPEED = "playbackspeed";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private static final String STATE_RESUME_POSITION = "resume";

    private TrackGroupArray lastSeenTrackGroupArray;
    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    RelativeLayout video_quality_layout;
    ImageView btFullScreen, img_back, settings_layout;

    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    SessionManager session;
    String audURL = "";
    JSONObject video_detail = new JSONObject();

    ArrayList<MergingMediaSource> mergeMediaList = new ArrayList<>();
    ArrayList<Integer> audioBit = new ArrayList<>();
    //Minimum Video you want to buffer while Playing
    private final int MIN_BUFFER_DURATION = 2000;
    //Max Video you want to buffer during PlayBack
    private final int MAX_BUFFER_DURATION = 3000;
    //Min Video you want to buffer before start Playing it
    private final int MIN_PLAYBACK_START_BUFFER = 1000;
    //Min video You want to buffer when user resumes video
    private final int MIN_PLAYBACK_RESUME_BUFFER = 2000;

    // Firebase Chat Stat
    private FirebaseDatabase mDatabase;
    DatabaseReference messagesRef;
    RecyclerView recycler_view;
    ImageView sendchat;
    EditText typemessage;
    private ValueEventListener mSearchedLocationReferenceListener;
    JSONArray arrayListchat = new JSONArray();
    private static LinearLayoutManager mLayoutManager;

    // Firebase Chat End

    @Override
    protected void onStart() {
        super.onStart();
        if (Util.SDK_INT >= 24) {
            initPlayer();
        }
    }

    @Override
    protected void onStop() {
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
        super.onStop();

    }

    private void releasePlayer() {
        if (player != null) {
            playWhenReady = player.getPlayWhenReady();
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            player.release();
            player = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Util.SDK_INT < 24 || player == null) {
            initPlayer();
        }
    }

    private void hideSystemUI() {
        playerView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE |
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }

    @Override
    protected void onPause() {
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        binding = ActivityNewVideoPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        session = new SessionManager(getApplicationContext());
        playerView = (PlayerView) findViewById(R.id.video_view);
        pDialog = Constant.getProgressBar(NewVideoPlayer.this);
        pDialog.show();
        settings_layout = findViewById(R.id.speedlayout);
        btFullScreen = findViewById(R.id.exoMinimalFullscreen);
        img_back = findViewById(R.id.img_back);
        try {
            video_detail = new JSONObject(getIntent().getStringExtra("video_detail"));
            YOUTUBE_VIDEO_ID = extractVideoIdFromUrl(video_detail.getString("vid_url"));
            binding.txtTitle.setText(video_detail.getString("vid_title"));
            String img = video_detail.getString("vid_image");
            Glide.with(NewVideoPlayer.this).load(img).into(binding.vidIconImg);
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
            mYoutubeLink = BASE_URL + "/watch?v=" + YOUTUBE_VIDEO_ID;
            Constant.logPrint("dkdddkdkdd", mYoutubeLink);

            String MESSAGE_CHANNEL = "/neonmain/" + video_detail.getString("vid_id") + "/";
//            Constant.logPrint(MESSAGE_CHANNEL,"MESSAGE_CHANNEL");
            mDatabase = FirebaseDatabase.getInstance();
            messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        binding.username.setText(session.getUserShareCode());

        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
            mCurrentWindow = savedInstanceState.getInt(KEY_WINDOW);
            mPlaybackPosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            speed = savedInstanceState.getFloat(KEY_SPEED);
        } else {
            DefaultTrackSelector.ParametersBuilder builder =
                    new DefaultTrackSelector.ParametersBuilder(/* context= */ this);
            trackSelectorParameters = builder.build();
            clearStartPosition();
        }


        initPlayer();
        getYoutubeDownloadUrl(mYoutubeLink);


        settings_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), settings_layout);
                popupMenu.inflate(R.menu.speed);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        if (player != null) {

                            switch (menuItem.getItemId()) {
                                case R.id.x05: {
                                    if (player != null) {
                                        speed = 0.5f;
                                        player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                    }
                                    break;
                                }
                                case R.id.normal: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1f));
                                        speed = 1f;
                                    }
                                    break;
                                }
                                case R.id.x125: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.25f));
                                        speed = 1.25f;
                                    }
                                    break;
                                }
                                case R.id.x150: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.50f));
                                        speed = 1.50f;
                                    }
                                    break;
                                }
                                case R.id.x175: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.75f));
                                        speed = 1.60f;
                                    }
                                    break;
                                }
                                case R.id.x2: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(2f));
                                        speed = 1.70f;
                                    }
                                    break;
                                }

                            }

                            // Showing the popup menu


                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    player.release();
                    finish();
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    ViewGroup.LayoutParams parms = binding.videoView.getLayoutParams();
                    parms.height = (int) getResources().getDimension(R.dimen.height1); // LayoutParams: android.view.ViewGroup.LayoutParams
                    ((ViewGroup.MarginLayoutParams) parms).topMargin = (int) getResources().getDimension(R.dimen.margin_top_prt);
                    binding.videoView.requestLayout();
                }
            }
        });

        btFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    ViewGroup.LayoutParams parms = binding.videoView.getLayoutParams();
                    parms.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    parms.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                    binding.videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                    binding.videoView.requestLayout();
                    binding.relChat.setVisibility(View.GONE);
                    hideSystemUI();
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    ViewGroup.LayoutParams parms = binding.videoView.getLayoutParams();
                    parms.height = (int) getResources().getDimension(R.dimen.height1); // LayoutParams: android.view.ViewGroup.LayoutParams
                    ((ViewGroup.MarginLayoutParams) parms).topMargin = (int) getResources().getDimension(R.dimen.margin_top_prt);
                    binding.videoView.requestLayout();
                    binding.relChat.setVisibility(View.VISIBLE);
                }
            }
        });


        recycler_view = findViewById(R.id.recycler_view);
        sendchat = findViewById(R.id.img_send);
        typemessage = findViewById(R.id.edit_chat_box);
        sendchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = typemessage.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    typemessage.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();

                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("video_chat_reply", "");
                    formData.put("video_reply_time", "");
                    formData.put("videochat_created", formattedDate + "");
                    formData.put("videochat_message", username_string);
//                    Constant.logPrint(formData+"","dddddddddddddddd");
//                    Constant.logPrint(daedat+"","daedatdaedatdaedatdaedatdaedat"+formData+"");
                    // messagesRef.child(daedat+"").setValue(formData);
                    messagesRef.push().setValue(formData);
                    binding.editChatBox.setText("");
                    Toast.makeText(NewVideoPlayer.this, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                }
            }
        });
        Activity activity = NewVideoPlayer.this;
        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
//                    Log.d("Locations updated", "location: "+vid_id + location);

                    if (locationSnapshot != null) {
                        try {
                            JSONObject objd = new JSONObject();
                            objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                            objd.put("videochat_created", locationSnapshot.child("videochat_created").getValue().toString());
                            objd.put("videochat_message", locationSnapshot.child("videochat_message").getValue().toString());
                            objd.put("video_chat_reply", locationSnapshot.child("video_chat_reply").getValue().toString());
                            objd.put("video_reply_time", locationSnapshot.child("video_reply_time").getValue().toString());
                            objd.put("doubts_user_email", locationSnapshot.child("doubts_user_email").getValue().toString());
                            objd.put("doubts_user_name", locationSnapshot.child("doubts_user_name").getValue().toString());
                            objd.put("doubts_user_phone", locationSnapshot.child("doubts_user_phone").getValue().toString());

                            ///       Log.d("updatedupdatedupdatedupdatedupdated", "location: " +  objd);
                            if (objd.getString("user_id").equals(session.getUserId())) {
                                arrayListchat.put(objd);

                            }

                            if (!binding.recyclerView.equals(null)) {
                                final CustomAdapterCat adapter = new CustomAdapterCat(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                binding.recyclerView.setLayoutManager(mLayoutManager);
                                binding.recyclerView.setHasFixedSize(true);
                                binding.recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    protected void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.relChat.setVisibility(View.GONE);
            onFullscreen(true);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            onFullscreen(false);

        }
    }

    public void onFullscreen(boolean fullscreen) {
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        releasePlayer();
        clearStartPosition();
        setIntent(intent);
    }

    public String extractVideoIdFromUrl(String url) {
        String vId = null;
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);
        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    private void initPlayer() {

        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs(MIN_BUFFER_DURATION,
                        MAX_BUFFER_DURATION,
                        MIN_PLAYBACK_START_BUFFER,
                        MIN_PLAYBACK_RESUME_BUFFER)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

        player = new SimpleExoPlayer.Builder(this)
                .setLoadControl(loadControl)
                .build();
        playerView.setPlayer(player);

    }

    private void getYoutubeDownloadUrl(String youtubeLink) {
        try {
            new YouTubeExtractor(this) {

                @Override
                public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                    int audioTag = 140;

                    try {
                        Boolean isLive = vMeta.isLiveStream();
                        if (!isLive){
                            getUrls(ytFiles);

                        }else {
                            getUrls(ytFiles);
//                            Constant.setToast(getApplicationContext(),"Please try Player 1 for this video");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Intent videoplay = new Intent(getApplicationContext(), YoutubeActivity2.class);
//                                            Intent videoplay = new Intent(getApplicationContext(), PlayerActivity.class);
                        videoplay.putExtra("video_detail", video_detail+"");
                        startActivity(videoplay);
                        pDialog.dismiss();
//                        Constant.setToast(getApplicationContext(),"Please try Player 1 for this video");
                        finish();
                    }


                }
            }.extract(youtubeLink);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void getUrls(SparseArray<YtFile> ytFiles){
        if (ytFiles == null) {
            // Something went wrong we got no urls. Always check this.
            finish();
            return;
        }


        // Iterate over itags
        for (int i = 0; i < ytFiles.size(); i++) {
            JSONObject vidObject = new JSONObject();
            JSONObject audioObject = new JSONObject();
            int itag = 0;
            itag = ytFiles.keyAt(i);


            // ytFile represents one file with its url and meta data
            YtFile ytFile = ytFiles.get(itag);
            if (ytFile.getFormat().getExt().equals("mp4") && ytFile.getFormat().getAudioBitrate() == -1) {
                MediaSource videoSource = new ProgressiveMediaSource
                        .Factory(new DefaultHttpDataSource.Factory())
                        .createMediaSource(MediaItem.fromUri(ytFiles.get(ytFile.getFormat().getItag()).getUrl()));

                try {
                    vidObject.put("vidItag", ytFile.getFormat().getItag());
                    vidObject.put("vidHeight", ytFile.getFormat().getHeight());
                    vidObject.put("videoSource", ytFiles.get(ytFile.getFormat().getItag()).getUrl());
                    viddata.put(vidObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (ytFile.getFormat().getAudioBitrate() != -1 && ytFile.getFormat().getHeight() == -1) {
                try {
                    audioObject.put("vidItag", ytFile.getFormat().getItag());
                    audioObject.put("vidHeight", ytFile.getFormat().getHeight());
                    audioObject.put("audioBitRate", ytFile.getFormat().getAudioBitrate());
                    audioObject.put("audioSource", ytFiles.get(ytFile.getFormat().getItag()).getUrl());
                    audioData.put(audioObject);
                    audioBit.add(ytFile.getFormat().getAudioBitrate());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // Just add videos in a decent format => height -1 = audio
                   /* if (ytFile.getFormat().getHeight() == -1 || ytFile.getFormat().getHeight() >= 144) {

                    }*/
        }


        addButtonToMainLayout(viddata);
        int indexOfMinimum = audioBit.indexOf(Collections.min(audioBit));
        int minAudio = audioBit.get(indexOfMinimum);
        for (int i = 0; i < audioData.length(); i++) {
            try {
                if (audioData.getJSONObject(i).getInt("audioBitRate") == minAudio) {
                    audURL = audioData.getJSONObject(i).getString("audioSource");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
//                getSmallest(audioBit);
        Log.d("viddataviddata", audioBit.toString() + "\n" + indexOfMinimum + "\n" + minAudio + "\n" + audURL);
    }


    private void addButtonToMainLayout(JSONArray list) {
        pDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_player_qulity_list_layout, null);
        builder.setView(customLayout);
        RecyclerView recycler;
        recycler = customLayout.findViewById(R.id.recyclerView);
        ImageView cancel_btn = customLayout.findViewById(R.id.cancel_btn);
        RecyclerAdapter2 recyclerAdapter = new RecyclerAdapter2(getApplicationContext(), list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(recyclerAdapter);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

    }

    private void mergePlayer(MediaSource audioSource, MediaSource videoSource) {
//        MediaSource mediaSource = null;
        Log.d("MediaSource123", audioSource + "\n" + audioSource + "");

        player.setMediaSource(new MergingMediaSource(
                        false,
                        videoSource,
                        audioSource),
                false
        );
        playerPosition = player.getContentPosition();
        if (playerPosition == 0) {
            playerPosition = 2000;
        } else {
            playerPosition = player.getContentPosition();
        }

        player.prepare();
        player.seekTo(playerPosition);
        player.setPlayWhenReady(playWhenReady);

    }

    private void mergePlayer1(MediaSource mediaSource) {
//        MediaSource mediaSource = null;
        Log.d("MediaSource123", mediaSource + "");

        playerPosition = player.getContentPosition();
        if (playerPosition == 0) {
            playerPosition = 2000;
        } else {
            playerPosition = player.getContentPosition();
        }


        player.prepare(mediaSource, false, false);
        player.seekTo(playerPosition);
        player.setPlayWhenReady(playWhenReady);
    }

    private void mergePlayer2(String mediaSource, String audio) {
        Log.d("MediaSource123", mediaSource + "");

        playerPosition = player.getContentPosition();
        if (playerPosition == 0) {
            playerPosition = 2000;
        } else {
            playerPosition = player.getContentPosition();
        }

        MediaItem mediaItem = MediaItem.fromUri(mediaSource);
        player.addMediaItem(mediaItem);
        player.prepare();
        player.seekTo(playerPosition);
        player.setPlayWhenReady(playWhenReady);
    }

    private void mergePlayer3(MergingMediaSource mediaItem) {
        player.release();
        binding.vidIconImg.setVisibility(View.GONE);
        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs(MIN_BUFFER_DURATION,
                        MAX_BUFFER_DURATION,
                        MIN_PLAYBACK_START_BUFFER,
                        MIN_PLAYBACK_RESUME_BUFFER)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

        player = new SimpleExoPlayer.Builder(this)
                .setLoadControl(loadControl)
                .build();
        playerView.setPlayer(player);


        playerPosition = player.getContentPosition();
        if (playerPosition == 0) {
            playerPosition = 2000;
        } else {
            playerPosition = player.getContentPosition();
        }

        player.prepare(mediaItem, false, false);
        player.seekTo(playerPosition);
        player.setPlayWhenReady(playWhenReady);
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
        VideoPxItemLayoutBinding binding_data;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding_data = VideoPxItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter2.RecyclerViewHolder holder = new RecyclerAdapter2.RecyclerViewHolder(binding_data);

            return holder;
        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter2.RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
            try {

                JSONObject item = listItem.getJSONObject(position);
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(NewVideoPlayer.this,
                        Util.getUserAgent(NewVideoPlayer.this, getString(R.string.app_name)));
                MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(MediaItem.fromUri(item.getString("videoSource")));
                MediaSource audioSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(MediaItem.fromUri(audURL));
                MergingMediaSource mergedSource = new MergingMediaSource(videoSource, audioSource);
                binding_data.video144.setText(item.getString("vidHeight") + "p");

                binding_data.video144.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            JSONObject item = listItem.getJSONObject(position);
                        /*    DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(MainActivity.this,
                                    Util.getUserAgent(MainActivity.this, getString(R.string.app_name)));

                            MediaSource video = new ProgressiveMediaSource.Factory(dataSourceFactory)
                                    .createMediaSource(MediaItem.fromUri(Uri.parse(item.getString("videoSource"))));

                            MediaSource audio = new ProgressiveMediaSource.Factory(dataSourceFactory)
                                    .createMediaSource(MediaItem.fromUri(Uri.parse(audURL)));
                            MediaSource mediaSource = new MergingMediaSource(video, audio);
                            mergePlayer1(mediaSource);*/


                          /*  MediaSource videoSource = new ProgressiveMediaSource
                                    .Factory(new DefaultHttpDataSource.Factory())
                                    .createMediaSource(MediaItem.fromUri(Uri.parse(item.getString("videoSource"))));
                            MediaSource audioSource = new ProgressiveMediaSource
                                    .Factory(new DefaultHttpDataSource.Factory())
                                    .createMediaSource(MediaItem.fromUri(Uri.parse(audURL)));

                            mergePlayer(audioSource,videoSource);*/

//                            mergePlayer2(item.getString("videoSource"),item.getString("videoSource"));


                            binding.relativeLayout.setVisibility(View.VISIBLE);
                            mergePlayer3(mergedSource);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull VideoPxItemLayoutBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }


    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            CustomAdapterCat.MainListHolder listHolder = new CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final CustomAdapterCat.MainListHolder mainHolder = (CustomAdapterCat.MainListHolder) holder;


            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("videochat_message"));
                String comment = arrayListchat.getJSONObject(position).getString("videochat_message");
                String reply = arrayListchat.getJSONObject(position).getString("video_chat_reply");
                if (!comment.equals("")) {
                    mainHolder.admincomment.setText(comment);
                }

                if (!reply.equals("")) {
                    mainHolder.usercomment.setText(reply);
                } else {
                    mainHolder.usercomment.setVisibility(View.GONE);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title, admincomment, usercomment;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.txt_des);
                usercomment = view.findViewById(R.id.txt_reply);
            }
        }
    }


    @Override
    public void onBackPressed() {
        /*switch (getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT: {
                player.release();
                finish();
                break;
            }
            case Configuration.ORIENTATION_LANDSCAPE: {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            }
        }*/
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            player.release();
            finish();
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ViewGroup.LayoutParams parms = binding.videoView.getLayoutParams();
            parms.height = (int) getResources().getDimension(R.dimen.height1); // LayoutParams: android.view.ViewGroup.LayoutParams
            ((ViewGroup.MarginLayoutParams) parms).topMargin = (int) getResources().getDimension(R.dimen.margin_top_prt);
            binding.videoView.requestLayout();
        }
    }

}