package codexo.neonclasses.service;


import android.app.Activity;
import android.app.DownloadManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.FileProvider;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;

import org.json.JSONException;

import java.io.File;
import codexo.neonclasses.R;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;

public class DownloadableServices extends IntentService {
    private Context context;
    private int result = Activity.RESULT_CANCELED;
    public static final String urlpath = "urlpath";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = "filepath";
    public static final String TYPE = "filetype";
    public static final String ID = "id";
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "Notification";
    SessionManager session;
    NotificationCompat.Builder builder;
    String downloadId = "";
    int id = 0;
    File videofile = null;
    Bitmap logo;
    NotificationManager notificationManager;
    NotificationManagerCompat managerCompat;
    Notification notificationCompat;
    public DownloadableServices() {
        super("DownloadService");
    }

    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {
        session = new SessionManager(this);
        context = this;
        downloadId = intent.getStringExtra(ID);

        String urlPath = intent.getStringExtra(urlpath);
        String fileName = intent.getStringExtra(FILENAME);
        String type  = intent.getStringExtra(TYPE);
        File output = null;
        String file = null;
        builder = new NotificationCompat.Builder(this, getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
            builder.setColor(getResources().getColor(R.color.black));
        } else {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
        }
        logo = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            channel.enableVibration(true);
            channel.setSound(null, null);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);



        } else {
            notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);

        }

        if (type.equalsIgnoreCase("video")) {
            file = fileName + ".mp4";

            output = new File(DownloadSupport.getVideoDirectory(this, session));
            videofile = new File(DownloadSupport.getVideoDirectory(this, session) + "/" + File.separator + file);
            // videofile.createNewFile();
        } else if (type.equalsIgnoreCase("pdf")) {
            file = fileName + ".pdf";

            output = new File(DownloadSupport.getPdfDirectory(this, session));
            videofile = new File(DownloadSupport.getPdfDirectory(this, session) + "/" + File.separator + file);
        }else if(type.equalsIgnoreCase("pdfJpg")){
            file = fileName + ".jpg";
            output = new File(DownloadSupport.getPdfImageDirectory(this, session));
            videofile = new File(DownloadSupport.getPdfImageDirectory(this, session), file);
        }

        else {
            file = fileName + ".jpg";
            output = new File(DownloadSupport.getImageDirectory(this, session));
            videofile = new File(DownloadSupport.getImageDirectory(this, session), file);
        }

        if (type.equalsIgnoreCase("video")) {
            showNotification(fileName, "Downloading Start", 0);

            AndroidNetworking.download(urlPath, output.getAbsolutePath(), file)
                    .setTag("downloadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                            showNotification(fileName, "Downloading : " + percent + " %", Integer.parseInt(percent));
                        }
                    })
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            result = Activity.RESULT_OK;
                            Intent intent=new Intent("DownloadedVideo")
                                    .putExtra("isCommplete",true);
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloaded: " + 100 + "%", 100);
                            publishResults(videofile.getAbsolutePath(), result);

                        }

                        @Override
                        public void onError(ANError error) {
                            result = Activity.RESULT_CANCELED;
                            Intent intent=new Intent("DownloadedVideo")
                                    .putExtra("isCommplete",false);
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloading Failed", 0);
                            new File(fileName).delete();
                            //notification.failDownloadNotification(context,downloadId);
                            Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                        }
                    });


            // repository.UpdateDownloadId(VIDEO_ID,downloadId);
        }
        else if (type.equalsIgnoreCase("pdf")) {
            // showNotification(fileName, "Downloading Start", 0);
            AndroidNetworking.download(urlPath, output.getAbsolutePath(), file)
                    .setTag("downloadPDF")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                            showNotification(fileName, "Downloading : " + percent + " %", Integer.parseInt(percent));


                        }
                    })
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            result = Activity.RESULT_OK;
                            Intent intent=new Intent("DownloadedPdf")
                                    .putExtra("isCommplete",true);
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloaded: " + 100 + "%", 100);
                            publishResults(videofile.getAbsolutePath(), result);

                        }

                        @Override
                        public void onError(ANError error) {
                            result = Activity.RESULT_CANCELED;
                            Intent intent=new Intent("DownloadedPdf")
                                    .putExtra("isCommplete",false);
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloading Failed", 0);
                            new File(fileName).delete();
                            //notification.failDownloadNotification(context,downloadId);
                            Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                        }
                    });


        }
        else if(type.equalsIgnoreCase("pdfJpg")){
            AndroidNetworking.download(urlPath, output.getAbsolutePath(), file)
                    .setTag("downloadPDFImage")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                            showNotification(fileName, "Downloading : " + percent + " %", Integer.parseInt(percent));
                        }
                    })
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            result = Activity.RESULT_OK;
                            Intent intent=new Intent("Downloaded");
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloaded: " + 100 + "%", 100);
                            publishResults(videofile.getAbsolutePath(), result);

                        }

                        @Override
                        public void onError(ANError error) {
                            result = Activity.RESULT_CANCELED;
                            Intent intent=new Intent("DownloadFailed");
                            sendBroadcast(intent);
                            showNotification(fileName, "Downloading Failed", 0);
                            new File(fileName).delete();
                            //notification.failDownloadNotification(context,downloadId);
                            Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                        }
                    });
        }
        else {
            AndroidNetworking.download(urlPath, output.getAbsolutePath(), file)
                    .setTag("downloadPDFImage")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                            showNotification(fileName, "Downloading : " + percent + " %", Integer.parseInt(percent));
                        }
                    })
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            result = Activity.RESULT_OK;
                            showNotification(fileName, "Downloaded: " + 100 + "%", 100);
                            publishResults(videofile.getAbsolutePath(), result);

                        }

                        @Override
                        public void onError(ANError error) {
                            result = Activity.RESULT_CANCELED;
                            showNotification(fileName, "Downloading Failed", 0);
                            new File(fileName).delete();
                            //notification.failDownloadNotification(context,downloadId);
                            Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                        }
                    });
        }


    }

    private void publishResults(String outputPath, int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(FILEPATH, outputPath);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }

    public void showNotification(String title, String body, int progress) {
        builder.setContentTitle(title)
                .setContentText(body)
                .setLargeIcon(logo)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setColorized(true)
                .setAutoCancel(true)
                .setProgress(100, progress, false)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.BADGE_ICON_LARGE)
                .setLights(1, 1, 1)
                .setOngoing(false)
                .setVibrate(null)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (progress == 100) {
                notificationManager.notify(Integer.parseInt(downloadId), builder.build());
                notificationManager.cancel(Integer.parseInt(downloadId));
            } else {
                notificationManager.notify(Integer.parseInt(downloadId), builder.build());
            }

        } else {

            if (progress == 100) {
                managerCompat.notify(Integer.parseInt(downloadId), notificationCompat);
                managerCompat.cancel(Integer.parseInt(downloadId));
            } else {
                managerCompat.notify(Integer.parseInt(downloadId), notificationCompat);
            }
        }


    }


}