package codexo.neonclasses.service.action;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.R;
import codexo.neonclasses.session.SessionManager;

public class MultiDownload {
    private Context context;
    private List<Integer> getDownloadIdList=new ArrayList<>();
    SessionManager session;
    String file = "";
    File output ;
    int id = 0;
    File videofile = null;

    public MultiDownload(Context context, SessionManager session) {
        this.context = context;
        this.session = session;
    }
    public void StartDownload(String url, String fileName ,String type){

        if (type.equalsIgnoreCase("video")) {
            file = fileName + ".mp4";
            output = new File(DownloadSupport.getVideoDirectory(context, session));
            videofile = new File(DownloadSupport.getVideoDirectory(context, session) + "/" + File.separator + file);
            DownloadNow(url,output,file);
        } else if (type.equalsIgnoreCase("pdf")) {
            file = fileName + ".pdf";
            output = new File(DownloadSupport.getPdfDirectory(context, session));
            videofile = new File(DownloadSupport.getPdfDirectory(context, session) + "/" + File.separator + file);
            DownloadNow(url,output,file);
        }else if(type.equalsIgnoreCase("pdfJpg")){
            file = fileName + ".jpg";
            output = new File(DownloadSupport.getPdfImageDirectory(context, session));
            videofile = new File(DownloadSupport.getPdfImageDirectory(context, session), file);
            DownloadNow(url,output,file);
        }
        else {
            file = fileName + ".jpg";
            output = new File(DownloadSupport.getImageDirectory(context, session));
            videofile = new File(DownloadSupport.getImageDirectory(context, session), file);
            DownloadNow(url,output,file);
        }
    }
    private void DownloadNow(String url, File output, String fileName){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                int downloadId = PRDownloader.download(url, output.getAbsolutePath(), fileName)
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {
                                showNotification(fileName,"Pause Downloading");
                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {
                                showNotification(fileName,"Downloading Cleared");
                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {
                                String percent = String.valueOf((progress.currentBytes * 100) / progress.totalBytes);
                                showNotification(fileName, "Downloading : " + percent + " %", Integer.parseInt(percent));
                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                Intent intent=new Intent("Downloaded");
                                context.sendBroadcast(intent);
                                showNotification(fileName, "Downloaded: " + 100 + "%", 100);
                            }

                            @Override
                            public void onError(Error error) {
                                Intent intent=new Intent("DownloadedFailed");
                                context.sendBroadcast(intent);
                                showNotification(fileName, "Downloading Failed", 0);
                                new File(fileName).delete();
                                //notification.failDownloadNotification(context,downloadId);
                                Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();
                            }

                        });
                id=downloadId;
                showNotification(fileName,"Start Downloading",id);
                getDownloadIdList.add(downloadId);
            }
        });


    }
    private void showNotification(String title, String body, int progress) {
        Bitmap logo;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
            builder.setColor(context.getResources().getColor(R.color.black));
        } else {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
        }
        logo = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        builder.setContentTitle(title)
                .setContentText(body)
                .setLargeIcon(logo)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setColorized(true)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setProgress(100, progress, false)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setDefaults(Notification.BADGE_ICON_LARGE)
                .setLights(1, 1, 1)
                .setOngoing(false)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.app_name);
            String description = context.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(context.getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            channel.enableVibration(true);
            channel.setSound(null, null);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            if (progress == 100) {
                notificationManager.notify(id, builder.build());
                notificationManager.cancel(id);
            } else {
                notificationManager.notify(id, builder.build());
            }

        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            Notification notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);

            if (progress == 100) {
                managerCompat.notify(id, notificationCompat);
                notificationManager.cancel(id);
            } else {
                managerCompat.notify(id, notificationCompat);
            }
        }


    }
    private void showNotification(String title, String body) {
        Bitmap logo;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
            builder.setColor(context.getResources().getColor(R.color.black));
        } else {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
        }
        logo = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        builder.setContentTitle(title)
                .setContentText(body)
                .setLargeIcon(logo)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setColorized(true)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.BADGE_ICON_LARGE)
                .setLights(1, 1, 1)
                .setOngoing(false)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.app_name);
            String description = context.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(context.getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            channel.enableVibration(true);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(id, builder.build());

        } else {
            Notification notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
            managerCompat.notify(id, notificationCompat);
        }


    }

}
