package codexo.neonclasses.service.action;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

import codexo.neonclasses.session.SessionManager;

public class DownloadSupport {

    public static String getVideoDirectory(Context context, SessionManager session) {
        File main = new File(context.getFilesDir().getAbsolutePath(), session.getUserId());
        //File main = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+ BuildConfig.APPLICATION_ID, SharedPre.getInstance(context).getUserId());
        if (!main.exists()) {
            main.mkdir();
        }
        File appFolder = new File(main, "NeonClasses");
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }
        File video = new File(appFolder, "Video");
        {
            if (!video.exists()) {
                video.mkdir();
            }
        }
         return video.getAbsolutePath();
    }
    public static String getoldpath(Context context, SessionManager session) {
        File main = new File(context.getFilesDir().getAbsolutePath(), session.getUserId());
        //File main = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+ BuildConfig.APPLICATION_ID, SharedPre.getInstance(context).getUserId());
        if (!main.exists()) {
            main.mkdir();
        }
        File appFolder = new File(main, "training_videos");
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }

        return appFolder.getAbsolutePath();
    }
    public static String getPdfDirectory(Context context, SessionManager session) {
        File main = new File(context.getFilesDir().getAbsolutePath(), session.getUserId());
        //File main = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+ BuildConfig.APPLICATION_ID, SharedPre.getInstance(context).getUserId());
        if (!main.exists()) {
            main.mkdir();
        }
        File appFolder = new File(main, "NeonClasses");
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }
        File video = new File(appFolder, "PDF");
        {
            if (!video.exists()) {
                video.mkdir();
            }
        }
        return video.getAbsolutePath();
    }

    public static String getImageDirectory(Context context, SessionManager session) {
        //  File main = new File(context.getFilesDir().getAbsolutePath(), SharedPre.getInstance(context).getUserId());
        File main = new File(context.getFilesDir().getAbsolutePath(), session.getUserId());
       // File main = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+ "codexo.neonclasses",session.getUserId());
        if (!main.exists()) {
            main.mkdir();
        }
        File appFolder = new File(main, "NeonClasses");
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }
        File image = new File(appFolder, "Image");
        {
            if (!image.exists()) {
                image.mkdir();
            }
        }
        return image.getAbsolutePath();
    }
    public static String getPdfImageDirectory(Context context, SessionManager session) {
        //  File main = new File(context.getFilesDir().getAbsolutePath(), SharedPre.getInstance(context).getUserId());
        File main = new File(context.getFilesDir().getAbsolutePath(), session.getUserId());
        // File main = new File(Environment.getExternalStorageDirectory()+"/Android/data/"+ "codexo.neonclasses",session.getUserId());
        if (!main.exists()) {
            main.mkdir();
        }
        File appFolder = new File(main, "NeonClasses");
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }
        File image = new File(appFolder, "PDFImages");
        {
            if (!image.exists()) {
                image.mkdir();
            }
        }
        return image.getAbsolutePath();
    }

    public static String getFormattedDate(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }

    public static ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.compare(f1.lastModified(), f2.lastModified());
                }
            });
            inFiles.addAll(Arrays.asList(files));
        }
        return inFiles;
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        String TAG = "Storage Permission";
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
                return true;
            } else {
//                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
            return true;
        }
    }
}
