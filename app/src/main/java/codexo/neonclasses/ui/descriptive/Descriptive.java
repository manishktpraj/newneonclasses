package codexo.neonclasses.ui.descriptive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityDescriptiveBinding;
import codexo.neonclasses.ui.courses.ThankyouActivity;
import codexo.neonclasses.ui.videos.CategoryVideosActivity;
import codexo.neonclasses.ui.videos.StudySubcategoryActivity;
import codexo.neonclasses.ui.videos.VideosubcategoryActivity;

public class Descriptive extends AppCompatActivity {
    ActivityDescriptiveBinding binding;
    String desId="118";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDescriptiveBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(Descriptive.this);
        Constant.checkAdb(Descriptive.this);
        binding.imgBack.setOnClickListener(v -> {
            finish();
        });
        JSONObject desvideo = new JSONObject();
        try {
            desvideo.put("bcat_id", 118);
            desvideo.put("bcat_name", "Descriptive");
            desvideo.put("bcat_slug", "descriptive");
            desvideo.put("bcat_parent", 0);
            desvideo.put("bcat_description", "");
            desvideo.put("bcat_meta", "");
            desvideo.put("bcat_status", true);
            desvideo.put("bcat_number", "");
            desvideo.put("bcat_order", 6);
            desvideo.put("bcat_image", "");
            desvideo.put("bcat_old", "");
            desvideo.put("child", "");
            desvideo.put("total", "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        JSONObject DesSM = new JSONObject();
        try {
            DesSM.put("preparation_category_id", 172);
            DesSM.put("preparation_category_title", "Descriptive Study Material");
            DesSM.put("preparation_category_desc", "");
            DesSM.put("preparation_category_meta", "");
            DesSM.put("preparation_category_slug", "descriptive-study-material");
            DesSM.put("preparation_category_parent", 0);
            DesSM.put("preparation_category_status", 1);
            DesSM.put("preparation_category_image", "");
            DesSM.put("preparation_category_paid", 1);
            DesSM.put("preparation_category_free", 0);
            DesSM.put("preparation_category_order", 125);
            DesSM.put("childdata", "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        binding.videoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent desInt = new Intent(Descriptive.this, CategoryVideosActivity.class);
                desInt.putExtra("parent_category", desvideo + "");
                startActivity(desInt);
            }
        });

        binding.smLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vidcategory = new Intent(Descriptive.this, StudySubcategoryActivity.class);
                vidcategory.putExtra("parent_category", DesSM + "");
                startActivity(vidcategory);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Descriptive.this);
        Constant.checkAdb(Descriptive.this);
    }
}