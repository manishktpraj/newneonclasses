package codexo.neonclasses.ui.help;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityTicketHistoryBinding;
import codexo.neonclasses.databinding.ItemTicketBinding;
import codexo.neonclasses.session.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;

public class TicketHistoryActivity extends AppCompatActivity {

    private ActivityTicketHistoryBinding binding;
    ProgressDialog pDialog;
    SessionManager session;
    JSONArray ticket_list = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityTicketHistoryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(TicketHistoryActivity.this);
        Constant.checkAdb(TicketHistoryActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        pDialog = Constant.getProgressBar(TicketHistoryActivity.this);
        session = new SessionManager(TicketHistoryActivity.this);
        binding.txtCreateNew.setOnClickListener(view -> {
            startActivity(new Intent(this, CreateTicketActivity.class));
        });

        tickethistory();
    }


    @Override
    public void onResume() {
        super.onResume();
        tickethistory();
        Constant.adbEnabled(TicketHistoryActivity.this);
        Constant.checkAdb(TicketHistoryActivity.this);
    }

    private void tickethistory() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getTicketHistory(session.getUserId());
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            ticket_list = jsonObject.getJSONArray("result");
                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(getApplicationContext(), ticket_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);


                        } else {
                            Constant.setToast(getApplicationContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                tickethistory();
            }
        });
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemTicketBinding binding;
        Context context;
        JSONArray listItem = new JSONArray();

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemTicketBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                binding.txtOrderId.setText("Ticket ID :-" + item.getString("issue_id"));
                String str = item.getString("issue_description").replaceAll("\\<.*?\\>", "");

                binding.txtDes.setText(str);
                if (item.getString("issue_status").equals("0")) {
                    binding.txtStatus.setText("Pending");
                    binding.txtStatus.setTextColor(getResources().getColor(R.color.red));

                } else {
                    binding.txtStatus.setText("Completed");
                    binding.txtStatus.setTextColor(getResources().getColor(R.color.green));
                }
                String replaced_d = item.getString("issue_created_time").replaceAll("T", " ");
                String replacedd_d = replaced_d.replace("+05:30", "");
                String[] replacedddd = replacedd_d.split(" ");
//                Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced", replacedddd[0]);
                binding.txtDate.setText("Date : " + replacedddd[0]);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemTicketBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}
