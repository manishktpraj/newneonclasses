package codexo.neonclasses.ui.help;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityFaqBinding;

public class FaqActivity extends AppCompatActivity {

    private ActivityFaqBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityFaqBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(FaqActivity.this);
        Constant.checkAdb(FaqActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(FaqActivity.this);
        Constant.checkAdb(FaqActivity.this);
    }
}

