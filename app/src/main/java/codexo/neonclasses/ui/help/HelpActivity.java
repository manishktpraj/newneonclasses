package codexo.neonclasses.ui.help;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityHelpBinding;
import codexo.neonclasses.ui.webview.WebActivity;

public class HelpActivity extends AppCompatActivity {

    private ActivityHelpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityHelpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(HelpActivity.this);
        Constant.checkAdb(HelpActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(HelpActivity.this));
        binding.txtSupport.setOnClickListener(view -> {
            startActivity(new Intent(this, TicketHistoryActivity.class));
        });
        binding.txtMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, Constant.Email_TO);
                emailIntent.setType("text/plain");
                startActivity(emailIntent);
            }
        });
        binding.txtFaq.setOnClickListener(view -> {
            Intent idf =new Intent(this, WebActivity.class);
            idf.putExtra("title","FAQ");
            idf.putExtra("url",Constant.FAQ);
            startActivity(idf);
        });

        binding.txtSupport.setOnClickListener(view -> {
          /*  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send/?phone=918875299901&text&app_absent=0"));
            startActivity(browserIntent);*/
            startActivity(new Intent(getApplicationContext(), TicketHistoryActivity.class));

           /* Intent idf =new Intent(this, WebActivity.class);
            idf.putExtra("title","CHAT");
            idf.putExtra("url",Constant.CHAT_URL);
            startActivity(idf);*/
        });

    }
    @Override
    public void onBackPressed(){
         this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(HelpActivity.this);
        Constant.checkAdb(HelpActivity.this);
    }
}
