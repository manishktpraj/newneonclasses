package codexo.neonclasses.ui.help;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityCreateTicketBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.download.DownloadsActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class CreateTicketActivity extends AppCompatActivity {

    private ActivityCreateTicketBinding binding;
    private static final int PICK_IMAGE = 1;
    private Uri selectedImageUri;
    AppCompatActivity activity;
    SessionManager session;
    ProgressDialog pDialog;
    Bitmap bitmap;
    String screenImage="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCreateTicketBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(CreateTicketActivity.this);
        Constant.checkAdb(CreateTicketActivity.this);
        activity = CreateTicketActivity.this;
        pDialog = Constant.getProgressBar(CreateTicketActivity.this);
        session = new SessionManager(getApplicationContext());
        binding.imgBack.setOnClickListener(view -> finish());
      ///  Bitmap bmap = binding.image.getDrawingCache();
     ///   String encodedImageData = getEncoded64ImageStringFromBitmap(bmap);
        binding.txtPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String go_next = "yes";
                String text_issue = binding.textIssue.getText().toString();
                if(text_issue.equals(""))
                {
                    binding.textIssue.setError("Please write your issue...");
                    go_next ="no";
                }

                if(go_next.equals("yes")) {
                    generateTicket(text_issue,screenImage);
                }
            }
        });


        binding.imgAddLayout.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        });
        //Log.d("userIduserId",session.getUserId()+"name. "+session.getUserName()+"mob. "+session.getUserPhone());

    }
    public void onActivityResult (int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            // compare the resultCode with the
            // SELECT_PICTURE constant
            if (requestCode == PICK_IMAGE) {
                // Get the url of the image from data
                selectedImageUri = data.getData();
//                Constant.logPrint("selectedImageUri",selectedImageUri+"");
                if (null != selectedImageUri) {

                    final InputStream imageStream;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        screenImage = encodeImage(selectedImage);
//                        Constant.logPrint("screenImage",screenImage+"");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }



                    // update the preview image in the layout
                    binding.image.setVisibility(View.VISIBLE);
                    binding.imgAddLayout.setVisibility(View.GONE);
                    binding.image.setImageURI(selectedImageUri);
                }
            }
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();

        // Get the Base64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }


    private void generateTicket(final String text_issue,final String encodedImageData) {
        if (!pDialog.isShowing()){
            pDialog.show();

        }
        Call<String> call = Constant.getUrl().uploadTicket(text_issue,encodedImageData,session.getUserId(),session.getUserName(),session.getUserPhone());
        call.clone().enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {
                pDialog.dismiss();
                String response = response_string.body();
//                Constant.logPrint("response", response);
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        //String notification = jsonObject.getString("notification");
                        if (message.equals("ok")) {
                            Constant.setToast(CreateTicketActivity.this, jsonObject.getString("notification"));
                            finish();
                        } else {
                            Constant.setToast(CreateTicketActivity.this, "Ticket not Generated Please Look at code");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
//                Constant.logPrint("response error",t+"");
                generateTicket(text_issue,encodedImageData);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(CreateTicketActivity.this);
        Constant.checkAdb(CreateTicketActivity.this);
    }
}
