package codexo.neonclasses.ui.player;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.EventLogger;

import org.json.JSONException;

import java.util.Collections;
import java.util.EventListener;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityOfflineVideoPlayerBinding;
import codexo.neonclasses.databinding.ActivityPlayerBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.offer.OfferActivity;

public class OfflineVideoPlayerActivity extends AppCompatActivity {
    private static String VIDEO_SAMPLE = "https://developers.google.com/training/images/tacoma_narrows.mp4";
    private VideoView mVideoView;
    private TextView mBufferingTextView;
    private ActivityOfflineVideoPlayerBinding binding;
    protected SimpleExoPlayer player;
    private AdsLoader adsLoader;
    private DataSource.Factory dataSourceFactory;
    SessionManager session;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private Float speed=1.25f;
    private List<MediaItem> mediaItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session =new SessionManager(OfflineVideoPlayerActivity.this);
        binding = ActivityOfflineVideoPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(OfflineVideoPlayerActivity.this);
        Constant.checkAdb(OfflineVideoPlayerActivity.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
        fullScreen();
        //// mVideoView.setMediaController(new MediaController(this));
        VIDEO_SAMPLE= getIntent().getStringExtra("video_detail");
//        Constant.logPrint(VIDEO_SAMPLE,"VIDEO_SAMPLE");
      ///  mVideoView = findViewById(R.id.videoview);
        dataSourceFactory = ExoUtilCustom.getDataSourceFactory(this);
            binding.username.setText(session.getUserPhone());
        binding.speedlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), binding.speed);
                popupMenu.inflate(R.menu.speed);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        if (player != null) {

                            switch (menuItem.getItemId()) {
                                case R.id.x05: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                        speed=0.5f;
                                    }
                                    break;
                                }
                                case R.id.normal: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1f));
                                        speed=1f;
                                    }
                                    break;
                                }
                                case R.id.x125: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.25f));
                                        speed=1.25f;
                                    }
                                    break;
                                }
                                case R.id.x150: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.50f));
                                        speed=1.50f;
                                    }
                                    break;
                                }
                                case R.id.x175: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.75f));
                                        speed=1.75f;
                                    }
                                    break;
                                }
                                case R.id.x1: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.5f));
                                        speed=1.5f;
                                    }
                                    break;
                                }
                                case R.id.x2: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(2f));
                                        speed=2f;
                                    }
                                    break;
                                }

                            }

                            // Showing the popup menu


                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });

    }



    private Uri getMedia(String mediaName) {
        return Uri.parse(mediaName);
    }
    @Override
    protected void onStart() {
        super.onStart();
        initializePlayers();
    }

    @Override
    protected void onStop() {
        if (binding.playerView != null) {
            binding.playerView.onPause();

        }
        super.onStop();
    }
    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    public boolean onError(MediaPlayer mp,int what,int extra)
    {
        return true;
    }

    private void fullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    protected boolean initializePlayers() {
        Uri videoUri = getMedia(VIDEO_SAMPLE);
        DefaultTrackSelector.ParametersBuilder builder =
                new DefaultTrackSelector.ParametersBuilder(/* context= */ this);
        trackSelectorParameters = builder.build();
        RenderersFactory renderersFactory =
                    ExoUtilCustom.buildRenderersFactory(/* context= */ this, true);
            MediaSourceFactory mediaSourceFactory =
                    new DefaultMediaSourceFactory(dataSourceFactory)
                            .setAdsLoaderProvider(this::getAdsLoader)
                            .setAdViewProvider(binding.playerView);

            trackSelector = new DefaultTrackSelector(/* context= */ this);
            trackSelector.setParameters(trackSelectorParameters);
             player = new SimpleExoPlayer.Builder(/* context= */ this, renderersFactory)
                    .setMediaSourceFactory(mediaSourceFactory)
                    .setTrackSelector(trackSelector)
                    .build();
         ///   player.addListener(new PlayerEventListener());

       //// player(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


            player.addAnalyticsListener(new EventLogger(trackSelector));
            player.setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true);
            binding.playerView.setPlayer(player);
            MediaItem mediaItem = MediaItem.fromUri(videoUri);
        player.setMediaItem(mediaItem);
        player.prepare();
        AspectRatioFrameLayout contentFrame = binding.playerView.findViewById(R.id.exo_content_frame);
        contentFrame.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

        player.play();
        return true;
    }
    private AdsLoader getAdsLoader(MediaItem.AdsConfiguration adsConfiguration) {
        // The ads loader is reused for multiple playbacks, so that ad playback can resume.
        if (adsLoader == null) {
            adsLoader = new ImaAdsLoader.Builder(/* context= */ this).build();
        }
        adsLoader.setPlayer(player);
        return adsLoader;
    }

    @Override
    public void onBackPressed() {
        player.release();
        finish();
    }
    protected void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            mediaItems = Collections.emptyList();
            trackSelector = null;
        }
        if (adsLoader != null) {
            adsLoader.setPlayer(null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(OfflineVideoPlayerActivity.this);
        Constant.checkAdb(OfflineVideoPlayerActivity.this);
    }
}