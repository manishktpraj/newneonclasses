package codexo.neonclasses.ui.player;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityWebuiPlayerBinding;
import codexo.neonclasses.session.SessionManager;

public class WebuiPlayer extends AppCompatActivity {
    ActivityWebuiPlayerBinding binding;

    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_SPEED = "playbackspeed";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private static final String STATE_RESUME_POSITION = "resume";
    private long mPlaybackPosition = 0;
    private int mCurrentWindow = 0;
    protected SimpleExoPlayer player;
    private boolean isShowingTrackSelectionDialog;
    private DataSource.Factory dataSourceFactory;
    private List<MediaItem> mediaItems;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private DebugTextViewHelper debugViewHelper;
    private TrackGroupArray lastSeenTrackGroupArray;
    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    private Float speed = 1f;
    private AdsLoader adsLoader;
    private Context context;
    private String YOUTUBE_VIDEO_ID = "";
    private String BASE_URL = "https://www.youtube.com";
    //private String mYoutubeLink = "";
    private String url = "";
    private SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener;
    private PopupMenu menu;
    private String image;
    private ValueEventListener mSearchedLocationReferenceListener;
    DatabaseReference messagesRef;
    AppCompatActivity activity;
    SessionManager session;
    JSONArray arrayListchat = new JSONArray();
    String vid_id = "";
    private FirebaseDatabase mDatabase;
    private static LinearLayoutManager mLayoutManager;
    RecyclerView recycler_view;
    JSONArray jsonarayquality = new JSONArray();
    Long currentpos = 0L;
    String student_id = "0";
    JSONObject video_detail = new JSONObject();
    String audioUrl = "";

    YouTubePlayerView mYoutubeplayerView;
    YouTubePlayer mPlayer;
    WebView webView;

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int idx = 0; idx < group.getChildCount(); idx++) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWebuiPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(WebuiPlayer.this);
        Constant.checkAdb(WebuiPlayer.this);
        session = new SessionManager(getApplicationContext());
        activity = WebuiPlayer.this;
        student_id = session.getUserId();
        Constant.fullScreen(WebuiPlayer.this);
        dataSourceFactory = ExoUtilCustom.getDataSourceFactory(this);
        mDatabase = FirebaseDatabase.getInstance();

        try {
            video_detail = new JSONObject(getIntent().getStringExtra("video_detail"));
            YOUTUBE_VIDEO_ID = extractVideoIdFromUrl(video_detail.getString("vid_url"));
            vid_id = video_detail.getString("vid_id");
            String custommg = video_detail.getString("vid_off_message");
            if (!custommg.equals("")) {
                binding.customMessageData.setText(custommg);
                binding.custommessage.setVisibility(View.VISIBLE);
            }
//            Constant.logPrint("video_detail", video_detail + "");
            binding.txtTitle.setText(video_detail.getString("vid_title"));
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
//            Constant.logPrint(YOUTUBE_VIDEO_ID, "sdsdddddddddddddd");
            //YOUTUBE_VIDEO_ID ="LDvq9TM0snc"; E1Ameff6F44
            //mYoutubeLink = BASE_URL + "/watch?v=" + YOUTUBE_VIDEO_ID;
            String MESSAGE_CHANNEL = "/neonmain/" + video_detail.getString("vid_id") + "/";
//            Constant.logPrint(MESSAGE_CHANNEL, "MESSAGE_CHANNEL");
            messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);
            context = this;
            /*binding.playerView.setErrorMessageProvider(new PlayerActivity.PlayerErrorMessageProvider());
            binding.playerView.requestFocus();*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
        binding.youtubePlayerView.enableBackgroundPlayback(true);
        binding.youtubePlayerView.enableBackgroundPlayback(true);
        //binding.youtubePlayerView.cancelLongPress();
        binding.youtubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                //String videoId = YOUTUBE_VIDEO_ID;
                youTubePlayer.loadVideo(YOUTUBE_VIDEO_ID, 0);
//                Constant.logPrint("YOUTUBE_VIDEO_ID123", YOUTUBE_VIDEO_ID);
                enableDisableView(binding.youtubePlayerView, false);
                binding.youtubePlayerView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

            }
        });


        binding.username.setText(session.getUserShareCode());


        binding.editChatBox.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String go_next = "yes";
                    String username_string = binding.editChatBox.getText().toString();
                    if (username_string.equals("")) {
                        int duration = Toast.LENGTH_SHORT;
                        binding.editChatBox.setError("Please Enter Your Question");
                        go_next = "no";
                    }

                    if (go_next.equals("yes")) {
                        Long daedat = new Date().getTime();
                        String userid = session.getUserId();
                        Map<String, String> formData = new HashMap<String, String>();
                        formData.put("user_id", userid);
                        formData.put("doubts_user_name", session.getUserName());
                        formData.put("doubts_user_email", session.getUserEmail());
                        formData.put("doubts_user_phone", session.getUserPhone());
                        formData.put("video_chat_reply", "");
                        formData.put("video_reply_time", "");
                        formData.put("videochat_created", daedat + "");
                        formData.put("videochat_message", username_string);
//                        Constant.logPrint(formData + "", "dddddddddddddddd");
//                        Constant.logPrint(daedat + "", "daedatdaedatdaedatdaedatdaedat" + formData + "");
                        // messagesRef.child(daedat+"").setValue(formData);
                        messagesRef.push().setValue(formData);
                        binding.editChatBox.setText("");
                        Toast.makeText(activity, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                    }
                    hideSoftKeyboard(WebuiPlayer.this);
                    return true;
                }
                return false;
            }
        });
        binding.imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = binding.editChatBox.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    binding.editChatBox.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();
                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("video_chat_reply", "");
                    formData.put("video_reply_time", "");
                    formData.put("videochat_created", formattedDate + "");
                    formData.put("videochat_message", username_string);
//                    Constant.logPrint(formData + "", "dddddddddddddddd");
//                    Constant.logPrint(daedat + "", "daedatdaedatdaedatdaedatdaedat" + formData + "");
                    // messagesRef.child(daedat+"").setValue(formData);
                    messagesRef.push().setValue(formData);
                    binding.editChatBox.setText("");
                    Toast.makeText(activity, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
//                    Log.d("Locations updated", "location: " + vid_id + location);

                    if (locationSnapshot != null) {
                        try {
                            JSONObject objd = new JSONObject();
                            objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                            objd.put("videochat_created", locationSnapshot.child("videochat_created").getValue().toString());
                            objd.put("videochat_message", locationSnapshot.child("videochat_message").getValue().toString());
                            objd.put("video_chat_reply", locationSnapshot.child("video_chat_reply").getValue().toString());
                            objd.put("video_reply_time", locationSnapshot.child("video_reply_time").getValue().toString());
                            objd.put("doubts_user_email", locationSnapshot.child("doubts_user_email").getValue().toString());
                            objd.put("doubts_user_name", locationSnapshot.child("doubts_user_name").getValue().toString());
                            objd.put("doubts_user_phone", locationSnapshot.child("doubts_user_phone").getValue().toString());

                            ///       Log.d("updatedupdatedupdatedupdatedupdated", "location: " +  objd);
                            if (objd.getString("user_id").equals(session.getUserId())) {
                                arrayListchat.put(objd);

                            }

                            if (!binding.recyclerView.equals(null)) {
                                final CustomAdapterCat adapter = new CustomAdapterCat(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                binding.recyclerView.setLayoutManager(mLayoutManager);
                                binding.recyclerView.setHasFixedSize(true);
                                binding.recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


    }

    boolean isLongClick = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP && isLongClick) {
            isLongClick = false;
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            isLongClick = false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            isLongClick = false;
        }

        return super.onTouchEvent(event);
    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.relChat.setVisibility(View.GONE);
            binding.txtTitle.setVisibility(View.GONE);
            onFullscreen(true);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            onFullscreen(false);

        }
    }

    public void onFullscreen(boolean fullscreen) {
    }
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
*/
    //Put this into the class

    public String extractVideoIdFromUrl(String url) {
        String vId = null;
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);
        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(),
                    0
            );
        }
    }

    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            CustomAdapterCat.MainListHolder listHolder = new CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final CustomAdapterCat.MainListHolder mainHolder = (CustomAdapterCat.MainListHolder) holder;


            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("videochat_message"));
                String comment = arrayListchat.getJSONObject(position).getString("videochat_message");
                String reply = arrayListchat.getJSONObject(position).getString("video_chat_reply");
                if (!comment.equals("")) {
                    mainHolder.admincomment.setText(comment);
                }

                if (!reply.equals("")) {
                    mainHolder.usercomment.setText(reply);
                } else {
                    mainHolder.usercomment.setVisibility(View.GONE);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title, admincomment, usercomment;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.txt_des);
                usercomment = view.findViewById(R.id.txt_reply);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(WebuiPlayer.this);
        Constant.checkAdb(WebuiPlayer.this);
    }
}