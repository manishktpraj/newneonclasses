package codexo.neonclasses.ui.player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityPlayerBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.youtube.YoutubeActivity;

public class PlayerActivity extends AppCompatActivity implements StyledPlayerControlView.VisibilityListener {

    private ActivityPlayerBinding binding;
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_SPEED = "playbackspeed";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private static final String STATE_RESUME_POSITION = "resume";
    private long mPlaybackPosition = 0;
    private int mCurrentWindow = 0;
    protected SimpleExoPlayer player;
    private boolean isShowingTrackSelectionDialog;
    private DataSource.Factory dataSourceFactory;
    private List<MediaItem> mediaItems;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private DebugTextViewHelper debugViewHelper;
    private TrackGroupArray lastSeenTrackGroupArray;
    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    private Float speed=1f;
    private AdsLoader adsLoader;
    private Context context;
    private String YOUTUBE_VIDEO_ID = "";
    private String BASE_URL = "https://www.youtube.com";
    private String mYoutubeLink = "";
    private String url = "";
    private SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener;
    private PopupMenu menu;
    private String image;
    private ValueEventListener mSearchedLocationReferenceListener;
    DatabaseReference messagesRef;
    AppCompatActivity activity;
    SessionManager session;
    JSONArray arrayListchat = new JSONArray();
    String vid_id = "";
    private FirebaseDatabase mDatabase;
    private static LinearLayoutManager mLayoutManager;
    RecyclerView recycler_view;
    JSONArray jsonarayquality = new JSONArray();
    Long currentpos = 0L;
    String student_id="0";
    JSONObject video_detail=new JSONObject();
    String audioUrl="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(PlayerActivity.this);
        Constant.checkAdb(PlayerActivity.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
        Constant.fullScreen(PlayerActivity.this);
        session = new SessionManager(getApplicationContext());
        activity = PlayerActivity.this;
        student_id = session.getUserId();
        dataSourceFactory = ExoUtilCustom.getDataSourceFactory(this);
        mDatabase = FirebaseDatabase.getInstance();


 /////        String MESSAGE_CHANNEL = "/neonmain/2549/";

        try {
            video_detail =new JSONObject(getIntent().getStringExtra("video_detail"));


            String custommg = video_detail.getString("vid_off_message");
            if(!custommg.equals(""))
            {
                binding.customMessageData.setText(custommg);
                binding.custommessage.setVisibility(View.VISIBLE);
            }
//            Constant.logPrint("video_detail",video_detail+"");
            binding.txtTitle.setText(video_detail.getString("vid_title"));
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
            YOUTUBE_VIDEO_ID = extractVideoIdFromUrl(video_detail.getString("vid_url"));
            vid_id = video_detail.getString("vid_id");
//            Constant.logPrint(YOUTUBE_VIDEO_ID,"sdsdddddddddddddd");
        /////    YOUTUBE_VIDEO_ID ="LDvq9TM0snc";
            mYoutubeLink = BASE_URL + "/watch?v=" + YOUTUBE_VIDEO_ID;
             String MESSAGE_CHANNEL = "/neonmain/" + video_detail.getString("vid_id")+"/";
//            Constant.logPrint(MESSAGE_CHANNEL,"MESSAGE_CHANNEL");
            messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);
            extractYoutubeUrl();
            context = this;
            binding.playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
            binding.playerView.requestFocus();
            if (savedInstanceState != null) {
                trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
                startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
                startWindow = savedInstanceState.getInt(KEY_WINDOW);
                startPosition = savedInstanceState.getLong(KEY_POSITION);
                mCurrentWindow = savedInstanceState.getInt(KEY_WINDOW);
                mPlaybackPosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
                speed =  savedInstanceState.getFloat(KEY_SPEED);
            } else {
                DefaultTrackSelector.ParametersBuilder builder =
                        new DefaultTrackSelector.ParametersBuilder(/* context= */ this);
                trackSelectorParameters = builder.build();
                clearStartPosition();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        binding.editChatBox.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String go_next = "yes";
                    String username_string = binding.editChatBox.getText().toString();
                    if (username_string.equals("")) {
                        int duration = Toast.LENGTH_SHORT;
                        binding.editChatBox.setError("Please Enter Your Question");
                        go_next = "no";
                    }

                    if (go_next.equals("yes")) {
                        Long daedat = new Date().getTime();
                        String userid = session.getUserId();
                        Map<String, String> formData = new HashMap<String, String>();
                        formData.put("user_id", userid);
                        formData.put("doubts_user_name", session.getUserName());
                        formData.put("doubts_user_email", session.getUserEmail());
                        formData.put("doubts_user_phone", session.getUserPhone());
                        formData.put("video_chat_reply", "");
                        formData.put("video_reply_time", "");
                        formData.put("videochat_created",daedat+"");
                        formData.put("videochat_message", username_string);
//                        Constant.logPrint(formData+"","dddddddddddddddd");
//                        Constant.logPrint(daedat+"","daedatdaedatdaedatdaedatdaedat"+formData+"");
                        // messagesRef.child(daedat+"").setValue(formData);
                        messagesRef.push().setValue(formData);
                        binding.editChatBox.setText("");
                        Toast.makeText(activity, "Message Sent Successfully"+userid, Toast.LENGTH_SHORT).show();
                    }
                    hideSoftKeyboard(PlayerActivity.this);
                     return true;
                }
                return false;
            }
        });
        binding.imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = binding.editChatBox.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    binding.editChatBox.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();
                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => "+c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("video_chat_reply", "");
                    formData.put("video_reply_time", "");
                    formData.put("videochat_created",formattedDate+"");
                     formData.put("videochat_message", username_string);
//                    Constant.logPrint(formData+"","dddddddddddddddd");
//                    Constant.logPrint(daedat+"","daedatdaedatdaedatdaedatdaedat"+formData+"");
                   // messagesRef.child(daedat+"").setValue(formData);
                    messagesRef.push().setValue(formData);
                    binding.editChatBox.setText("");
                    Toast.makeText(activity, "Message Sent Successfully"+userid, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
//                    Log.d("Locations updated", "location: "+vid_id + location);

                    if(locationSnapshot!=null) {
                        try {
                            JSONObject objd = new JSONObject();
                             objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                             objd.put("videochat_created", locationSnapshot.child("videochat_created").getValue().toString());
                             objd.put("videochat_message", locationSnapshot.child("videochat_message").getValue().toString());
                            objd.put("video_chat_reply", locationSnapshot.child("video_chat_reply").getValue().toString());
                            objd.put("video_reply_time", locationSnapshot.child("video_reply_time").getValue().toString());
                            objd.put("doubts_user_email", locationSnapshot.child("doubts_user_email").getValue().toString());
                            objd.put("doubts_user_name", locationSnapshot.child("doubts_user_name").getValue().toString());
                            objd.put("doubts_user_phone", locationSnapshot.child("doubts_user_phone").getValue().toString());

//                               Log.d("updateddupdated", "location: " +  objd);
                            if (objd.getString("user_id").equals(session.getUserId())) {
                                arrayListchat.put(objd);
                            }

                            if (!binding.recyclerView.equals(null)) {
                                final PlayerActivity.CustomAdapterCat adapter = new PlayerActivity.CustomAdapterCat(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                binding.recyclerView.setLayoutManager(mLayoutManager);
                                binding.recyclerView.setHasFixedSize(true);
                                binding.recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        binding.exoMinimalFullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (getResources().getConfiguration().orientation) {
                    case Configuration.ORIENTATION_PORTRAIT: {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        break;
                    }
                    case Configuration.ORIENTATION_LANDSCAPE: {

                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


                    }
                }
            }
        });

        binding.username.setText(session.getUserShareCode());
        binding.speedlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, binding.speed);
                popupMenu.inflate(R.menu.speed);
                 popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        if (player != null) {

                            switch (menuItem.getItemId()) {
                                case R.id.x05: {
                                    if (player != null) {
                                        speed=0.5f;
                                        player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                    }
                                    break;
                                }
                                case R.id.normal: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1f));
                                        speed=1f;
                                    }
                                    break;
                                }
                                case R.id.x125: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.25f));
                                        speed=1.25f;
                                    }
                                    break;
                                }
                                case R.id.x150: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.50f));
                                        speed=1.50f;
                                    }
                                    break;
                                }
                                case R.id.x175: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.75f));
                                        speed=1.75f;
                                    }
                                    break;
                                }
                                case R.id.x1: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(1.5f));
                                        speed=1.5f;
                                    }
                                    break;
                                }
                                case R.id.x2: {
                                    if (player != null) {
                                        player.setPlaybackParameters(new PlaybackParameters(2f));
                                        speed=2f;
                                    }
                                    break;
                                }

                            }

                            // Showing the popup menu


                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });
        binding.selectTracksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, binding.quality);
                popupMenu.inflate(R.menu.quality);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        if (player != null) {

                            switch (menuItem.getItemId()) {

                                case R.id.p240_menu: {
                                    if (player != null) {
                                        ///   player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                        setquality(0);

                                    }
                                    break;
                                }
                                case R.id.p360_menu: {
                                    if (player != null) {
                                        ///     player.setPlaybackParameters(new PlaybackParameters(1f));
                                        setquality(0);
                                    }
                                    break;
                                }

                                case R.id.p720_menu: {
                                    if (player != null) {
                                        setquality(1);

                                        ////   player.setPlaybackParameters(new PlaybackParameters(2f));
                                    }
                                    break;
                                }

                            }
                            // Showing the popup menu


                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });


    }
    public void setquality(Integer i) {
        ///  player.release();
        try {
            if (jsonarayquality.length() > 0) {
                Integer d = 0;

                url = jsonarayquality.get(d).toString();

//                Log.d(url, "urlmanishgarg");
                if (!url.equals("")) {


                   /* MediaItem mediaItem = MediaItem.fromUri(url);
                    player.setMediaItem(mediaItem);
                    player.prepare();
                    player.seekTo(mCurrentWindow, mPlaybackPosition);*/

                    Long ppposition = player.getCurrentPosition();
                    int mCurrentWindows = mCurrentWindow;
                    releasePlayer();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            initializePlayer();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    Log.d(ppposition + "", "demobalance");
                                    if (ppposition != 0L && player != null) {
                                        player.seekTo(mCurrentWindows, ppposition);
                                        player.setPlaybackParameters(new PlaybackParameters(speed));
                                        player.getPlayWhenReady();
                                    }
                                }
                            }, 1000);
                        }
                    }, 1000);


                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager.isAcceptingText()){
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(),
                    0
            );
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.relChat.setVisibility(View.GONE);
            onFullscreen(true);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
             onFullscreen(false);

        }
    }

    public void onFullscreen(boolean fullscreen) {
    }
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        releasePlayer();
        releaseAdsLoader();
        clearStartPosition();
        setIntent(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (player == null) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.adbEnabled(PlayerActivity.this);
        Constant.checkAdb(PlayerActivity.this);
        if (player == null) {
            if (binding.playerView != null) {
                binding.playerView.onResume();
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mPlaybackPosition != 0L && player != null) {
                        player.seekTo(mCurrentWindow, mPlaybackPosition);
                        player.getPlayWhenReady();
                    }
                    if(player != null) {
                        player.setPlaybackParameters(new PlaybackParameters(speed));
                    }

                }
            }, 3000);

        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        if (binding.playerView != null) {
            binding.playerView.onPause();
        }
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    ///    messagesRef.removeEventListener(mSearchedLocationReferenceListener);
        releaseAdsLoader();
    }
    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) {
            // Empty results are triggered if a permission is requested while another request was already
            // pending and can be safely ignored in this case.
            return;
        }
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initializePlayer();
        } else {
            Constant.setToast(getApplicationContext(),getString(R.string.storage_permission_denied));
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        // super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        updateStartPosition();
        outState.putInt(KEY_WINDOW, mCurrentWindow);
        outState.putLong(STATE_RESUME_POSITION, mPlaybackPosition);
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_WINDOW, startWindow);
        outState.putFloat(KEY_SPEED, speed);
        outState.putLong(KEY_POSITION, startPosition);
        releasePlayer();
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // See whether the player view wants to handle media or DPAD keys events.
        return binding.playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }


    @Override
    public void onVisibilityChange(int visibility) {
        binding.framePlayer.setVisibility(visibility);
    }

    @Override
    public void onBackPressed() {
        switch (getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT: {
                player.release();
                finish();
                break;
            }
            case Configuration.ORIENTATION_LANDSCAPE: {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            }
        }
    }
    protected void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            mediaItems = Collections.emptyList();
            trackSelector = null;
        }
        if (adsLoader != null) {
            adsLoader.setPlayer(null);
        }
    }

    private void releaseAdsLoader() {
        if (adsLoader != null) {
            adsLoader.release();
            adsLoader = null;
            binding.playerView.getOverlayFrameLayout().removeAllViews();
        }
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }
    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            mPlaybackPosition = player.getCurrentPosition();
            currentpos = player.getCurrentPosition();
             //startPosition = Math.max(0, player.getContentPosition());
        }
    }

    public String extractVideoIdFromUrl(String url) {
        String vId = null;
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);
        if(matcher.find()){
            vId= matcher.group();
        }
        return vId;
    }
    private void extractYoutubeUrl() {
        @SuppressLint("StaticFieldLeak")
        YouTubeExtractor mExtractor = new YouTubeExtractor(this) {
            @Override
            protected void onExtractionComplete(SparseArray<YtFile> sparseArray, VideoMeta videoMeta) {
                if (sparseArray != null) {




                    String state ="";

                    int   itag = 134;
                    String downloadUrl = sparseArray.get(itag).getUrl();
//                    Constant.logPrint("url"+134, downloadUrl + "mgggg");

                    if (state.equals("")) {
                        url = downloadUrl;
                        itag = 140;
                        String exurl = sparseArray.get(itag).getUrl();
//                        Constant.logPrint("url"+140, downloadUrl + "mgggg");
                        url = downloadUrl;
                        jsonarayquality.put(url);

                        if (state.equals("")) {
                            audioUrl = exurl;

                        }
                        initializePlayer();
                        if (binding.playerView != null) {
                            ///   binding.playerView.onResume();
                        }
                    }
                     itag = 18;

                    downloadUrl = sparseArray.get(itag).getUrl();
//                    Constant.logPrint("url"+18, downloadUrl + "mgggg");
                    if (jsonarayquality.length() <= 0 && downloadUrl!="") {
                        url = downloadUrl;
                        jsonarayquality.put(url);

                        initializePlayer();
                        ////state="yes";
                        if (binding.playerView != null) {
                            ///   binding.playerView.onResume();
                        }
                    }



                  ////  jsonarayquality.put(downloadUrl);



//                    Constant.logPrint("sparseArray", sparseArray + "sparseArray");
                   for (int i = 0; i < 5000; i++) {
                        if (sparseArray.get(i) != null) {
                            if (sparseArray.get(i).getUrl() != null && sparseArray.get(i).getUrl()!="") {
                                url = sparseArray.get(i).getUrl();
//                                Constant.logPrint("url"+i, url + "sparseArraysparseArraysparseArraysparseArray");
                                if (jsonarayquality.length() <= 0) {
                                    if(downloadUrl=="") {
                                          initializePlayer();
                                    if (binding.playerView != null) {
                                        binding.playerView.onResume();
                                    }
                                    }
                                }
                                jsonarayquality.put(url);
                            } else {
                                Toast.makeText(PlayerActivity.this, "Video Url Not Supported", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonarayquality.length() >= 4) {
                                break;
                            }
                        }
                    }
                }
//                Log.d(jsonarayquality + "", "jsonarayquality" + jsonarayquality.length());
            }
        };
        mExtractor.extract(mYoutubeLink, true, true);
        // playVideo(mYoutubeLink);
    }
    protected boolean initializePlayer() {
        if (player == null) {
            RenderersFactory renderersFactory =
                    ExoUtilCustom.buildRenderersFactory( context=  this, true);
            MediaSourceFactory mediaSourceFactory =
                    new DefaultMediaSourceFactory(dataSourceFactory)
                            .setAdsLoaderProvider(this::getAdsLoader)
                            .setAdViewProvider(binding.playerView);

            trackSelector = new DefaultTrackSelector( context=  this);
            trackSelector.setParameters(trackSelectorParameters);
            lastSeenTrackGroupArray = null;
            player = new SimpleExoPlayer.Builder( context=  this, renderersFactory)
                    .setMediaSourceFactory(mediaSourceFactory)
                    .setTrackSelector(trackSelector)
                    .build();
            DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(104*1024, 104*1024, 32*1024, 32*1024).createDefaultLoadControl();
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            player.addListener(new PlayerEventListener());
            player.addAnalyticsListener(new EventLogger(trackSelector));
            player.setAudioAttributes(AudioAttributes.DEFAULT,  true);
            player.setPlayWhenReady(startAutoPlay);
            binding.playerView.setPlayer(player);
         /////   player.setBufferDurationsMs
            binding.playerView.setAspectRatioListener(new AspectRatioFrameLayout.AspectRatioListener() {
                @Override
                public void onAspectRatioUpdated(float targetAspectRatio, float naturalAspectRatio, boolean aspectRatioMismatch) {

                }
            });
        }

        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            ////////   player.seekTo(startWindow, startPosition);
        }
//Constant.logPrint(url,"urlurlurlurlurlurlurlurlurlurlurlurl");
        //MediaItem mediaItem = MediaItem.fromUri("https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4");
        MediaItem mediaItem = MediaItem.fromUri(url);

       /// player.setAudioAttributes(AudioAttributes.DEFAULT,true);

        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyApp"));
        ConcatenatingMediaSource concatenatedSource = new ConcatenatingMediaSource();

        MediaSource videoSource =  new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(url));
        MediaSource audioSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(audioUrl));

        concatenatedSource.addMediaSource(new MergingMediaSource(videoSource, audioSource));

     /////   DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(32*1024, 64*1024, 1024, 1024).createDefaultLoadControl();

        player.setMediaSource(concatenatedSource);
        player.prepare();
      ///  player.play();
     /*   binding.selectTracksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackSelectionDialog trackSelectionDialog = TrackSelectionDialog.createForTrackSelector(
                        trackSelector, dismissedDialog -> isShowingTrackSelectionDialog = false);
                trackSelectionDialog.show(getSupportFragmentManager(),  tag=  null);
            }
        });*/
        AspectRatioFrameLayout contentFrame = binding.playerView.findViewById(R.id.exo_content_frame);
        if (getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE) {
            contentFrame.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        } else {
            contentFrame.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
        }
        updateButtonVisibility();
        return true;
    }

    private AdsLoader getAdsLoader(MediaItem.AdsConfiguration adsConfiguration) {
        // The ads loader is reused for multiple playbacks, so that ad playback can resume.
        if (adsLoader == null) {
            adsLoader = new ImaAdsLoader.Builder(/* context= */ this).build();
        }
        adsLoader.setPlayer(player);
        return adsLoader;
    }
    private void showControls() {
        //binding.debugTextView.setVisibility(View.VISIBLE);
    }
    private void showBuffring(boolean b) {
    }
    private void updateButtonVisibility() {
     ///   binding.selectTracksButton.setEnabled(player != null && TrackSelectionDialog.willHaveContent(trackSelector));
    }
    protected void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    private class PlayerEventListener implements Player.EventListener {

        @Override
        public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {

        }


        @Override
        public void onPlaybackStateChanged(@Player.State int playbackState) {
            switch (playbackState) {
                case Player.STATE_ENDED: {
                    showControls();
                    break;
                }
                case Player.STATE_BUFFERING: {
                    showBuffring(true);
                    break;
                }
                case Player.STATE_READY: {
                    showBuffring(false);
                    break;
                }

            }

            updateButtonVisibility();
        }

        @Override
        public void onPlayerError(@NonNull ExoPlaybackException e) {
            if (isBehindLiveWindow(e)) {
                clearStartPosition();
                initializePlayer();
            } else {
                updateButtonVisibility();
                showControls();
            }
        }
        private  boolean isBehindLiveWindow(ExoPlaybackException e) {
            if (e.type != ExoPlaybackException.TYPE_SOURCE) {
                return false;
            }
            Throwable cause = e.getSourceException();
            while (cause != null) {
                if (cause instanceof BehindLiveWindowException) {
                    return true;
                }
                cause = cause.getCause();
            }
            return false;
        }


        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(@NonNull TrackGroupArray trackGroups, @NonNull TrackSelectionArray trackSelections) {
            updateButtonVisibility();
            if (trackGroups != lastSeenTrackGroupArray) {
                MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Constant.setToast(getApplicationContext(),getString(R.string.error_unsupported_video));

                    }
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Constant.setToast(getApplicationContext(),getString(R.string.error_unsupported_audio));
                    }
                }
                lastSeenTrackGroupArray = trackGroups;
            }
        }
    }
    private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {

        @Override
        @NonNull
        public Pair<Integer, String> getErrorMessage(@NonNull ExoPlaybackException e) {
            String errorString = getString(R.string.error_generic);
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                Exception cause = e.getRendererException();
                if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                    // Special case for decoder initialization failures.
                    MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                            (MediaCodecRenderer.DecoderInitializationException) cause;
                    if (decoderInitializationException.codecInfo == null) {
                        if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                            errorString = getString(R.string.error_querying_decoders);
                        } else if (decoderInitializationException.secureDecoderRequired) {
                            errorString =
                                    getString(
                                            R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                        } else {
                            errorString =
                                    getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                        }
                    } else {
                        errorString =
                                getString(
                                        R.string.error_instantiating_decoder,
                                        decoderInitializationException.codecInfo.name);
                    }
                }
            }

            Intent vidcategory = new Intent(getApplicationContext(), YoutubeActivity.class);
            vidcategory.putExtra("video_detail", video_detail+ "");
            startActivity(vidcategory);
            finish();
            return Pair.create(0, errorString);
        }
    }
    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            PlayerActivity.CustomAdapterCat.MainListHolder listHolder = new PlayerActivity.CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final PlayerActivity.CustomAdapterCat.MainListHolder mainHolder = (PlayerActivity.CustomAdapterCat.MainListHolder) holder;


            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("videochat_message"));
                String comment = arrayListchat.getJSONObject(position).getString("videochat_message");
                String reply = arrayListchat.getJSONObject(position).getString("video_chat_reply");
                if (!comment.equals("")) {
                    mainHolder.admincomment.setText(comment);
                }

                if (!reply.equals("")) {
                    mainHolder.usercomment.setText(reply);
                } else {
                    mainHolder.usercomment.setVisibility(View.GONE);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top, title, admincomment, usercomment;
            ImageView video_image;
            CardView card;

            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.txt_des);
                usercomment = view.findViewById(R.id.txt_reply);
            }
        }
    }

}