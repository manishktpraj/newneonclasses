package codexo.neonclasses.ui.player;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import codexo.neonclasses.R;

public class IframeplayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iframeplayer);
    }
}