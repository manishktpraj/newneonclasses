package codexo.neonclasses.ui.webview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityWebBinding;
import codexo.neonclasses.ui.videos.VideosubcategoryActivity;

public class WebActivity extends AppCompatActivity {
    private WebView webView = null;
 String url = "https://www.neonclasses.com";
 //// String url = "https://www.youtube-nocookie.com/embed/Shwm4-Rl-NQ?modestbranding=1&rel=0&gyroscope=1";
  private ActivityWebBinding binding;
    ProgressBar progressBar;
    String title="Neon Classes";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    ///    setContentView(R.layout.activity_web);
       binding =  ActivityWebBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.imgBack.setOnClickListener(view -> finish());
        Constant.adbEnabled(WebActivity.this);
        Constant.checkAdb(WebActivity.this);

        webView = (WebView) findViewById(R.id.webview);
        title =getIntent().getStringExtra("title");
        binding.txtHello.setText(title);
        binding.txtHello.setTypeface(Constant.getFontsBold(WebActivity.this));
         url = getIntent().getStringExtra("url");
        webView.clearHistory();
        if(title.equals("CHAT"))
        {
            binding.headerbox.setVisibility(View.GONE);
        }
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
/*
        String playVideo= "<html><body><iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube-nocookie.com/embed/QKm-SOOMC4c?enablejsapi=1&modestbranding=1&rel=1&loop=1\" style=\"padding-bottom:30px\" frameborder=\"0\" allow=\"accelerometer;  autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe></body></html>";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.loadData(playVideo, "text/html", "utf-8");
        webView.loadUrl("javascript:playVideo()");

        progressBar.setVisibility(View.GONE);*/

     webView.clearFormData();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

//        Constant.logPrint(url,"urlurlurlurlurl");
        webView.loadUrl(url);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);

    }

    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
           if(url.indexOf("neonclasses.com") > -1 || url.indexOf("neonclasses.co.in") > -1) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onBackPressed(){
         this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(WebActivity.this);
        Constant.checkAdb(WebActivity.this);
    }
}