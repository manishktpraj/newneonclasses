package codexo.neonclasses.ui.videos;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityModuleBinding;
import codexo.neonclasses.databinding.ItemModuleBinding;

public class ModuleActivity extends AppCompatActivity {

    private ActivityModuleBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityModuleBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(ModuleActivity.this);
        Constant.checkAdb(ModuleActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());

        List<String> item = new ArrayList<>();
        item.add("image");
        item.add("image");
        item.add("image");
        item.add("image");

        RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(this, item);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(recyclerAdapter);
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemModuleBinding binding;
        Context context;
        List<String> listItem;

        public RecyclerAdapter1(Context context, List<String> listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemModuleBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            String item = listItem.get(position);
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.size();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemModuleBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(ModuleActivity.this);
        Constant.checkAdb(ModuleActivity.this);
    }
}

