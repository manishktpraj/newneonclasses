package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import codexo.neonclasses.Constant;
import codexo.neonclasses.Login;
import codexo.neonclasses.databinding.ActivityFreevideolistBinding;
import codexo.neonclasses.databinding.FreeVideoListBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.player.PlayerActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class FreevideolistAvtivity extends AppCompatActivity {
    ProgressDialog pDialog;
    SessionManager session;
    JSONArray videolist =new JSONArray();
    private ActivityFreevideolistBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFreevideolistBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(FreevideolistAvtivity.this);
        Constant.checkAdb(FreevideolistAvtivity.this);
        binding.imgBack.setOnClickListener(view -> finish());

         binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        pDialog = Constant.getProgressBar(FreevideolistAvtivity.this);
        session = new SessionManager(FreevideolistAvtivity.this);
        freevideolist();
    }
    private void freevideolist() {



        if (!pDialog.isShowing()){
            pDialog.show();
        }
            Call<String> call = Constant.getUrl().freevideolist(session.getUserId());
            call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                                 videolist = jsonObject.getJSONArray("results");
                            FreevideolistAvtivity.RecyclerAdapter2 recyclerAdapter = new FreevideolistAvtivity.RecyclerAdapter2(getApplicationContext(), videolist);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(getApplicationContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                freevideolist();
            }
        });
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<FreevideolistAvtivity.RecyclerAdapter2.RecyclerViewHolder> {
        FreeVideoListBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public FreevideolistAvtivity.RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = FreeVideoListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            FreevideolistAvtivity.RecyclerAdapter2.RecyclerViewHolder holder = new FreevideolistAvtivity.RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }



        @Override
        public void onBindViewHolder(@NonNull FreevideolistAvtivity.RecyclerAdapter2.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("vid_image");
                Constant.setImage(img,binding.image,getApplicationContext());
                binding.txtTitle.setText(item.getString("vid_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(FreevideolistAvtivity.this));
binding.download.setVisibility(View.GONE);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull FreeVideoListBinding itemView) {
                super(itemView.getRoot());
                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                          /*  JSONObject item = listItem.getJSONObject(getBindingAdapterPosition());
                            Intent videoplay = new Intent(getApplicationContext(), PlayerActivity.class);
                            videoplay.putExtra("video_detail",item+"");

                            Constant.logPrint("mggggggg",item+"");
                            startActivity(videoplay);*/


                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            Intent resultIntent = new Intent(Intent.ACTION_VIEW);
                            resultIntent.setData(Uri.parse(item.getString("vid_url")));
                            startActivity(resultIntent);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(FreevideolistAvtivity.this);
        Constant.checkAdb(FreevideolistAvtivity.this);
    }
}