package codexo.neonclasses.ui.videos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.CategoryRowBinding;
import codexo.neonclasses.databinding.FragmentStudyTabBinding;
import codexo.neonclasses.databinding.ItemPopularCoursesBinding;
import codexo.neonclasses.ui.home.HomeViewModel;

public class StudyMaterialTabFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentStudyTabBinding binding;
    JSONArray category_list_new = new JSONArray();
    String category_url_new = "";
    private String allow_test_ = "0";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentStudyTabBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ////binding.txtMath.setOnClickListener(view -> startActivity(new Intent(getContext(), SubCategoryActivity.class)));
        //////   binding.txtEnglish.setOnClickListener(view -> startActivity(new Intent(getContext(), ModuleActivity.class)));

        try {
            category_list_new = new JSONArray(getArguments().getString("study_category_new"));
            category_url_new = getArguments().getString("category_url_new");
            allow_test_ = getArguments().getString("allow_test");
//         Constant.logPrint(allow_test_,"allow_test_allow_test_allow_test_allow_test_");
            StudyMaterialTabFragment.RecyclerAdapter4 recyclerAdapter = new StudyMaterialTabFragment.RecyclerAdapter4(getContext(), category_list_new);
            GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.recyclerCourses.setLayoutManager(layoutManager);
            binding.recyclerCourses.setAdapter(recyclerAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public class RecyclerAdapter4 extends RecyclerView.Adapter<StudyMaterialTabFragment.RecyclerAdapter4.RecyclerViewHolder> {
        CategoryRowBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter4(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public StudyMaterialTabFragment.RecyclerAdapter4.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = CategoryRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            StudyMaterialTabFragment.RecyclerAdapter4.RecyclerViewHolder holder = new StudyMaterialTabFragment.RecyclerAdapter4.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull StudyMaterialTabFragment.RecyclerAdapter4.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = category_url_new + item.getString("preparation_category_image");
                Constant.setImage(img, binding.image, getContext());
                binding.textTitle.setText(item.getString("preparation_category_title"));
                binding.textTitle.setTypeface(Constant.getFontsBold(getContext()));
                if (allow_test_.equals("0")) {
                    binding.imagelock.setVisibility(View.VISIBLE);
                } else {
                    binding.imagelock.setVisibility(View.GONE);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull CategoryRowBinding itemView) {
                super(itemView.getRoot());

                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!allow_test_.equals("0")) {
                            try {
                                JSONObject item = listItem.getJSONObject(getAdapterPosition());
                                JSONArray child = item.getJSONArray("childdata");
                                if (child.length() > 0) {
                                    Intent vidcategory = new Intent(getContext(), StudySubcategoryActivity.class);
                                    vidcategory.putExtra("parent_category", item + "");
                                    startActivity(vidcategory);
                                } else {
                                    Intent vidcategory = new Intent(getContext(), StudymaterialActivity.class);
                                    vidcategory.putExtra("parent_category", item + "");
                                    startActivity(vidcategory);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        }
    }

}
