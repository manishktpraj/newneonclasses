package codexo.neonclasses.ui.videos;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentVideoTabBinding;
import codexo.neonclasses.databinding.ItemCategoryHorizontalBinding;
import codexo.neonclasses.databinding.ItemPopularCoursesBinding;
import codexo.neonclasses.gpexoplayer.NewVideoPlayer;
import codexo.neonclasses.gpyoutubeplayer.Player3;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.courses.CourseDetailActivity;
import codexo.neonclasses.ui.courses.CoursesActivity;
import codexo.neonclasses.ui.home.HomeViewModel;
import codexo.neonclasses.ui.player.OfflineVideoPlayerActivity;
import codexo.neonclasses.ui.player.PlayerActivity;
import codexo.neonclasses.ui.youtube.YoutubeActivity;
import codexo.neonclasses.ui.youtube.YoutubeActivity2;
import retrofit2.Call;
import retrofit2.Callback;

public class VideoTabFragment extends Fragment implements RecyclerAdapter2.ClickListener {

    private HomeViewModel homeViewModel;
    private FragmentVideoTabBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray free_video = new JSONArray();
    JSONArray recent_video = new JSONArray();
    JSONArray live_video = new JSONArray();
    JSONArray video_course = new JSONArray();
    JSONArray video_category = new JSONArray();
    String category_url = "", package_url = "";
    JSONArray study_category = new JSONArray();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentVideoTabBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());
        /// init();
        binding.videoCourseClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CoursesActivity.class));

            }
        });
        binding.viewAllFreevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), FreevideolistAvtivity.class));

            }
        });

        try {
            free_video = new JSONArray(getArguments().getString("free_video"));
            if (free_video.length() == 0) {
                binding.freevideolayout.setVisibility(View.GONE);
            } else {
                binding.freevideolayout.setVisibility(View.VISIBLE);

            }
            video_course = new JSONArray(getArguments().getString("video_course"));
            recent_video = new JSONArray(getArguments().getString("recent_video"));
            live_video = new JSONArray(getArguments().getString("live_video"));
            video_category = new JSONArray(getArguments().getString("video_category"));
            category_url = getArguments().getString("category_url");
            package_url = getArguments().getString("package_url");
            init();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        binding.viewalllivevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent liveintent = new Intent(getContext(), AllLiveClasses.class);
                liveintent.putExtra("live", live_video + "");
                startActivity(liveintent);
            }
        });


        return root;
    }

    private void init() {


        if (video_course.length() > 0) {

            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(getContext(), video_course);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            binding.recyclerCourses.setLayoutManager(layoutManager);
            binding.recyclerCourses.setAdapter(recyclerAdapter);

        } else {
            binding.videoCourseHead.setVisibility(View.GONE);
            binding.recyclerCourses.setVisibility(View.GONE);

        }
        binding.livevideohead.setVisibility(View.VISIBLE);
        binding.recentvideohead.setVisibility(View.VISIBLE);
        binding.videoCategories.setVisibility(View.VISIBLE);
        int abc = 0;
        if (live_video.length() <= 0) {
            binding.livevideohead.setVisibility(View.GONE);
            abc = 1;
        }
        if (recent_video.length() <= 0) {
            binding.recentvideohead.setVisibility(View.GONE);
            abc++;
        }
        if (video_category.length() <= 0) {
            binding.videoCategories.setVisibility(View.GONE);
            abc++;
        }
        if (abc == 3) {
            binding.commonhide.setVisibility(View.GONE);
        } else {
            binding.commonhide.setVisibility(View.VISIBLE);

        }
        RecyclerAdapter2 recyclerAdapter2 = new RecyclerAdapter2(getContext(), free_video, 0, "free_video");
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getContext());
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        recyclerAdapter2.setClickListener(this);
        binding.recyclerFreeVideos.setLayoutManager(layoutManager2);
        binding.recyclerFreeVideos.setAdapter(recyclerAdapter2);


        RecyclerAdapter2 recyclerAdapter3 = new RecyclerAdapter2(getContext(), live_video, 1, "live_video");
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(getContext());
        layoutManager3.setOrientation(RecyclerView.HORIZONTAL);
        recyclerAdapter3.setClickListener(this);
        binding.recyclerLiveVideos.setLayoutManager(layoutManager3);
        binding.recyclerLiveVideos.setAdapter(recyclerAdapter3);


        RecyclerAdapter2 recyclerAdapter4 = new RecyclerAdapter2(getContext(), recent_video, 2, "recent_video");
        LinearLayoutManager layoutManager4 = new LinearLayoutManager(getContext());
        layoutManager4.setOrientation(RecyclerView.HORIZONTAL);
        recyclerAdapter4.setClickListener(this);
        binding.recyclerRecentVideos.setLayoutManager(layoutManager4);
        binding.recyclerRecentVideos.setAdapter(recyclerAdapter4);


        RecyclerAdapter7 recyclerAdapter7 = new RecyclerAdapter7(getContext(), video_category);
        LinearLayoutManager layoutManager7 = new LinearLayoutManager(getContext());
        layoutManager7.setOrientation(RecyclerView.VERTICAL);
//        recyclerAdapter7.setClickListener(this);
        binding.videoCategoriesRecycler.setLayoutManager(layoutManager7);
        binding.videoCategoriesRecycler.setAdapter(recyclerAdapter7);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onClickItem(int post, int type) throws JSONException {
        /////  startActivity(new Intent(getContext(), PlayerActivity.class));
        ///   Constant.logPrint("testettttttttttttttttttttt",post+"");
        ////   Constant.logPrint("testettttttttttttttttttttt",free_video+"");
        if (Constant.player_setting.equals("0")) {

            if (type == 0) {
                Intent vidcategory = new Intent(getContext(), PlayerActivity.class);
                vidcategory.putExtra("video_detail", free_video.getJSONObject(post) + "");
                startActivity(vidcategory);

            }
            if (type == 1) {
                Intent vidcategory = new Intent(getContext(), YoutubeActivity.class);
                vidcategory.putExtra("video_detail", live_video.getJSONObject(post) + "");
                startActivity(vidcategory);
            }
            /*if (type == 1) {
                Intent vidcategory = new Intent(getContext(), Player3.class);
                vidcategory.putExtra("video_detail", live_video.getJSONObject(post) + "");
                startActivity(vidcategory);
            }*/
            if (type == 2) {
              /*  Intent vidcategory = new Intent(getContext(), PlayerActivity.class);
                vidcategory.putExtra("video_detail", recent_video.getJSONObject(post) + "");
                startActivity(vidcategory);*/

                final Dialog builder = new Dialog(requireContext());
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_player_change, null);
                Button player_1 = dialogLayout.findViewById(R.id.player_1);
                Button player_2 = dialogLayout.findViewById(R.id.player_2);
                Button player_3 = dialogLayout.findViewById(R.id.player_3);
                ImageView cancel_btn = dialogLayout.findViewById(R.id.cancel_btn);
                builder.setContentView(dialogLayout);
                builder.setCancelable(true);
                builder.show();
                player_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent videoplay = new Intent(requireContext(), YoutubeActivity2.class);
//                          Intent videoplay = new Intent(getApplicationContext(), PlayerActivity.class);
                            videoplay.putExtra("video_detail", recent_video.getJSONObject(post) + "");
                            startActivity(videoplay);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        builder.dismiss();
                    }
                });
                player_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            Intent webPlayer = new Intent(requireContext(), NewVideoPlayer.class);
                            webPlayer.putExtra("video_detail", recent_video.getJSONObject(post) + "");
                            startActivity(webPlayer);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        builder.dismiss();
                    }
                });
                player_3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent webPlayer = new Intent(requireContext(), Player3.class);
                            webPlayer.putExtra("video_detail", recent_video.getJSONObject(post) + "");
                            startActivity(webPlayer);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        builder.dismiss();
                    }
                });
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

            }


        } else {

            if (type == 0) {
                Intent vidcategory = new Intent(getContext(), YoutubeActivity.class);
                vidcategory.putExtra("video_detail", free_video.getJSONObject(post) + "");
                startActivity(vidcategory);

            }
            if (type == 1) {
                Intent vidcategory = new Intent(getContext(), YoutubeActivity.class);
                vidcategory.putExtra("video_detail", live_video.getJSONObject(post) + "");
                startActivity(vidcategory);
            }
            if (type == 2) {
                Intent vidcategory = new Intent(getContext(), YoutubeActivity.class);
                vidcategory.putExtra("video_detail", recent_video.getJSONObject(post) + "");
                startActivity(vidcategory);

            }


        }

    }


    /**
     * Create these is separate files later as per your need
     */
    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemPopularCoursesBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemPopularCoursesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = package_url + item.getString("sub_pack_image");
                Constant.setImage(img, binding.image, getContext());
                binding.txtTitle.setText(item.getString("sub_pack_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));


                binding.txtDiscountedPrice.setText("₹" + item.getString("sub_pack_price"));
                binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setText("₹" + item.getString("sub_actual_price"));
                binding.txtPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

                String disperc = Constant.getdiscount(item.getString("sub_actual_price"), item.getString("sub_pack_price"));
//                Constant.logPrint(disperc + "", "dispercdispercdispercdispercdisperc");
                if (!disperc.equals("")) {
                    binding.txtDiscount.setText(disperc + "%");
                    binding.txtDiscount.setTypeface(Constant.getFontsBold(getContext()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemPopularCoursesBinding itemView) {
                super(itemView.getRoot());
                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());

                            Intent vidcategory = new Intent(getContext(), CourseDetailActivity.class);
                            vidcategory.putExtra("course_detail", item + "");
                            startActivity(vidcategory);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                String s = "₹10000";
                itemView.txtPrice.setText(s, TextView.BufferType.SPANNABLE);
                Spannable spannable = (Spannable) itemView.txtPrice.getText();
                spannable.setSpan(STRIKE_THROUGH_SPAN, 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    public class RecyclerAdapter7 extends RecyclerView.Adapter<RecyclerAdapter7.RecyclerViewHolder> {
        ItemCategoryHorizontalBinding binding_data;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter7(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter7.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding_data = ItemCategoryHorizontalBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter7.RecyclerViewHolder holder = new RecyclerAdapter7.RecyclerViewHolder(binding_data);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter7.RecyclerViewHolder holder, int position) {

            if (position == 0) {
                binding_data.topspace.setVisibility(View.VISIBLE);
            } else {
                binding_data.topspace.setVisibility(View.GONE);

            }
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("bcat_image");
                Constant.setImage(img, binding_data.image, getContext());


                binding_data.txtTitle.setText(item.getString("bcat_name"));
                binding_data.txtTitle.setTypeface(Constant.getFontsBold(context.getApplicationContext()));

                binding_data.txtDes.setText(item.getString("bcat_description"));
                binding_data.txtDes.setTypeface(Constant.getFonts(context.getApplicationContext()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

          /*  View vd = null;
            vd = binding.videoCategoriesRecycler.getChildAt(position).findViewById(R.id.childViewLl);
            vd.setVisibility((vd.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);*/
//            binding_data.childViewLl.setVisibility(false ? View.VISIBLE : View.GONE);
//            binding_data.childViewLl.setVisibility(false ? View.VISIBLE : View.GONE);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            public RecyclerViewHolder(@NonNull ItemCategoryHorizontalBinding itemView) {
                super(itemView.getRoot());

                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            JSONArray child = item.getJSONArray("child");
                            if (child.length() > 0) {
//                                String parent_id = item.getString("bcat_id");
//                                getvideosubcategory(binding_data.childRv,parent_id);
//                                binding_data.childViewLl.setVisibility(true ? View.VISIBLE : View.GONE);
//                                notifyItemChanged(getAdapterPosition());
                                Intent vidcategory = new Intent(getContext(), VideosubcategoryActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                vidcategory.putExtra("course_url", package_url + "");
                                startActivity(vidcategory);
                            } else {
                                Intent vidcategory = new Intent(getContext(), CategoryVideosActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                startActivity(vidcategory);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }


        }
    }

    private void getvideosubcategory(RecyclerView recyclerSubCategory, String parent_id) {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getvideosubcategory(session.getUserId(), parent_id);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            video_category = jsonObject.getJSONArray("results");
                            RecyclerAdapter7 recyclerAdapter4 = new RecyclerAdapter7(requireContext(), video_category);
                            LinearLayoutManager layoutManager4 = new LinearLayoutManager(requireContext());
                            layoutManager4.setOrientation(RecyclerView.VERTICAL);
                            recyclerSubCategory.setLayoutManager(layoutManager4);
                            recyclerSubCategory.setAdapter(recyclerAdapter4);
                        } else {
                            Constant.setToast(requireContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getvideosubcategory(recyclerSubCategory, parent_id);
            }
        });
    }

}
