package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityStudymaterialBinding;
import codexo.neonclasses.databinding.ItemStudymaterialMaterialRowBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.DownloadStatus;
import codexo.neonclasses.ui.download.YourService;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class StudymaterialActivity extends AppCompatActivity {
    ActivityStudymaterialBinding bindingparent;
    ProgressDialog pDialog;
    SessionManager session;
    JSONObject parent_category;
    String parent_id = "";
    JSONArray videolist = new JSONArray();
    String study_url = "", URL = "";
    private BroadcastReceiver mMessageReceiver;
    String percent = "0";
    boolean isCompleted = false;
    MutableLiveData<String> getPercent = new MutableLiveData<>();
    MutableLiveData<Boolean> isCompletedDownload = new MutableLiveData<>();
    private int mainPostion = 0;
    RecyclerAdapter2 recyclerAdapter;
    private String IDS = "0";
    private DatabaseRepository repository;
    Context context;
    private final OkHttpClient nclient = new OkHttpClient();
    private long startTime;
    private long endTime;
    private long fileSize;
    int kilobytePerSec = 0;
    double speed_data;
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studymaterial);
        bindingparent = ActivityStudymaterialBinding.inflate(getLayoutInflater());
        setContentView(bindingparent.getRoot());
        Constant.adbEnabled(StudymaterialActivity.this);
        Constant.checkAdb(StudymaterialActivity.this);
        bindingparent.imgBack.setOnClickListener(view -> finish());
        pDialog = Constant.getProgressBar(StudymaterialActivity.this);
        session = new SessionManager(StudymaterialActivity.this);
        repository = new DatabaseRepository(StudymaterialActivity.this);
        context = getApplicationContext();
     /*   mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                percent = intent.getStringExtra("progress");
                mainPostion = intent.getIntExtra("pos",0);
                IDS = intent.getStringExtra("pd_id");
                isCompleted = intent.getBooleanExtra("isCompleted", false);
                getPercent.postValue(percent);
                isCompletedDownload.postValue(isCompleted);

                if(isCompleted) {
                    if (recyclerAdapter != null) {
                      recyclerAdapter.notifyDataSetChanged();

                     }

                }

                Constant.logPrint("isCompletedDownloadisCompletedDownloadisCompletedDownload",getPercent+"");
                /////    recyclerAdapter.notifyDataSetChanged();
            }
        };
*/
        try {
            parent_category = new JSONObject(getIntent().getStringExtra("parent_category"));
            bindingparent.txtHello.setText(parent_category.getString("preparation_category_title") + " Study Material");
            bindingparent.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
            parent_id = parent_category.getString("preparation_category_id");
            categorywisestudy();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void categorywisestudy() {
        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().categorywisestudy(session.getUserId(), parent_id);
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            videolist = jsonObject.getJSONArray("results");
                            study_url = jsonObject.getString("study_url");
                            if (videolist.length() <= 0) {
                                bindingparent.emptyView.setVisibility(View.VISIBLE);
                                bindingparent.recyclerStudymaterial.setVisibility(View.GONE);
                            } else {
                                bindingparent.emptyView.setVisibility(View.GONE);
                                bindingparent.recyclerStudymaterial.setVisibility(View.VISIBLE);
                                recyclerAdapter = new RecyclerAdapter2(getApplicationContext(), videolist);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
                                bindingparent.recyclerStudymaterial.setLayoutManager(layoutManager);
                                bindingparent.recyclerStudymaterial.setAdapter(recyclerAdapter);
                            }

                        } else {
                            Constant.setToast(getApplicationContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                categorywisestudy();
            }
        });
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
        ItemStudymaterialMaterialRowBinding binding;
        Context context;
        JSONArray listItem;
        List<File> fileItem = new ArrayList<>();

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemStudymaterialMaterialRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter2.RecyclerViewHolder holder = new RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter2.RecyclerViewHolder holder, int position) {

            try {
                JSONObject item = listItem.getJSONObject(position);
                String id = item.getString("pd_id");
                binding.txtTitles.setText(item.getString("pd_title"));
                binding.txtTitles.setTypeface(Constant.getFontsBold(StudymaterialActivity.this));
                if (position == 0) {
                    binding.topspace.setVisibility(View.VISIBLE);
                } else {
                    binding.topspace.setVisibility(View.GONE);
                }


                String file_name = item.getString("pd_title");
                String filePath = DownloadSupport.getPdfDirectory(getApplicationContext(), new SessionManager(context)) + "/" + file_name + ".pdf";
                Constant.logPrint(filePath, "filePath");
                File file = new File(filePath);

                final Boolean[] status = {file.exists()};
                if (status[0]) {
                    binding.imgDownload.setImageResource(R.drawable.pdf_ico);
                    binding.imgDownload.setMaxHeight(18);
                    binding.imgDownload.setMaxWidth(42);

                } else {
                    binding.imgDownload.setImageResource(R.drawable.downloadicon);
                    binding.imgDownload.setMaxHeight(18);
                    binding.imgDownload.setMaxWidth(42);
                }

               /* Constant.logPrint(id,"ididididid");
                if(id.equals(IDS)){
                    getPercent.observe(StudymaterialActivity.this, new Observer<String>() {
                        @Override
                        public void onChanged(String s) {
                            if(s!=null && !s.isEmpty()){
                         ///    binding.progressBar.setProgress(Integer.parseInt(s));
                            }

                        }
                    });
                    isCompletedDownload.observe(StudymaterialActivity.this, new Observer<Boolean>() {
                        @Override
                        public void onChanged(Boolean isCompletedDownload) {
                            if (isCompletedDownload) {
                                binding.imgDownload.setImageResource(R.drawable.pdf_ico);
                                binding.imgDownload.setMaxHeight(18);
                                binding.imgDownload.setMaxWidth(42);
                            }
                        }
                    });
                }*/


                binding.imgDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            JSONObject item = listItem.getJSONObject(position);
                            String pdfurl = item.getString("pd_file");
                            URL = study_url + item.getString("pd_file");
                            status[0] = file.exists();
                            if (status[0]) {
                                startActivity(new Intent(context, PdfViewer.class).putExtra("file", filePath));
                            } else {
                                Constant.logPrint(URL, "URLURLURLURLURL");
                                customalert(item);

                                /*  if (!pdfurl.equals("")) {

                                 *//*    Intent intent = new Intent(context, DownloadableServices.class);
                                    intent.putExtra(DownloadableServices.FILENAME, item.getString("pd_title"));
                                    intent.putExtra(DownloadableServices.urlpath, URL);
                                    intent.putExtra(DownloadableServices.TYPE, "pdf");
                                    intent.putExtra(DownloadableServices.ID, item.getString("pd_id"));
                                    intent.putExtra("pos", position);
                                    intent.putExtra("pd_id", item.getString("pd_id"));
                                    startService(intent);
                                    Toast.makeText(getApplicationContext(), "Downloading....", Toast.LENGTH_SHORT).show();*//*
                                } else {
                                    Constant.setToast(getApplicationContext(), "Donwload Not Available");
                                }*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

            /*    if (fileItem != null && fileItem.size() > 0) {
                    for (int i = 0; i < fileItem.size(); i++) {
                        String name=fileItem.get(i).getName();
                        if (name.equalsIgnoreCase(item.getString("pd_title")+".pdf")) {
                            binding.imgDownload.setImageResource(R.drawable.pdf_ico);
                            binding.imgDownload.setMaxHeight(18);
                            binding.imgDownload.setMaxWidth(42);

                            int finalI = i;
                            binding.imgDownload.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(context, PdfViewer.class).putExtra("file",fileItem.get(finalI).getAbsolutePath()));
                                }
                            });
                            break;
                        }else{
                            binding.imgDownload.setImageResource(R.drawable.downloadicon);
                            binding.imgDownload.setMaxHeight(18);
                            binding.imgDownload.setMaxWidth(42);
                            binding.imgDownload.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    try {
                                        JSONObject item = listItem.getJSONObject(position);
                                        String pdfurl = item.getString("pd_file");
                                        URL =study_url+item.getString("pd_file");

                                        Constant.logPrint(URL,"URLURLURLURLURL");
                                        if (!pdfurl.equals("")) {

                                            Intent intent = new Intent(context, DownloadableServices.class);
                                            // add infos for the service which file to download and where to store
                                            intent.putExtra(DownloadableServices.FILENAME, item.getString("pd_title"));

                                            intent.putExtra(DownloadableServices.urlpath, URL);
                                            intent.putExtra(DownloadableServices.TYPE, "pdf");
                                            intent.putExtra(DownloadableServices.ID, item.getString("pd_id"));
                                            //  startActivity(intent);
                                            startService(intent);

                                            Toast.makeText(getApplicationContext(), "Downloading....", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Constant.setToast(getApplicationContext(), "Donwload Not Available");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });


                        }
                    }
                }else{
                    binding.imgDownload.setImageResource(R.drawable.downloadicon);
                    binding.imgDownload.setMaxHeight(16);
                    binding.imgDownload.setMaxWidth(42);

                    binding.imgDownload.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                JSONObject item = listItem.getJSONObject(position);
                                String pdfurl = item.getString("pd_file");
                                URL =study_url+item.getString("pd_file");

                                Constant.logPrint(URL,"URLURLURLURLURL");
                                if (!pdfurl.equals("")) {
                                    Intent intent = new Intent(context, DownloadableServices.class);
                                    // add infos for the service which file to download and where to store
                                    intent.putExtra(DownloadableServices.FILENAME, item.getString("pd_title"));

                                    intent.putExtra(DownloadableServices.urlpath, URL);
                                    intent.putExtra(DownloadableServices.TYPE, "pdf");
                                    intent.putExtra(DownloadableServices.ID, item.getString("pd_id"));
                                    //  startActivity(intent);
                                    startService(intent);
                                    Toast.makeText(getApplicationContext(), "Downloading....", Toast.LENGTH_SHORT).show();
                                } else {
                                    Constant.setToast(getApplicationContext(), "Donwload Not Available");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                }*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemStudymaterialMaterialRowBinding itemView) {
                super(itemView.getRoot());
                itemView.imgDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        }
    }


    public void customalert(JSONObject item) {


        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(StudymaterialActivity.this);
        bottomSheetDialog.setContentView(R.layout.download_layout);
        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
        TextView txt_title = bottomSheetDialog.findViewById(R.id.txt_title);


        tv_go_inside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canceldownload(bottomSheetDialog, item);
            }
        });

        try {
            txt_title.setText(item.getString("pd_title"));
            setindatabase(item, bottomSheetDialog);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();

    }

    public void setindatabase(JSONObject item, BottomSheetDialog bottomSheetDialog) {
        String fileName = null;
        try {
            fileName = item.getString("pd_title");
            File videofile = new File(DownloadSupport.getPdfDirectory(getApplicationContext(), session) + fileName + ".pdf");
            String Livepath = study_url + item.getString("pd_file");
            DownloadFile downloadFile = new DownloadFile();
            downloadFile.setId(item.getString("pd_id"));
            downloadFile.setDownloadId(item.getString("pd_id"));
            downloadFile.setCurrentBytes(0);
            downloadFile.setTotalBytes(0);
            downloadFile.setFiletype("1");
            downloadFile.setFileName(fileName);
            downloadFile.setDownloadStatus(DownloadStatus.START);
            downloadFile.setFilepath(DownloadSupport.getPdfDirectory(getApplicationContext(), session));
            downloadFile.setThumbPath("");
            Long startTime = System.currentTimeMillis();
            downloadFile.setStartTime(startTime);
            downloadFile.setVideoUrlOffline(Livepath);
            repository.Insert(downloadFile);


            Intent intent = new Intent(getApplicationContext(), YourService.class);
            // add infos for the service which file to download and where to store
            intent.putExtra(YourService.FILENAME, downloadFile.getFileName());
            //  intent.putExtra(YourService.urlpath, url);
            intent.putExtra(YourService.urlpath, downloadFile.getVideoUrlOffline());
            intent.putExtra(YourService.TYPE, "1");
            intent.putExtra(YourService.VIDEO_ID, downloadFile.getId());
            getApplicationContext().startService(intent);

            TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);

            view_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), PdfViewer.class).putExtra("file", downloadFile.getFilepath() + "/" + downloadFile.getFileName() + ".pdf"));
                    recyclerAdapter.notifyDataSetChanged();
                    bottomSheetDialog.setCancelable(true);
                    bottomSheetDialog.dismiss();
                }
            });

            setobserver(bottomSheetDialog, item.getString("pd_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void setobserver(BottomSheetDialog bottomSheetDialog, String downloadFile) {

        ProgressBar pg = bottomSheetDialog.findViewById(R.id.progressBar);
        TextView percent = bottomSheetDialog.findViewById(R.id.percent);
        TextView minuteleft = bottomSheetDialog.findViewById(R.id.minuteleft);
        TextView speed = bottomSheetDialog.findViewById(R.id.speed);
        TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
        Constant.logPrint(downloadFile, "downloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFile");
        downloadInfo();

        repository.getDownloadTable(downloadFile, "1").observe((StudymaterialActivity.this), new Observer<DownloadFile>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onChanged(DownloadFile file) {
                if (file != null) {
                    if (!file.isDownloadComplete()) {
                        downloadInfo();
                        view_file.setVisibility(View.GONE);
                        tv_go_inside.setVisibility(View.VISIBLE);
                        try {
                            pg.setProgress((int) Integer.parseInt(file.getPercentage()));
                            percent.setText(file.getPercentage() + "%");
                            minuteleft.setText(file.getTimeremain() + " s left");
                            speed.setText(roundTwoDecimals(Double.parseDouble(kilobytePerSec + "")) + " KB/S");

                        } catch (Exception e) {

                        }
                    } else {
                        pg.setProgress((int) 100);
                        percent.setText(100 + "%");
                        minuteleft.setText(0 + " s left");
                        view_file.setVisibility(View.VISIBLE);
                        tv_go_inside.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    private void canceldownload(BottomSheetDialog bottomSheetDialog, JSONObject item) {
        String fileName = null;
        try {
            fileName = item.getString("pd_title");
            new File(DownloadSupport.getPdfDirectory(getApplicationContext(), session) + fileName + ".pdf").delete();
            repository.Delete(item.getString("pd_id"), "1");
            bottomSheetDialog.setCancelable(true);
            bottomSheetDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    Double roundTwoDecimals(Double vLongValue) {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        return Double.valueOf(twoDForm.format(vLongValue));
    }

    private void downloadInfo() {
//        Log.d(TAG, "downloadInfo: iN downloadInfo");
        Request request = new Request.Builder()
                .url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png") // replace image url
                .build();
        startTime = System.currentTimeMillis();
        nclient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
//                Log.d(TAG, "downloadInfo: iN failure");
                //check when device is connected to Router but there is no internet

            }

            @Override
            public void onResponse(okhttp3.Call call, Response response) throws IOException {
//                Log.d(TAG, "downloadInfo: iN success");
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }
                InputStream input = response.body().byteStream();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) != -1) {
                        bos.write(buffer);
                    }
                    byte[] docBuffer = bos.toByteArray();
                    fileSize = bos.size();
                } finally {
                    input.close();
                }
                endTime = System.currentTimeMillis();

                // calculate how long it took by subtracting endtime from starttime
                final double timeTakenMills = Math.floor(endTime - startTime);  // time taken in milliseconds
                final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                int ds = (int) Math.round(1024 / timeTakenInSecs);
                if (ds > 0) {
                    kilobytePerSec = ds / 1000;
                }
                speed_data = Math.round(fileSize / timeTakenMills);
//                Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
//                Log.d(TAG, "Kb per sec: " + kilobytePerSec);
//                Log.d(TAG, "Download Speed: " + speed_data);
//                Log.d(TAG, "File size in kb: " + fileSize);
                // update the UI with the speed test results
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(StudymaterialActivity.this);
        Constant.checkAdb(StudymaterialActivity.this);
    }
}