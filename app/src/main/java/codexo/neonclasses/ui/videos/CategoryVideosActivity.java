package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCategoryVideosBinding;
import codexo.neonclasses.databinding.FreeVideoListBinding;
import codexo.neonclasses.gpexoplayer.NewVideoPlayer;
import codexo.neonclasses.gpyoutubeplayer.Player3;
import codexo.neonclasses.service.DownloadableServices;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.DownloadStatus;
import codexo.neonclasses.ui.download.YourService;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.jwplayer.PlayerJW;
import codexo.neonclasses.ui.player.OfflineVideoPlayerActivity;
import codexo.neonclasses.ui.player.PlayerActivity;
import codexo.neonclasses.ui.player.WebuiPlayer;
import codexo.neonclasses.ui.youtube.YoutubeActivity;
import codexo.neonclasses.ui.youtube.YoutubeActivity2;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class CategoryVideosActivity extends AppCompatActivity {
    ActivityCategoryVideosBinding bindingparent;
    ProgressDialog pDialog;
    SessionManager session;
    JSONObject parent_category;
    String parent_id = "", desparent_id = "", jsonparent_id = "";
    JSONArray videolist = new JSONArray();
    private BroadcastReceiver mMessageReceiver;
    private DatabaseRepository repository;
    Context context;
    RecyclerAdapter2 recyclerAdapter;
    private final OkHttpClient nclient = new OkHttpClient();
    private long startTime;
    private long endTime;
    private long fileSize;
    int kilobytePerSec = 0;
    double speed_data;
    private static int SPLASH_TIME_OUT = 10000;

    /* @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mMessageReceiver,new IntentFilter("DownloadedVideo"));
    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingparent = ActivityCategoryVideosBinding.inflate(getLayoutInflater());
        setContentView(bindingparent.getRoot());

        Constant.adbEnabled(CategoryVideosActivity.this);
        Constant.checkAdb(CategoryVideosActivity.this);
        bindingparent.imgBack.setOnClickListener(view -> finish());
        pDialog = Constant.getProgressBar(CategoryVideosActivity.this);
        session = new SessionManager(CategoryVideosActivity.this);
        repository = new DatabaseRepository(CategoryVideosActivity.this);
        context = getApplicationContext();
  /* mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Extract data included in the Intent
               *//* String message = intent.getAction();
                categorywisevideo();*//*
            }
        };*/

        try {
            parent_category = new JSONObject(getIntent().getStringExtra("parent_category"));
            bindingparent.txtHello.setText(parent_category.getString("bcat_name"));
            bindingparent.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
            parent_id = parent_category.getString("bcat_id");
//            Constant.logPrint("parent_id123456", parent_id + "++++++" + session.getUserId() + "\n" + parent_category);
            categorywisevideo();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

    }

    /*  @Override
      public void onStop() {
          super.onStop();
          unregisterReceiver(mMessageReceiver);
      }*/
    private void categorywisevideo() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().categorywisevideo(session.getUserId(), parent_id);
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            videolist = jsonObject.getJSONArray("results");
                            if (videolist.length() <= 0) {
                                bindingparent.emptyView.setVisibility(View.VISIBLE);
                                bindingparent.recycler.setVisibility(View.GONE);
                            } else {
                                bindingparent.emptyView.setVisibility(View.GONE);
                                bindingparent.recycler.setVisibility(View.VISIBLE);
                                recyclerAdapter = new RecyclerAdapter2(getApplicationContext(), videolist);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
                                bindingparent.recycler.setLayoutManager(layoutManager);
                                bindingparent.recycler.setAdapter(recyclerAdapter);
                            }

                        } else {
                            Constant.setToast(getApplicationContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                categorywisevideo();
            }
        });
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
        FreeVideoListBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = FreeVideoListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter2.RecyclerViewHolder holder = new RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter2.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("vid_image");
                //Constant.setImage(img,binding.image);
                Glide.with(CategoryVideosActivity.this).load(img).into(binding.image);
                binding.txtTitle.setText(item.getString("vid_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(CategoryVideosActivity.this));


                JSONArray videolist = item.getJSONArray("neon_video_detail");
                String filePath = "";
                Boolean statusplayoffline = false;
                if (videolist.length() == 1) {
                    String fileName = item.getString("vid_title");
                    String url = videolist.getJSONObject(0).getString("vidd_url");
                    fileName = url.substring(url.lastIndexOf('/') + 1);

                    filePath = DownloadSupport.getVideoDirectory(getApplicationContext(), new SessionManager(context)) + "/" + fileName;
//                    Constant.logPrint(filePath, "filePath");
                    File videofile = new File(filePath);
//                    Constant.logPrint(videofile.exists() + "", "filePath");
                    if (videofile.exists()) {
                        statusplayoffline = true;
                        ///  binding.txtDownload.setText("View");
                        binding.download.setImageResource(R.drawable.playbutton);
                    } else {
                        binding.download.setImageResource(R.drawable.downloadicon);
                        //// binding.txtDownload.setBackground(getResources().getDrawable(R.drawable.corner_gray));
                    }
                }

                Boolean finalStatusplayoffline = statusplayoffline;
                String finalFilePath = filePath;
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                /*        try {
                            if (!finalStatusplayoffline) {
                                JSONObject item = listItem.getJSONObject(position);
                                Intent videoplay = new Intent(getApplicationContext(), NewVideoPlayer.class);
                                videoplay.putExtra("video_detail", item + "");
                                startActivity(videoplay);
                            } else {
                                Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                videoplay.putExtra("video_detail", finalFilePath);
                                startActivity(videoplay);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                        final Dialog builder = new Dialog(CategoryVideosActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogLayout = inflater.inflate(R.layout.dialog_player_change, null);
                        Button player_1 = dialogLayout.findViewById(R.id.player_1);
                        Button player_2 = dialogLayout.findViewById(R.id.player_2);
                        Button player_3 = dialogLayout.findViewById(R.id.player_3);
                        ImageView cancel_btn = dialogLayout.findViewById(R.id.cancel_btn);
                        builder.setContentView(dialogLayout);
                        builder.setCancelable(true);
                        builder.show();
                        player_1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    if (!finalStatusplayoffline) {
                                        if (Constant.player_setting.equals("0")) {
                                            JSONObject item = listItem.getJSONObject(position);
                                            Intent videoplay = new Intent(getApplicationContext(), YoutubeActivity2.class);
//                                            Intent videoplay = new Intent(getApplicationContext(), PlayerActivity.class);
                                            videoplay.putExtra("video_detail", item + "");
                                            startActivity(videoplay);
                                        } else {
                                            JSONObject item = listItem.getJSONObject(position);
                                            Intent videoplay = new Intent(getApplicationContext(), YoutubeActivity.class);
                                            videoplay.putExtra("video_detail", item + "");
                                            startActivity(videoplay);
                                        }
                                    } else {
                                        Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                        videoplay.putExtra("video_detail", finalFilePath);
                                        startActivity(videoplay);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                builder.dismiss();
                            }
                        });
                        player_2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    if (!finalStatusplayoffline) {
                                        JSONObject item = listItem.getJSONObject(position);
                                        Intent webPlayer = new Intent(getApplicationContext(), NewVideoPlayer.class);
                                        webPlayer.putExtra("video_detail", item + "");
                                        startActivity(webPlayer);
                                    } else {
                                        Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                        videoplay.putExtra("video_detail", finalFilePath);
                                        startActivity(videoplay);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                builder.dismiss();
                            }
                        });
                        player_3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    if (!finalStatusplayoffline) {
                                        JSONObject item = listItem.getJSONObject(position);
                                        Intent newPlayer = new Intent(getApplicationContext(), Player3.class);
                                        newPlayer.putExtra("video_detail", item + "");
                                        startActivity(newPlayer);
                                    } else {
                                        Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                        videoplay.putExtra("video_detail", finalFilePath);
                                        startActivity(videoplay);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                builder.dismiss();
                            }
                        });

                        cancel_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                builder.dismiss();
                            }
                        });
                    }
                });
                binding.download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pDialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pDialog.dismiss();
                                try {
                                    JSONArray videolist = item.getJSONArray("neon_video_detail");
                                    if (videolist.length() > 1) {
                                        pDialog.dismiss();
                                        Intent vidcategory = new Intent(getApplicationContext(), VideoPartListActivity.class);
                                        vidcategory.putExtra("parent_category", item + "");
                                        startActivity(vidcategory);
                                    } else if (videolist.length() > 0) {
                                        String fileName = item.getString("vid_title");
                                        String urls = videolist.getJSONObject(0).getString("vidd_url");
//                                        Constant.logPrint("urlsurlsurlsurlsurlsurlsurlsurlsurls", urls + exists(urls) + "");
                                        if (!urls.equals("") && exists(urls)) {
                                            fileName = urls.substring(urls.lastIndexOf('/') + 1);
                                            String filePath = DownloadSupport.getVideoDirectory(getApplicationContext(), new SessionManager(context)) + "/" + fileName;
                                            File videofile = new File(filePath);
                                            if (!videofile.exists()) {
                                                String url = videolist.getJSONObject(0).getString("vidd_url");
                                                JSONObject videodata = videolist.getJSONObject(0);
                                                if (!url.equals("") && !url.equals("null")) {

                                                    customalert(videodata, item);




                                       /* Intent intent = new Intent(context, DownloadableServices.class);
                                        // add infos for the service which file to download and where to store
                                        intent.putExtra(DownloadableServices.FILENAME, item.getString("vid_title"));
                                        intent.putExtra(DownloadableServices.urlpath, url);
                                        intent.putExtra(DownloadableServices.TYPE, "video");
                                        intent.putExtra(DownloadableServices.ID, item.getString("vid_id"));
                                        //  startActivity(intent);
                                        startService(intent);
                                        Toast.makeText(CategoryVideosActivity.this, "Downloading....", Toast.LENGTH_SHORT).show();*/
                                                } else {
                                                    Constant.setToast(CategoryVideosActivity.this, "Donwload Not Available");
                                                }

                                            } else {
                                                pDialog.dismiss();
                                   /* Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                    videoplay.putExtra("video_detail",filePath);
                                    startActivity(videoplay);*/
                                                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CategoryVideosActivity.this);
                                                bottomSheetDialog.setContentView(R.layout.dialog_box);
                                                LinearLayout deletelayout = bottomSheetDialog.findViewById(R.id.deletelayout);
                                                LinearLayout viewlayout = bottomSheetDialog.findViewById(R.id.viewlayout);
                                                TextView textView = bottomSheetDialog.findViewById(R.id.title_play);
                                                textView.setText("Play");
                                                viewlayout.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                                        videoplay.putExtra("video_detail", filePath);
                                                        startActivity(videoplay);

                                                        bottomSheetDialog.dismiss();
                                                    }
                                                });
                                                deletelayout.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        new File(filePath).delete();
                                                        try {
                                                            repository.Delete(videolist.getJSONObject(0).getString("vidd_id"), "0");
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        bottomSheetDialog.dismiss();
                                                        recyclerAdapter.notifyDataSetChanged();
                                                    }
                                                });
                                                bottomSheetDialog.show();

                                            }
                                        } else {
                                            binding.download.setVisibility(View.GONE);
                                        }
                                    } else {
                                        Constant.setToast(CategoryVideosActivity.this, "Donwload Available in 4-7 Days");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 2000);


                    }
                });

/*
                binding.download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONArray videolist =item.getJSONArray("neon_video_detail");
                            if(videolist.length()>1)
                            {
                                Intent vidcategory = new Intent(getApplicationContext(), VideoPartListActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                startActivity(vidcategory);
                            }else if(videolist.length()>0)
                            {
                                String fileName = item.getString("vid_title");
                                String urls = videolist.getJSONObject(0).getString("vidd_url");
                                fileName = urls.substring(urls.lastIndexOf('/') + 1);
                                String filePath =DownloadSupport.getVideoDirectory(getApplicationContext(), new SessionManager(context))+"/"+fileName;
                                File videofile = new File(filePath);
                                if(!videofile.exists()) {
                                    String url = videolist.getJSONObject(0).getString("vidd_url");
                                    JSONObject videodata = videolist.getJSONObject(0);
                                    if (!url.equals("") && !url.equals("null")) {
                                        customalert(videodata, item);


                                       */
/* Intent intent = new Intent(context, DownloadableServices.class);
                                        // add infos for the service which file to download and where to store
                                        intent.putExtra(DownloadableServices.FILENAME, item.getString("vid_title"));
                                        intent.putExtra(DownloadableServices.urlpath, url);
                                        intent.putExtra(DownloadableServices.TYPE, "video");
                                        intent.putExtra(DownloadableServices.ID, item.getString("vid_id"));
                                        //  startActivity(intent);
                                        startService(intent);
                                        Toast.makeText(CategoryVideosActivity.this, "Downloading....", Toast.LENGTH_SHORT).show();*//*

                                    } else {
                                        Constant.setToast(CategoryVideosActivity.this, "Donwload Not Available");
                                    }
                                }else{
                                    Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                    videoplay.putExtra("video_detail",filePath);
                                    startActivity(videoplay);
                                }
                            }else{
                                Constant.setToast(CategoryVideosActivity.this, "Donwload Available in 4-7 Days");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull FreeVideoListBinding itemView) {
                super(itemView.getRoot());

            }
        }
    }

    public void playerAlert() {
        final Dialog builder = new Dialog(CategoryVideosActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_player_change, null);
        Button player_1 = dialogLayout.findViewById(R.id.player_1);
        Button player_2 = dialogLayout.findViewById(R.id.player_2);


        builder.setContentView(dialogLayout);
        builder.setCancelable(true);
        builder.show();
    }

    public void customalert(JSONObject item, JSONObject itemd) {


        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CategoryVideosActivity.this);
        bottomSheetDialog.setContentView(R.layout.download_layout);
        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
        TextView txt_title = bottomSheetDialog.findViewById(R.id.txt_title);


        tv_go_inside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canceldownload(bottomSheetDialog, itemd, item);
            }
        });

        try {
            txt_title.setText(itemd.getString("vid_title"));
            setindatabase(item, bottomSheetDialog, itemd);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();

    }


    public void setindatabase(JSONObject item, BottomSheetDialog bottomSheetDialog, JSONObject itemd) {
        String fileName = null;
        try {
            String Livepath = item.getString("vidd_url");
            fileName = Livepath.substring(Livepath.lastIndexOf('/') + 1);

//            Constant.logPrint("LivepathLivepathLivepath", Livepath);
            DownloadFile downloadFile = new DownloadFile();
            downloadFile.setId(item.getString("vidd_id"));
            String titled = itemd.getString("vid_title");
            downloadFile.setTitle(titled);
            downloadFile.setDownloadId(item.getString("vidd_id"));
            downloadFile.setCurrentBytes(0);
            downloadFile.setTotalBytes(0);
            downloadFile.setFileName(fileName);
            downloadFile.setFiletype("0");
            downloadFile.setDownloadStatus(DownloadStatus.START);
            downloadFile.setFilepath(DownloadSupport.getVideoDirectory(getApplicationContext(), session));
            downloadFile.setThumbPath("");
            downloadFile.setVideoUrlOffline(Livepath);
            Long startTime = System.currentTimeMillis();
            downloadFile.setStartTime(startTime);
            repository.Insert(downloadFile);


            Intent intent = new Intent(getApplicationContext(), YourService.class);
            // add infos for the service which file to download and where to store
            intent.putExtra(YourService.FILENAME, downloadFile.getFileName());
            //  intent.putExtra(YourService.urlpath, url);
            intent.putExtra(YourService.urlpath, downloadFile.getVideoUrlOffline());
            intent.putExtra(YourService.TYPE, "0");
            intent.putExtra(YourService.VIDEO_ID, downloadFile.getId());
            getApplicationContext().startService(intent);

            TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);

            view_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ////  startActivity(new Intent(getApplicationContext(), PdfViewer.class).putExtra("file", downloadFile.getFilepath()+"/"+downloadFile.getFileName()+".pdf"));
                    recyclerAdapter.notifyDataSetChanged();
                    bottomSheetDialog.setCancelable(true);
                    bottomSheetDialog.dismiss();
                }
            });

            setobserver(bottomSheetDialog, item.getString("vidd_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setobserver(BottomSheetDialog bottomSheetDialog, String downloadFile) {

        ProgressBar pg = bottomSheetDialog.findViewById(R.id.progressBar);
        TextView percent = bottomSheetDialog.findViewById(R.id.percent);
        TextView minuteleft = bottomSheetDialog.findViewById(R.id.minuteleft);
        TextView speed = bottomSheetDialog.findViewById(R.id.speed);
        TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
//        Constant.logPrint(downloadFile, "downloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFile");
        downloadInfo();
        repository.getDownloadTable(downloadFile, "0").observe((CategoryVideosActivity.this), new Observer<DownloadFile>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onChanged(DownloadFile file) {
                if (file != null) {
                    if (!file.isDownloadComplete()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                downloadInfo();
                            }
                        }, SPLASH_TIME_OUT);
                        view_file.setVisibility(View.GONE);
                        tv_go_inside.setVisibility(View.VISIBLE);
                        try {
                            pg.setProgress((int) Integer.parseInt(file.getPercentage()));
                            percent.setText(file.getPercentage() + "%");
                            minuteleft.setText(file.getTimeremain() + " s left");
                            speed.setText(roundTwoDecimals(Double.parseDouble(kilobytePerSec + "")) + " KB/S");

                        } catch (Exception e) {

                        }
                    } else {
                        pg.setProgress((int) 100);
                        percent.setText(100 + "%");
                        minuteleft.setText(0 + " s left");
                        view_file.setVisibility(View.VISIBLE);
                        tv_go_inside.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    public static boolean exists(String URLName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //// HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void canceldownload(BottomSheetDialog bottomSheetDialog, JSONObject item, JSONObject itemd) {
        String fileName = null;
        try {
            String Livepath = itemd.getString("vidd_url");
            fileName = Livepath.substring(Livepath.lastIndexOf('/') + 1);
//            Constant.logPrint("LivepathLivepathLivepath", fileName);
            String path = DownloadSupport.getVideoDirectory(getApplicationContext(), session) + "/" + fileName;
//            Constant.logPrint("pathpathpathpathpath", path);
            new File(path).delete();
            repository.Delete(itemd.getString("vidd_id"), "0");
            bottomSheetDialog.setCancelable(true);
            bottomSheetDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    Double roundTwoDecimals(Double vLongValue) {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        return Double.valueOf(twoDForm.format(vLongValue));
    }

    private void downloadInfo() {
//        Log.d(TAG, "downloadInfo: iN downloadInfo");
        Request request = new Request.Builder()
                .url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png") // replace image url
                .build();
        startTime = System.currentTimeMillis();
        nclient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
//                Log.d(TAG, "downloadInfo: iN failure");
                //check when device is connected to Router but there is no internet

            }

            @Override
            public void onResponse(okhttp3.Call call, Response response) throws IOException {
//                Log.d(TAG, "downloadInfo: iN success");
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }
                InputStream input = response.body().byteStream();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) != -1) {
                        bos.write(buffer);
                    }
                    byte[] docBuffer = bos.toByteArray();
                    fileSize = bos.size();
                } finally {
                    input.close();
                }
                endTime = System.currentTimeMillis();

                // calculate how long it took by subtracting endtime from starttime
                final double timeTakenMills = Math.floor(endTime - startTime);  // time taken in milliseconds
                final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                int ds = (int) Math.round(1024 / timeTakenInSecs);
                if (ds > 0) {
                    kilobytePerSec = ds / 1000;
                }
                speed_data = Math.round(fileSize / timeTakenMills);
//                Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
//                Log.d(TAG, "Kb per sec: " + kilobytePerSec);
//                Log.d(TAG, "Download Speed: " + speed_data);
//                Log.d(TAG, "File size in kb: " + fileSize);
                // update the UI with the speed test results
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(CategoryVideosActivity.this);
        Constant.checkAdb(CategoryVideosActivity.this);
    }
}