package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityFreevideolistBinding;
import codexo.neonclasses.databinding.ActivityStudySubcategoryBinding;
import codexo.neonclasses.databinding.ItemCategoryHorizontalBinding;
import codexo.neonclasses.session.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;

public class StudySubcategoryActivity extends AppCompatActivity {
    ActivityStudySubcategoryBinding parentbinding;
    ProgressDialog pDialog;
    SessionManager session;
     JSONArray video_category =new JSONArray();
    JSONObject parent_category =new JSONObject();
     String parent_id="",category_url_new="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentbinding = ActivityStudySubcategoryBinding.inflate(getLayoutInflater());
        setContentView(parentbinding.getRoot());
        Constant.adbEnabled(StudySubcategoryActivity.this);
        Constant.checkAdb(StudySubcategoryActivity.this);
        parentbinding.imgBack.setOnClickListener(view -> finish());

        parentbinding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        pDialog = Constant.getProgressBar(StudySubcategoryActivity.this);
        session = new SessionManager(StudySubcategoryActivity.this);
        try {
            parent_category =new JSONObject(getIntent().getStringExtra("parent_category"));
//            Constant.logPrint(parent_category+"","parent_categoryparent_categoryparent_categoryparent_category44444");
            parentbinding.txtHello.setText(parent_category.getString("preparation_category_title"));
            parentbinding.txtHello.setTypeface(Constant.getFontsBold(StudySubcategoryActivity.this));
            parent_id = parent_category.getString("preparation_category_id");
//            Constant.logPrint("parent_id123456", parent_id+"+++++"+session.getUserId()+"++++"+"\n"+parent_category);
            getstudysubcategory();

        } catch (JSONException e) {
            e.printStackTrace();
        }
     }


    private void getstudysubcategory() {

        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().gtestudysubcategory(session.getUserId(),parent_id);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            video_category =jsonObject.getJSONArray("results");
                            category_url_new =jsonObject.getString("img_url");
                            RecyclerAdapter7 recyclerAdapter4 = new RecyclerAdapter7(StudySubcategoryActivity.this, video_category);
                            LinearLayoutManager layoutManager4 = new LinearLayoutManager(StudySubcategoryActivity.this);
                            layoutManager4.setOrientation(RecyclerView.VERTICAL);
                            parentbinding.recyclerSubCategory.setLayoutManager(layoutManager4);
                            parentbinding.recyclerSubCategory.setAdapter(recyclerAdapter4);
                        } else {
                            Constant.setToast(StudySubcategoryActivity.this,"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getstudysubcategory();
            }
        });
    }

    public class RecyclerAdapter7 extends RecyclerView.Adapter<RecyclerAdapter7.RecyclerViewHolder> {
        ItemCategoryHorizontalBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter7(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter7.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemCategoryHorizontalBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter7.RecyclerViewHolder holder = new RecyclerAdapter7.RecyclerViewHolder(binding);
            return holder;
        }

        @SuppressLint("ResourceType")
        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter7.RecyclerViewHolder holder, int position) {
            if(position==0)
            {
                binding.topspace.setVisibility(View.VISIBLE);
            }else{
                binding.topspace.setVisibility(View.GONE);

            }
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = category_url_new+item.getString("preparation_category_image");

                    Constant.setImage(img,binding.image,getApplicationContext());




                binding.txtTitle.setText(item.getString("preparation_category_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            public RecyclerViewHolder(@NonNull ItemCategoryHorizontalBinding itemView) {
                super(itemView.getRoot());

                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            JSONArray child = item.getJSONArray("childdata");

                            if(child.length()>0)
                            {
                                parent_category = item;
                                parent_id = parent_category.getString("preparation_category_id");
                                parentbinding.txtHello.setText(parent_category.getString("preparation_category_title"));
                                parentbinding.txtHello.setTypeface(Constant.getFontsBold(StudySubcategoryActivity.this));

                                getstudysubcategory();
                            }else{
                                Intent vidcategory = new Intent(getApplicationContext(), StudymaterialActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                startActivity(vidcategory);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(StudySubcategoryActivity.this);
        Constant.checkAdb(StudySubcategoryActivity.this);
    }
}