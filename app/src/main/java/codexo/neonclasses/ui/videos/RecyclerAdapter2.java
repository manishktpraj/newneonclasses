package codexo.neonclasses.ui.videos;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ItemFreeClassesBinding;
import codexo.neonclasses.ui.player.PlayerActivity;

public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
    ItemFreeClassesBinding binding;
    Context context;
    JSONArray listItem;
    ClickListener clickListener;
    String video_type;
    int type=0;
    public RecyclerAdapter2(Context context, JSONArray listItem,int typed,String video_type) {
        this.context = context;
        this.listItem = listItem;
        this.type =typed;
        this.video_type = video_type;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemFreeClassesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        RecyclerViewHolder holder = new RecyclerViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        try {
            JSONObject item = listItem.getJSONObject(position);
             String vid_url = item.getString("vid_image");
            Constant.setImage(vid_url,binding.image,context);
            binding.txtTitle.setText(item.getString("vid_title"));
            String vid_start_time = item.getString("vid_start_time");
            if (!vid_start_time.equals("null") && video_type.equals("live_video")){
                binding.vidStartTime.setVisibility(View.VISIBLE);
            String replaced = vid_start_time.replaceAll("/+"," ");
            String replacedd = replaced.replace("+05:30","");
            String[] replaceddd = replacedd.split(" ");

            String ordat = replaceddd[0];
            String replacedT = ordat.replaceAll("T"," ");
            DateFormat parser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date Ordate = (Date) parser.parse(replacedT);
            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            DateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
            binding.vidStartTime.setText("Start : "+formatter1.format(Ordate)+" @"+formatter.format(Ordate));
//            Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replacedT);
            }else {
                binding.vidStartTime.setVisibility(View.GONE);
            }
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
        holder.setIsRecyclable(false);
        holder.setClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        return listItem.length();
    }

    interface ClickListener {
        void onClickItem(int post,int type) throws JSONException;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ClickListener clickListener;

        public RecyclerViewHolder(@NonNull ItemFreeClassesBinding itemView) {
            super(itemView.getRoot());

            itemView.parent.setOnClickListener(view -> {
                try {
                    clickListener.onClickItem(getAdapterPosition(),type);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
        }

        public void setClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }
    }
}

