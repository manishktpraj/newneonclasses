package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCategoryVideosBinding;
import codexo.neonclasses.databinding.ActivityVideoPartListBinding;
import codexo.neonclasses.databinding.FreeVideoListBinding;
import codexo.neonclasses.databinding.ItemVideoPartBinding;
import codexo.neonclasses.service.DownloadableServices;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.service.action.MultiDownload;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.DownloadStatus;
import codexo.neonclasses.ui.download.StudyMaterialDownloadsFragment;
import codexo.neonclasses.ui.download.YourService;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.player.OfflineVideoPlayerActivity;
import codexo.neonclasses.ui.player.PlayerActivity;
import codexo.neonclasses.ui.study.StudyMaterialFragment;
import codexo.neonclasses.ui.test.RecyclerAdapter1;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class VideoPartListActivity extends AppCompatActivity {
    ActivityVideoPartListBinding bindingparent;
    SessionManager session;
    ProgressDialog  pDialog;
    JSONObject video_detail = new JSONObject();
    JSONArray videolist = new JSONArray();
    private BroadcastReceiver mMessageReceiver;
    private DownloadableServices mgr = null;
    double speed_data;
    String[] permissions = new String[]{
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private MultiDownload mydownloads;
    private IntentFilter filter;
     private DatabaseRepository repository;
    RecyclerAdapter2 recyclerAdapter;
    private final OkHttpClient nclient = new OkHttpClient();
    private long startTime;
    private long endTime;
    private long fileSize;
    int kilobytePerSec=0;
    /* @Override
    public void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(mMessageReceiver, filter);
    }*/
    private static int SPLASH_TIME_OUT = 10000;
    private ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingparent = ActivityVideoPartListBinding.inflate(getLayoutInflater());
        setContentView(bindingparent.getRoot());
        Constant.adbEnabled(VideoPartListActivity.this);
        Constant.checkAdb(VideoPartListActivity.this);
        bindingparent.imgBack.setOnClickListener(view -> finish());
        pDialog = Constant.getProgressBar(VideoPartListActivity.this);
        session = new SessionManager(VideoPartListActivity.this);
        repository=new DatabaseRepository(VideoPartListActivity.this);
        try {
            video_detail = new JSONObject(getIntent().getStringExtra("parent_category"));
            bindingparent.txtHello.setText(video_detail.getString("vid_title"));
            videolist = video_detail.getJSONArray("neon_video_detail");
            bindingparent.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
            recyclerAdapter = new RecyclerAdapter2(getApplicationContext(), videolist);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            bindingparent.recycler.setLayoutManager(layoutManager);
            bindingparent.recycler.setAdapter(recyclerAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    /*    mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Extract data included in the Intent
                String message = intent.getAction();
                boolean isComplete = intent.getBooleanExtra("isCommplete", false);
                if (isComplete) {
                    ///   getfreestudymaterial();
                    recyclerAdapter.notifyDataSetChanged();
                }

            }
        };*/
    }

 /*   @Override
    public void onStop() {
        super.onStop();
        getApplicationContext().unregisterReceiver(mMessageReceiver);
    }*/


    public class RecyclerAdapter2 extends RecyclerView.Adapter<VideoPartListActivity.RecyclerAdapter2.RecyclerViewHolder> {
        ItemVideoPartBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public VideoPartListActivity.RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemVideoPartBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            VideoPartListActivity.RecyclerAdapter2.RecyclerViewHolder holder = new VideoPartListActivity.RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }


        @Override
        public void onBindViewHolder(@NonNull VideoPartListActivity.RecyclerAdapter2.RecyclerViewHolder holder, int position) {

            if(position==0)
            {
                binding.topspace.setVisibility(View.VISIBLE);
            }else
            {
                binding.topspace.setVisibility(View.GONE);

            }




            try {

                JSONObject item = listItem.getJSONObject(position);
                int pos = position+1;
                binding.txtTitle.setText(video_detail.getString("vid_title")+" PART "+pos);
                binding.txtTitle.setTypeface(Constant.getFontsBold(VideoPartListActivity.this));
                 String fileName = item.getString("vidd_url").substring(item.getString("vidd_url").lastIndexOf('/') + 1);
//                Constant.logPrint("fileNamefileNamefileNamefileName",fileName);
                String filePath =DownloadSupport.getVideoDirectory(getApplicationContext(), new SessionManager(context))+"/"+fileName;
                File videofile = new File(filePath);

//                Constant.logPrint("videofilevideofilevideofile",videofile.exists()+"");

                if(videofile.exists())
                {
                    ///  binding.txtDownload.setText("View");
                    binding.download.setImageResource(R.drawable.playbutton);

                }else{
                    binding.download.setImageResource(R.drawable.downloadicon);
                    //// binding.txtDownload.setBackground(getResources().getDrawable(R.drawable.corner_gray));
                }
                binding.download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            if(!videofile.exists()) {
                                String url = item.getString("vidd_url");
                                if (!url.equals("") && !url.equals("null")) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            pDialog.dismiss();
                                            customalert(item, video_detail, pos);
                                        }
                                    }, 2000);
                                    pDialog.show();
                                    


                                       /* Intent intent = new Intent(context, DownloadableServices.class);
                                        // add infos for the service which file to download and where to store
                                        intent.putExtra(DownloadableServices.FILENAME, item.getString("vid_title"));
                                        intent.putExtra(DownloadableServices.urlpath, url);
                                        intent.putExtra(DownloadableServices.TYPE, "video");
                                        intent.putExtra(DownloadableServices.ID, item.getString("vid_id"));
                                        //  startActivity(intent);
                                        startService(intent);
                                        Toast.makeText(CategoryVideosActivity.this, "Downloading....", Toast.LENGTH_SHORT).show();*/
                                } else {
                                    Constant.setToast(VideoPartListActivity.this, "Donwload Not Available");
                                }
                            }else{
                                pDialog.dismiss();
                                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoPartListActivity.this);
                                bottomSheetDialog.setContentView(R.layout.dialog_box);
                                LinearLayout deletelayout = bottomSheetDialog.findViewById(R.id.deletelayout);
                                LinearLayout viewlayout = bottomSheetDialog.findViewById(R.id.viewlayout);
                                TextView textView = bottomSheetDialog.findViewById(R.id.title_play);
                                textView.setText("Play");
                                viewlayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                        videoplay.putExtra("video_detail", filePath);
                                        startActivity(videoplay);

                                        bottomSheetDialog.dismiss();
                                    }
                                });
                                deletelayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        new File(filePath).delete();
                                        try {
                                            repository.Delete(item.getString("vidd_id"), "0");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        bottomSheetDialog.dismiss();
                                        recyclerAdapter.notifyDataSetChanged();
                                    }
                                });
                                bottomSheetDialog.show();

                             /*   Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                                videoplay.putExtra("video_detail",filePath);
                                startActivity(videoplay);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });



            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }
        
        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemVideoPartBinding itemView) {
                super(itemView.getRoot());
                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent videoplay = new Intent(getApplicationContext(), PlayerActivity.class);
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            String url = item.getString("vid_url");
                            String title = item.getString("vid_title");
                            videoplay.putExtra("url", url);
                            videoplay.putExtra("vid_title", title);
                            startActivity(videoplay);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        }
    }
    public void customalert(JSONObject item,JSONObject itemd,Integer pos) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoPartListActivity.this);
        bottomSheetDialog.setContentView(R.layout.download_layout);
        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tv_go_inside=bottomSheetDialog.findViewById(R.id.tv_go_inside);
        TextView txt_title=bottomSheetDialog.findViewById(R.id.txt_title);
        tv_go_inside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canceldownload(bottomSheetDialog,itemd,item,pos);
            }
        });

        try {
            txt_title.setText(itemd.getString("vid_title")+" Part "+pos);
            setindatabase(item,bottomSheetDialog,itemd,pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();

    }

    public void  setindatabase(JSONObject item,BottomSheetDialog bottomSheetDialog,JSONObject itemd,Integer pos)
    {
        String fileName = null;
        try {
            String Livepath = item.getString("vidd_url");
            fileName = Livepath.substring(Livepath.lastIndexOf('/') + 1);
           ////  File videofile = new File(DownloadSupport.getVideoDirectory(getApplicationContext(), session)+fileName);


            DownloadFile downloadFile=new DownloadFile();
            downloadFile.setId(item.getString("vidd_id"));
            String titled = itemd.getString("vid_title")+" PART "+pos;
            downloadFile.setTitle(titled);
            downloadFile.setDownloadId(item.getString("vidd_id"));
            downloadFile.setCurrentBytes(0);
            downloadFile.setTotalBytes(0);
            downloadFile.setFileName(fileName);
            downloadFile.setFiletype("0");
            downloadFile.setDownloadStatus(DownloadStatus.START);
            downloadFile.setFilepath(DownloadSupport.getVideoDirectory(getApplicationContext(), session));
            downloadFile.setThumbPath("");
            downloadFile.setVideoUrlOffline(Livepath);
            Long  startTime = System.currentTimeMillis();
            downloadFile.setStartTime(startTime);
            repository.Insert(downloadFile);


            Intent intent = new Intent(getApplicationContext(), YourService.class);
            // add infos for the service which file to download and where to store
            intent.putExtra(YourService.FILENAME, downloadFile.getFileName());
            //  intent.putExtra(YourService.urlpath, url);
            intent.putExtra(YourService.urlpath, downloadFile.getVideoUrlOffline());
            intent.putExtra(YourService.TYPE, "0");
            intent.putExtra(YourService.VIDEO_ID, downloadFile.getId());
            getApplicationContext().startService(intent);

            TextView view_file=bottomSheetDialog.findViewById(R.id.view_file);

            view_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ////  startActivity(new Intent(getApplicationContext(), PdfViewer.class).putExtra("file", downloadFile.getFilepath()+"/"+downloadFile.getFileName()+".pdf"));
                    Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
                    videoplay.putExtra("video_detail",downloadFile.getFilepath()+"/"+downloadFile.getFileName());
                    startActivity(videoplay);
                     recyclerAdapter.notifyDataSetChanged();
                    bottomSheetDialog.setCancelable(true);
                    bottomSheetDialog.dismiss();
                }
            });

            setobserver(bottomSheetDialog,item.getString("vidd_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    private  void setobserver(BottomSheetDialog bottomSheetDialog, String downloadFile) {

        ProgressBar pg = bottomSheetDialog.findViewById(R.id.progressBar);
        TextView percent = bottomSheetDialog.findViewById(R.id.percent);
        TextView minuteleft = bottomSheetDialog.findViewById(R.id.minuteleft);
        TextView speed = bottomSheetDialog.findViewById(R.id.speed);
        TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
//        Constant.logPrint(downloadFile,"downloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFile");
        downloadInfo();
        repository.getDownloadTable(downloadFile,"0").observe((VideoPartListActivity.this), new Observer<DownloadFile>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onChanged(DownloadFile file) {
                if(file!=null){
                    if(!file.isDownloadComplete() ){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                downloadInfo();
                          }
                        }, SPLASH_TIME_OUT);

                        view_file.setVisibility(View.GONE);
                        tv_go_inside.setVisibility(View.VISIBLE);
                        try {
                       /*    Long endTime = System.currentTimeMillis();
                            // calculate how long it took by subtracting endtime from starttime
                            final double timeTakenMills = Math.floor(endTime - file.getStartTime());  // time taken in milliseconds
                            final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                            final int kilobytePerSec = (int) Math.round(1024 / timeTakenInSecs);
                            final double speede = Math.round(file.getTotalBytes() / timeTakenMills);
                            Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
                            Log.d(TAG, "Kb per sec: " + kilobytePerSec);
                            Log.d(TAG, "Download Speed: " + speede);
                            Log.d(TAG, "File size in kb: " + file.getTotalBytes());
*/
                            pg.setProgress((int) Integer.parseInt(file.getPercentage()));
                            percent.setText(file.getPercentage()+"%");
                            minuteleft.setText(file.getTimeremain()+" s left");
                            speed.setText(roundTwoDecimals(Double.parseDouble(kilobytePerSec+""))+" KB/S");

                        }catch (Exception e) {

                        }
                    }else{
                        pg.setProgress((int) 100);
                        percent.setText(100+"%");
                        minuteleft.setText(0+" s left");
                        view_file.setVisibility(View.VISIBLE);
                        tv_go_inside.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    private void canceldownload(BottomSheetDialog bottomSheetDialog,JSONObject item,JSONObject itemd ,Integer pos )
    {
        String fileName = null;
        try {
            fileName = item.getString("vid_title")+" PART "+pos;
            new File(DownloadSupport.getPdfDirectory(getApplicationContext(), session)+fileName+".mp4").delete();
            repository.Delete( itemd.getString("vidd_id"),"0");
            bottomSheetDialog.setCancelable(true);
            bottomSheetDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    Double roundTwoDecimals(Double vLongValue)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        return Double.valueOf(twoDForm.format(vLongValue));
    }
    private void downloadInfo() {
//        Log.d(TAG, "downloadInfo: iN downloadInfo");
        Request request = new Request.Builder()
                .url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png") // replace image url
                .build();
        startTime = System.currentTimeMillis();
        nclient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
//                Log.d(TAG, "downloadInfo: iN failure");
                //check when device is connected to Router but there is no internet

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
//                Log.d(TAG, "downloadInfo: iN success");
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }
                InputStream input = response.body().byteStream();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) != -1) {
                        bos.write(buffer);
                    }
                    byte[] docBuffer = bos.toByteArray();
                    fileSize = bos.size();
                } finally {
                    input.close();
                }
                endTime = System.currentTimeMillis();

                // calculate how long it took by subtracting endtime from starttime
                final double timeTakenMills = Math.floor(endTime - startTime);  // time taken in milliseconds
                final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                int ds = (int) Math.round(1024 / timeTakenInSecs);
                if(ds>0) {
                    kilobytePerSec = ds/1000;
                }
                speed_data = Math.round(fileSize / timeTakenMills);
//                Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
//                Log.d(TAG, "Kb per sec: " + kilobytePerSec);
//                Log.d(TAG, "Download Speed: " + speed_data);
//                Log.d(TAG, "File size in kb: " + fileSize);
                // update the UI with the speed test results
             }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(VideoPartListActivity.this);
        Constant.checkAdb(VideoPartListActivity.this);
    }
}