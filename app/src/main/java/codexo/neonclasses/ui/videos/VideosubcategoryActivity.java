package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityMainBinding;
import codexo.neonclasses.databinding.ActivityVideosubcategoryBinding;
import codexo.neonclasses.databinding.ItemCategoryHorizontalBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.help.HelpActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class VideosubcategoryActivity extends AppCompatActivity {
    private static ActivityVideosubcategoryBinding bindingparent;
    SessionManager session;
    JSONArray video_category = new JSONArray();
    JSONObject parent_category = new JSONObject();
    ProgressDialog pDialog;
    String parent_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingparent = ActivityVideosubcategoryBinding.inflate(getLayoutInflater());
        setContentView(bindingparent.getRoot());
        Constant.adbEnabled(VideosubcategoryActivity.this);
        Constant.checkAdb(VideosubcategoryActivity.this);
        session = new SessionManager(getApplicationContext());
        pDialog = Constant.getProgressBar(VideosubcategoryActivity.this);
        bindingparent.imgBack.setOnClickListener(view -> finish());
        setContentView(bindingparent.getRoot());
        parent_id = getIntent().getStringExtra("parent_id");
        bindingparent.txtHello.setText(getIntent().getStringExtra("type"));
        try {
            parent_category = new JSONObject(getIntent().getStringExtra("parent_category"));
//            Constant.logPrint(parent_category+"","parent_categoryparent_categoryparent_categoryparent_category44444");
            bindingparent.txtHello.setText(parent_category.getString("bcat_name"));
            bindingparent.txtHello.setTypeface(Constant.getFontsBold(VideosubcategoryActivity.this));
            parent_id = parent_category.getString("bcat_id");
//            Constant.logPrint("parent_id123", parent_id+"+++"+session.getUserId());
            getvideosubcategory();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getvideosubcategory() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getvideosubcategory(session.getUserId(), parent_id);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            video_category = jsonObject.getJSONArray("results");
                            RecyclerAdapter7 recyclerAdapter4 = new RecyclerAdapter7(VideosubcategoryActivity.this, video_category);
                            LinearLayoutManager layoutManager4 = new LinearLayoutManager(VideosubcategoryActivity.this);
                            layoutManager4.setOrientation(RecyclerView.VERTICAL);
                            bindingparent.recyclerSubCategory.setLayoutManager(layoutManager4);
                            bindingparent.recyclerSubCategory.setAdapter(recyclerAdapter4);
                        } else {
                            Constant.setToast(VideosubcategoryActivity.this, "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getvideosubcategory();
            }
        });
    }

    public class RecyclerAdapter7 extends RecyclerView.Adapter<RecyclerAdapter7.RecyclerViewHolder> {
        ItemCategoryHorizontalBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter7(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter7.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemCategoryHorizontalBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter7.RecyclerViewHolder holder = new RecyclerAdapter7.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter7.RecyclerViewHolder holder, int position) {
            if (position == 0) {
                binding.topspace.setVisibility(View.VISIBLE);
            } else {
                binding.topspace.setVisibility(View.GONE);
            }
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("bcat_image");
                Constant.setImage(img, binding.image, getApplicationContext());

                binding.txtTitle.setText(item.getString("bcat_name"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(context.getApplicationContext()));

                binding.txtDes.setText(item.getString("bcat_description"));
                binding.txtDes.setTypeface(Constant.getFonts(context.getApplicationContext()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            public RecyclerViewHolder(@NonNull ItemCategoryHorizontalBinding itemView) {
                super(itemView.getRoot());

                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            JSONArray child = item.getJSONArray("child");
                            if (child.length() > 0) {
                                parent_category = item;
                                parent_id = parent_category.getString("bcat_id");
                                bindingparent.txtHello.setText(parent_category.getString("bcat_name"));
                                bindingparent.txtHello.setTypeface(Constant.getFontsBold(VideosubcategoryActivity.this));

                                getvideosubcategory();


                            } else {
                                Intent vidcategory = new Intent(getApplicationContext(), CategoryVideosActivity.class);
                                vidcategory.putExtra("parent_category", item + "");
                                startActivity(vidcategory);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(VideosubcategoryActivity.this);
        Constant.checkAdb(VideosubcategoryActivity.this);
    }
}