package codexo.neonclasses.ui.videos;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentPscBinding;
import codexo.neonclasses.databinding.FragmentVideoTabBinding;
import codexo.neonclasses.ui.psc.MyReceiver;

/**
 * A simple {@link Fragment} subclass.
  * create an instance of this fragment.
 */
public class PscFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentPscBinding binding;

    private String CHANNEL_ID = "My Notification";
    private Context mContext;

    private ProgressDialog pDialog;
    private BroadcastReceiver MyReceiver = null;
    String Current_url = "https://neonclasses.com/contact";
    Activity activity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPscBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        mParam1 = "0";
        pDialog = Constant.getProgressBar(activity);
        if (mParam1.equals("0")){
            binding.opentelegram.setVisibility(View.VISIBLE);
            binding.pscWeb.setVisibility(View.GONE);
            binding.opentelegram.setText(Constant.PSC_TEXT);
            binding.opentelegram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.PSC_URL));
                    startActivity(browserIntent);
                }
            });
        }else {
            pDialog.show();
            binding.opentelegram.setVisibility(View.GONE);
            binding.pscWeb.setVisibility(View.VISIBLE);
            setWebPsc();
        }


        return root;
    }
    public void setWebPsc(){
        MyReceiver = new MyReceiver();
        binding.pscWeb.clearCache(true);

        WebSettings webSettings = binding.pscWeb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        WebViewClientImpl webViewClient = new WebViewClientImpl(activity);
        binding.pscWeb.setWebViewClient(webViewClient);
        binding.pscWeb.loadUrl("https://neonclasses.com/contact");

       /* dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();*/
    }


    class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if (isNetworkConnectionAvailable(activity)) return false;
            if (url.indexOf("codexocoder.com") > -1) return false;
            Log.d("urlurlurlurl", url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Current_url = url;
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            }
            binding.pscWeb.loadUrl("file:///android_asset/no_connection.html");
            super.onReceivedError(view, request, error);
        }
    }
    public boolean isNetworkConnectionAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected to internet");
            return true;
        } else {
            binding.pscWeb.loadData("<html><body><div><p>Sorry, something went wrong. We're working on fixing it.</p></div></body></html>",
                    "text/html", "UTF-8");
            Dialog dialog;
            TextView nettext;
            dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_NoActionBar_TranslucentDecor);
            dialog.setContentView(R.layout.custom_no_internet_layout);
            Button restartapp = (Button) dialog.findViewById(R.id.restartapp);
            nettext = (TextView) dialog.findViewById(R.id.nettext);

            restartapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isNetworkConnectionAvailable(activity)) {
                        Intent i = new Intent(context, PscFragment.class);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    } else {
                        Toast.makeText(context, "Please connect your mobile to internet", Toast.LENGTH_SHORT).show();

                    }
                }
            });

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (isNetworkConnectionAvailable(activity)) {
                        WebViewClientImpl webViewClient = new WebViewClientImpl(activity);
                        binding.pscWeb.setWebViewClient(webViewClient);
                        binding.pscWeb.loadUrl(Current_url);
                    } else {
                        Toast.makeText(context, "Please connect your mobile to internet", Toast.LENGTH_SHORT).show();

                    }

                }
            });
            dialog.show();
            dialog.setCancelable(false);
            return false;
        }
    }
}