package codexo.neonclasses.ui.videos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityAllLiveClassesBinding;
import codexo.neonclasses.databinding.ItemHomeLiveClassesBinding;
import codexo.neonclasses.databinding.ItemRowAllLiveClassesBinding;
import codexo.neonclasses.ui.home.HomeFragment;
import codexo.neonclasses.ui.test.ResultActivity;
import codexo.neonclasses.ui.youtube.YoutubeActivity;

public class AllLiveClasses extends AppCompatActivity {
    ActivityAllLiveClassesBinding binding;
    JSONArray liveVideo = new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAllLiveClassesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(AllLiveClasses.this);
        Constant.checkAdb(AllLiveClasses.this);
        Constant.setStatusBar(AllLiveClasses.this);
        binding.imgBack.setOnClickListener(v ->  {
            finish();
        });
        try {
            liveVideo = new JSONArray(getIntent().getStringExtra("live"));
//            Constant.logPrint("livelive", liveVideo+"");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (liveVideo.length()<=0){
            binding.emptyView.setVisibility(View.VISIBLE);
            binding.recycler.setVisibility(View.GONE);
        }else {
            binding.emptyView.setVisibility(View.GONE);
            binding.recycler.setVisibility(View.VISIBLE);
            LiveClassAdapter liveClassAdapter = new LiveClassAdapter(getApplicationContext(), liveVideo);
            LinearLayoutManager livelayoutmanager = new LinearLayoutManager(getApplicationContext());
            livelayoutmanager.setOrientation(RecyclerView.VERTICAL);
            binding.recycler.setLayoutManager(livelayoutmanager);
            binding.recycler.setAdapter(liveClassAdapter);
        }
    }


    public class LiveClassAdapter extends RecyclerView.Adapter<LiveClassAdapter.RecyclerViewHolder> {
        ItemRowAllLiveClassesBinding binding;
        Context context;
        JSONArray listItem;

        public LiveClassAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemRowAllLiveClassesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("vid_image");
                Constant.setImage(img, binding.vidIcon, getApplicationContext());
                binding.txtTitle.setText(item.getString("vid_title"));

                String vid_start_time = item.getString("vid_start_time");
                if (!vid_start_time.equals("null")){
                    binding.vidStartTime.setVisibility(View.VISIBLE);
                    String replaced = vid_start_time.replaceAll("/+"," ");
                    String replacedd = replaced.replace("+05:30","");
                    String[] replaceddd = replacedd.split(" ");

                    String ordat = replaceddd[0];
                    String replacedT = ordat.replaceAll("T"," ");
                    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date Ordate = (Date) parser.parse(replacedT);
                    DateFormat formatter = new SimpleDateFormat("hh:mm aa");
                    DateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                    binding.vidStartTime.setText("Start : "+formatter1.format(Ordate)+" @"+formatter.format(Ordate));
//                    Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replacedT);
                }else {
                    binding.vidStartTime.setVisibility(View.GONE);
                }

                binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
                Constant.setBlinkAnim(binding.liveIcon);

                //String url = item.getString("product_affurl");
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(position);
                            Intent livedetail = new Intent(getApplicationContext(), YoutubeActivity.class);
                            livedetail.putExtra("video_detail", item + "");
                            startActivity(livedetail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ///  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        ///  startActivity(Intent.createChooser(browserIntent, "Browse with"));

                        ////      startActivity(browserIntent);
                    }
                });

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return liveVideo.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemRowAllLiveClassesBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(AllLiveClasses.this);
        Constant.checkAdb(AllLiveClasses.this);
    }
}