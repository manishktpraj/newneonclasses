package codexo.neonclasses.ui.videos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.FragmentVideosBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.cart.CartActivity;
import codexo.neonclasses.ui.home.HomeViewModel;
import retrofit2.Call;
import retrofit2.Callback;

public class VideosFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentVideosBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray free_video =new JSONArray();
    JSONArray recent_video =new JSONArray();
    JSONArray live_video =new JSONArray();
    JSONArray video_course =new JSONArray();
    JSONArray video_category =new JSONArray();
    String category_url="",package_url="";
    JSONArray study_category_new = new JSONArray();
    String allow_test="";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentVideosBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());
        binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Videos"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Study Material"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("PSC"));



        binding.imgCart.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), CartActivity.class));
        });
        getvideos();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void getvideos() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getvideos(session.getUserId());
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
               // Constant.logPrint("response_video", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            String dashboard_message = jsonObject.getString("dashboard_message");

                            if(!dashboard_message.equals(""))
                            {
                                //binding.customMessageData.setText(dashboard_message);
                                binding.customMessageData.loadData(dashboard_message, "text/html", "UTF-8");
                                binding.custommessage.setVisibility(View.VISIBLE);
                               // binding.customMessageData.setTypeface(Constant.getFontsBold(getContext()));
                            }else
                            {
                                binding.custommessage.setVisibility(View.GONE);
                            }
                            video_course =jsonObject.getJSONArray("video_course");
                            free_video =jsonObject.getJSONArray("free");
                            recent_video =jsonObject.getJSONArray("recent");
                            live_video =jsonObject.getJSONArray("live");
                            study_category_new =jsonObject.getJSONArray("prepration");
                            category_url =jsonObject.getString("category_url");
                            package_url =jsonObject.getString("package_url");
                            video_category =jsonObject.getJSONArray("results");
                            allow_test =jsonObject.getString("allow_test");
                            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
                            VideoTabFragment ldf =new VideoTabFragment();
                            Bundle args = new Bundle();
                            args.putString("video_course", video_course+"");
                            args.putString("free_video", free_video+"");
                            args.putString("live_video", live_video+"");
                            args.putString("recent_video", recent_video+"");
                            args.putString("category_url", category_url+"");
                            args.putString("package_url", package_url+"");
                            args.putString("video_category", video_category+"");
                            ldf.setArguments(args);

                            adapter.addFragment(ldf, "Videos");


                            StudyMaterialTabFragment study =new StudyMaterialTabFragment();
                            Bundle argsnew = new Bundle();
                            argsnew.putString("category_url_new", category_url+"");
                            argsnew.putString("study_category_new", study_category_new+"");
                            argsnew.putString("allow_test", allow_test+"");
                            study.setArguments(argsnew);


                            adapter.addFragment(study, "Study Material");


                            PscFragment psc =new PscFragment();
                            Bundle argsnewpsc = new Bundle();
                            argsnewpsc.putString("category_url_new", category_url+"");
                            argsnewpsc.putString("study_category_new", study_category_new+"");
                            argsnewpsc.putString("allow_test", allow_test+"");
                            psc.setArguments(argsnewpsc);


                            adapter.addFragment(psc, "PSC");


                            binding.viewPager.setAdapter(adapter);
                            binding.tabLayout.setupWithViewPager(binding.viewPager);
                        } else {
                            Constant.setToast(getContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getvideos();
            }
        });
    }

}
