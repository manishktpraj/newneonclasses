package codexo.neonclasses.ui.youtube.modules;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class FlippingView extends FrameLayout {

    private final ImageView flipOutView;
    private final ImageView flipInView;
    private final AnimatorSet animations;
    private final Listener listener;

    public interface Listener {
        void onFlipped(FlippingView view);
    }

    public FlippingView(Context context, Listener listener, int width, int height) {
        super(context);
        this.listener = listener;
        this.flipOutView = new ImageView(context);
        this.flipInView = new ImageView(context);
        addView(flipOutView, width, height);
        addView(flipInView, width, height);
        flipInView.setRotationY(-90);
        ObjectAnimator flipOutAnimator = ObjectAnimator.ofFloat(flipOutView, "rotationY", 0, 90);
        flipOutAnimator.setInterpolator(new AccelerateInterpolator());
        Animator flipInAnimator = ObjectAnimator.ofFloat(flipInView, "rotationY", -90, 0);
        flipInAnimator.setInterpolator(new DecelerateInterpolator());
        animations = new AnimatorSet();
        animations.playSequentially(flipOutAnimator, flipInAnimator);
        animations.addListener(new AnimationListener());
    }

    public void setFlipInDrawable(Drawable drawable) {
        flipInView.setImageDrawable(drawable);
    }

    public void setFlipOutDrawable(Drawable drawable) {
        flipOutView.setImageDrawable(drawable);
    }

    public void setFlipDuration(int flipDuration) {
        animations.setDuration(flipDuration);
    }

    public void flip() {
        animations.start();
    }


    public final class AnimationListener extends AnimatorListenerAdapter {

        @Override
        public void onAnimationEnd(Animator animation) {
            flipOutView.setRotationY(0);
            flipInView.setRotationY(-90);
            listener.onFlipped(FlippingView.this);
        }
    }

}
