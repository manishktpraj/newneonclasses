package codexo.neonclasses.ui.youtube.customController;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

import codexo.neonclasses.R;


public class CustomPlayerUiController extends AbstractYouTubePlayerListener /*implements YouTubePlayerFullScreenListener*/ {

    private final View playerUi;

    private Context context;
    private YouTubePlayer youTubePlayer;
    private YouTubePlayerView youTubePlayerView;

    // panel is used to intercept clicks on the WebView, I don't want the user to be able to click the WebView directly.
    private View panel;
    private View progressbar;
    private TextView videoCurrentTimeTextView,title;
    private TextView videoDurationTextView;
    private ImageView playPauseButton, enterExitFullscreenButton, setting, back10Sec, forward10Sec;
    private SeekBar currentSeekbar;
    private final YouTubePlayerTracker playerTracker;
    private boolean fullscreen = false;
    private String titleString;
private float duration;
    public CustomPlayerUiController(Context context, View customPlayerUi, YouTubePlayer youTubePlayer, YouTubePlayerView youTubePlayerView, String TTILE) {
        this.playerUi = customPlayerUi;
        this.context = context;
        this.youTubePlayer = youTubePlayer;
        this.youTubePlayerView = youTubePlayerView;
        this.titleString = TTILE;
        playerTracker = new YouTubePlayerTracker();
        youTubePlayer.addListener(playerTracker);
        initViews(customPlayerUi);
    }

    private void initViews(View playerUi) {
        panel = playerUi.findViewById(R.id.panel);
        progressbar = playerUi.findViewById(R.id.progressbar);
        videoCurrentTimeTextView = playerUi.findViewById(R.id.video_current_time);
        videoDurationTextView = playerUi.findViewById(R.id.video_duration);
        playPauseButton = playerUi.findViewById(R.id.play_pause_button);
        enterExitFullscreenButton = playerUi.findViewById(R.id.enter_exit_fullscreen_button);
        back10Sec = playerUi.findViewById(R.id.back10Sec);
        forward10Sec = playerUi.findViewById(R.id.forward10Sec);
        currentSeekbar = playerUi.findViewById(R.id.video_seekbar);
        currentSeekbar.setOnSeekBarChangeListener(mVideoSeekBarChangeListener);
        setting = playerUi.findViewById(R.id.setting);
        title = playerUi.findViewById(R.id.tvTitle);
        title.setText(titleString);
        playPauseButton.setOnClickListener((view) -> {
            if (playerTracker.getState() == PlayerConstants.PlayerState.PLAYING) {
                youTubePlayer.pause();
            } else {
                youTubePlayer.play();
            }
        });

        enterExitFullscreenButton.setOnClickListener((view) -> {
            if (fullscreen) {
                youTubePlayerView.exitFullScreen();
            } else {
                youTubePlayerView.enterFullScreen();
            }
            fullscreen = !fullscreen;
        });
        back10Sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float duration=new Float(playerTracker.getCurrentSecond());
                float val =duration-10.0f;
                if(val>0){
                    youTubePlayer.seekTo( val);

                }
            }
        });
        forward10Sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float duration=new Float(playerTracker.getCurrentSecond());
                float val =duration+10.0f;
                if(val<playerTracker.getVideoDuration()){
                    youTubePlayer.seekTo( val);

                }
            }
        });

    }

    @Override
    public void onReady(@NonNull YouTubePlayer youTubePlayer) {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
        switch (state) {
            case PLAYING: {
                playPauseButton.setImageResource(R.drawable.pause_icon);
                panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
                progressbar.setVisibility(View.GONE);
                break;
            }
            case BUFFERING: {
                progressbar.setVisibility(View.VISIBLE);
                panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
                break;
            }
            case ENDED: {
                panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
                break;
            }
            case PAUSED: {
                playPauseButton.setImageResource(R.drawable.play_icon);
                panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
                break;
            }

        }


    }
    private String formatTime(long millis) {
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCurrentSecond(@NonNull YouTubePlayer youTubePlayer, float second) {
        Float measure=new Float(second);
        long val=measure.longValue()*1000;
        videoCurrentTimeTextView.setText(formatTime(val));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onVideoDuration(@NonNull YouTubePlayer youTubePlayer, float duration) {
        Float measure=new Float(duration);
        videoDurationTextView.setText(formatTime(measure.longValue()*1000));

    }

  /*  @Override
    public void onYouTubePlayerEnterFullScreen() {
        ViewGroup.LayoutParams viewParams = playerUi.getLayoutParams();
        viewParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        viewParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        playerUi.setLayoutParams(viewParams);
    }

    @Override
    public void onYouTubePlayerExitFullScreen() {
        ViewGroup.LayoutParams viewParams = playerUi.getLayoutParams();
        viewParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        viewParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        playerUi.setLayoutParams(viewParams);
    }*/

    @Override
    public void onPlaybackQualityChange(@NotNull YouTubePlayer youTubePlayer, @NotNull PlayerConstants.PlaybackQuality playbackQuality) {
        super.onPlaybackQualityChange(youTubePlayer, playbackQuality);
        switch (playbackQuality){
            case SMALL:{
                //320p
            }case MEDIUM:{
                //480p
            }case HD720:{
                //720p
            }case HD1080:{
                //1080p
            }

        }
    }

    @Override
    public void onPlaybackRateChange(@NotNull YouTubePlayer youTubePlayer, @NotNull PlayerConstants.PlaybackRate playbackRate) {
        super.onPlaybackRateChange(youTubePlayer, playbackRate);
        switch (playbackRate){
            case RATE_1:{
                break;
            }
            case RATE_0_5:{

            }
            case RATE_1_5:{
                break;
            }
            case RATE_2:{
                break;
            }
            case RATE_0_25:{
                break;
            }
        }
    }
    SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            float lengthPlayed = (playerTracker.getVideoDuration() * progress) / 100;
            youTubePlayer.seekTo( lengthPlayed);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
}
