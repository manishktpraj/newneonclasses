package codexo.neonclasses.ui.youtube;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.PlaybackParams;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityYoutube2Binding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.youtube.base.YouTubeFailureRecoveryActivity;

public class YoutubeActivity2  extends YouTubeFailureRecoveryActivity implements YouTubePlayer.OnFullscreenListener {
    private YouTubePlayer player;
    private String url;
    private int IS_LIVE_VIDEO = 0;
    private boolean loading = false,isChangeOrientation=false;
    private YouTubePlayer.OnFullscreenListener onFullscreenListener;
    private ActivityYoutube2Binding binding;
    private static String VIDEOID = "", DIS = "", TTILE = "";
    private boolean isPlayerRunning = true;
    private Handler mHandler = null;
    private FirebaseDatabase mDatabase;
    ImageView sendchat;
    EditText typemessage;
    SessionManager session;
    private ValueEventListener mSearchedLocationReferenceListener;
    DatabaseReference messagesRef;
    private boolean doubleBackToExitPressedOnce=false;
    JSONArray arrayListchat = new JSONArray();

    private static LinearLayoutManager mLayoutManager;
    RecyclerView recycler_view;
    private Float speed=1f;

    JSONObject video_detail =new JSONObject();
    String vid_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_youtube2);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //getWindow().getInsetsController().hide(WindowInsets.Type.statusBars());
        } else {

            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            );
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        ///     VIDEOID = getIntent().getStringExtra("video_id");
        //// DIS = getIntent().getStringExtra("dis");
        ///     TTILE = getIntent().getStringExtra("title");
        /////   binding.tvTitle.setText(TTILE);
        onFullscreenListener = this;
        binding.youtubePlayerView.initialize(getString(R.string.youtube_app_id), this);
        AccessClickLIstners();
        mHandler = new Handler();
        session = new SessionManager(getApplicationContext());


        try {
            video_detail = new JSONObject(getIntent().getStringExtra("video_detail"));
            binding.tvTitle.setText(video_detail.getString("vid_title"));
            vid_id = video_detail.getString("vid_id");
            binding.tvTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));
            VIDEOID = video_detail.getString("vid_url");
            String MESSAGE_CHANNEL = "/neonmain/" + video_detail.getString("vid_id") + "/";
//            Constant.logPrint(MESSAGE_CHANNEL,"MESSAGE_CHANNEL");
            mDatabase = FirebaseDatabase.getInstance();
            messagesRef = mDatabase.getReference().child(MESSAGE_CHANNEL);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        recycler_view = findViewById(R.id.recycler_view);
        sendchat = findViewById(R.id.img_send);
        typemessage = findViewById(R.id.edit_chat_box);
        sendchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String go_next = "yes";
                String username_string = typemessage.getText().toString();
                if (username_string.equals("")) {
                    int duration = Toast.LENGTH_SHORT;
                    typemessage.setError("Please Enter Your Question");
                    go_next = "no";
                }

                if (go_next.equals("yes")) {
                    Long daedat = new Date().getTime();

                    Date currentTime = Calendar.getInstance().getTime();
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    String userid = session.getUserId();
                    Map<String, String> formData = new HashMap<String, String>();
                    formData.put("user_id", userid);
                    formData.put("doubts_user_name", session.getUserName());
                    formData.put("doubts_user_email", session.getUserEmail());
                    formData.put("doubts_user_phone", session.getUserPhone());
                    formData.put("video_chat_reply", "");
                    formData.put("video_reply_time", "");
                    formData.put("videochat_created", formattedDate + "");
                    formData.put("videochat_message", username_string);
//                    Constant.logPrint(formData+"","dddddddddddddddd");
//                    Constant.logPrint(daedat+"","daedatdaedatdaedatdaedatdaedat"+formData+"");
                    // messagesRef.child(daedat+"").setValue(formData);
                    messagesRef.push().setValue(formData);
                    binding.editChatBox.setText("");
                    Toast.makeText(YoutubeActivity2.this, "Message Sent Successfully" + userid, Toast.LENGTH_SHORT).show();
                }
            }
        });
        Activity activity = YoutubeActivity2.this;
        mSearchedLocationReferenceListener = messagesRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayListchat = new JSONArray();
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String location = locationSnapshot.getValue().toString();
//                    Log.d("Locations updated", "location: "+vid_id + location);

                    if (locationSnapshot != null) {
                        try {
                            JSONObject objd = new JSONObject();
                            objd.put("user_id", locationSnapshot.child("user_id").getValue().toString());
                            objd.put("videochat_created", locationSnapshot.child("videochat_created").getValue().toString());
                            objd.put("videochat_message", locationSnapshot.child("videochat_message").getValue().toString());
                            objd.put("video_chat_reply", locationSnapshot.child("video_chat_reply").getValue().toString());
                            objd.put("video_reply_time", locationSnapshot.child("video_reply_time").getValue().toString());
                            objd.put("doubts_user_email", locationSnapshot.child("doubts_user_email").getValue().toString());
                            objd.put("doubts_user_name", locationSnapshot.child("doubts_user_name").getValue().toString());
                            objd.put("doubts_user_phone", locationSnapshot.child("doubts_user_phone").getValue().toString());

                            ///       Log.d("updatedupdatedupdatedupdatedupdated", "location: " +  objd);
                            if (objd.getString("user_id").equals(session.getUserId())) {
                                arrayListchat.put(objd);

                            }

                            if (!binding.recyclerView.equals(null)) {
                                final CustomAdapterCat adapter = new CustomAdapterCat(activity);
                                mLayoutManager = new LinearLayoutManager(activity,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                binding.recyclerView.setLayoutManager(mLayoutManager);
                                binding.recyclerView.setHasFixedSize(true);
                                binding.recyclerView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void AccessClickLIstners() {

        binding.videoSeekbar.setOnSeekBarChangeListener(mVideoSeekBarChangeListener);
       /* binding.autoHideLay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        binding.lay.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_MOVE:

                        binding.lay.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_UP:
                        binding.lay.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.lay.setVisibility(View.INVISIBLE);
                            }
                        }, 3000);
                        break;
                }
                return true;
            }
        });*/
        binding.back10Sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long getmillNew=player.getCurrentTimeMillis()-10000;
                if(getmillNew>0){
                    player.seekToMillis((int) getmillNew);
                    displayCurrentTime();
                }
            }
        });
        binding.forward10Sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long getmillNew=player.getCurrentTimeMillis()+10000;
                if(getmillNew<player.getDurationMillis()){
                    player.seekToMillis((int) getmillNew);
                    displayCurrentTime();
                }
            }
        });

        binding.playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!loading) {
                    if (isPlayerRunning) {
                        if (null != player && player.isPlaying())
                            player.pause();
                        isPlayerRunning = false;
                        binding.playPauseBtn.setImageResource(R.drawable.exo_controls_play);
                    } else {
                        isPlayerRunning = true;
                        if (null != player && !player.isPlaying())
                            player.play();
                        binding.playPauseBtn.setImageResource(R.drawable.exo_controls_pause);
                    }
                } else {
                    Toast.makeText(YoutubeActivity2.this, "Please Wait...", Toast.LENGTH_SHORT).show();
                }

            }
        });
        binding.screenOrientationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (getResources().getConfiguration().orientation){
                    case Configuration.ORIENTATION_PORTRAIT:{
                        player.setFullscreen(true);
                        break;
                    }
                    case Configuration.ORIENTATION_LANDSCAPE:{
                        player.setFullscreen(false);

                    }
                }
            }
        });
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.tvTitle.setVisibility(View.GONE);
            binding.linearLayout.setVisibility(View.VISIBLE);
            player.setFullscreen(true);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            binding.tvTitle.setVisibility(View.VISIBLE);
            binding.linearLayout.setVisibility(View.VISIBLE);
            player.setFullscreen(false);
        }
    }

    @Override
    public void onFullscreen(boolean fullscreen) {
        //  binding.viewContainer.setEnablePadding(!fullscreen);
        ///  ViewGroup.LayoutParams playerParams = binding.mainLay.getLayoutParams();
        if (fullscreen) {
            //playerParams.width = MATCH_PARENT;
            /////  playerParams.height = MATCH_PARENT;
        } else {
            //   playerParams.width = MATCH_PARENT;
            ////    playerParams.height = WRAP_CONTENT;
        }
        //// player.setFullscreen(fullscreen);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            player.play();
        }

    }

    @Override
    protected void onPause() {
        if (player != null) {
            player.pause();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (player != null) {
            player.release();
        }
        messagesRef.removeEventListener(mSearchedLocationReferenceListener);
        super.onDestroy();
    }

    YouTubePlayer.PlaybackEventListener mPlaybackEventListener = new YouTubePlayer.PlaybackEventListener() {


        @Override
        public void onBuffering(boolean loadingMain) {
            loading = loadingMain;

        }

        @Override
        public void onPaused() {
            binding.playPauseBtn.setImageResource(R.drawable.exo_controls_play);
            mHandler.removeCallbacks(runnable);
        }

        @Override
        public void onPlaying() {
            binding.playPauseBtn.setImageResource(R.drawable.exo_controls_pause);
            mHandler.postDelayed(runnable, 100);
            displayCurrentTime();
        }

        @Override
        public void onSeekTo(int arg0) {
            mHandler.postDelayed(runnable, 100);
        }

        @Override
        public void onStopped() {
            mHandler.removeCallbacks(runnable);
        }
    };

    YouTubePlayer.PlayerStateChangeListener mPlayerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
            loading = true;
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
            switch (arg0){
                case NETWORK_ERROR:{
                    Toast.makeText(YoutubeActivity2.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    break;
                }
                case PLAYER_VIEW_TOO_SMALL:{
                    Toast.makeText(YoutubeActivity2.this, "Player View Not Visible as Per Api Documentation", Toast.LENGTH_SHORT).show();
                    break;
                }
                case UNAUTHORIZED_OVERLAY:{

                    Toast.makeText(YoutubeActivity2.this, "Any View overlay Your Player", Toast.LENGTH_SHORT).show();
                    break;
                }
                case AUTOPLAY_DISABLED:{
                    Toast.makeText(YoutubeActivity2.this, "Auto Play Disabled", Toast.LENGTH_SHORT).show();
                    break;
                }
                case NOT_PLAYABLE:{
                    Toast.makeText(YoutubeActivity2.this, "This Video Not Playable", Toast.LENGTH_SHORT).show();
                    break;
                }
            }

        }

        @Override
        public void onLoaded(String arg0) {
            loading = false;

        }

        @Override
        public void onLoading() {

            loading = true;
        }

        @Override
        public void onVideoEnded() {
            binding.playPauseBtn.setImageResource(R.drawable.play_icon);
            loading = false;
            finish();
        }

        @Override
        public void onVideoStarted() {
            binding.playPauseBtn.setImageResource(R.drawable.pause_icon);
            loading = false;
            isPlayerRunning = true;
            displayCurrentTime();

        }
    };

    SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            long lengthPlayed = (player.getDurationMillis() * progress) / 100;
            player.seekToMillis((int) lengthPlayed);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private void displayCurrentTime() {
        if (null == player) return;
        int mills=player.getDurationMillis() - player.getCurrentTimeMillis();

        String formattedTime = formatTime(player.getDurationMillis()-mills);
        binding.mPlayTimeTextView.setText(formattedTime);
    }
    private void disPlayEndTime() {
        if (null == player) return;
        String formattedTime = formatTime(player.getDurationMillis() );
        binding.totalVideoSize.setText(formattedTime);
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayCurrentTime();
            disPlayEndTime();
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;

        this.player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        this.player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        //  this.player.setOnFullscreenListener(onFullscreenListener);
        this.player.setPlayerStateChangeListener(mPlayerStateChangeListener);
        this.player.setPlaybackEventListener(mPlaybackEventListener);
        binding.quality1.setTextColor(getResources().getColor(R.color.red));
        binding.quality1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long lengthPlayed = (player.getCurrentTimeMillis());
                player.seekToMillis((int) lengthPlayed+1);
                binding.quality1.setTextColor(getResources().getColor(R.color.red));
                binding.quality2.setTextColor(getResources().getColor(R.color.white));
                binding.quality3.setTextColor(getResources().getColor(R.color.white));

            }
        });
        binding.quality2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long lengthPlayed = (player.getCurrentTimeMillis());
                player.seekToMillis((int) lengthPlayed+1);

                binding.quality1.setTextColor(getResources().getColor(R.color.white));
                binding.quality2.setTextColor(getResources().getColor(R.color.red));
                binding.quality3.setTextColor(getResources().getColor(R.color.white));
            }
        });
        binding.quality3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long lengthPlayed = (player.getCurrentTimeMillis());
                player.seekToMillis((int) lengthPlayed+1);

                binding.quality1.setTextColor(getResources().getColor(R.color.white));
                binding.quality2.setTextColor(getResources().getColor(R.color.white));
                binding.quality3.setTextColor(getResources().getColor(R.color.red));
            }
        });
/*
        binding.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(YoutubeActivity.this, binding.quality);
                popupMenu.inflate(R.menu.quality);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked
                        if (player != null) {

                            switch (menuItem.getItemId()) {

                                case R.id.p240_menu: {
                                    if (player != null) {
                                        ///   player.setPlaybackParameters(new PlaybackParameters(0.5f));
                                        ///setquality(0);
                                        loading = true;

                                    }
                                    break;
                                }
                                case R.id.p360_menu: {
                                    if (player != null) {
                                        loading = true;
                                        ///     player.setPlaybackParameters(new PlaybackParameters(1f));
                                        ////  setquality(0);
                                    }
                                    break;
                                }

                                case R.id.p720_menu: {
                                    if (player != null) {
                                        loading = true;
                                        ///    setquality(1);

                                        ////   player.setPlaybackParameters(new PlaybackParameters(2f));
                                    }
                                    break;
                                }

                            }
                            // Showing the popup menu


                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });
*/
/*
        binding.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.qualityLayout.setVisibility((binding.qualityLayout.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);
                binding.p240Menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            ///     player.setPlaybackParameters(new PlaybackParameters(1f));
                              ///setquality(0);

                            binding.qualityLayout.setVisibility(View.GONE);
                            binding.p240Menu.setTextColor(Color.RED);
                            binding.p360Menu.setTextColor(Color.WHITE);
                            binding.p720Menu.setTextColor(Color.WHITE);
                        }
                    }
                });
                binding.p360Menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            ///     player.setPlaybackParameters(new PlaybackParameters(1f));
                            ////  setquality(0);
                            binding.qualityLayout.setVisibility(View.GONE);
                            binding.p720Menu.setTextColor(Color.WHITE);
                            binding.p360Menu.setTextColor(Color.RED);
                            binding.p240Menu.setTextColor(Color.WHITE);
                        }
                    }
                });
                binding.p720Menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            ///    setquality(1);
                            ////   player.setPlaybackParameters(new PlaybackParameters(2f));
                            binding.qualityLayout.setVisibility(View.GONE);
                            binding.p720Menu.setTextColor(Color.RED);
                            binding.p360Menu.setTextColor(Color.WHITE);
                            binding.p240Menu.setTextColor(Color.WHITE);
                        }
                    }
                });
            }
        });
*/



        if (VIDEOID != null && !VIDEOID.isEmpty()) {
            String videoId = new YouTubeHelper().extractVideoIdFromUrl(VIDEOID);
            if (videoId != null && !videoId.isEmpty() && player != null) {
                this.player.loadVideo(videoId);
                this.player.play();
                disPlayEndTime();
                displayCurrentTime();
            }
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return binding.youtubePlayerView;
    }

    public class CustomAdapterCat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        Activity activity;

        private static final int TYPE_ITEM = 1;

        public CustomAdapterCat(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            CustomAdapterCat.MainListHolder listHolder = new CustomAdapterCat.MainListHolder(itemView);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final CustomAdapterCat.MainListHolder mainHolder = (CustomAdapterCat.MainListHolder) holder;



            try {

                ///     mainHolder.title.setText( result_cat.getJSONObject(position).getString("bcat_name"));
                mainHolder.usercomment.setText(arrayListchat.getJSONObject(position).getString("videochat_message"));
                String comment = arrayListchat.getJSONObject(position).getString("videochat_message");
                String reply = arrayListchat.getJSONObject(position).getString("video_chat_reply");
                if (!comment.equals("")) {
                    mainHolder.admincomment.setText(comment);
                }

                if (!reply.equals("")) {
                    mainHolder.usercomment.setText(reply);
                } else {
                    mainHolder.usercomment.setVisibility(View.GONE);
                }



            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayListchat.length();
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }

        public class MainListHolder extends RecyclerView.ViewHolder {
            TextView top,title,admincomment,usercomment;
            ImageView video_image;
            CardView card;
            public MainListHolder(View view) {
                super(view);
                admincomment = view.findViewById(R.id.txt_des);
                usercomment = view.findViewById(R.id.txt_reply);
            }
        }
    }

    @Override
    public void onBackPressed() {
        switch (getResources().getConfiguration().orientation){
            case Configuration.ORIENTATION_PORTRAIT:{
                finish();
                break;
            }
            case Configuration.ORIENTATION_LANDSCAPE:{

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                player.setFullscreen(false);

            }
        }
    }
}