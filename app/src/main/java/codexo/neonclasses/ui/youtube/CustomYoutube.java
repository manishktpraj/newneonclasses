package codexo.neonclasses.ui.youtube;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerUtils;

import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCustomYoutubeBinding;
import codexo.neonclasses.ui.youtube.customController.CustomPlayerUiController;


public class CustomYoutube extends AppCompatActivity {
    private static String VIDEOID = "", DIS = "", TTILE = "";
    private boolean isPlayerRunning = true;
    private ActivityCustomYoutubeBinding binding;
    private YouTubePlayer player;
    private  YouTubePlayerListener playerTracker;
    private boolean fullscreen = false;
    private YouTubePlayerFullScreenListener fullScreenListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeMain);
        ActionBar actionbar = getSupportActionBar();
        actionbar.hide();
        binding= DataBindingUtil.setContentView(this,R.layout.activity_custom_youtube);
        VIDEOID = getIntent().getStringExtra("video_id");
        String videoId = new YouTubeHelper().extractVideoIdFromUrl(VIDEOID);
        DIS = getIntent().getStringExtra("dis");
        TTILE = getIntent().getStringExtra("title");
        getLifecycle().addObserver(binding.youtubePlayerView);
        View customPlayerUi = binding.youtubePlayerView.inflateCustomPlayerUi(R.layout.custom_player_ui);
        binding.youtubePlayerView.enableBackgroundPlayback(false);
        binding.youtubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                CustomPlayerUiController customPlayerUiController = new CustomPlayerUiController(CustomYoutube.this, customPlayerUi, youTubePlayer, binding.youtubePlayerView,TTILE);
                youTubePlayer.addListener(customPlayerUiController);
                player=youTubePlayer;
              /*  binding.youtubePlayerView.addFullScreenListener(customPlayerUiController);*/
                YouTubePlayerUtils.loadOrCueVideo(
                        youTubePlayer, getLifecycle(),
                        videoId,0f
                );

            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
          binding.youtubePlayerView.enterFullScreen();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            binding.youtubePlayerView.exitFullScreen();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
if(player!=null)player.play();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player!=null){
            player.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.youtubePlayerView.release();
    }



}