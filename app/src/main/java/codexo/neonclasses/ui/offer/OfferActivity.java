package codexo.neonclasses.ui.offer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityOfferBinding;
import codexo.neonclasses.databinding.ItemMyPurchaseBinding;
import codexo.neonclasses.databinding.ItemOffersBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.notification.NotificationActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class OfferActivity extends AppCompatActivity {
    SessionManager session;
    ProgressDialog pDialog;
    private ActivityOfferBinding binding;
    JSONArray mypurchaselist = new JSONArray();
    String clickType = "";
    String amount = "";
    String apply_on = "";
    String apply_on_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(OfferActivity.this);
        pDialog = Constant.getProgressBar(OfferActivity.this);
        binding = ActivityOfferBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Constant.adbEnabled(OfferActivity.this);
        Constant.checkAdb(OfferActivity.this);

//        clickType = getIntent().getStringExtra("type");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            clickType = extras.getString("type");
            amount = extras.getString("amount");
            apply_on = extras.getString("apply_on");
            apply_on_id = extras.getString("apply_on_id");
        } else {
            // handle case
        }
        Constant.logPrint("dkdkdkkdkdfkkkmbm", clickType);

        binding.txtHello.setTypeface(Constant.getFontsBold(OfferActivity.this));
        binding.imgBack.setOnClickListener(view -> finish());
        getpurchase();
    }


    public class RecyclerAdapter1 extends RecyclerView.Adapter<OfferActivity.RecyclerAdapter1.RecyclerViewHolder> {
        ItemOffersBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public OfferActivity.RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemOffersBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            OfferActivity.RecyclerAdapter1.RecyclerViewHolder holder = new OfferActivity.RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull OfferActivity.RecyclerAdapter1.RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                binding.txtTitle.setText(item.getString("coupon_title"));
                binding.tvCouponsName.setText(item.getString("coupon_code"));
                if (item.getString("coupon_description").equals("null") || item.getString("coupon_description").equals("")) {
                    binding.txtDes.setVisibility(View.GONE);
                } else {
                    binding.txtDes.setText(item.getString("coupon_description"));
                }

                String upd_expiry_date = item.getString("coupon_end_date");


                if (!upd_expiry_date.equals("null")) {
                    String replaced = upd_expiry_date.replaceAll("T", " ");
                    String[] replaceddd = replaced.split(" ");
                    String ordat = replaceddd[0];
                    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
                    Date Ordate = (Date) parser.parse(ordat);
                    DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
                    String packageexpDate = formatter.format(Ordate);

                    SimpleDateFormat dateFormatJoin = new SimpleDateFormat("yyyy-MM-dd");
                    Date endtime = dateFormatJoin.parse(upd_expiry_date);
                    Date pasTime = new Date();
                    long ptime = pasTime.getTime();
                    long expdata = endtime.getTime();
                    long presentday = TimeUnit.MILLISECONDS.toDays(ptime);
                    long expday = TimeUnit.MILLISECONDS.toDays(expdata);

                    if (presentday <= expday) {
                        binding.tvCouponTime.setText("Ends on : " + packageexpDate);
                    } else {
                        binding.tvCouponTime.setText("Ended on : " + packageexpDate);
                    }
                    // binding.orderDate.setText("Order Date : "+order_date);
                    binding.tvCouponTime.setVisibility(View.VISIBLE);
                } else {
                    binding.tvCouponTime.setVisibility(View.GONE);
                }


                try {
                    if (clickType.equals("cart")) {
                        binding.btnApply.setVisibility(View.VISIBLE);
                        binding.btnApply.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    JSONObject item = listItem.getJSONObject(position);
                                    String coupon_code = item.getString("coupon_code");
                                    String coupon_apply_on = item.getString("coupon_apply_on");
                                    checkPromoCode(coupon_code,coupon_apply_on);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    } else if (clickType.equals("home")) {
                        binding.btnApply.setVisibility(View.GONE);
                    } else {
                        binding.btnApply.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemOffersBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }


    private void checkPromoCode(String couponCode,String coupon_apply_on) {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getcoupondetail(session.getUserId(), couponCode, apply_on_id, coupon_apply_on, amount);
//        Constant.logPrint("alttfffefef", "studentId = "+session.getUserId()+"\ncoupon_code = "+couponCode+ "\napply_on_id = "+apply_on_id+"\namount = "+amount+"\ncoupon_apply_on = "+coupon_apply_on);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            Constant.amountAfterCoupon = jsonObject.getString("amount");
                            Constant.couponDiscount = jsonObject.getString("discount");
                            Constant.setToast(OfferActivity.this, jsonObject.getString("result"));
                            finish();
                        } else {
                            Constant.setToast(OfferActivity.this, jsonObject.getString("result"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                checkPromoCode(couponCode,coupon_apply_on);
            }
        });
    }

    private void getpurchase() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getoffers(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            mypurchaselist = jsonObject.getJSONArray("result");
                            OfferActivity.RecyclerAdapter1 recyclerAdapter = new OfferActivity.RecyclerAdapter1(OfferActivity.this, mypurchaselist);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(OfferActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(OfferActivity.this, "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getpurchase();
            }
        });
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(OfferActivity.this);
        Constant.checkAdb(OfferActivity.this);
    }
}