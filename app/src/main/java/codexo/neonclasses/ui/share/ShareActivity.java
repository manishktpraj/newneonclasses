package codexo.neonclasses.ui.share;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityShareBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.purchase.MyPurchaseActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class ShareActivity extends AppCompatActivity {

    private ActivityShareBinding binding;
    String shareBody;
    SessionManager session;
    ProgressDialog pDialog;
    String share_code="",text_share_message="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityShareBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(ShareActivity.this);
        Constant.checkAdb(ShareActivity.this);
        session = new SessionManager(getApplicationContext());
        pDialog = Constant.getProgressBar(ShareActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtShareCode.setText(session.getUserShareCode());
        binding.txtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doshhare();
            }
        });
        getpurchase();
    }

    private void getpurchase() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getsharecontent(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                        share_code = jsonObject.getString("user_share_code");
                            text_share_message = jsonObject.getString("text_share_message");
                            binding.txtShareCode.setText(share_code);

                        } else {
                            Constant.setToast(ShareActivity.this,"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getpurchase();
            }
        });
    }


    public void doshhare()
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
         sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Neon Classes");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_share_message);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(ShareActivity.this);
        Constant.checkAdb(ShareActivity.this);
    }
}
