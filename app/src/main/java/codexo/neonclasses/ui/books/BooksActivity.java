package codexo.neonclasses.ui.books;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityBooksBinding;
import codexo.neonclasses.databinding.BooksRowBinding;
import codexo.neonclasses.databinding.FreeVideoListBinding;
import codexo.neonclasses.session.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;

public class BooksActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    SessionManager session;
    JSONArray books_list =new JSONArray();
    ActivityBooksBinding binding;
    String books_url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding  =ActivityBooksBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(BooksActivity.this);
        Constant.checkAdb(BooksActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        pDialog = Constant.getProgressBar(BooksActivity.this);
        session = new SessionManager(BooksActivity.this);
        neonbooks();
    }

    private void neonbooks() {



        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().neonbooks(session.getUserId());
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            books_list = jsonObject.getJSONArray("results");
                            books_url = jsonObject.getString("books_url");
                            BooksActivity.RecyclerAdapter2 recyclerAdapter = new BooksActivity.RecyclerAdapter2(getApplicationContext(), books_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(getApplicationContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                neonbooks();
            }
        });
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<BooksActivity.RecyclerAdapter2.RecyclerViewHolder> {
        BooksRowBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public BooksActivity.RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = BooksRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            BooksActivity.RecyclerAdapter2.RecyclerViewHolder holder = new BooksActivity.RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }



        @Override
        public void onBindViewHolder(@NonNull BooksActivity.RecyclerAdapter2.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = books_url+item.getString("product_image");
                Constant.setImage(img,binding.image,getApplicationContext());
                binding.txtTitle.setText(item.getString("product_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(BooksActivity.this));
                binding.txtAuthor.setText("By "+item.getString("product_author"));
                binding.txtAuthor.setTypeface(Constant.getsemiFonts( BooksActivity.this));
                binding.txtDiscountedPrice.setText("₹"+item.getString("product_discount_price"));
                binding.txtDiscountedPrice.setTypeface(Constant.getsemiFonts( BooksActivity.this));

                binding.txtPrice.setText("₹"+item.getString("product_discount_selling"));
                binding.txtPrice.setTypeface(Constant.getsemiFonts( BooksActivity.this));
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                 String disperc = Constant.getdiscount(item.getString("product_original_price"),item.getString("product_discount_price"));
                if(!disperc.equals(""))
                {
                    binding.txtDiscount.setText(disperc+"%");
                    binding.txtDiscount.setTypeface(Constant.getsemiFonts(getApplicationContext()));
                }
                String url =item.getString("product_affurl");
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!url.equals(""))
                        {
                            Intent vidcategory = new Intent(getApplicationContext(), BookDetailActivity.class);
                            vidcategory.putExtra("bookdetail", item + "");
                            vidcategory.putExtra("book_url", books_url + "");
                            startActivity(vidcategory);
                          /*  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(Intent.createChooser(browserIntent, "Browse with"));*/

                            ////      startActivity(browserIntent);
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull BooksRowBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(BooksActivity.this);
        Constant.checkAdb(BooksActivity.this);
    }
}