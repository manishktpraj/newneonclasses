package codexo.neonclasses.ui.books;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.google.android.exoplayer2.extractor.mp4.Track;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.AnalyticsApplication;
import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.Register;
import codexo.neonclasses.databinding.ActivityBookDetailBinding;
import codexo.neonclasses.databinding.ActivityBooksBinding;

public class BookDetailActivity extends AppCompatActivity {

    ActivityBookDetailBinding binding;
    JSONObject bookdetail = new JSONObject();
    String book_url ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding  = ActivityBookDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Constant.adbEnabled(BookDetailActivity.this);
        Constant.checkAdb(BookDetailActivity.this);
        Constant.setStatusBar(BookDetailActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));

      ////  Tracker mTracker = ((AnalyticsApplication) getApplication()).getDefaultTracker();

        try {
            book_url = getIntent().getStringExtra("book_url");
            bookdetail =new JSONObject(getIntent().getStringExtra("bookdetail"));
            String img = book_url+bookdetail.getString("product_image");
            Constant.setImage(img,binding.image,getApplicationContext());
            binding.txtTitle.setText(bookdetail.getString("product_title"));
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));

           /* mTracker.setScreenName("books~" + bookdetail.getString("product_title"));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
*/

            binding.txtDiscountedPrice.setText("₹"+bookdetail.getString("product_discount_price"));
            binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
            binding.txtPrice.setText("₹"+bookdetail.getString("product_original_price"));
            binding.txtPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
            binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            String disperc = Constant.getdiscount(bookdetail.getString("product_original_price"),
                    bookdetail.getString("product_discount_price"));
//            Constant.logPrint(disperc+"","dispercdispercdispercdispercdisperc");
            if(!disperc.equals(""))
            {
                binding.txtDiscount.setText(disperc+"%");
                binding.txtDiscount.setTypeface(Constant.getFontsBold(getApplicationContext()));
            }
            String url =bookdetail.getString("product_affurl");
            String url2 =bookdetail.getString("product_flipkart");

            binding.flipkartbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url2));
                    startActivity(Intent.createChooser(browserIntent, "Browse with"));
                }
            });
            binding.amazonbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(Intent.createChooser(browserIntent, "Browse with"));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(BookDetailActivity.this);
        Constant.checkAdb(BookDetailActivity.this);
    }
}