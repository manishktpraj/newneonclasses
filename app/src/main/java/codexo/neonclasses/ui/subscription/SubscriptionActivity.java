package codexo.neonclasses.ui.subscription;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivitySubscriptionBinding;
import codexo.neonclasses.databinding.ItemMyPurchaseBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.purchase.MyPurchaseActivity;
import codexo.neonclasses.ui.share.ShareActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class SubscriptionActivity extends AppCompatActivity {

    private ActivitySubscriptionBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray subscrption_list =new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySubscriptionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(SubscriptionActivity.this);
        Constant.checkAdb(SubscriptionActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(SubscriptionActivity.this));


        session = new SessionManager(SubscriptionActivity.this);
        pDialog = Constant.getProgressBar(SubscriptionActivity.this);

        getsubscription();

    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemMyPurchaseBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemMyPurchaseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            if(position==0)
            {
               binding.topspace.setVisibility(View.VISIBLE);
            }else{
                binding.topspace.setVisibility(View.GONE);
            }
            try {
                JSONObject item = listItem.getJSONObject(position);
                binding.txtOrderId.setText("Order ID : "+item.getString("upd_id"));
                if(!item.getString("upd_activation_date").equals("null")) {
                 ////   binding.txtDate.setText("Date : "+item.getString("upd_activation_date"));

                    String replaced = item.getString("upd_activation_date").replaceAll("T"," ");
                    String replacedd = replaced.replace("+05:30","");
                    String[] replaceddd = replacedd.split(" ");
//                    Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replaceddd[0]);
                    binding.txtDate.setText("Date : "+replaceddd[0]);


                    String replaced_d = item.getString("upd_expiry_date").replaceAll("T"," ");
                    String replacedd_d = replaced_d.replace("+05:30","");
                    String[] replacedddd = replacedd_d.split(" ");
//                    Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replacedddd[0]);
                    binding.txtDateEx.setText("Expiry Date : "+replacedddd[0]);


                }else{

                    binding.txtDate.setText("");
                    binding.txtDateEx.setVisibility(View.GONE);
                }
                binding.txtDes.setText(item.getJSONObject("neon_package").getString("sub_pack_title"));
                binding.txtPrice.setText("₹"+item.getString("upd_package_amount"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemMyPurchaseBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    private void getsubscription() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getsubscription(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            subscrption_list = jsonObject.getJSONArray("results");
                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(SubscriptionActivity.this, subscrption_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(SubscriptionActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(SubscriptionActivity.this,"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getsubscription();
            }
        });
    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(SubscriptionActivity.this);
        Constant.checkAdb(SubscriptionActivity.this);
    }
}
