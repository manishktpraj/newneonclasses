package codexo.neonclasses.ui.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityMockTestSubCategoryBinding;
import codexo.neonclasses.databinding.ItemRowTestSeriesListBinding;

public class MockTestSubCategoryActivity extends AppCompatActivity {
    ActivityMockTestSubCategoryBinding binding;
    JSONArray childArray = new JSONArray();
    String title="",imageUrl="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMockTestSubCategoryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.setStatusBar(MockTestSubCategoryActivity.this);
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        try {
            childArray = new JSONArray(getIntent().getStringExtra("child"));
            title = getIntent().getStringExtra("title");
            imageUrl = getIntent().getStringExtra("imageUrl");
            binding.txtHello.setText(title);
            if (childArray.length() <= 0) {
                binding.recycler.setVisibility(View.GONE);
                binding.emptyView.setVisibility(View.VISIBLE);
            } else {
                binding.recycler.setVisibility(View.VISIBLE);
                binding.emptyView.setVisibility(View.GONE);
                SectionalTestAdapter adapter = new SectionalTestAdapter(getApplicationContext(), childArray);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                binding.recycler.setLayoutManager(layoutManager);
                binding.recycler.setAdapter(adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class SectionalTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemRowTestSeriesListBinding binding;
        Context context;
        JSONArray listItem;

        public SectionalTestAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public SectionalTestAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemRowTestSeriesListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            SectionalTestAdapter.RecyclerViewHolder holder = new SectionalTestAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

            try {

                JSONObject item = childArray.getJSONObject(i);
                binding.txtTitle.setText(item.getString("exam_cat_name"));
                String imgname = item.getString("exam_cat_image");
                Constant.setImage(imageUrl+imgname,binding.testImage,getApplicationContext());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return childArray.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemRowTestSeriesListBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}