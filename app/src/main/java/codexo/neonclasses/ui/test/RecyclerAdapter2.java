package codexo.neonclasses.ui.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;

import java.util.List;

import codexo.neonclasses.databinding.ItemFreeMockTestBinding;

public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
    ItemFreeMockTestBinding binding;
    Context context;
    JSONArray listItem;
    ClickListener clickListener;

    public RecyclerAdapter2(Context context, JSONArray listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemFreeMockTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        RecyclerAdapter2.RecyclerViewHolder holder = new RecyclerAdapter2.RecyclerViewHolder(binding);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter2.RecyclerViewHolder holder, int position) {

        holder.setIsRecyclable(false);
        holder.setClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        return listItem.length();
    }

    interface ClickListener {
        void onClickItem(int post);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ClickListener clickListener;

        public RecyclerViewHolder(@NonNull ItemFreeMockTestBinding itemView) {
            super(itemView.getRoot());

            itemView.txtResult.setOnClickListener(view -> {
                clickListener.onClickItem(getAdapterPosition());
            });
        }

        public void setClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }
    }
}