package codexo.neonclasses.ui.test;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import codexo.neonclasses.databinding.ItemAllTestBinding;

public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
    ItemAllTestBinding binding;
    Context context;
    List<String> listItem;
    ClickListener clickListener;

    public RecyclerAdapter1(Context context, List<String> listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemAllTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
        String item = listItem.get(position);
        holder.setIsRecyclable(false);
        holder.setClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    interface ClickListener {
        void onClickItem(int post);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ClickListener clickListener;


        public RecyclerViewHolder(@NonNull ItemAllTestBinding itemView) {
            super(itemView.getRoot());

            StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
            String s = "₹1000";
            itemView.txtPrice.setText(s, TextView.BufferType.SPANNABLE);
            Spannable spannable = (Spannable) itemView.txtPrice.getText();
            spannable.setSpan(STRIKE_THROUGH_SPAN, 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            itemView.parent.setOnClickListener(view -> {
                clickListener.onClickItem(getAdapterPosition());
            });
        }

        public void setClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }
    }
}