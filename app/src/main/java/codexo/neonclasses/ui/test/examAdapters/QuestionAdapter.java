package codexo.neonclasses.ui.test.examAdapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ItemRowQuestionLayoutBinding;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.model.SubjectInfoModel;
import codexo.neonclasses.model.UstQuesAnswerModel;
import codexo.neonclasses.ui.test.AttemptInstruction;
import codexo.neonclasses.ui.test.ExamActivity;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.RecyclerViewHolder> {
    ItemRowQuestionLayoutBinding binding;
    Context context;
    List<QuestionModel> listItem;
    List<UstQuesAnswerModel> ustqalistItem;
    List<SubjectInfoModel> data = new ArrayList<>();
    SqlDatabase db;
    String test_id;
    int Language, examType;
    private int selectedItemPosition = 0;
    private ExamActivity examActivity;
    int row_index = -1, row_index2 = -1, row_index3 = -1, row_index4 = -1, row_index5 = -1;
    UstQuesAnswerModel ustqam;
    Handler handler;
    Cursor getnextprevdata;
    int checkallstatus, quecorrectOpt;
    public QuestionAdapter(Context context, List<QuestionModel> listItem,List<UstQuesAnswerModel> ustqalistItem, String test_id, int examType) {
        this.context = context;
        this.listItem = listItem;
        this.test_id = test_id;
        this.examType = examType;
        this.ustqalistItem = ustqalistItem;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemRowQuestionLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        RecyclerViewHolder holder = new RecyclerViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
        QuestionModel questionModel = listItem.get(position);
        ustqam = ustqalistItem.get(position);
        binding.txtHeaderTitle.setText("Q. " + questionModel.tlq_question_text);
        quecorrectOpt = questionModel.tlq_id;
        db = new SqlDatabase(context);
        data = db.getRawExamInfo(Integer.parseInt(test_id));
        getnextprevdata = db.getallquestiondataofnewxtprev(Integer.parseInt(test_id), quecorrectOpt);
        getnextprevdata.moveToFirst();
        checkallstatus = getnextprevdata.getInt(getnextprevdata.getColumnIndex("ustqa_user_ans"));
        for (int i = 0; i < data.size(); i++) {
            binding.tvquestionNo.setText("Q. " + String.valueOf(questionModel.tlq_counterid));
            binding.negativeMarks.setText("-" + data.get(i).test_negative_marks);
            binding.totalMarks.setText("+" + data.get(i).test_marks);
            Language = data.get(i).test_language;

        }
        String QueOption = questionModel.tlq_options;
        String QueOptionHindi = questionModel.tlq_options_hindi;
        String QueOptionHindiAttach = questionModel.tlq_option_hindi_attach;
        String QueOptionEnglishAttach = questionModel.tlq_option_attach;
        ArrayList<String> questionOption = new ArrayList<String>(Arrays.asList(QueOption.split("###OPT###")));
        ArrayList<String> questionOptionHindi = new ArrayList<String>(Arrays.asList(QueOptionHindi.split("###OPT###")));
        ArrayList<String> questionOptionHindiAttach = new ArrayList<String>(Arrays.asList(QueOptionHindiAttach.split("###OPT###")));
        ArrayList<String> questionOptionEnglishAttach = new ArrayList<String>(Arrays.asList(QueOptionEnglishAttach.split("###OPT###")));
        if (Language == 0) {
            if (questionOption.size() > 0) {
                binding.txtOptionA.setText(questionOption.get(0));
                binding.txtOptionA.setVisibility(View.VISIBLE);
                binding.imgOptionA.setVisibility(View.GONE);
                if (questionOption.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOption.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
                    binding.txtOptionB.setText(questionOption.get(1));
                    binding.txtOptionB.setVisibility(View.VISIBLE);
                    binding.imgOptionB.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOption.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
                    binding.txtOptionC.setText(questionOption.get(2));
                    binding.txtOptionC.setVisibility(View.VISIBLE);
                    binding.imgOptionC.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOption.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
                    binding.txtOptionD.setText(questionOption.get(3));
                    binding.txtOptionD.setVisibility(View.VISIBLE);
                    binding.imgOptionD.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOption.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
                    binding.txtOptionE.setText(questionOption.get(4));
                    binding.txtOptionE.setVisibility(View.VISIBLE);
                    binding.imgOptionE.setVisibility(View.GONE);
                } else {
                }
            } else if (questionOptionEnglishAttach.size() > 0) {
//                binding.txtOptionA.setText(questionOptionEnglishAttach.get(0));
                binding.txtOptionA.setVisibility(View.GONE);
                binding.imgOptionA.setVisibility(View.VISIBLE);
                if (questionOptionEnglishAttach.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
//                    binding.txtOptionB.setText(questionOptionEnglishAttach.get(1));
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
//                    binding.txtOptionC.setText(questionOptionEnglishAttach.get(2));
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
//                    binding.txtOptionD.setText(questionOptionEnglishAttach.get(3));
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
//                    binding.txtOptionE.setText(questionOptionEnglishAttach.get(4));
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                }
            } else if (questionOptionEnglishAttach.size() <= 0 && questionOption.size() <= 0) {
                if (!questionModel.tlq_option1_attachments.equals("")) {
                    binding.txtOptionA.setVisibility(View.GONE);
                    binding.imgOptionA.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionA.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option2_attachments.equals("")) {
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionB.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option3_attachments.equals("")) {
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionC.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option4_attachments.equals("")) {
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionD.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option5_attachments.equals("")) {
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionE.setVisibility(View.GONE);
                }
            } else {
                binding.cardOptA.setVisibility(View.GONE);
                binding.cardOptB.setVisibility(View.GONE);
                binding.cardOptC.setVisibility(View.GONE);
                binding.cardOptD.setVisibility(View.GONE);
                binding.optCardE.setVisibility(View.GONE);
            }

            // get our html content
            /*String htmlAsString = questionModel.tlq_question_text;
            Spanned htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView

            binding.webQuestion.setText(htmlAsSpanned);*/

            binding.webQuestion.getSettings().setBuiltInZoomControls(true);
            binding.webQuestion.getSettings().setJavaScriptEnabled(true);
            binding.webQuestion.setHorizontalScrollBarEnabled(true);
            binding.webQuestion.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            String instruction_English = "";
            if (!questionModel.tlq_english_instruction.equals("") && !questionModel.tlq_english_instruction.equals("<p><br></p>")) {
                instruction_English = questionModel.tlq_english_instruction;
            }

            String url = instruction_English + questionModel.tlq_question_text;
            String urlStr = "https://neonclasses.com/";
            String mimeType = "text/html";
            String encoding = null;

            binding.webQuestion.loadDataWithBaseURL(urlStr, getHtmlData(url), mimeType, encoding, urlStr);
        }
        else if (Language == 1) {
            if (questionOptionHindi.size() > 0) {
                binding.txtOptionA.setText(questionOptionHindi.get(0));
                binding.txtOptionA.setVisibility(View.VISIBLE);
                binding.imgOptionA.setVisibility(View.GONE);
                if (questionOptionHindi.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOptionHindi.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
                    binding.txtOptionB.setText(questionOptionHindi.get(1));
                    binding.txtOptionB.setVisibility(View.VISIBLE);
                    binding.imgOptionB.setVisibility(View.GONE);
                } else {
                }
                if (questionOptionHindi.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOptionHindi.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
                    binding.txtOptionC.setText(questionOptionHindi.get(2));
                    binding.txtOptionC.setVisibility(View.VISIBLE);
                    binding.imgOptionC.setVisibility(View.GONE);
                } else {
                }
                if (questionOptionHindi.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOptionHindi.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
                    binding.txtOptionD.setText(questionOptionHindi.get(3));
                    binding.txtOptionD.setVisibility(View.VISIBLE);
                    binding.imgOptionD.setVisibility(View.GONE);
                } else {
                }
                if (questionOptionHindi.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOptionHindi.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
                    binding.txtOptionE.setText(questionOptionHindi.get(4));
                    binding.txtOptionE.setVisibility(View.VISIBLE);
                    binding.imgOptionE.setVisibility(View.GONE);
                } else {
                }
            } else if (questionOptionHindi.size() <= 0 && questionOption.size() > 0) {
                binding.txtOptionA.setText(questionOption.get(0));
                binding.txtOptionA.setVisibility(View.VISIBLE);
                binding.imgOptionA.setVisibility(View.GONE);
                if (questionOption.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOption.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
                    binding.txtOptionB.setText(questionOption.get(1));
                    binding.txtOptionB.setVisibility(View.VISIBLE);
                    binding.imgOptionB.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOption.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
                    binding.txtOptionC.setText(questionOption.get(2));
                    binding.txtOptionC.setVisibility(View.VISIBLE);
                    binding.imgOptionC.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOption.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
                    binding.txtOptionD.setText(questionOption.get(3));
                    binding.txtOptionD.setVisibility(View.VISIBLE);
                    binding.imgOptionD.setVisibility(View.GONE);
                } else {
                }
                if (questionOption.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOption.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
                    binding.txtOptionE.setText(questionOption.get(4));
                    binding.txtOptionE.setVisibility(View.VISIBLE);
                    binding.imgOptionE.setVisibility(View.GONE);
                } else {
                }
            } else if (questionOptionHindiAttach.size() > 0) {
//                binding.txtOptionA.setText(questionOptionEnglishAttach.get(0));
                binding.txtOptionA.setVisibility(View.GONE);
                binding.imgOptionA.setVisibility(View.VISIBLE);
                if (questionOptionHindiAttach.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOptionHindiAttach.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
//                    binding.txtOptionB.setText(questionOptionEnglishAttach.get(1));
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionHindiAttach.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOptionHindiAttach.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
//                    binding.txtOptionC.setText(questionOptionEnglishAttach.get(2));
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionHindiAttach.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOptionHindiAttach.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
//                    binding.txtOptionD.setText(questionOptionEnglishAttach.get(3));
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionHindiAttach.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOptionHindiAttach.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
//                    binding.txtOptionE.setText(questionOptionEnglishAttach.get(4));
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                }
            } else if (questionOptionHindiAttach.size() <= 0 && questionOptionEnglishAttach.size() > 0) {
//                binding.txtOptionA.setText(questionOptionEnglishAttach.get(0));
                binding.txtOptionA.setVisibility(View.GONE);
                binding.imgOptionA.setVisibility(View.VISIBLE);
                if (questionOptionEnglishAttach.size() == 1) {
                    binding.cardOptB.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 2) {
                    binding.cardOptB.setVisibility(View.VISIBLE);
//                    binding.txtOptionB.setText(questionOptionEnglishAttach.get(1));
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 2) {
                    binding.cardOptC.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 3) {
                    binding.cardOptC.setVisibility(View.VISIBLE);
//                    binding.txtOptionC.setText(questionOptionEnglishAttach.get(2));
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 3) {
                    binding.cardOptD.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 4) {
                    binding.cardOptD.setVisibility(View.VISIBLE);
//                    binding.txtOptionD.setText(questionOptionEnglishAttach.get(3));
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                }
                if (questionOptionEnglishAttach.size() == 4) {
                    binding.optCardE.setVisibility(View.GONE);
                } else if (questionOptionEnglishAttach.size() >= 5) {
                    binding.optCardE.setVisibility(View.VISIBLE);
//                    binding.txtOptionE.setText(questionOptionEnglishAttach.get(4));
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                }
            } else if (questionOptionHindi.size() <= 0 && questionOptionHindi.size() <= 0 && questionOption.size() <= 0
                    && questionOptionHindiAttach.size() <= 0 && questionOptionEnglishAttach.size() <= 0 && !questionModel.tlq_option1_attachments.equals("")
                    && !questionModel.tlq_option2_attachments.equals("") && !questionModel.tlq_option3_attachments.equals("") &&
                    !questionModel.tlq_option4_attachments.equals("") && !questionModel.tlq_option5_attachments.equals("")
            ) {
                if (!questionModel.tlq_option1_attachments_hindi.equals("")) {
                    binding.txtOptionA.setVisibility(View.GONE);
                    binding.imgOptionA.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionA.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option2_attachments_hindi.equals("")) {
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionB.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option3_attachments_hindi.equals("")) {
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionC.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option4_attachments_hindi.equals("")) {
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionD.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option5_attachments_hindi.equals("")) {
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionE.setVisibility(View.GONE);
                }
            } else if (questionModel.tlq_option1_attachments_hindi.equals("")
                    && questionModel.tlq_option2_attachments_hindi.equals("") && questionModel.tlq_option3_attachments_hindi.equals("") &&
                    questionModel.tlq_option4_attachments_hindi.equals("") && questionModel.tlq_option5_attachments_hindi.equals("")) {
                if (!questionModel.tlq_option1_attachments.equals("")) {
                    binding.txtOptionA.setVisibility(View.GONE);
                    binding.imgOptionA.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionA.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option2_attachments.equals("")) {
                    binding.txtOptionB.setVisibility(View.GONE);
                    binding.imgOptionB.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionB.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option3_attachments.equals("")) {
                    binding.txtOptionC.setVisibility(View.GONE);
                    binding.imgOptionC.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionC.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option4_attachments.equals("")) {
                    binding.txtOptionD.setVisibility(View.GONE);
                    binding.imgOptionD.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionD.setVisibility(View.GONE);
                }
                if (!questionModel.tlq_option5_attachments.equals("")) {
                    binding.txtOptionE.setVisibility(View.GONE);
                    binding.imgOptionE.setVisibility(View.VISIBLE);
                } else {
                    binding.imgOptionE.setVisibility(View.GONE);
                }
            } else {
                binding.cardOptA.setVisibility(View.GONE);
                binding.cardOptB.setVisibility(View.GONE);
                binding.cardOptC.setVisibility(View.GONE);
                binding.cardOptD.setVisibility(View.GONE);
                binding.optCardE.setVisibility(View.GONE);
            }

            // get our html content
            /*String htmlAsString = questionModel.tlq_question_text_hindi;
            Spanned htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView
            binding.webQuestion.setText(htmlAsSpanned);*/
            binding.webQuestion.getSettings().setBuiltInZoomControls(true);
            binding.webQuestion.getSettings().setJavaScriptEnabled(true);
            binding.webQuestion.setHorizontalScrollBarEnabled(true);
            binding.webQuestion.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            String instruction_hindi = "";
            String question_text_text = "";
            if (!questionModel.tlq_hindi_instruction.equals("") && !questionModel.tlq_hindi_instruction.equals("<p><br></p>")) {
                instruction_hindi = questionModel.tlq_hindi_instruction;
            }
            else if (!questionModel.tlq_english_instruction.equals("") && !questionModel.tlq_english_instruction.equals("<p><br></p>")) {
                instruction_hindi = questionModel.tlq_english_instruction;
            }
            else {
            }

            if (!questionModel.tlq_question_text_hindi.equals("") && !questionModel.tlq_question_text_hindi.equals("<p><br></p>")) {
                question_text_text = questionModel.tlq_question_text_hindi;
            } else if (!questionModel.tlq_question_text.equals("")) {
                question_text_text = questionModel.tlq_question_text;
            }
            else {
            }
            String url = instruction_hindi + question_text_text;
            String urlStr = "https://neonclasses.com/";
            String mimeType = "text/html";
            String encoding = null;

            binding.webQuestion.loadDataWithBaseURL(urlStr, getHtmlData(url), mimeType, encoding, urlStr);

        }
        else {

        }


        if (examType == 0 || examType == 1) {

            if (ustqam.ustqa_status == 1) {
                binding.llReview.setVisibility(View.GONE);
                binding.llReview1.setVisibility(View.VISIBLE);
            } else {
                binding.llReview.setVisibility(View.VISIBLE);
                binding.llReview1.setVisibility(View.GONE);
            }
            if (ustqam.ustqa_user_ans == 1) {
                Log.e("Check", "Check=" + 1);
                binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }
            else if (ustqam.ustqa_user_ans == 2) {
                Log.e("Check", "Check=" + 2);
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }
            else if (ustqam.ustqa_user_ans == 3) {
                Log.e("Check", "Check=" + 3);
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }
            else if (ustqam.ustqa_user_ans == 4) {
                Log.e("Check", "Check=" + 4);
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }
            else if (ustqam.ustqa_user_ans == 5) {
                Log.e("Check", "Check=" + 5);
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
            }
            else {
                Log.e("Check", "Check=" + 6);
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
            }

            //Section for selectiong options
            binding.llOptionA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(questionModel.tlq_id, 1, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            binding.llOptionB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(questionModel.tlq_id, 2, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            binding.llOptionC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(questionModel.tlq_id, 3, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            binding.llOptionD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(questionModel.tlq_id, 4, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            binding.llOptionE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateQuestionTable(questionModel.tlq_id, 5, Constant.ANSWERED);
//                    changecolorofbutton(checkallstatus);
                }
            });
            binding.llReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    binding.llReview.setVisibility(View.GONE);
                    binding.llReview1.setVisibility(View.VISIBLE);
                    updateQuestionTableWithReview(questionModel.tlq_id, 0, Constant.REVIEW);
                }
            });
            binding.llReview1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    binding.llReview1.setVisibility(View.GONE);
                    binding.llReview.setVisibility(View.VISIBLE);
                    updateQuestionTableWithReview(questionModel.tlq_id, 0, Constant.UNANSWERED);
                }
            });
        }
        else if (examType == 2) {
            if (ustqam.ustqa_correct.equals("1")) {
                if (ustqam.ustqa_user_ans == 0) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 1) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 2) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 3) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 4) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 5) {
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            }
            else if (ustqam.ustqa_correct.equals("2")) {
                if (ustqam.ustqa_user_ans == 0) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 1) {
                    binding.llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 2) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 3) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 4) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 5) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            }
            else if (ustqam.ustqa_correct.equals("3")) {
                if (ustqam.ustqa_user_ans == 0) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 1) {
                    binding.llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 2) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 3) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 4) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 5) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            }
            else if (ustqam.ustqa_correct.equals("4")) {
                if (ustqam.ustqa_user_ans == 0) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 1) {
                    binding.llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 2) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 3) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 4) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                } else if (ustqam.ustqa_user_ans == 5) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.red_circle);
                }
            }
            else if (ustqam.ustqa_correct.equals("5")) {
                if (ustqam.ustqa_user_ans == 0) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqam.ustqa_user_ans == 1) {
                    binding.llOptionA.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqam.ustqa_user_ans == 2) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqam.ustqa_user_ans == 3) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqam.ustqa_user_ans == 4) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.worng_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.red_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                } else if (ustqam.ustqa_user_ans == 5) {
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                }
            }
        }

        holder.setIsRecyclable(false);
    }


    @Override
    public int getItemCount() {
        return listItem.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public RecyclerViewHolder(@NonNull ItemRowQuestionLayoutBinding itemView) {
            super(itemView.getRoot());
        }
    }

    public class ViewHolder {
    }

    public void updateQuestionTable(int questionId, int selectedAnswer, int questionStatus) {
        Boolean checkupdatedata = db.updateQuestionTable(questionId, selectedAnswer, questionStatus);
        Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_status"));
        @SuppressLint("Range") int tlqid = res.getInt(res.getColumnIndex("tlq_id"));

        notifyItemChanged(selectedans);
//        notifyDataSetChanged();
        changecolor(selectedans);
    }

    public void changecolor(int selectedAnswer) {
        Log.e("checkss",selectedAnswer+"");
        switch (selectedAnswer) {
            case 1:
                binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                Log.e("Checks", "Check=" + 1);
                break;
            case 2:
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                Log.e("Checks", "Check=" + 2);
                break;
            case 3:
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                Log.e("Checks", "Check=" + 3);
                break;
            case 4:
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                Log.e("Checks", "Check=" + 4);
                break;
            case 5:
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                Log.e("Checks", "Check=" + 5);
                break;
            default:
                binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                Log.e("Checks", "Check=" + 6);
                break;
        }

    }

    public void updateQuestionTableWithReview(int questionId, int selectedAnswer, int questionStatus) {
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_review"));
        UstQuesAnswerModel quedata = new UstQuesAnswerModel();
        quedata.ustqa_review = quesSta;
        quedata.ustqa_user_ans = selectedans;
        if (selectedans != 0) {
            Log.e("getDatass", "if re=" + quedata.ustqa_review +"   "+ questionStatus);
            db.updateQuestionReviewTable(questionId, quedata.ustqa_user_ans, questionStatus);
            notifyItemChanged(res.getPosition());
            Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("getDatass", "else re=" + questionStatus);
            db.updateQuestionReviewTable(questionId, selectedAnswer, questionStatus);
            notifyItemChanged(res.getPosition());
            Toast.makeText(context, "Data Updated", Toast.LENGTH_SHORT).show();
        }
    }


    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if (url.indexOf("neonclasses.com") > -1 || url.indexOf("neonclasses.co.in") > -1)
                return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }
    }

    private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }
  
//    private void changecolorofbutton(int checkallstatus){
//        Log.e("Check", "Check=out" + checkallstatus);
//        new Handler() {
//            public void postDelayed(Runnable runnable, int i) {
//            }
//
//            @Override
//            public void publish(LogRecord record) {
//                Log.e("Check", "Check=" + "publish");
//            }
//
//            @Override
//            public void flush() {
//                Log.e("Check", "Check=" + "flush");
//
//            }
//
//            @Override
//            public void close() throws SecurityException {
//                Log.e("Check", "Check=" + "close");
//
//            }
//        }.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
////                if (ustqam.ustqa_status == 1) {
////                    binding.llReview.setVisibility(View.GONE);
////                    binding.llReview1.setVisibility(View.VISIBLE);
////                } else {
////                    binding.llReview.setVisibility(View.VISIBLE);
////                    binding.llReview1.setVisibility(View.GONE);
////                }
//                if (checkallstatus == 1) {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
//                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                }
//                else if (checkallstatus == 2) {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
//                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                }
//                else if (checkallstatus == 3) {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
//                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                }
//                else if (checkallstatus == 4) {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
//                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                }
//                else if (checkallstatus == 5) {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
//                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
//                }
//                else {
//                    Log.e("Check", "Check=" + checkallstatus);
//                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
//                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
//                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
//                }
//
//            }
//        }, 1000);
//
//    }
}
