package codexo.neonclasses.ui.test;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.Forgot;
import codexo.neonclasses.databinding.FragmentAllTestBinding;
import codexo.neonclasses.databinding.ItemAllTestBinding;
import codexo.neonclasses.databinding.ItemRowTestSeriesListBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.home.HomeViewModel;
import codexo.neonclasses.ui.player.PlayerActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class AllTestFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentAllTestBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    String package_url = "";
    JSONArray all_test_data = new JSONArray();
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentAllTestBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());
        try {
            all_test_data = new JSONArray(getArguments().getString("all_test_data"));
            package_url = getArguments().getString("package_url");
//            Log.d("helloo456All",package_url+"++"+ all_test_data.toString());
            if (all_test_data.length() <= 0) {
                binding.recycler.setVisibility(View.GONE);
                binding.emptyView.setVisibility(View.VISIBLE);
            } else {
                binding.recycler.setVisibility(View.VISIBLE);
                binding.emptyView.setVisibility(View.GONE);
                AllTestAdapter adapter = new AllTestAdapter(getContext(), all_test_data);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                binding.recycler.setLayoutManager(layoutManager);
                binding.recycler.setAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //getTestSeries();
        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /*private void getTestSeries() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getTestSeriesUrl().department(session.getUserId(),"0");
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
                // Constant.logPrint("response_video", response);
                Log.d("helloo", response);
                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            imageUrl = jsonObject.getString("department_url");
                            mock_testArray = jsonObject.getJSONArray("result");
                            Log.d("helloo", mock_testArray.toString());
                            if (mock_testArray.length() <= 0) {
                                binding.notiy.setVisibility(View.GONE);
                                binding.emptyView.setVisibility(View.VISIBLE);
                            } else {
                                binding.notiy.setVisibility(View.VISIBLE);
                                binding.emptyView.setVisibility(View.GONE);
                                AllTestAdapter adapter = new AllTestAdapter(getContext(), mock_testArray);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
                                binding.recycler.setLayoutManager(layoutManager);
                                binding.recycler.setAdapter(adapter);
                            }


                        } else {
                            Constant.setToast(getContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getTestSeries();
            }
        });
    }*/

    public class AllTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemAllTestBinding binding;
        Context context;
        JSONArray listItem;

        public AllTestAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public AllTestAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemAllTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            AllTestAdapter.RecyclerViewHolder holder = new AllTestAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int i) {

            try {
                JSONObject item = all_test_data.getJSONObject(i);

                binding.txtTitle.setText(item.getString("sub_pack_title"));
                String sub_pack_price = item.getString("sub_pack_price");
                String sub_actual_price = item.getString("sub_actual_price");
                binding.txtPrice.setText("₹"+sub_actual_price);
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                binding.txtPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtDiscountedPrice.setText("₹"+sub_pack_price);
                binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getContext()));

                String imgname = item.getString("sub_pack_image");
                Constant.setImage(package_url+imgname,binding.testImage,getContext());

                String disperc = Constant.getdiscount(item.getString("sub_actual_price"),item.getString("sub_pack_price"));
//                Constant.logPrint(disperc+"","dispercdispercdispercdispercdisperc");
                if(!disperc.equals(""))
                {
                    binding.txtDiscount.setText(disperc+"% Off");
                    binding.txtDiscount.setTypeface(Constant.getFontsBold(getContext()));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = all_test_data.getJSONObject(i);
                        String myTestId = item.getString("sub_pack_id");
                        Intent intent = new Intent(getContext(), DetailsActivity.class);
                        intent.putExtra("myTestId", myTestId);
                        intent.putExtra("type", "alltest");
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return all_test_data.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemAllTestBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}