package codexo.neonclasses.ui.test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityAnalyticsBinding;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.SubjectInfoModel;
import codexo.neonclasses.session.SessionManager;

public class AnalyticsActivity extends AppCompatActivity {
    ActivityAnalyticsBinding binding;
    private WebView webView = null;
    String url = Constant.ANALYTICSURL;
    SqlDatabase db;
    SessionManager session;
    int langStatus,examType;
    List<SubjectInfoModel> data = new ArrayList<>();
    //// String url = "https://www.youtube-nocookie.com/embed/Shwm4-Rl-NQ?modestbranding=1&rel=0&gyroscope=1";
    ProgressBar progressBar;
    String title="Neon Classes",test_id="",testType="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAnalyticsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(AnalyticsActivity.this);
        Constant.checkAdb(AnalyticsActivity.this);
        session = new SessionManager(AnalyticsActivity.this);
        test_id =getIntent().getStringExtra("test_id");
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        webView = (WebView) findViewById(R.id.webview);
        webView.clearHistory();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.clearFormData();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        webView.getSettings().setBuiltInZoomControls(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.loadUrl(url+test_id+"/"+session.getUserId());
        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
    }
    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if(url.indexOf("neonclasses.com") > -1 || url.indexOf("neonclasses.co.in") > -1) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(AnalyticsActivity.this);
        Constant.checkAdb(AnalyticsActivity.this);
    }
}