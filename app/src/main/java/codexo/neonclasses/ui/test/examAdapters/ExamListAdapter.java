/*
package codexo.neonclasses.ui.test.examAdapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.model.QuestionModel;

public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.RecyclerviewAdapter>{
    Context context;
    List<QuestionModel> questionModels;
//    private QuestionItem questionItem;
    static int quesId;
//    static Viewmodel mViewModel;

//    public ExamListAdapter(Context context, List<QuestionTable> questionTables, QuestionAdapter.QuestionItem questionItem) {
//        this.context = context;
//        this.questionTables = questionTables;
//        this.questionItem = questionItem;
//    }


    public ExamListAdapter(Context context, List<QuestionModel> questionModels) {
        this.context = context;
        this.questionModels = questionModels;
    }

    @NonNull
    @Override
    public RecyclerviewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.exam_list_layout, parent, false);
        return new RecyclerviewAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewAdapter holder, int position) {
        QuestionModel questionTable = questionModels.get(position);
        int finalPosition1 = position;
        if (questionTable.user_que_status == Constant.ANSWERED) {
            holder.exam_ques.setBackgroundResource(R.drawable.answered_circle);
            holder.exam_ques.setTextColor(Color.WHITE);
        } else if (questionTable.user_que_status == Constant.REVIEW) {
            holder.exam_ques.setBackgroundResource(R.drawable.review_cricle);
            holder.exam_ques.setTextColor(Color.WHITE);
        } else if (questionTable.user_que_status == Constant.UNANSWERED) {
            holder.exam_ques.setBackgroundResource(R.drawable.not_answered_circle);
            holder.exam_ques.setTextColor(Color.WHITE);
        } else if (questionTable.user_que_status == Constant.NOT_VISITED) {
            holder.exam_ques.setBackgroundResource(R.drawable.grey_border);
            holder.exam_ques.setTextColor(Color.BLACK);
        }

        holder.exam_ques.setText(String.valueOf(questionTable.tlq_id));
        Log.e("quNO", questionTable.tlq_id+"");

        holder.ll_exam_ques.setOnClickListener(v -> {

        Constant.qItemClick = String.valueOf(questionTable.tlq_id);

//            if (questionTables.get(quesId).getUserQueStatus() == 0) {
//                questionTables.get(quesId).setUserQueStatus(Constant.UNANSWERED);
//                mViewModel.updateQuestionTable(questionTables.get(quesId));
//                questionRecycler.scrollToPosition(finalPosition - 1);
//            } else {
//                binding.questionRecycler.scrollToPosition(finalPosition - 1);
//            }
//            if (binding.drawelayout.isDrawerOpen(Gravity.END)) {
//                binding.drawelayout.closeDrawer(Gravity.END);
//            } else {
//                binding.drawelayout.openDrawer(Gravity.END);
//            }
        });
    }

    @Override
    public int getItemCount() {
        return questionModels.size();
    }

    public class RecyclerviewAdapter extends RecyclerView.ViewHolder {
        TextView exam_ques;
        LinearLayout ll_exam_ques;
        public RecyclerviewAdapter(@NonNull View itemView) {
            super(itemView);
            ll_exam_ques = itemView.findViewById(R.id.ll_exam_ques);
            exam_ques = itemView.findViewById(R.id.exam_ques);
        }
    }
    public interface QuestionItem {
        void onClickQuestionItem(int clickedItem, int subjectId);
    }
}
*/
