package codexo.neonclasses.ui.test.examAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import codexo.neonclasses.R;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.OtUserTestSubjectViewItem;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.ui.test.ExamActivity;

public class OnlinePaperAdapter extends RecyclerView.Adapter<OnlinePaperAdapter.ViewHolder>{
    List<OtUserTestSubjectViewItem> otUserTestSubjectViewItems;
    Context mContext;
    SqlDatabase db;
    List<QuestionModel> questionModels;
    public OnlinePaperAdapter(List<OtUserTestSubjectViewItem> otUserTestSubjectViewItems, Context mContext) {
        this.otUserTestSubjectViewItems = otUserTestSubjectViewItems;
        this.mContext = mContext;
    }

    //    List<OtQuestionItem> otUserTestSubjectViewItems;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_menu_ques_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OtUserTestSubjectViewItem otUserTestSubjectViewItem = otUserTestSubjectViewItems.get(position);
        holder.txt_ques.setText(otUserTestSubjectViewItem.getSubName());
        holder.im_arrow_down.setOnClickListener(v -> {
            holder.im_arrrow_up.setVisibility(View.VISIBLE);
            holder.sub_recyler.setVisibility(View.GONE);
            holder.im_arrow_down.setVisibility(View.GONE);

            GridLayoutManager lms = new GridLayoutManager(mContext, 5);
            lms.setOrientation(RecyclerView.VERTICAL);
            holder.sub_recyler.setLayoutManager(lms);
//            db = new SqlDatabase(mContext);
//            questionModels = db.getQuestionData(otUserTestSubjectViewItem.getSubId(), otUserTestSubjectViewItem.getTsdTestId());
//
////            Log.d("questionModels123", questionModels+"+sub_name+"+subjectModel.sub_name+"+test_id+"+subjectModel.test_id+"+SIze+"+questionModels.size());
//            if (questionModels.size() == 0) {
//                holder.sub_recyler.setVisibility(View.GONE);
//            } else {
//                holder.sub_recyler.setVisibility(View.VISIBLE);
//                ExamActivity.ExamListAdapter examListAdapter = new ExamActivity.ExamListAdapter(mContext, questionModels);
//                holder.sub_recyler.setAdapter(examListAdapter);
//            }

//            questionModels = db.getQuestionData(subjectModel.test_id);
//            if (questionModels.size() == 0) {
//                holder.sub_recyler.setVisibility(View.GONE);
//            } else {
//                holder.sub_recyler.setVisibility(View.VISIBLE);
//                ExamListAdapter examListAdapter = new ExamListAdapter(mContext, questionModels);
//                holder.sub_recyler.setAdapter(examListAdapter);
//            }
        });
        holder.im_arrrow_up.setOnClickListener(v -> {
            holder.im_arrow_down.setVisibility(View.VISIBLE);
            holder.sub_recyler.setVisibility(View.GONE);
            holder.im_arrrow_up.setVisibility(View.GONE);

        });
    }

    @Override
    public int getItemCount() {
        return otUserTestSubjectViewItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView im_arrow_down, im_arrrow_up;
        TextView txt_ques;
        RecyclerView sub_recyler;
        LinearLayout sub_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sub_layout = itemView.findViewById(R.id.sub_layout);
            txt_ques = itemView.findViewById(R.id.txt_ques);
            im_arrow_down = itemView.findViewById(R.id.im_arrow_down);
            im_arrrow_up = itemView.findViewById(R.id.im_arrrow_up);
            sub_recyler = itemView.findViewById(R.id.sub_recyler);
        }
    }
}
