package codexo.neonclasses.ui.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityTestResultBinding;

public class TestResultActivity extends AppCompatActivity {
    ActivityTestResultBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTestResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.back.setOnClickListener(v -> {
            finish();
        });
    }
}