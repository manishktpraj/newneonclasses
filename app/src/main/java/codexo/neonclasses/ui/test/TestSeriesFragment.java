package codexo.neonclasses.ui.test;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.FragmentTestBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.cart.CartActivity;
import codexo.neonclasses.ui.home.HomeViewModel;
import retrofit2.Call;
import retrofit2.Callback;

public class TestSeriesFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentTestBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray all_test_data = new JSONArray();
    JSONArray free_test_data = new JSONArray();
    JSONArray my_test_data = new JSONArray();
    String package_url = "";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentTestBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());
        binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("All Test"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Free Test"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("My Test"));

       /* ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new FreeTestFragment(), "All Test");
        adapter.addFragment(new AllTestFragment(), "Free Test");
        adapter.addFragment(new MyTestFragment(), "My Test");
        binding.viewPager.setAdapter(adapter);*/

//        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.imgCart.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), CartActivity.class));
        });
        getTestSeries();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getTestSeries() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getTestSeriesUrl().exam(session.getUserId());
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
//                 Constant.logPrint("response_video", response);
//                Log.d("helloo", response);
                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        package_url = jsonObject.getString("package_url");
                        if (message.equals("ok")) {

                            all_test_data = jsonObject.getJSONArray("testpackage");
                            free_test_data = jsonObject.getJSONArray("testdata");
                            my_test_data = jsonObject.getJSONArray("all_array");

                            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
                            AllTestFragment ldf =new AllTestFragment();
                            Bundle args = new Bundle();
                            args.putString("all_test_data", all_test_data+"");
                            args.putString("package_url", package_url+"");
                            ldf.setArguments(args);

                            adapter.addFragment(ldf, "All Test");


                            FreeTestFragment study =new FreeTestFragment();
                            Bundle argsnew = new Bundle();
                            argsnew.putString("free_test_data", free_test_data+"");
                            argsnew.putString("package_url", package_url+"");
                            study.setArguments(argsnew);

                            adapter.addFragment(study, "Free Test");


                            MyTestFragment psc =new MyTestFragment();
                            Bundle argsnewpsc = new Bundle();
                            argsnewpsc.putString("my_test_data", my_test_data+"");
                            argsnewpsc.putString("package_url", package_url+"");
                            psc.setArguments(argsnewpsc);
                            adapter.addFragment(psc, "My Test");


                            binding.viewPager.setAdapter(adapter);
                            binding.tabLayout.setupWithViewPager(binding.viewPager);
                        } else {
                            Constant.setToast(getContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getTestSeries();
            }
        });
    }
}