package codexo.neonclasses.ui.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import codexo.neonclasses.R;

public class ExamSubmitDone extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_submit_done);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}