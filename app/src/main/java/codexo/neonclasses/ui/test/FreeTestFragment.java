package codexo.neonclasses.ui.test;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentAllTestBinding;
import codexo.neonclasses.databinding.FragmentFreeTestBinding;
import codexo.neonclasses.databinding.ItemFreeTestBinding;
import codexo.neonclasses.databinding.ItemRowTestSeriesListBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.home.HomeViewModel;

public class FreeTestFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentFreeTestBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    String package_url = "";
    JSONArray free_test_data = new JSONArray();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentFreeTestBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());
        try {
            free_test_data = new JSONArray(getArguments().getString("free_test_data"));
            package_url = getArguments().getString("package_url");
//            Log.d("helloo456Free", package_url + "++" + free_test_data.toString());
            if (free_test_data.length() <= 0) {
                binding.recycler.setVisibility(View.GONE);
                binding.emptyView.setVisibility(View.VISIBLE);
            } else {
                binding.recycler.setVisibility(View.VISIBLE);
                binding.emptyView.setVisibility(View.GONE);
                FreeTestAdapter adapter = new FreeTestAdapter(getContext(), free_test_data);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                binding.recycler.setLayoutManager(layoutManager);
                binding.recycler.setAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public class FreeTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemFreeTestBinding binding;
        Context context;
        JSONArray listItem;

        public FreeTestAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public FreeTestAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemFreeTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            FreeTestAdapter.RecyclerViewHolder holder = new FreeTestAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int i) {

            try {
                JSONObject item = free_test_data.getJSONObject(i);

                String test_total_question = "", test_total_marks = "", test_duration = "";
                test_total_question = item.getString("test_total_question");
                test_total_marks = item.getString("test_total_marks");
                test_duration = item.getString("test_duration");
                binding.txtTitle.setText(item.getString("test_title"));
                binding.txt.setText(test_total_question + " Q | " + test_total_marks + " M | " + test_duration + " Min");


                String user_section_test;
                String test_end_date = "";
                user_section_test = item.getString("user_section_test");
                test_end_date = item.getString("test_end_date");
                if (test_end_date.equals("null") && user_section_test.equals("null")) {
                    binding.imgLock.setImageResource(R.drawable.play_icon);
                }
                JSONObject user_section_test1 = new JSONObject();
                String ust_status = "";
                user_section_test1 = item.getJSONObject("user_section_test");
                ust_status = user_section_test1.getString("ust_status");
                if (user_section_test1 != null && ust_status.equals("0")) {
                    binding.imgLock.setImageResource(R.drawable.pause_icon);
                }
                if (user_section_test1 != null && ust_status.equals("1")) {

                    binding.imgLock.setImageResource(R.drawable.analytics);
                }
                else {

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            binding.imgStatusLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = free_test_data.getJSONObject(i);
                        String test_id = item.getString("test_id");
                        String user_section_test;
                        String test_end_date = "";
                        user_section_test = item.getString("user_section_test");
                        test_end_date = item.getString("test_end_date");
                        if (test_end_date.equals("null") && user_section_test.equals("null")) {
                            Intent startTest = new Intent(getContext(), AttemptInstruction.class);
                            startTest.putExtra("test_id", test_id);
                            startTest.putExtra("type", "mytest");
                            startTest.putExtra("examType", 0);
                            startActivity(startTest);
                        }

                        JSONObject user_section_test1 = new JSONObject();
                        String ust_status = "";
                        user_section_test1 = item.getJSONObject("user_section_test");
                        ust_status = user_section_test1.getString("ust_status");
                        if (user_section_test1 != null && ust_status.equals("0")) {
                            Intent startTest = new Intent(getContext(), AttemptInstruction.class);
                            startTest.putExtra("test_id", test_id);
                            startTest.putExtra("type", "mytest");
                            startTest.putExtra("examType", 1);
                            startActivity(startTest);
                        }
                        if (user_section_test1 != null && ust_status.equals("1")) {
                            Intent startTest = new Intent(getContext(), ExamActivity.class);
                            startTest.putExtra("test_id", test_id);
                            startTest.putExtra("type", "result");
                            startTest.putExtra("examType", 2);
                            startActivity(startTest);
                        }
                        else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return free_test_data.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemFreeTestBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}