package codexo.neonclasses.ui.test.examAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import codexo.neonclasses.R;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.model.SubjectModel;
import codexo.neonclasses.model.UstQuesAnswerModel;
import codexo.neonclasses.ui.test.ExamActivity;

// PopularCoursesAdapter
public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder> {
    List<SubjectModel> subjectDataTables;
    List<QuestionModel> questionModels;
    List<UstQuesAnswerModel> ustQuesAnswerModels;
//    QuestionAdapter.QuestionItem questionItem;
    private Context mContext;
    SqlDatabase db;
    int examType;


    public PaperAdapter(List<SubjectModel> subjectDataTables, Context mContext, int examType) {
        this.subjectDataTables = subjectDataTables;
        this.mContext = mContext;
        this.examType = examType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu_ques_subject, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SubjectModel subjectModel = subjectDataTables.get(position);
        holder.txt_ques.setText(subjectModel.sub_name);
        db = new SqlDatabase(mContext);
        GridLayoutManager lms = new GridLayoutManager(mContext, 5);
        lms.setOrientation(RecyclerView.VERTICAL);
        holder.sub_recyler.setLayoutManager(lms);
        questionModels = db.getQuestionData(subjectModel.sub_id, subjectModel.tsd_test_id);
        ustQuesAnswerModels = db.getUstQuesAnswerTable(subjectModel.tsd_test_id);

//        Log.d("questionModels123", questionModels+"+sub_name+"+subjectModel.sub_name+"+test_id+"+subjectModel.test_id+"+SIze+"+questionModels.size());
        if (questionModels.size() == 0) {
            holder.sub_recyler.setVisibility(View.GONE);
        } else {
            holder.sub_recyler.setVisibility(View.VISIBLE);
            ExamActivity.ExamListAdapter examListAdapter = new ExamActivity.ExamListAdapter(mContext, questionModels,ustQuesAnswerModels, examType);
            holder.sub_recyler.setAdapter(examListAdapter);
        }
       holder.im_arrow_down.setOnClickListener(v -> {
           holder.im_arrrow_up.setVisibility(View.VISIBLE);
           holder.sub_recyler.setVisibility(View.GONE);
           holder.im_arrow_down.setVisibility(View.GONE);


        });
//        Log.d("questionModels123", questionModels+"");
        holder.im_arrrow_up.setOnClickListener(v -> {
            holder.im_arrow_down.setVisibility(View.VISIBLE);
            holder.sub_recyler.setVisibility(View.VISIBLE);
            holder.im_arrrow_up.setVisibility(View.GONE);

        });
//        holder.sub_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                GridLayoutManager lms = new GridLayoutManager(mContext, 5);
//                lms.setOrientation(RecyclerView.VERTICAL);
//                holder.sub_recyler.setLayoutManager(lms);
//
//                questionModels = db.getQuestionData(subjectModel.test_id);
//                if (questionModels.size() == 0) {
//                    holder.sub_recyler.setVisibility(View.GONE);
//                } else {
//                    holder.sub_recyler.setVisibility(View.VISIBLE);
//                    ExamListAdapter examListAdapter = new ExamListAdapter(mContext, questionModels);
//                    holder.sub_recyler.setAdapter(examListAdapter);
//                }
//
//                //Toast.makeText(mContext, "Subject Id : " + subjectModel.test_id+"++"+subjectDataTables.size(), Toast.LENGTH_SHORT).show();
//                //Toast.makeText(mContext, "subid="+subjectModel.test_id, Toast.LENGTH_SHORT).show();
////                Intent getMydata = new Intent("takeMyData");
////                getMydata.putExtra("subject_id", subjectModel.test_id);
////                LocalBroadcastManager.getInstance(mContext).sendBroadcast(getMydata);
//            }
//        });
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return subjectDataTables.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView im_arrow_down, im_arrrow_up;
        TextView txt_ques;
        RecyclerView sub_recyler;
        LinearLayout sub_layout;

        public ViewHolder(@NonNull View view) {
            super(view);
            sub_layout = view.findViewById(R.id.sub_layout);
            txt_ques = view.findViewById(R.id.txt_ques);
            im_arrow_down = view.findViewById(R.id.im_arrow_down);
            im_arrrow_up = view.findViewById(R.id.im_arrrow_up);
            sub_recyler = view.findViewById(R.id.sub_recyler);
        }
    }
}
