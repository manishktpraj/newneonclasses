package codexo.neonclasses.ui.test;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityDetailsBinding;
import codexo.neonclasses.databinding.ItemAllTestBinding;
import codexo.neonclasses.databinding.ItemFreeMockTestBinding;
import codexo.neonclasses.databinding.ItemRowAllMockTestDetailsActivityBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.session.examtempsession;
import codexo.neonclasses.ui.subscription.SubscriptionActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailsActivity extends AppCompatActivity /*implements RecyclerAdapter2.ClickListener*/ {

    private ActivityDetailsBinding binding;

    SessionManager session;
    ProgressDialog pDialog;
    String myTestId = "", testType = "", c_package_selling_price = "";
    JSONArray free_mock_test = new JSONArray();
    JSONArray all_mock_test = new JSONArray();
    examtempsession Examtempsession;
    JSONArray exam_array = new JSONArray();
    JSONArray free_exam_array = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(DetailsActivity.this);
        Constant.checkAdb(DetailsActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        session = new SessionManager(DetailsActivity.this);
        pDialog = Constant.getProgressBar(DetailsActivity.this);
        myTestId = getIntent().getStringExtra("myTestId");
        testType = getIntent().getStringExtra("type");
        Examtempsession = new examtempsession(getApplicationContext());
        getTestSeries();
    }

    private void getTestSeries() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getTestSeriesUrl().getcoursedetail(session.getUserId(), myTestId);
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
                // Constant.logPrint("response_video", response);
//                Log.d("helloo", response);
                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            JSONObject resultsObject = jsonObject.getJSONObject("results");
                            binding.txtTitle.setText(resultsObject.getString("sub_pack_title"));
                            binding.txt.setText(resultsObject.getString("sub_pack_no_test") + " Tests");
                            free_mock_test = jsonObject.getJSONArray("results_freeexam");
                            all_mock_test = jsonObject.getJSONArray("results_mocexam");
                            c_package_selling_price = resultsObject.getString("sub_actual_price");
                            if (testType.equals("mytest")) {
                                binding.relBottom.setVisibility(View.GONE);
                            } else if (testType.equals("alltest")) {
                                binding.relBottom.setVisibility(View.VISIBLE);
                                binding.txtPriceOriginal.setText("₹" + resultsObject.getString("sub_actual_price"));
                                binding.orderPrice.setText("₹" + resultsObject.getString("sub_pack_price"));
                                binding.txtPriceOriginal.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                binding.txtPriceOriginal.setTypeface(Constant.getFontsBold(getApplicationContext()));
                                binding.orderPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
                            }
                            if (free_mock_test.length() <= 0) {
                                binding.txtFmt.setVisibility(View.GONE);
                                binding.recycler.setVisibility(View.GONE);
                            } else {
                                binding.txtFmt.setVisibility(View.VISIBLE);
                                binding.recycler.setVisibility(View.VISIBLE);
                                FMTAdapter adapter = new FMTAdapter(getApplicationContext(), free_mock_test);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                layoutManager.setOrientation(RecyclerView.HORIZONTAL);
                                binding.recycler.setLayoutManager(layoutManager);
                                binding.recycler.setAdapter(adapter);

                            }

                            if (all_mock_test.length() <= 0) {
                                binding.textAmt.setVisibility(View.GONE);
                                binding.amtRecycler.setVisibility(View.GONE);
                            } else {
                                binding.txtFmt.setVisibility(View.VISIBLE);
                                binding.amtRecycler.setVisibility(View.VISIBLE);
                                AMTAdapter adapter = new AMTAdapter(getApplicationContext(), all_mock_test);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
                                binding.amtRecycler.setLayoutManager(layoutManager);
                                binding.amtRecycler.setAdapter(adapter);

                            }


                        } else {
                            Constant.setToast(getApplicationContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getTestSeries();
            }
        });
    }

    public class FMTAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemFreeMockTestBinding binding;
        Context context;
        JSONArray listItem;

        public FMTAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public FMTAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemFreeMockTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            FMTAdapter.RecyclerViewHolder holder = new FMTAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int i) {

            try {
                JSONObject item = listItem.getJSONObject(i);
                binding.txtTitle.setText(item.getString("test_title"));
                binding.maxM.setText(item.getString("test_total_marks"));
                binding.txtNoQues.setText(item.getString("test_total_question"));
                binding.time.setText(item.getString("test_duration") + "Min");



                String user_section_test;
                String test_end_date = "";
                user_section_test = item.getString("user_section_test");
                test_end_date = item.getString("test_end_date");
                if (test_end_date.equals("null") && user_section_test.equals("null")) {
                    binding.txtResult.setText("Start");
                    binding.txtResult.setBackgroundDrawable(getResources().getDrawable(R.drawable.gloden_border));
                }
                JSONObject user_section_test1 = new JSONObject();
                String ust_status = "";
                user_section_test1 = item.getJSONObject("user_section_test");
                ust_status = user_section_test1.getString("ust_status");
                if (user_section_test1 != null && ust_status.equals("0")) {
                    binding.txtResult.setText("Resume");
                    binding.txtResult.setBackgroundDrawable(getResources().getDrawable(R.drawable.gloden_border));
                }
                if (user_section_test1 != null && ust_status.equals("1")) {

                    binding.txtResult.setText("Result");
                    binding.txtResult.setBackgroundDrawable(getResources().getDrawable(R.drawable.gloden_border));
                }
                else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            binding.txtResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = listItem.getJSONObject(i);
                        String test_id = item.getString("test_id");
                            String user_section_test;
                            String test_end_date = "";
                            user_section_test = item.getString("user_section_test");
                            test_end_date = item.getString("test_end_date");
                            if (test_end_date.equals("null") && user_section_test.equals("null")) {
                                Intent startTest = new Intent(getApplicationContext(), AttemptInstruction.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "mytest");
                                startTest.putExtra("examType", 0);
                                startActivity(startTest);
                                finish();
                            }

                            JSONObject user_section_test1 = new JSONObject();
                            String ust_status = "";
                            user_section_test1 = item.getJSONObject("user_section_test");
                            ust_status = user_section_test1.getString("ust_status");
                            if (user_section_test1 != null && ust_status.equals("0")) {
                                Intent startTest = new Intent(getApplicationContext(), AttemptInstruction.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "mytest");
                                startTest.putExtra("examType", 1);
                                startActivity(startTest);
                                finish();
                            }
                            if (user_section_test1 != null && ust_status.equals("1")) {
                                Intent startTest = new Intent(getApplicationContext(), ExamActivity.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "result");
                                startTest.putExtra("examType", 2);
                                startActivity(startTest);
                                finish();
                            }
                            else {

                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemFreeMockTestBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    public class AMTAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemRowAllMockTestDetailsActivityBinding binding;
        Context context;
        JSONArray listItem;

        public AMTAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public AMTAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemRowAllMockTestDetailsActivityBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            AMTAdapter.RecyclerViewHolder holder = new AMTAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int i) {

            try {
                JSONObject item = all_mock_test.getJSONObject(i);

                binding.txtTitle.setText(item.getString("test_title"));

                binding.txt.setText(item.getString("test_total_question") + " Q | " + item.getString("test_total_marks") + " M | " + item.getString("test_duration") + " Min");


                    if (testType.equals("mytest")) {
                        String user_section_test;
                        String test_end_date = "";
                        user_section_test = item.getString("user_section_test");
                        test_end_date = item.getString("test_end_date");
                        if (test_end_date.equals("null") && user_section_test.equals("null")) {
                            binding.imgLock.setImageResource(R.drawable.play_icon);
                        }
                        JSONObject user_section_test1 = new JSONObject();
                        String ust_status = "";
                        user_section_test1 = item.getJSONObject("user_section_test");
                        ust_status = user_section_test1.getString("ust_status");
                        if (user_section_test1 != null && ust_status.equals("0")) {
                            binding.imgLock.setImageResource(R.drawable.pause_icon);
                        }
                        if (user_section_test1 != null && ust_status.equals("1")) {

                            binding.imgLock.setImageResource(R.drawable.analytics);
                        }
                        else {

                        }
                    }
                if (testType.equals("alltest")) {
                    binding.imgLock.setImageResource(R.drawable.padlock);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

            binding.imgStatusLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = all_mock_test.getJSONObject(i);

                        String test_id = item.getString("test_id");
                        if (testType.equals("mytest")) {
                            String user_section_test;
                            String test_end_date = "";
                            user_section_test = item.getString("user_section_test");
                            test_end_date = item.getString("test_end_date");
                            if (test_end_date.equals("null") && user_section_test.equals("null")) {
                                Intent startTest = new Intent(getApplicationContext(), AttemptInstruction.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "mytest");
                                startTest.putExtra("examType", 0);
                                startActivity(startTest);
                                finish();
//                                Constant.logPrint("helloClick", "click Start");
                            }

                            JSONObject user_section_test1 = new JSONObject();
                            String ust_status = "";
                            user_section_test1 = item.getJSONObject("user_section_test");
                            ust_status = user_section_test1.getString("ust_status");
                            if (user_section_test1 != null && ust_status.equals("0")) {
                                Intent startTest = new Intent(getApplicationContext(), AttemptInstruction.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "mytest");
                                startTest.putExtra("examType", 1);
//                                Constant.logPrint("helloClick", "click Resume");
                                startActivity(startTest);
                                finish();
                            }
                            if (user_section_test1 != null && ust_status.equals("1")) {
                                Intent startTest = new Intent(getApplicationContext(), ExamActivity.class);
                                startTest.putExtra("test_id", test_id);
                                startTest.putExtra("type", "result");
                                startTest.putExtra("examType", 2);
                                startActivity(startTest);
                                finish();
//                                Constant.logPrint("helloClick", "click Result");
                            }
                            else {

                            }

                        }
                        if (testType.equals("alltest")) {
                            Constant.setToast(getApplicationContext(), "Please Buy this Course first");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return all_mock_test.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemRowAllMockTestDetailsActivityBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(DetailsActivity.this);
        Constant.checkAdb(DetailsActivity.this);
    }
}
