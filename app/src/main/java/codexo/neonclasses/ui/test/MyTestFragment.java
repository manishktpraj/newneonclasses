package codexo.neonclasses.ui.test;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.FragmentMyTestBinding;
import codexo.neonclasses.databinding.ItemAllTestBinding;
import codexo.neonclasses.databinding.ItemFreeTestBinding;
import codexo.neonclasses.databinding.ItemRowTestSeriesListBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.home.HomeViewModel;
import retrofit2.Call;
import retrofit2.Callback;

public class MyTestFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentMyTestBinding binding;
    String package_url = "";
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray my_test_data = new JSONArray();
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentMyTestBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        session = new SessionManager(getContext());
        pDialog = Constant.getProgressBar(getContext());

        try {
            my_test_data = new JSONArray(getArguments().getString("my_test_data"));
            package_url = getArguments().getString("package_url");
//            Log.d("helloo456My",package_url+"++"+ my_test_data.toString());
            if (my_test_data.length() <= 0) {
                binding.recycler.setVisibility(View.GONE);
                binding.emptyView.setVisibility(View.VISIBLE);
            } else {
                binding.recycler.setVisibility(View.VISIBLE);
                binding.emptyView.setVisibility(View.GONE);
                MyTestAdapter adapter = new MyTestAdapter(getContext(), my_test_data);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                binding.recycler.setLayoutManager(layoutManager);
                binding.recycler.setAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        getTestSeries();
        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /*private void getTestSeries() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getTestSeriesUrl().department(session.getUserId(),"4");
//        Constant.logPrint("getUserIdddddd", session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();
                // Constant.logPrint("response_video", response);
                Log.d("helloo", response);
                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            imageUrl = jsonObject.getString("department_url");
                            sectional_testArray = jsonObject.getJSONArray("result");
                            Log.d("helloo", sectional_testArray.toString());
                            if (sectional_testArray.length() <= 0) {
                                binding.notiy.setVisibility(View.GONE);
                                binding.emptyView.setVisibility(View.VISIBLE);
                            } else {
                                binding.notiy.setVisibility(View.VISIBLE);
                                binding.emptyView.setVisibility(View.GONE);
                                MyTestAdapter adapter = new MyTestAdapter(getContext(), sectional_testArray);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
                                binding.recycler.setLayoutManager(layoutManager);
                                binding.recycler.setAdapter(adapter);
                            }


                        } else {
                            Constant.setToast(getContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getTestSeries();
            }
        });
    }*/

    public class MyTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemAllTestBinding binding;
        Context context;
        JSONArray listItem;

        public MyTestAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public MyTestAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemAllTestBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            MyTestAdapter.RecyclerViewHolder holder = new MyTestAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int i) {

            try {

                JSONObject item = my_test_data.getJSONObject(i);
                JSONObject neon_package = item.getJSONObject("neon_package");
                binding.txtTitle.setText(neon_package.getString("sub_pack_title"));
                String imgname = neon_package.getString("sub_pack_image");
                binding.priceLayout.setVisibility(View.GONE);
                Constant.setImage(package_url+imgname,binding.testImage,getContext());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = my_test_data.getJSONObject(i);
                        String myTestId = item.getString("upd_package_id");
                        Intent intent = new Intent(getContext(), DetailsActivity.class);
                        intent.putExtra("myTestId", myTestId);
                        intent.putExtra("type", "mytest");
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return my_test_data.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemAllTestBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}
