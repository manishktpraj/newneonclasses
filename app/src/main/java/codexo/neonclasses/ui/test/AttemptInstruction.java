package codexo.neonclasses.ui.test;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityAttemptInstructionBinding;
import codexo.neonclasses.databinding.ActivityWebBinding;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.model.SubjectInfoModel;
import codexo.neonclasses.ui.webview.WebActivity;

public class AttemptInstruction extends AppCompatActivity {
    ActivityAttemptInstructionBinding binding;
    private WebView webView = null;
    String url = Constant.INSTRUCTION;
    SqlDatabase db;
    int langStatus,examType;
    List<SubjectInfoModel> data = new ArrayList<>();
    //// String url = "https://www.youtube-nocookie.com/embed/Shwm4-Rl-NQ?modestbranding=1&rel=0&gyroscope=1";
    ProgressBar progressBar;
    String title="Neon Classes",test_id="",testType="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAttemptInstructionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        test_id =getIntent().getStringExtra("test_id");
        testType = getIntent().getStringExtra("type");
        examType = getIntent().getIntExtra("examType",0);
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        db = new SqlDatabase(AttemptInstruction.this);
        Constant.adbEnabled(AttemptInstruction.this);
        Constant.checkAdb(AttemptInstruction.this);

        webView = (WebView) findViewById(R.id.webview);
        webView.clearHistory();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.clearFormData();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(url);
        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
        binding.btnStartTest.setText("Please wait...");


//        Log.d("testtesttest_id_id", test_id);
        //Fetching data from SubjectDetails table
        if (db.checkDataExistOrNot("test_id",test_id) ==true) {
//            Log.d("checkDataExistOrNot", "check Data Exist");
            data = db.getRawExamInfo(Integer.parseInt(test_id));
            for (int i = 0; i < data.size(); i++) {
                langStatus = data.get(i).test_language;
                if (langStatus==0){
                    binding.tvLang.setText("English");
                    Constant.exLang = langStatus;
                }else if (langStatus==1){
                    binding.tvLang.setText("Hindi");
                    Constant.exLang = langStatus;
                }

                binding.btnSelectLanguage.setOnClickListener(v -> {
                    final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AttemptInstruction.this);
                    bottomSheetDialog.setContentView(R.layout.lang_layout);
                    bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                    bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView tv_hindi = bottomSheetDialog.findViewById(R.id.tv_hindi);
                    TextView tv_english = bottomSheetDialog.findViewById(R.id.tv_english);
                    TextView tv_Cancle = bottomSheetDialog.findViewById(R.id.tv_Cancle);
                    TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
                    bottomSheetDialog.show();
                    if (langStatus==0){
                        tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                        tv_hindi.setTextColor(getResources().getColor(R.color.black));
                    }else if (langStatus==1){
                        tv_english.setTextColor(getResources().getColor(R.color.black));
                        tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
                    }else {}
                    tv_hindi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                    updateSubjectDetailTable(Integer.parseInt(test_id), 1);
                            bottomSheetDialog.dismiss();
                            langStatus=1;
                            Constant.exLang = langStatus;
                            binding.tvLang.setText("Hindi");
                            tv_english.setTextColor(getResources().getColor(R.color.black));
                            tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
                        }
                    });
                    tv_english.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                    updateSubjectDetailTable(Integer.parseInt(test_id), 0);
                            bottomSheetDialog.dismiss();
                            langStatus=0;
                            Constant.exLang = langStatus;
                            binding.tvLang.setText("English");
                            tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                            tv_hindi.setTextColor(getResources().getColor(R.color.black));
                        }
                    });
                    tv_Cancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bottomSheetDialog.dismiss();
                        }
                    });
                });
            }
        }

        else {

//            Log.d("checkDataExistOrNot", "check Data NOt Exist");
            if (langStatus==0){
                binding.tvLang.setText("English");
            }else if (langStatus==1){
                binding.tvLang.setText("Hindi");
            }

            binding.btnSelectLanguage.setOnClickListener(v -> {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AttemptInstruction.this);
                bottomSheetDialog.setContentView(R.layout.lang_layout);
                bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TextView tv_hindi = bottomSheetDialog.findViewById(R.id.tv_hindi);
                TextView tv_english = bottomSheetDialog.findViewById(R.id.tv_english);
                TextView tv_Cancle = bottomSheetDialog.findViewById(R.id.tv_Cancle);
                TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
                bottomSheetDialog.show();
                if (langStatus==0){
                    tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                    tv_hindi.setTextColor(getResources().getColor(R.color.black));
                }else if (langStatus==1){
                    tv_english.setTextColor(getResources().getColor(R.color.black));
                    tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
                }else {}
                tv_hindi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        updateSubjectDetailTable(Integer.parseInt(test_id), 1);
                        bottomSheetDialog.dismiss();
                        langStatus=1;
                        binding.tvLang.setText("Hindi");
                        tv_english.setTextColor(getResources().getColor(R.color.black));
                        tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
                    }
                });
                tv_english.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        updateSubjectDetailTable(Integer.parseInt(test_id), 0);
                        bottomSheetDialog.dismiss();
                        langStatus=0;
                        binding.tvLang.setText("English");
                        tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                        tv_hindi.setTextColor(getResources().getColor(R.color.black));
                    }
                });
                tv_Cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });
            });
        }
        }

    public void updateSubjectDetailTable(int examId, int selectedLanguage) {
        Cursor res = db.getSubjectDetailsInfoData(examId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt( res.getColumnIndex("test_language"));
        @SuppressLint("Range") int tlqid = res.getInt( res.getColumnIndex("test_id"));
        SubjectInfoModel quedata = new SubjectInfoModel();
        quedata.test_language=selectedans;
//        Log.e("getDatass","if="+selectedLanguage);
        Boolean checkupdatedata = db.updateSubjectDetailTable(examId, selectedLanguage);
        Toast.makeText(AttemptInstruction.this, "Data Updated", Toast.LENGTH_SHORT).show();
    }

    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if(url.indexOf("neonclasses.com") > -1 || url.indexOf("neonclasses.co.in") > -1) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);

            binding.btnStartTest.setText("Start Test");
            binding.btnStartTest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent startTest = new Intent(getApplicationContext(), ExamActivity.class);
                    startTest.putExtra("test_id", test_id);
                    startTest.putExtra("type", testType);
                    startTest.putExtra("langStatus", langStatus);
                    startTest.putExtra("examType", examType);
                    startActivity(startTest);
                    finish();
                }
            });
        }

    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(AttemptInstruction.this);
        Constant.checkAdb(AttemptInstruction.this);
    }
}