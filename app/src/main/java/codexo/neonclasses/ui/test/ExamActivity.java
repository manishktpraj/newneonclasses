package codexo.neonclasses.ui.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.api.ApiClient;
import codexo.neonclasses.api.ApiInterface;
import codexo.neonclasses.databinding.ActivityExamBinding;
import codexo.neonclasses.examDatabase.SqlDatabase;
import codexo.neonclasses.helperclasses.HelperFile;
import codexo.neonclasses.model.ExamResponse;
import codexo.neonclasses.model.OtQuestionModel;
import codexo.neonclasses.model.OtUserTestSubjectViewItem;
import codexo.neonclasses.model.QuestionModel;
import codexo.neonclasses.model.RawExamInfoTable;
import codexo.neonclasses.model.RowUserSectionTestModel;
import codexo.neonclasses.model.SubjectInfoModel;
import codexo.neonclasses.model.SubjectModel;
import codexo.neonclasses.model.SubmitModel.SubmitModel;
import codexo.neonclasses.model.UstQuesAnswerModel;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.test.examAdapters.OnlinePaperAdapter;
import codexo.neonclasses.ui.test.examAdapters.PaperAdapter;
import codexo.neonclasses.ui.test.examAdapters.QuestionAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExamActivity extends AppCompatActivity {
    ActivityExamBinding binding;
    ProgressDialog pDialog;
    AlertDialog alertDialogExamsubmit;
    RelativeLayout ll_pay, rel_bottom;
    ImageView img_pause, img_play, img_lang, img_menu;
    LinearLayout ll_sub, lyr_img, ll_review, ll_review1, btn_clear, button_layout;
    TextView txt_title, txt_sub_title, review, review_check, time, submittest, tvquestion_no, total_marks, negative_marks, total_q, clear, previous, next;
    static RecyclerView question_recycler;
    RecyclerView dialog_recycler;
    RecyclerView sub_recyler;
    static CardView btn_previous, btn_next;
    Button txt_submit;
    static DrawerLayout drawelayout;
    String examTime = "", testType = "", finalSub = "";
    SqlDatabase db;
    RawExamInfoTable rawExamInfoTable;
    List<SubjectInfoModel> data = new ArrayList<>();
    List<RowUserSectionTestModel> RowUserSection = new ArrayList<>();
    List<OtQuestionModel> subinfoalldata = new ArrayList<>();
    List<SubjectModel> subData = new ArrayList<>();
    List<RowUserSectionTestModel> ustidData = new ArrayList<>();
    List<SubjectModel> subjectModels = new ArrayList<>();
    List<QuestionModel> questionModels = new ArrayList<>();
    List<UstQuesAnswerModel> ustQuesAnswerModels = new ArrayList<>();
    JSONArray all_quesion = new JSONArray();
    JSONArray sub_dataArray = new JSONArray();
    List<OtUserTestSubjectViewItem> otUserTestSubjectView = new ArrayList<>();
    //    List<OtQuestionItem> all_quesion = new ArrayList<>();
    PaperAdapter paperAdapter;
    OnlinePaperAdapter onlinePaperAdapter;
    ExamListAdapter examListAdapter;
    ApiInterface apiInterface;
    ExamResponse examResponse;
    static QuestionAdapter questionAdapter;
    int firstVisible;
    int lastVisible;
    static int clicked = 0;
    Constant helperFile;
    HelperFile examHelper;
    String Exam_id = "", examResumeTime = "", all_question, sub_data, ustid;
    SessionManager session;
    int counter = 1, langStatus, examType, prevnextid;
    CountDownTimer timer;
    String ust_dob1;
    ViewGroup viewGroup123;
    JSONObject alldataq = new JSONObject();
    JSONObject sub_dataObject = new JSONObject();
    JSONObject ustidDataObject = new JSONObject();
    JSONObject ustidjsonObject;
    JSONArray ustidArray = new JSONArray();

    String ust_id = "", getQueCorrectOption = "null";
    int getQueId = 0;
    int correctQ = 0;
    int wrongQ = 0;
    int skippedQ = 0;
    int correctQ1 = 0;
    int wrongQ1 = 0;
    int skippedQ1 = 0;
    List<QuestionModel> getnextprevdata = new ArrayList<>();
    int counters = 1;
    long examTimerTime;
    Cursor getnextprevdataCursor;

    //    List<String> all_quesion= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExamBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ll_pay = findViewById(R.id.ll_pay);
        img_pause = findViewById(R.id.img_pause);
        img_play = findViewById(R.id.img_play);
        ll_sub = findViewById(R.id.ll_sub);
        txt_sub_title = findViewById(R.id.txt_sub_title);
        lyr_img = findViewById(R.id.lyr_img);
        img_lang = findViewById(R.id.img_lang);
        review_check = findViewById(R.id.review_check);
        submittest = findViewById(R.id.submittest);
        tvquestion_no = findViewById(R.id.tvquestion_no);
        total_marks = findViewById(R.id.total_marks);
        negative_marks = findViewById(R.id.negative_marks);
        rel_bottom = findViewById(R.id.rel_bottom);
        btn_previous = findViewById(R.id.btn_previous);
        drawelayout = findViewById(R.id.drawelayout);
//        btn_clear = findViewById(R.id.btn_clear);
        clear = findViewById(R.id.clear);
        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        button_layout = findViewById(R.id.button_layout);
        question_recycler = findViewById(R.id.question_recycler);
        txt_submit = findViewById(R.id.txt_submit);
        session = new SessionManager(ExamActivity.this);
        Exam_id = getIntent().getStringExtra("test_id");
        testType = getIntent().getStringExtra("type");
        langStatus = getIntent().getIntExtra("langStatus", 0);
        examType = getIntent().getIntExtra("examType", 0);
//        Constant.logPrint("test_id_id", Exam_id);
        Constant.examTyped = examType;
        pDialog = Constant.getProgressBar(ExamActivity.this);
        viewGroup123 = findViewById(android.R.id.content);
        alertDialogExamsubmit = Constant.alertDialog(ExamActivity.this, viewGroup123, String.valueOf(Constant.subCorrect), String.valueOf(Constant.subWorng), String.valueOf(Constant.subSkippe));
        db = new SqlDatabase(ExamActivity.this);
        helperFile = new Constant(ExamActivity.this);
        examHelper = new HelperFile(db, ExamActivity.this);
        Constant.tvquestion_no = findViewById(R.id.tvquestion_no);
        Constant.webQuestion = findViewById(R.id.web_question);
        Constant.txtOptionA = findViewById(R.id.txt_optionA);
        Constant.txtOptionB = findViewById(R.id.txt_optionB);
        Constant.txtOptionC = findViewById(R.id.txt_optionC);
        Constant.txtOptionD = findViewById(R.id.txt_optionD);
        Constant.txtOptionE = findViewById(R.id.txt_optionE);
        Constant.llReview = findViewById(R.id.ll_review);
        Constant.llReview1 = findViewById(R.id.ll_review1);
        Constant.llOptionA = findViewById(R.id.ll_optionA);
        Constant.llOptionB = findViewById(R.id.ll_optionB);
        Constant.llOptionC = findViewById(R.id.ll_optionC);
        Constant.llOptionD = findViewById(R.id.ll_optionD);
        Constant.llOptionE = findViewById(R.id.ll_optionE);
        Constant.llIndexA = findViewById(R.id.ll_indexA);
        Constant.llIndexB = findViewById(R.id.ll_indexB);
        Constant.llIndexC = findViewById(R.id.ll_indexC);
        Constant.llIndexD = findViewById(R.id.ll_indexD);
        Constant.llIndexE = findViewById(R.id.ll_indexE);
        Constant.cardOptA = findViewById(R.id.card_opt_A);
        Constant.cardOptB = findViewById(R.id.card_opt_B);
        Constant.cardOptC = findViewById(R.id.card_opt_C);
        Constant.cardOptD = findViewById(R.id.card_opt_D);
        Constant.optCardE = findViewById(R.id.opt_card_E);
        Constant.imgOptionA = findViewById(R.id.img_optionA);
        Constant.imgOptionB = findViewById(R.id.img_optionB);
        Constant.imgOptionC = findViewById(R.id.img_optionC);
        Constant.imgOptionD = findViewById(R.id.img_optionD);
        Constant.imgOptionE = findViewById(R.id.img_optionE);
        Constant.btnPrevious = findViewById(R.id.btn_previous);
        Constant.btnNext = findViewById(R.id.btn_next);
        Constant.webSolution = findViewById(R.id.solution_webview);
        Constant.imgSolution = findViewById(R.id.solution_image);
        Constant.solutionCardView = findViewById(R.id.solution_cardView);
        examHelper.examType = examType;
        //Recyclerview for Subject
        //Recyclerview for Subject details
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ExamActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.dialogRecycler.setLayoutManager(linearLayoutManager);
        //Recyclerview for Questions details
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(ExamActivity.this);
        linearLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.questionRecycler.setLayoutManager(linearLayoutManager1);

        try {
            if (db.checkDataExistOrNot("test_id", Exam_id) == true) {
                getAllQuestionData();
                fetchSubject();
                examHelper.ExamQuestions(Exam_id, counters);
            } else {
                examdetails(session.getUserId(), Exam_id);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Disabled Recyclerview Scroll
        binding.questionRecycler.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                // Stop only scrolling.
                return rv.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            }
        });


        if (examType == 0 || examType == 1) {
            binding.solutionCardView.setVisibility(View.GONE);
            //Fetching data from SubjectDetails table
            data = db.getRawExamInfo(Integer.parseInt(Exam_id));
            RowUserSection = db.getrowUserSectionTestTable(Integer.parseInt(Exam_id));
            for (int i = 0; i < data.size(); i++) {
                binding.txtTitle.setText(data.get(i).test_title);
//                binding.time.setText(data.get(i).test_duration);
                binding.totalQ.setText("Total Q: " + data.get(i).test_total_question);
//                examTime = data.get(i).test_duration;
                langStatus = data.get(i).test_language;
                examHelper.langStatus = langStatus;
                counters = data.get(i).test_pause_on_que_num;

            }
            for (int i = 0; i < RowUserSection.size(); i++) {
                binding.time.setText(RowUserSection.get(i).ust_remainingtime);
                examTime = RowUserSection.get(i).ust_remainingtime;
                String[] time = examTime.split(":");
                examTime = time[0];
                startTimer();
            }

            if (counters != 1) {
                examHelper.ExamQuestions(Exam_id, counters);
            }


            binding.btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constant.clickedQue != 0) {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = Constant.clickedQue + 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                        Constant.clickedQue = 0;
                    } else {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = counters + 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                    }


                }
            });


            binding.btnPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constant.clickedQue != 0) {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = Constant.clickedQue - 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                        Constant.clickedQue = 0;
                    } else {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = counters - 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                    }

                }
            });
            binding.btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Constant.clickedQue != 0) {
                        examHelper.updateClearQuestionTable(Constant.tlqUiniId, 0, Constant.UNANSWERED);
//                        Log.e("MyTAGS", "if=lastVisible=" + Constant.tlqUiniId);
                    } else {
                        examHelper.updateClearQuestionTable(Constant.tlqUiniId, 0, Constant.UNANSWERED);
//                        Log.e("MyTAGS", "else=lastVisible=" + Constant.tlqUiniId);
                    }
                }
            });

            binding.llPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.llPay1.setVisibility(View.VISIBLE);
                    binding.llPay.setVisibility(View.GONE);
//                    callsubmitdata(Integer.parseInt(Exam_id));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit_exam_box();
                        }
                    }, 350);
                }
            });
            binding.llPay1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constant.setToast(getApplicationContext(), "Please wait...");
                }
            });

            binding.submittest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.submittest1.setVisibility(View.VISIBLE);
                    binding.submittest.setVisibility(View.GONE);
                    callsubmitdata(Integer.parseInt(Exam_id));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            submit_Box();
                        }
                    }, 350);
                }
            });
            binding.submittest1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constant.setToast(getApplicationContext(), "Please wait...");
                }
            });

            try {
                binding.imgMenu.setOnClickListener(view -> {
                    if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                        binding.drawelayout.openDrawer(GravityCompat.END);
                    else binding.drawelayout.closeDrawer(GravityCompat.END);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fetchSubject();
                        }
                    }, 350);

                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (examType == 2) {
            binding.solutionCardView.setVisibility(View.VISIBLE);
            //Fetching data from SubjectDetails table
            data = db.getRawExamInfo(Integer.parseInt(Exam_id));
            for (int i = 0; i < data.size(); i++) {
                binding.txtTitle.setText(data.get(i).test_title);
                binding.time.setText(data.get(i).test_duration);
                binding.totalQ.setText("Total Q: " + data.get(i).test_total_question);
                examTime = data.get(i).test_duration;
                langStatus = data.get(i).test_language;
                counters = data.get(i).test_pause_on_que_num;
                //startTimer();
            }
            for (int i = 0; i < RowUserSection.size(); i++) {
                binding.time.setText(RowUserSection.get(i).ust_remainingtime);
            }

            if (counters != 1) {
                examHelper.ExamQuestions(Exam_id, counters);
            }

            binding.next.setText("   Next   ");
            binding.btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constant.clickedQue != 0) {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = Constant.clickedQue + 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                        Constant.clickedQue = 0;
                    } else {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = counters + 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                    }


                }
            });


            binding.btnPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constant.clickedQue != 0) {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = Constant.clickedQue - 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                        Constant.clickedQue = 0;
                    } else {
                        binding.btnPrevious.setVisibility(View.VISIBLE);
                        counters = counters - 1;
                        examHelper.ExamQuestions(Exam_id, counters);
                    }

                }
            });

            binding.clear.setText("Analytics");
            binding.btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent analytics = new Intent(getApplicationContext(), AnalyticsActivity.class);
                    analytics.putExtra("test_id", Exam_id);
                    startActivity(analytics);
                }
            });


            binding.submittest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    submit_Box();
                }
            });
            binding.imgMenu.setOnClickListener(view -> {
                if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                    binding.drawelayout.openDrawer(GravityCompat.END);
                else binding.drawelayout.closeDrawer(GravityCompat.END);
            });
        }
        if (langStatus == 0) {
            binding.imgLang.setImageResource(R.drawable.translate);
        } else if (langStatus == 1) {
            binding.imgLang.setImageResource(R.drawable.translate_changed);
        }

        binding.imgLang.setOnClickListener(v -> {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ExamActivity.this);
            bottomSheetDialog.setContentView(R.layout.lang_layout);
            bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView tv_hindi = bottomSheetDialog.findViewById(R.id.tv_hindi);
            TextView tv_english = bottomSheetDialog.findViewById(R.id.tv_english);
            TextView tv_Cancle = bottomSheetDialog.findViewById(R.id.tv_Cancle);
            TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
            bottomSheetDialog.show();
            if (langStatus == 0) {
                tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                tv_hindi.setTextColor(getResources().getColor(R.color.black));
            } else if (langStatus == 1) {
                tv_english.setTextColor(getResources().getColor(R.color.black));
                tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
            } else {
            }
            tv_hindi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateSubjectDetailTable(Integer.parseInt(Exam_id), 1);
                    bottomSheetDialog.dismiss();
                    langStatus = 1;
                    Constant.exLang = langStatus;
                    binding.imgLang.setImageResource(R.drawable.translate_changed);
                    tv_english.setTextColor(getResources().getColor(R.color.black));
                    tv_hindi.setTextColor(getResources().getColor(R.color.purple_200));
                    examHelper.setAllQData(langStatus);
                }
            });
            tv_english.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateSubjectDetailTable(Integer.parseInt(Exam_id), 0);
                    bottomSheetDialog.dismiss();
                    langStatus = 0;
                    Constant.exLang = langStatus;
                    binding.imgLang.setImageResource(R.drawable.translate);
                    tv_english.setTextColor(getResources().getColor(R.color.purple_200));
                    tv_hindi.setTextColor(getResources().getColor(R.color.black));
                    examHelper.setAllQData(langStatus);
                }
            });
            tv_Cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                }
            });
        });


    }


    //Fecthing All Subjects
    public void fetchSubject() {
        try {
            subjectModels = db.getOtUserTestSubjectViewItem(Integer.parseInt(Exam_id));
            if (subjectModels.size() == 0) {
                binding.dialogRecycler.setVisibility(View.GONE);
            } else {
//                Log.e("ShowMessage", "I am in Subject");
                binding.dialogRecycler.setVisibility(View.VISIBLE);
                paperAdapter = new PaperAdapter(subjectModels, ExamActivity.this, examType);
                binding.dialogRecycler.setAdapter(paperAdapter);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    //Fecthing All Subjects
    public void getAllQuestionData() {
        try {
            questionModels = db.getAllQuestionData(Integer.parseInt(Exam_id));
            ustQuesAnswerModels = db.getUstQuesAnswerTable(Integer.parseInt(Exam_id));
            Constant.questionModelList = questionModels;

//            Log.e("qsdqdwd", "count=" + Constant.questionModelList.size());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    //delete questions
    public void deleteQuestionsDetails(int questionId) {
//        Log.e("delete", "deletedata=" + questionId);
        Boolean checkupdatedata = db.deleteQuestionsDetails(questionId);
        if (checkupdatedata) {
            Toast.makeText(ExamActivity.this, "Data Deleted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ExamActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

    }

    public void examdetails(String student_id, String test_id) {

        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ExamResponse> call = apiInterface.examdetails(student_id, test_id);
        call.enqueue(new Callback<ExamResponse>() {
            @Override
            public void onResponse(Call<ExamResponse> call, Response<ExamResponse> response) {
                pDialog.dismiss();
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        examResponse = response.body();
                        binding.txtTitle.setText(examResponse.getRowExamInfo().getTestTitle());
                        binding.totalQ.setText("Total Q: " + examResponse.getRowExamInfo().getTestTotalQuestion());
//                        examTime = examResponse.getRowExamInfo().getTestDuration();
//                        binding.time.setText(examResponse.getRowExamInfo().getTestDuration());


                        //Fetching subject
                        otUserTestSubjectView = examResponse.getRowExamInfo().getOtUserTestSubjectView();
                        binding.dialogRecycler.setVisibility(View.VISIBLE);
                        onlinePaperAdapter = new OnlinePaperAdapter(otUserTestSubjectView, ExamActivity.this);
                        binding.dialogRecycler.setAdapter(onlinePaperAdapter);
                        helperFile.writeInsertedData(true);
                        db.addRawExam(examResponse.getRowExamInfo().getTestId(), langStatus, examResponse.getRowExamInfo().getTestAdminId(), examResponse.getRowExamInfo().getTestDuration(), 1,
                                examResponse.getRowExamInfo().isTestFreeStatus(), examResponse.getRowExamInfo().getTestDptId(), examResponse.getRowExamInfo().getTestSolutionFile(),
                                examResponse.getRowExamInfo().getTestDatetime(), examResponse.getRowExamInfo().getTestTitle(), examResponse.getRowExamInfo().getTestTotalMarks(), examResponse.getRowExamInfo().getProductInStock(),
                                examResponse.getRowExamInfo().getTestMarks(), examResponse.getRowExamInfo().getTestSeriesId(), examResponse.getRowExamInfo().getTestTsId(), examResponse.getRowExamInfo().getTestType(),
                                examResponse.getRowExamInfo().getTestEtId(), examResponse.getRowExamInfo().getTestPaperFile(), examResponse.getRowExamInfo().getTestSubQuestion(), examResponse.getRowExamInfo().getTestSectionalStatus(),
                                examResponse.getRowExamInfo().getTestStatus(), examResponse.getRowExamInfo().getTestTotalQuestion(), examResponse.getRowExamInfo().getTestStartDate(), examResponse.getRowExamInfo().getOtTestSolutionDatetime(),
                                examResponse.getRowExamInfo().getTestSubjectStopeTime(), examResponse.getRowExamInfo().getTestLiveStatus(), examResponse.getRowExamInfo().getTestSubId(), examResponse.getRowExamInfo().getTestCommingDate(),
                                examResponse.getRowExamInfo().getTestType(), examResponse.getRowExamInfo().getTestEndDate(), examResponse.getRowExamInfo().getTestNegativeMarks(), examResponse.getRowExamInfo().getTestSyllabusInfo(),
                                examResponse.getRowExamInfo().getProductPublishDate(), examResponse.getRowExamInfo().getTestNumber());

                        db.addrowUserSectionTestTable(examResponse.getRowUserSectionTest().getUstId(),
                                examResponse.getRowUserSectionTest().getUstTestId(),
                                examResponse.getRowUserSectionTest().getUstUserId(),
                                examResponse.getRowUserSectionTest().getUstCreated(),
                                examResponse.getRowUserSectionTest().getUstRemainingtime(),
                                examResponse.getRowUserSectionTest().getUstStatus(),
                                examResponse.getRowUserSectionTest().getUstQuestionMark(),
                                examResponse.getRowUserSectionTest().getUstWrongQuestion(),
                                examResponse.getRowUserSectionTest().getUstCorrectQuestion(),
                                examResponse.getRowUserSectionTest().getUstDob());

                        examTime = examResponse.getRowUserSectionTest().getUstRemainingtime();
                        binding.time.setText(examResponse.getRowUserSectionTest().getUstRemainingtime());
                        String[] time = examTime.split(":");
                        examTime = time[0];
                        if (examType == 0 || examType == 1) {
                            startTimer();
                        } else {
                            binding.time.setText(examResponse.getRowUserSectionTest().getUstRemainingtime());
                        }


                        for (int i = 0; i < examResponse.getRowExamInfo().getOtUserTestSubjectView().size(); i++) {
                            db.addSubject(examResponse.getRowExamInfo().getTestSubId(), examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdNegativeMarks(),
                                    examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdStopTime(), examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTestId(),
                                    examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
                                    examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubStatus(), examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubParentId(),
                                    examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalQuestion(), examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalMarks(),
                                    examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubName(), examResponse.getRowExamInfo().getOtUserTestSubjectView().size());


                            for (int k = 0; k < examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size(); k++) {
                                db.addOtQuestionData(examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueTestId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueOrder(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueCorrectOption(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueArea(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueCorrectOptionHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueSubId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueTlqId(),
                                        null, null);

                                if (examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer() == null) {
                                    db.addUstQuesAnswerTable(counter + k, 0,
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getQueCorrectOption(),
                                            0, "0",
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTestId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
                                            examResponse.getRowUserSectionTest().getUstId(), session.getUserId(), 2, 0);

                                } else {
                                    db.addUstQuesAnswerTable(counter + k, examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaQueId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaCorrect(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaUserAns(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaMark(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTestId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaUstId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaUserId(),
                                            examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(k).getUstQuesAnswer().getUstqaStatus(), 0);
                                }

                            }

                            for (int j = 0; j < examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size(); j++) {
                                db.addQuestions(counter + j, examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqId(),
                                        examResponse.getRowExamInfo().getTestId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUniqueId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionType(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptions(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1Attachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2Attachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3Attachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4Attachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5Attachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqLangId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionText(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionTextHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachments(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1AttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2AttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3AttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4AttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5AttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionTypeHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachmentsHindi(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishInstruction(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiInstruction(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolution(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolution(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolutionAttach(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolutionAttach(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionAttach(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionStatus(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionOrder(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUserId(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionHindiAttach(),
                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),

                                        examResponse.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqCorrectOption(),
                                        0, 0);

                            }


                        }
                        getAllQuestionData();
                        fetchSubject();
                        examHelper.ExamQuestions(Exam_id, counters);
//                        storeAssignment("saved");

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ExamResponse> call, Throwable t) {
//                Log.d("testing", "out=" + t.getMessage());
                // Log.e("checktest","test="+t.getMessage());
            }
        });
    }


    public void updateQuestionTable1(int questionId, int selectedAnswer, int questionStatus) {
        Cursor res = db.getquestiondata(questionId);
        res.moveToFirst();
        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("ustqa_user_ans"));
        @SuppressLint("Range") int quesSta = res.getInt(res.getColumnIndex("ustqa_status"));
        @SuppressLint("Range") int tlqid = res.getInt(res.getColumnIndex("tlq_id"));
        UstQuesAnswerModel quedata = new UstQuesAnswerModel();
        quedata.ustqa_status = quesSta;
        quedata.ustqa_user_ans = selectedans;
        if (selectedans != 0) {
            Boolean checkupdatedata = db.updateQuestionTable(questionId, quedata.ustqa_user_ans, quedata.ustqa_status);
        } else {
            Boolean checkupdatedata = db.updateQuestionTable(questionId, selectedAnswer, questionStatus);
        }
    }


    public void updateSubjectDetailTable(int examId, int selectedLanguage) {
//        Cursor res = db.getSubjectDetailsInfoData(examId);
//        res.moveToFirst();
//        @SuppressLint("Range") int selectedans = res.getInt(res.getColumnIndex("test_language"));
//        @SuppressLint("Range") int tlqid = res.getInt(res.getColumnIndex("test_id"));
//        SubjectInfoModel quedata = new SubjectInfoModel();
//        quedata.test_language = selectedans;
//        Log.e("getDatass", "if=" + selectedLanguage);
        Boolean checkupdatedata = db.updateSubjectDetailTable(examId, selectedLanguage);
//        Toast.makeText(ExamActivity.this, "Data Updated", Toast.LENGTH_SHORT).show();
//        notifyDataSetChanged();
    }


    private void startTimer() {
//        examTime = "1";
        long totalTime = Long.parseLong(examTime) * 60 * 1000;
        timer = new CountDownTimer(totalTime + 1000, 1000) {
            @Override
            public void onTick(long remainingTime) {
                String times = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                        TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime))
                );
                binding.time.setText(times + " min");
                examResumeTime = times;

//                Log.d("remainingTime123", "In Timer= " + examResumeTime);
            }

            @Override
            public void onFinish() {

                callsubmitdata(Integer.parseInt(Exam_id));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        time_end_submit_Box();
                    }
                }, 350);
            }
        };
        timer.start();

    }


    public void callsubmitdata(int testId) {
        subinfoalldata = db.getOtQuestionData(testId);
        subData = db.getOtUserTestSubjectViewItem(testId);
        ustidData = db.getrowUserSectionTestTable(testId);


//        JSONArray ustidDataArray = new JSONArray();

        for (int i = 0; i < subinfoalldata.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            JSONObject jsonObject1 = new JSONObject();
            JSONObject jsonObject2 = new JSONObject();
            String que_id = String.valueOf(subinfoalldata.get(i).que_id);
            String que_sub_id = subinfoalldata.get(i).que_sub_id;
            String que_test_id = subinfoalldata.get(i).que_test_id;
            String que_correct_option = subinfoalldata.get(i).que_correct_option;
            String que_order = String.valueOf(subinfoalldata.get(i).que_order);
            String que_correct_option_hindi = subinfoalldata.get(i).que_correct_option_hindi;
            String que_area = String.valueOf(subinfoalldata.get(i).que_area);
            String que_tlq_id = String.valueOf(subinfoalldata.get(i).que_tlq_id);
            String ust_ques_answer = subinfoalldata.get(i).ust_ques_answer;
            String ot_matchword_ques = subinfoalldata.get(i).ot_matchword_ques;


            Cursor res = db.getQuestionIdData11(Integer.parseInt(que_tlq_id));
            res.moveToFirst();

            if (res != null && res.getCount() > 0) {
                if (res.moveToFirst()) {
                    do {
                        @SuppressLint("Range") int tlq_id1 = res.getInt(res.getColumnIndex("tlq_id"));
                        @SuppressLint("Range") int tlq_counterid = res.getInt(res.getColumnIndex("tlq_counterid"));
                        @SuppressLint("Range") int que_test_id1 = res.getInt(res.getColumnIndex("que_test_id"));
                        @SuppressLint("Range") int tlq_question_id = res.getInt(res.getColumnIndex("tlq_question_id"));
                        @SuppressLint("Range") String tlq_unique_id = res.getString(res.getColumnIndex("tlq_unique_id"));
                        @SuppressLint("Range") int tlq_question_type = res.getInt(res.getColumnIndex("tlq_question_type"));
                        @SuppressLint("Range") String tlq_options = res.getString(res.getColumnIndex("tlq_options"));
                        @SuppressLint("Range") String tlq_option1_attachments = res.getString(res.getColumnIndex("tlq_option1_attachments"));
                        @SuppressLint("Range") String tlq_option2_attachments = res.getString(res.getColumnIndex("tlq_option2_attachments"));
                        @SuppressLint("Range") String tlq_option3_attachments = res.getString(res.getColumnIndex("tlq_option3_attachments"));
                        @SuppressLint("Range") String tlq_option4_attachments = res.getString(res.getColumnIndex("tlq_option4_attachments"));
                        @SuppressLint("Range") String tlq_option5_attachments = res.getString(res.getColumnIndex("tlq_option5_attachments"));
                        @SuppressLint("Range") String tlq_lang_id = res.getString(res.getColumnIndex("tlq_lang_id"));
                        @SuppressLint("Range") String tlq_question_text = res.getString(res.getColumnIndex("tlq_question_text"));
                        @SuppressLint("Range") String tlq_question_text_hindi = res.getString(res.getColumnIndex("tlq_question_text_hindi"));
                        @SuppressLint("Range") String tlq_options_hindi = res.getString(res.getColumnIndex("tlq_options_hindi"));
                        @SuppressLint("Range") String tlq_question_attachments = res.getString(res.getColumnIndex("tlq_question_attachments"));
                        @SuppressLint("Range") String tlq_option1_attachments_hindi = res.getString(res.getColumnIndex("tlq_option1_attachments_hindi"));
                        @SuppressLint("Range") String tlq_option2_attachments_hindi = res.getString(res.getColumnIndex("tlq_option2_attachments_hindi"));
                        @SuppressLint("Range") String tlq_option3_attachments_hindi = res.getString(res.getColumnIndex("tlq_option3_attachments_hindi"));
                        @SuppressLint("Range") String tlq_option4_attachments_hindi = res.getString(res.getColumnIndex("tlq_option4_attachments_hindi"));
                        @SuppressLint("Range") String tlq_option5_attachments_hindi = res.getString(res.getColumnIndex("tlq_option5_attachments_hindi"));
                        @SuppressLint("Range") boolean tlq_question_type_hindi = res.getInt(res.getColumnIndex("tlq_question_type_hindi")) == 0;
                        @SuppressLint("Range") String tlq_question_attachments_hindi = res.getString(res.getColumnIndex("tlq_question_attachments_hindi"));
                        @SuppressLint("Range") String tlq_english_instruction = res.getString(res.getColumnIndex("tlq_english_instruction"));
                        @SuppressLint("Range") String tlq_hindi_instruction = res.getString(res.getColumnIndex("tlq_hindi_instruction"));
                        @SuppressLint("Range") String tlq_hindi_solution = res.getString(res.getColumnIndex("tlq_hindi_solution"));
                        @SuppressLint("Range") String tlq_english_solution = res.getString(res.getColumnIndex("tlq_english_solution"));
                        @SuppressLint("Range") String tlq_hindi_solution_attach = res.getString(res.getColumnIndex("tlq_hindi_solution_attach"));
                        @SuppressLint("Range") String tlq_english_solution_attach = res.getString(res.getColumnIndex("tlq_english_solution_attach"));
                        @SuppressLint("Range") String tlq_option_attach = res.getString(res.getColumnIndex("tlq_option_attach"));
                        @SuppressLint("Range") boolean tlq_question_status = res.getInt(res.getColumnIndex("tlq_question_status")) == 0;
                        @SuppressLint("Range") int tlq_question_order = res.getInt(res.getColumnIndex("tlq_question_order"));
                        @SuppressLint("Range") int tlq_user_id = res.getInt(res.getColumnIndex("tlq_user_id"));
                        @SuppressLint("Range") String tlq_option_hindi_attach = res.getString(res.getColumnIndex("tlq_option_hindi_attach"));
                        @SuppressLint("Range") int tlq_sub_id = res.getInt(res.getColumnIndex("tlq_sub_id"));
                        @SuppressLint("Range") int tlq_correct_option = res.getInt(res.getColumnIndex("tlq_correct_option"));
                        @SuppressLint("Range") int user_que_status = res.getInt(res.getColumnIndex("user_que_status"));
                        @SuppressLint("Range") int UserSelectedAns = res.getInt(res.getColumnIndex("UserSelectedAns"));


                        try {
                            jsonObject1.put("tlq_id", tlq_question_id);
//                            jsonObject1.put("tlq_counterid", tlq_counterid);
//                            jsonObject1.put("que_test_id", que_test_id1);
                            jsonObject1.put("tlq_question_id", que_id);
                            jsonObject1.put("tlq_unique_id", tlq_unique_id);
                            jsonObject1.put("tlq_question_type", tlq_question_type);
                            jsonObject1.put("tlq_options", tlq_options);
                            jsonObject1.put("tlq_option1_attachments", tlq_option1_attachments);
                            jsonObject1.put("tlq_option2_attachments", tlq_option2_attachments);
                            jsonObject1.put("tlq_option3_attachments", tlq_option3_attachments);
                            jsonObject1.put("tlq_option4_attachments", tlq_option4_attachments);
                            jsonObject1.put("tlq_option5_attachments", tlq_option5_attachments);
                            jsonObject1.put("tlq_lang_id", tlq_lang_id);
                            jsonObject1.put("tlq_question_text", tlq_question_text);
                            jsonObject1.put("tlq_question_text_hindi", tlq_question_text_hindi);
                            jsonObject1.put("tlq_options_hindi", tlq_options_hindi);
                            jsonObject1.put("tlq_question_attachments", tlq_question_attachments);
                            jsonObject1.put("tlq_option1_attachments_hindi", tlq_option1_attachments_hindi);
                            jsonObject1.put("tlq_option2_attachments_hindi", tlq_option2_attachments_hindi);
                            jsonObject1.put("tlq_option3_attachments_hindi", tlq_option3_attachments_hindi);
                            jsonObject1.put("tlq_option4_attachments_hindi", tlq_option4_attachments_hindi);
                            jsonObject1.put("tlq_option5_attachments_hindi", tlq_option5_attachments_hindi);
                            jsonObject1.put("tlq_question_type_hindi", tlq_question_type_hindi);
                            jsonObject1.put("tlq_question_attachments_hindi", tlq_question_attachments_hindi);
                            jsonObject1.put("tlq_english_instruction", tlq_english_instruction);
                            jsonObject1.put("tlq_hindi_instruction", tlq_hindi_instruction);
                            jsonObject1.put("tlq_hindi_solution", tlq_hindi_solution);
                            jsonObject1.put("tlq_english_solution", tlq_english_solution);
                            jsonObject1.put("tlq_hindi_solution_attach", tlq_hindi_solution_attach);
                            jsonObject1.put("tlq_english_solution_attach", tlq_english_solution_attach);
                            jsonObject1.put("tlq_option_attach", tlq_option_attach);
                            jsonObject1.put("tlq_question_status", tlq_question_status);
                            jsonObject1.put("tlq_question_order", tlq_question_order);
                            jsonObject1.put("tlq_user_id", tlq_user_id);
                            jsonObject1.put("tlq_option_hindi_attach", tlq_option_hindi_attach);
                            jsonObject1.put("tlq_sub_id", tlq_sub_id);
                            jsonObject1.put("tlq_correct_option", tlq_correct_option);
//                            jsonObject1.put("user_que_status", user_que_status);
//                            jsonObject1.put("UserSelectedAns", UserSelectedAns);

//                            Log.e("matchwordArray", tlq_question_id + "    " + i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } while (res.moveToNext());
                    res.close();
                }

            }

            Cursor ustqa = db.getUstQuesAnswerTableSubmit(que_id);
            ustqa.moveToFirst();
            if (ustqa != null && ustqa.getCount() > 0) {
                if (ustqa.moveToFirst()) {
                    do {
                        @SuppressLint("Range") int ustqa_id = ustqa.getInt(ustqa.getColumnIndex("ustqa_id"));
                        @SuppressLint("Range") int ustqa_que_id = ustqa.getInt(ustqa.getColumnIndex("ustqa_que_id"));
                        @SuppressLint("Range") String ustqa_correct = ustqa.getString(ustqa.getColumnIndex("ustqa_correct"));
                        @SuppressLint("Range") int ustqa_user_ans = ustqa.getInt(ustqa.getColumnIndex("ustqa_user_ans"));
                        @SuppressLint("Range") String ustqa_mark = ustqa.getString(ustqa.getColumnIndex("ustqa_mark"));
                        @SuppressLint("Range") int ustqa_test_id = ustqa.getInt(ustqa.getColumnIndex("ustqa_test_id"));
                        @SuppressLint("Range") int ustqa_sub = ustqa.getInt(ustqa.getColumnIndex("ustqa_sub"));
                        @SuppressLint("Range") int ustqa_ust_id = ustqa.getInt(ustqa.getColumnIndex("ustqa_ust_id"));
                        @SuppressLint("Range") String ustqa_user_id = ustqa.getString(ustqa.getColumnIndex("ustqa_user_id"));
                        @SuppressLint("Range") int ustqa_status = ustqa.getInt(ustqa.getColumnIndex("ustqa_status"));
                        @SuppressLint("Range") int ustqa_review = ustqa.getInt(ustqa.getColumnIndex("ustqa_review"));
                        int crAns = Integer.parseInt(ustqa_correct);
                        /*if (crAns == ustqa_user_ans) {
                            correctQ = correctQ + 1;
                        }
                        if (crAns != ustqa_user_ans && ustqa_user_ans != 0) {
                            wrongQ = wrongQ + 1;
                        }*/
                        //for skipped Question
                        if (ustqa_user_ans == 0) {
                            skippedQ = skippedQ + 1;
                            skippedQ1 = skippedQ1 + 1;
                            Constant.subSkippe = skippedQ1;
                        }

                        // for answered question
                        if (ustqa_user_ans > 0) {
                            correctQ = correctQ + 1;
                            correctQ1 = correctQ1 + 1;
                            Constant.subCorrect = correctQ1;
                        }

                        // for unanswered question
                        if (ustqa_status == 0) {
                            wrongQ = wrongQ + 1;
                            wrongQ1 = wrongQ1 + 1;
                            Constant.subWorng = wrongQ1;
                        }
                        try {
                            jsonObject2.put("ustqa_id", ustqa_id);
                            jsonObject2.put("ustqa_que_id", ustqa_que_id);
                            jsonObject2.put("ustqa_correct", ustqa_correct);
                            jsonObject2.put("ustqa_user_ans", ustqa_user_ans);
                            jsonObject2.put("ustqa_mark", ustqa_mark);
                            jsonObject2.put("ustqa_test_id", ustqa_test_id);
                            jsonObject2.put("ustqa_sub", ustqa_sub);
                            jsonObject2.put("ustqa_ust_id", ustqa_ust_id);
                            jsonObject2.put("ustqa_user_id", ustqa_user_id);
                            jsonObject2.put("ustqa_status", ustqa_status);
                            jsonObject2.put("ustqa_review", ustqa_review);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } while (ustqa.moveToNext());
                    ustqa.close();
                }

            }

            JSONArray matchwordArray = new JSONArray();

            try {
                jsonObject.put("que_id", que_id);
                jsonObject.put("que_sub_id", que_sub_id);
                jsonObject.put("que_test_id", que_test_id);
                jsonObject.put("que_correct_option", que_correct_option);
                jsonObject.put("que_order", que_order);
                jsonObject.put("que_correct_option_hindi", que_correct_option_hindi);
                jsonObject.put("que_area", que_area);
                jsonObject.put("que_tlq_id", que_tlq_id);
                jsonObject.put("ust_ques_answer", jsonObject2);
                jsonObject.put("ot_matchword_ques", matchwordArray);
                jsonObject.put("ot_test_language_question", jsonObject1);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            all_quesion.put(jsonObject);
        }


        for (int i = 0; i < subData.size(); i++) {
            JSONObject subdatajsonObject = new JSONObject();
            String test_id = String.valueOf(subData.get(i).test_id);
            String tsd_subject_id = subData.get(i).tsd_subject_id;
            String tsd_negative_marks = String.valueOf(subData.get(i).tsd_negative_marks);
            String tsd_stop_time = subData.get(i).tsd_stop_time;
            String tsd_test_id = String.valueOf(subData.get(i).tsd_test_id);
            String sub_id = String.valueOf(subData.get(i).sub_id);
            String sub_status = String.valueOf(subData.get(i).sub_status);
            String sub_parent_id = String.valueOf(subData.get(i).sub_parent_id);
            String tsd_total_question = subData.get(i).tsd_total_question;
            String tsd_total_marks = subData.get(i).tsd_total_marks;
            String sub_name = subData.get(i).sub_name;
            int tsd_id = Integer.parseInt(test_id) - 1;
            try {
                subdatajsonObject.put("tsd_id", tsd_id);
                subdatajsonObject.put("tsd_subject_id", tsd_subject_id);
                subdatajsonObject.put("tsd_negative_marks", tsd_negative_marks);
                subdatajsonObject.put("tsd_stop_time", tsd_stop_time);
                subdatajsonObject.put("tsd_test_id", tsd_test_id);
                subdatajsonObject.put("sub_id", sub_id);
                subdatajsonObject.put("sub_status", sub_status);
                subdatajsonObject.put("sub_parent_id", sub_parent_id);
                subdatajsonObject.put("tsd_total_question", tsd_total_question);
                subdatajsonObject.put("tsd_total_marks", tsd_total_marks);
                subdatajsonObject.put("sub_name", sub_name);
                subdatajsonObject.put("ot_question", all_quesion);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sub_dataArray.put(subdatajsonObject);

        }


        for (int i = 0; i < ustidData.size(); i++) {
            ustidjsonObject = new JSONObject();
            ust_id = String.valueOf(ustidData.get(i).ust_id);
            String ust_test_id = String.valueOf(ustidData.get(i).ust_test_id);
            String ust_user_id = String.valueOf(ustidData.get(i).ust_user_id);
            String ust_created = ustidData.get(i).ust_created;
            String ust_remainingtime = String.valueOf(ustidData.get(i).ust_remainingtime);
            String ust_status = String.valueOf(ustidData.get(i).ust_status);
            String ust_question_mark = String.valueOf(ustidData.get(i).ust_question_mark);
            String ust_wrong_question = String.valueOf(ustidData.get(i).ust_wrong_question);
            String ust_correct_question = ustidData.get(i).ust_correct_question;
            String ust_dob = ustidData.get(i).ust_dob;
//            sub_dataArray


            try {
                ustidjsonObject.put("ust_id", ust_id);
                ustidjsonObject.put("ust_test_id", ust_test_id);
                ustidjsonObject.put("ust_user_id", ust_user_id);
                ustidjsonObject.put("ust_created", ust_created);
                ustidjsonObject.put("ust_remainingtime", ust_remainingtime);
                ustidjsonObject.put("ust_status", ust_status);
                ustidjsonObject.put("ust_question_mark", ust_question_mark);
                ustidjsonObject.put("ust_wrong_question", ust_wrong_question);
                ustidjsonObject.put("ust_correct_question", ust_correct_question);
                if (ust_dob == null) {
                    ustidjsonObject.put("ust_dob", "null");
                } else {
                    ustidjsonObject.put("ust_dob", ust_dob);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            ustidArray.put(ustidjsonObject);
//            Log.e("ustidjsonObject", ustidjsonObject + "");
        }
//        Log.e("sub_dataArray", "Correct Que  " + correctQ + "  Wrong Que " + wrongQ + "  skipped Que " + skippedQ);
//        binding.loopdata.setText("test_it = "+Exam_id+"\n\n\"timer= "+examResumeTime+"\n\n\"sub_data\n\n"+sub_dataArray.toString()+"\n\n all_quesion \n\n"+all_quesion+"\n\n ustid \n\n"+ustidjsonObject);
//        String abc= binding.loopdata.getText().toString();
//        Log.e("datas",abc);

    }

    @SuppressLint("WrongConstant")
    private void submit_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_submit_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        Button btn_submit1 = dialogView.findViewById(R.id.btn_submit1);
        TextView correctedQ, WrongedQ, SkipQ;
        correctedQ = dialogView.findViewById(R.id.correctQ);
        WrongedQ = dialogView.findViewById(R.id.wrongQ);
        SkipQ = dialogView.findViewById(R.id.skippedQ);
        correctedQ.setText(String.valueOf(correctQ));
        WrongedQ.setText(String.valueOf(wrongQ));
        SkipQ.setText(String.valueOf(skippedQ));
        builder.setView(dialogView);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_cancle.setOnClickListener(v -> {
            alertDialog.dismiss();
            correctQ = 0;
            wrongQ = 0;
            skippedQ = 0;

            binding.submittest1.setVisibility(View.GONE);
            binding.submittest.setVisibility(View.VISIBLE);
        });
        btn_submit.setOnClickListener(v -> {
            btn_submit1.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            alertDialogExamsubmit = Constant.alertDialog(ExamActivity.this, viewGroup123, String.valueOf(Constant.subCorrect), String.valueOf(Constant.subWorng), String.valueOf(Constant.subSkippe));
            submitTest(all_quesion, sub_dataArray, Exam_id, examResumeTime, ustidjsonObject);
            finalSub = "submit";
            correctQ = 0;
            wrongQ = 0;
            skippedQ = 0;
            alertDialog.dismiss();
            binding.submittest1.setVisibility(View.GONE);
            binding.submittest.setVisibility(View.VISIBLE);
            Constant.setToast(getApplicationContext(), Constant.subCorrect+" "+ Constant.subWorng+" "+Constant.subSkippe);
        });
        btn_submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.setToast(getApplicationContext(), "Please wait....");
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void time_end_submit_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_submit_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        btn_cancle.setVisibility(View.GONE);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        TextView text_tv0 = dialogView.findViewById(R.id.text_tv0);
        TextView text_tv1 = dialogView.findViewById(R.id.text_tv1);
        TextView correctedQ, WrongedQ, SkipQ;
        correctedQ = dialogView.findViewById(R.id.correctQ);
        WrongedQ = dialogView.findViewById(R.id.wrongQ);
        SkipQ = dialogView.findViewById(R.id.skippedQ);
        correctedQ.setText(String.valueOf(correctQ));
        WrongedQ.setText(String.valueOf(wrongQ));
        SkipQ.setText(String.valueOf(skippedQ));
        text_tv0.setVisibility(View.GONE);
        text_tv1.setText("Your test time is over.\nYou have to submit your test now.");
        builder.setView(dialogView);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_submit.setOnClickListener(v -> {

//            submitTest(all_quesion, sub_dataArray, Exam_id, examResumeTime, ustidjsonObject);

//            timer.cancel();
            finalSub = "submit";
            correctQ = 0;
            wrongQ = 0;
            skippedQ = 0;
            alertDialog.dismiss();
        });
    }


    private void exit_exam_box() {
        int testID = Integer.parseInt(Exam_id);
        final Dialog dialog = new Dialog(ExamActivity.this);
        dialog.setContentView(R.layout.exit_exam_box);
        TextView resume_cancle = dialog.findViewById(R.id.exam_cancle);
        TextView exam_resume = dialog.findViewById(R.id.exam_submit);
        dialog.setCancelable(false);
        exam_resume.setOnClickListener(v -> {
            binding.llPay1.setVisibility(View.GONE);
            binding.llPay.setVisibility(View.VISIBLE);
            updateTimerSubjectDetailTable(testID, examResumeTime);
            if (Constant.clickedQue != 0) {
                update_test_pause_on_que_numSubjectDetail(testID, Constant.clickedQue);
                Constant.clickedQue = 0;
            } else {
                update_test_pause_on_que_numSubjectDetail(testID, counters);
            }
//            storeAssignment("resumed");
//            resumeTest(all_quesion, sub_dataArray, Exam_id, examResumeTime, ustidjsonObject);

            timer.cancel();
            finish();
            dialog.dismiss();
        });
        resume_cancle.setOnClickListener(v -> {
            dialog.dismiss();
            binding.llPay1.setVisibility(View.GONE);
            binding.llPay.setVisibility(View.VISIBLE);
        });
        dialog.show();
    }

    public void updateTimerSubjectDetailTable(int examId, String timer) {
        Cursor res = db.getSubjectDetailsInfoData(examId);
        res.moveToFirst();
        @SuppressLint("Range") String stoptime = res.getString(res.getColumnIndex("test_subject_stope_time"));
        @SuppressLint("Range") int test_id = res.getInt(res.getColumnIndex("test_id"));
        Boolean checkupdatedata = db.updateTimerSubjectDetailTable(examId, timer);
    }

    public void update_test_pause_on_que_numSubjectDetail(int examId, int qid) {
        Cursor res = db.getSubjectDetailsInfoData(examId);
        res.moveToFirst();
        @SuppressLint("Range") String stoptime = res.getString(res.getColumnIndex("test_subject_stope_time"));
        @SuppressLint("Range") int test_id = res.getInt(res.getColumnIndex("test_id"));
        Boolean checkupdatedata = db.updateQuestionNumberSubjectDetailTable(examId, qid);
    }

    public static class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.RecyclerviewAdapter> {
        Context context;
        List<QuestionModel> questionModels;
        List<UstQuesAnswerModel> ustQuesAnswerModels;
        int examType;
        HelperFile examHelper;
        SqlDatabase db;

        public ExamListAdapter(Context context, List<QuestionModel> questionModels, List<UstQuesAnswerModel> ustQuesAnswerModels, int examType) {
            this.context = context;
            this.questionModels = questionModels;
            this.examType = examType;
            this.ustQuesAnswerModels = ustQuesAnswerModels;
        }

        @NonNull
        @Override
        public RecyclerviewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.exam_list_layout, parent, false);
            return new RecyclerviewAdapter(view);
        }

        @SuppressLint("WrongConstant")
        @Override
        public void onBindViewHolder(@NonNull RecyclerviewAdapter holder, int position) {
            db = new SqlDatabase(context);
            examHelper = new HelperFile(db, context);
            QuestionModel questionTable = questionModels.get(position);
            UstQuesAnswerModel ustquestionTable = ustQuesAnswerModels.get(position);
            int finalPosition1 = position;


            if (examType == 0 || examType == 1) {
                if (ustquestionTable.ustqa_review == Constant.REVIEW) {
                    holder.exam_ques.setBackgroundResource(R.drawable.review_cricle);
                    holder.exam_ques.setTextColor(Color.WHITE);
                } else {
                    if (ustquestionTable.ustqa_status == Constant.ANSWERED) {
                        holder.exam_ques.setBackgroundResource(R.drawable.answered_circle);
                        holder.exam_ques.setTextColor(Color.WHITE);
                    }
                /*else if (ustquestionTable.ustqa_review == Constant.REVIEW) {
                    holder.exam_ques.setBackgroundResource(R.drawable.review_cricle);
                    holder.exam_ques.setTextColor(Color.WHITE);
                }*/
                    else if (ustquestionTable.ustqa_status == Constant.UNANSWERED) {
                        holder.exam_ques.setBackgroundResource(R.drawable.not_answered_circle);
                        holder.exam_ques.setTextColor(Color.WHITE);
                    } else if (ustquestionTable.ustqa_status == Constant.NOT_VISITED) {
                        holder.exam_ques.setBackgroundResource(R.drawable.grey_border);
                        holder.exam_ques.setTextColor(Color.BLACK);
                    }
                }

            } else if (examType == 2) {

            }

            holder.exam_ques.setText(String.valueOf(questionTable.tlq_counterid));
//            Log.e("quNO", questionTable.tlq_id + "");


            holder.ll_exam_ques.setOnClickListener(v -> {
                Constant.clickedQue = questionTable.tlq_counterid;
                examHelper.ExamQuestions(String.valueOf(questionTable.que_test_id), questionTable.tlq_counterid);
//                Log.d("questiontestnumber", questionModels.get(position).tlq_counterid + "");
                clicked = questionModels.get(position).tlq_counterid;
                if (questionModels.get(position).tlq_counterid - 1 == 0) {
                    btn_previous.setVisibility(View.GONE);
                } else {
                    btn_previous.setVisibility(View.VISIBLE);
                }

                if (drawelayout.isDrawerOpen(Gravity.END)) {
                    drawelayout.closeDrawer(Gravity.END);
                } else {
                    drawelayout.openDrawer(Gravity.END);
                }
            });
        }

        @Override
        public int getItemCount() {
            return questionModels.size();
        }

        public class RecyclerviewAdapter extends RecyclerView.ViewHolder {
            TextView exam_ques;
            LinearLayout ll_exam_ques;

            public RecyclerviewAdapter(@NonNull View itemView) {
                super(itemView);
                ll_exam_ques = itemView.findViewById(R.id.ll_exam_ques);
                exam_ques = itemView.findViewById(R.id.exam_ques);
            }
        }

    }

    private void resumeTest(JSONArray all_question, JSONArray sub_data, String test_id, String timer1, JSONObject ustid) {
        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SubmitModel> call = apiInterface.resumeExamDetails(all_question, sub_data, test_id, timer1, ustid);
//        Log.d("getMyMessage", test_id + "   " + timer1 + "  " + ustid + "  " + sub_data + "   " + all_question);
//        Log.d("getMyMessage", all_question + "");
        call.enqueue(new Callback<SubmitModel>() {
            @Override
            public void onResponse(Call<SubmitModel> call, Response<SubmitModel> response) {
                pDialog.dismiss();
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getMessage().equals("ok")) {
//                            Log.e("getMyMessage", "a=" + response.body().getMessage());
                            timer.cancel();
                            finish();
                        } else {
//                            Log.e("getMyMessage", "No message");
                        }
                    } else {
//                        Log.e("getMyMessage", "b=" + response.message());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SubmitModel> call, Throwable t) {
                pDialog.dismiss();
//                Log.e("getMyMessage", "c=" + t.getMessage());
            }
        });

    }

    private void submitTest(JSONArray all_question, JSONArray sub_data, String test_id, String timer1, JSONObject ustid) {
       /* if (!pDialog.isShowing()) {
            pDialog.show();
        }*/
        alertDialogExamsubmit.show();
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SubmitModel> call = apiInterface.submitExamDetails(all_question, sub_data, test_id, timer1, ustid);
//        Log.d("getMyMessage",test_id+"   "+timer1+"  "+ ustid+"  "+ sub_data+"   "+all_question);
//        Log.d("getMyMessage", all_question + "");
        call.enqueue(new Callback<SubmitModel>() {
            @Override
            public void onResponse(Call<SubmitModel> call, Response<SubmitModel> response) {
//                pDialog.dismiss();
                alertDialogExamsubmit.dismiss();
                Constant.subCorrect = 0;
                Constant.subSkippe = 0;
                Constant.subWorng = 0;
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getMessage().equals("ok")) {
//                            Log.e("getMyMessage", "a=" + response.body().getMessage());
                            timer.cancel();
                            int testID = Integer.parseInt(test_id);
                            update_test_pause_on_que_numSubjectDetail(testID, 1);
                            Intent intent = new Intent(getApplicationContext(), ExamSubmitDone.class);
                            startActivity(intent);
                            finish();
                        } else {
//                            Log.e("getMyMessage", "No message");
                        }
                    } else {
//                        Log.e("getMyMessage", "b=" + response.message());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SubmitModel> call, Throwable t) {
                alertDialogExamsubmit.dismiss();
//                Log.e("getMyMessage", "c=" + t.getMessage());
                Constant.subCorrect = 0;
                Constant.subSkippe = 0;
                Constant.subWorng = 0;
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (examType == 0 || examType == 1) {
            Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(100);
            back_pressed_Box();
        } else {
            int testID = Integer.parseInt(Exam_id);
            if (Constant.clickedQue != 0) {
                update_test_pause_on_que_numSubjectDetail(testID, Constant.clickedQue);
                Constant.clickedQue = 0;
            } else {
                update_test_pause_on_que_numSubjectDetail(testID, counters);
            }
            super.onBackPressed();
        }
    }

    @SuppressLint("WrongConstant")
    private void back_pressed_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_back_pressed_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_cancle.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }

    private void storeAssignment(String exStatus) {
        SharedPreferences sharedPreferences = getSharedPreferences("ExamId" + Exam_id, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("examId", Exam_id);
        editor.putString("submit", exStatus);
        editor.commit();
        Constant.logPrint("assignmentpdfSaved", Exam_id);

    }

}