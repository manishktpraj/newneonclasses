package codexo.neonclasses.ui.test;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityResultBinding;
import codexo.neonclasses.databinding.ItemQuesBinding;

public class ResultActivity extends AppCompatActivity {

    private ActivityResultBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(ResultActivity.this);
        Constant.checkAdb(ResultActivity.this);
        binding.imgMenu.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(this);

            Window window = dialog.getWindow();
            window.setGravity(Gravity.RIGHT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);

            WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();
            DisplayMetrics dm = new DisplayMetrics();
            dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(dm);
            wlp.gravity = Gravity.RIGHT;
            wlp.dimAmount = 0.90f;
            dialog.getWindow().setAttributes(wlp);
            dialog.setContentView(R.layout.dialog_menu);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            dialog.show();
        });

        List<String> item = new ArrayList<>();
        item.add("image");

        RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(this, item);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(recyclerAdapter);
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemQuesBinding binding;
        Context context;
        List<String> listItem;

        public RecyclerAdapter1(Context context, List<String> listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemQuesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            String item = listItem.get(position);
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.size();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemQuesBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(ResultActivity.this);
        Constant.checkAdb(ResultActivity.this);
    }
}
