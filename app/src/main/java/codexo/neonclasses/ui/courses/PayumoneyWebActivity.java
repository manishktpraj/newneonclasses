package codexo.neonclasses.ui.courses;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import codexo.neonclasses.Constant;
import codexo.neonclasses.Login;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityPayumoneyWebBinding;
import codexo.neonclasses.databinding.ActivityWebBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.webview.WebActivity;

public class PayumoneyWebActivity extends AppCompatActivity {
    private WebView webView = null;
    String url = "";
    private ActivityPayumoneyWebBinding binding;
    ProgressBar progressBar;
    String porder_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  ActivityPayumoneyWebBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(PayumoneyWebActivity.this);
        Constant.checkAdb(PayumoneyWebActivity.this);
        ActionBar actionBar=getSupportActionBar();
        actionBar.hide();
        url =getIntent().getStringExtra("payment_url");
        porder_id =getIntent().getStringExtra("porder_id");
        webView = (WebView) findViewById(R.id.webview);
        webView.clearHistory();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView.clearFormData();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
//        Constant.logPrint(url,"urlurlurlurlurl");
        webView.loadUrl(url);
        PayumoneyWebActivity.WebViewClientImpl webViewClient = new PayumoneyWebActivity.WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);

    }

    public class WebViewClientImpl extends WebViewClient {
        private Activity activity = null;
        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {

            if (url.equals("https://neonclasses.com/webapi/orders/thankyou") ) {
                Intent intent1 = new Intent(getApplicationContext(), ThankyouActivity.class);
                activity.startActivity(intent1);
                finish();
            }


            if(url.indexOf("neonclasses.com") > -1 || url.indexOf("neonclasses.co.in") > -1 || url.indexOf("api.payu.in") > -1 ) return false;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
//            Constant.logPrint(url,"afterpayment");

            if (url.equals("https://neonclasses.com/webapi/orders/thankyou") ) {
                Intent intent1 = new Intent(getApplicationContext(), ThankyouActivity.class);
                activity.startActivity(intent1);
                finish();
            }
            return true;

        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Cancel Payment")
                .setMessage("Are you sure?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        webView.destroy();
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(PayumoneyWebActivity.this);
        Constant.checkAdb(PayumoneyWebActivity.this);
    }
}
