package codexo.neonclasses.ui.courses;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.AnalyticsApplication;
import codexo.neonclasses.Constant;
import codexo.neonclasses.Login;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCourseDetailBinding;
import codexo.neonclasses.databinding.ActivityCoursesBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.cart.CartActivity;
import codexo.neonclasses.ui.videos.StudySubcategoryActivity;
import codexo.neonclasses.ui.webview.WebActivity;

public class CourseDetailActivity extends AppCompatActivity {
    JSONObject course_detail= new JSONObject();
    ActivityCourseDetailBinding binding;
    ProgressBar progressBar;
    String url ="";
    WebView webView;
    String course_url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding = ActivityCourseDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(CourseDetailActivity.this);
        Constant.checkAdb(CourseDetailActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
     /*   AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();*/
        try {
           course_detail =new JSONObject(getIntent().getStringExtra("course_detail"));
           course_url =getIntent().getStringExtra("course_url");
            binding.txtHello.setText(course_detail.getString("sub_pack_title"));
            binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
            url = Constant.YOUR_NEW_API_URL_LIVE+"pages/packagedetail/"+course_detail.getString("sub_pack_slug");
          /*  mTracker.setScreenName("books~" + course_detail.getString("sub_pack_title"));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());*/
        } catch (JSONException e) {
        e.printStackTrace();
        }
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView = (WebView) findViewById(R.id.webview);
        webView.clearHistory();
        webView.clearFormData();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);


        webView.loadUrl(url);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);

        binding.lyrBottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(CourseDetailActivity.this, CoursePurchaseActivity.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                iLogin.putExtra("course_detail",course_detail+"");
                iLogin.putExtra("course_url",course_url+"");
                startActivity(iLogin);
            }
        });
    }

    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if(url.indexOf("neonclasses.com") > -1 ) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(CourseDetailActivity.this);
        Constant.checkAdb(CourseDetailActivity.this);
    }
}