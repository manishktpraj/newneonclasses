package codexo.neonclasses.ui.courses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCoursesBinding;
import codexo.neonclasses.databinding.BooksRowBinding;
import codexo.neonclasses.databinding.CoursesRowBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.books.BooksActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class CoursesActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    SessionManager session;
    JSONArray package_list =new JSONArray();
    String courses_url="";
    ActivityCoursesBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     ///   setContentView(R.layout.activity_courses);
        Constant.adbEnabled(CoursesActivity.this);
        Constant.checkAdb(CoursesActivity.this);
        binding = ActivityCoursesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        pDialog = Constant.getProgressBar(CoursesActivity.this);
        session = new SessionManager(CoursesActivity.this);
        courseslist();
    }
    private void courseslist() {



        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().popularcourse(session.getUserId());
        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            package_list = jsonObject.getJSONArray("results");
                            courses_url = jsonObject.getString("package_url");
                            CoursesActivity.RecyclerAdapter2 recyclerAdapter = new CoursesActivity.RecyclerAdapter2(getApplicationContext(), package_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(getApplicationContext(),"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                courseslist();
            }
        });
    }
    public class RecyclerAdapter2 extends RecyclerView.Adapter<CoursesActivity.RecyclerAdapter2.RecyclerViewHolder> {
        CoursesRowBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public CoursesActivity.RecyclerAdapter2.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = CoursesRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            CoursesActivity.RecyclerAdapter2.RecyclerViewHolder holder = new CoursesActivity.RecyclerAdapter2.RecyclerViewHolder(binding);

            return holder;
        }



        @Override
        public void onBindViewHolder(@NonNull CoursesActivity.RecyclerAdapter2.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = courses_url+item.getString("sub_pack_image");
                Constant.setImage(img,binding.image,getApplicationContext());
                binding.txtTitle.setText(item.getString("sub_pack_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));


                binding.txtDiscountedPrice.setText("₹"+item.getString("sub_pack_price"));
                binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
                binding.txtPrice.setText("₹"+item.getString("sub_actual_price"));
                binding.txtPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

                String disperc = Constant.getdiscount(item.getString("sub_actual_price"),item.getString("sub_pack_price"));
//                Constant.logPrint(disperc+"","dispercdispercdispercdispercdisperc");
                if(!disperc.equals(""))
                {
                    binding.txtDiscount.setText(disperc+"%");
                    binding.txtDiscount.setTypeface(Constant.getFontsBold(getApplicationContext()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull CoursesRowBinding itemView) {
                super(itemView.getRoot());

                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(getAdapterPosition());

                            Intent vidcategory = new Intent(getApplicationContext(), CourseDetailActivity.class);
                            vidcategory.putExtra("course_detail", item + "");
                            vidcategory.putExtra("course_url", courses_url + "");
                            startActivity(vidcategory);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(CoursesActivity.this);
        Constant.checkAdb(CoursesActivity.this);
    }
}