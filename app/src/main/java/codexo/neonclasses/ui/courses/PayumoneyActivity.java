package codexo.neonclasses.ui.courses;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;

import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.base.models.PaymentMode;
import com.payu.base.models.PaymentType;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.models.PayUCheckoutProConfig;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityPayumoneyBinding;

public class PayumoneyActivity extends AppCompatActivity {
    PayUPaymentParams payUPaymentParams;
    String hashString="";
    ActivityPayumoneyBinding binding;
    JSONObject student_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payumoney);
        Constant.adbEnabled(PayumoneyActivity.this);
        Constant.checkAdb(PayumoneyActivity.this);
//        try {
//            student_data = new JSONObject(getIntent().getStringExtra("results"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        initiatepayment();
    }

    private void paymentparam()
    {
        HashMap<String, Object> additionalParams = new HashMap<>();
        additionalParams.put(PayUCheckoutProConstants.CP_UDF1, "udf1");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF2, "udf2");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF3, "udf3");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF4, "udf4");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF5, "udf5");
        additionalParams.put(PayUCheckoutProConstants.CP_MERCHANT_ACCESS_KEY,"ytPUnaxK");


        PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount("100");
        builder.setIsProduction(false);
        builder.setProductInfo("SSC CGL COURSE");
        builder.setKey("ytPUnaxK");
        builder.setPhone("7610022611");
        builder.setTransactionId("10001");
        builder.setFirstName("Manish Garg");
        builder.setEmail("manish.h99group@gmail.com");
        builder.setSurl("https://neonclasses.com/contact");
        builder.setFurl("https://neonclasses.com/blog");
        builder.setUserCredential("7610022611");
        builder.setAdditionalParams(additionalParams); //Optional, can contain any additional PG params
        payUPaymentParams = builder.build();
    }
    private void initiatepayment()
    {
        paymentparam();
        PayUCheckoutProConfig payUCheckoutProConfig = new PayUCheckoutProConfig();
        payUCheckoutProConfig.setMerchantName("Neon Classes");
        payUCheckoutProConfig.setMerchantLogo(R.drawable.logo);
        payUCheckoutProConfig.setMerchantSmsPermission(true);
        payUCheckoutProConfig.setAutoApprove(true);
        ArrayList<PaymentMode> checkoutOrderList = new ArrayList<>();
        checkoutOrderList.add(new PaymentMode(PaymentType.UPI, PayUCheckoutProConstants.CP_GOOGLE_PAY));
        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PHONEPE));
        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PAYTM));
        checkoutOrderList.add(new PaymentMode(PaymentType.CARD));
        checkoutOrderList.add(new PaymentMode(PaymentType.UPI));
        payUCheckoutProConfig.setPaymentModesOrder (checkoutOrderList);

        PayUCheckoutPro.open(
                this,
                payUPaymentParams,
                payUCheckoutProConfig,
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                    }
                    @Override
                    public void onPaymentFailure(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                    }
                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                    }
                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                    }
                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }
                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Do not generate hash from local, it needs to be calculated from server side only. Here, hashString contains hash created from your server side.
                            String hash = hashString;
                            HashMap<String, String> dataMap = new HashMap<>();
                            dataMap.put(hashName, hash);
                            hashGenerationListener.onHashGenerated(dataMap);
                        }
                    }
                }
        );


    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(PayumoneyActivity.this);
        Constant.checkAdb(PayumoneyActivity.this);
    }
}