package codexo.neonclasses.ui.courses;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;

public class ThankyouActivity extends AppCompatActivity {
TextView welcomtoneon,tv_welcomtoneon;
ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        Constant.adbEnabled(ThankyouActivity.this);
        Constant.checkAdb(ThankyouActivity.this);
        ActionBar actionBar=getSupportActionBar();
        actionBar.hide();
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> {
            Intent intent=new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        });
        welcomtoneon=findViewById(R.id.welcomtoneon);
        tv_welcomtoneon=findViewById(R.id.tv_welcomtoneon);
        tv_welcomtoneon.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        welcomtoneon.setTypeface(Constant.getFontsBold(getApplicationContext()));
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finishAffinity();

     ////   finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(ThankyouActivity.this);
        Constant.checkAdb(ThankyouActivity.this);
    }
}