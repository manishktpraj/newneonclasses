package codexo.neonclasses.ui.courses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.Login;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityCourseDetailBinding;
import codexo.neonclasses.databinding.ActivityCoursePurchaseBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.offer.OfferActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class CoursePurchaseActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    ActivityCoursePurchaseBinding binding;
    JSONObject course_detail= new JSONObject();
    String courses_url="";
     String grand_total_amount,user_id,refferal_off,refferal_user_id,coupon_id,sub_pack_id,discount_amount,redeem_amount;
    SessionManager session;
    String finalAmount = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constant.adbEnabled(CoursePurchaseActivity.this);
        Constant.checkAdb(CoursePurchaseActivity.this);
        binding = ActivityCoursePurchaseBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        pDialog = Constant.getProgressBar(CoursePurchaseActivity.this);
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.amountAfterCoupon = "";
                Constant.couponDiscount = "";
                finish();
            }
        });
        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        try {
            course_detail =new JSONObject(getIntent().getStringExtra("course_detail"));
             binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
//             Log.d("GopalGopal", course_detail+"");
            courses_url =getIntent().getStringExtra("course_url");
//            Constant.logPrint(courses_url+course_detail.getString("sub_pack_image"),"");
            String img = courses_url+course_detail.getString("sub_pack_image");

            Constant.setImage(img,binding.image,getApplicationContext());
            binding.txtTitle.setText(course_detail.getString("sub_pack_title"));
            binding.txtTitle.setTypeface(Constant.getFontsBold(getApplicationContext()));


            binding.txtDiscountedPrice.setText("₹"+course_detail.getString("sub_pack_price"));
            binding.txtSubtotalPrice.setText("₹"+course_detail.getString("sub_actual_price"));
            binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
            binding.txtPrice.setText("₹"+course_detail.getString("sub_actual_price"));
            binding.txtPrice.setTypeface(Constant.getFontsBold(getApplicationContext()));
            binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            String disperc = Constant.getdiscount(course_detail.getString("sub_actual_price"),course_detail.getString("sub_pack_price"));
//            Constant.logPrint(disperc+"","dispercdispercdispercdispercdisperc");
            if(!disperc.equals(""))
            {
                binding.txtDiscount.setText(disperc+"%");
                //binding.txtDiscountPrice.setText(disperc+"%");
                binding.txtDiscount.setTypeface(Constant.getFontsBold(getApplicationContext()));
            }

            String dispercPrice = Constant.getdiscountPrice(course_detail.getString("sub_actual_price"),course_detail.getString("sub_pack_price"));

            if (!dispercPrice.equals("")){
                binding.txtDiscountPrice.setText(dispercPrice);
            }
            session =new SessionManager(CoursePurchaseActivity.this);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        binding.txtPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    grand_total_amount=course_detail.getString("sub_pack_price");
                    grand_total_amount=finalAmount;
                    user_id=session.getUserId();
                    sub_pack_id=course_detail.getString("sub_pack_id");
                    coupon_id="18";
                    refferal_user_id="";
                    refferal_off="";
                    discount_amount="";
                    redeem_amount="";
                    web_Open();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Intent vidcategory = new Intent(getApplicationContext(), PayumoneyWebActivity.class);
//               vidcategory.putExtra("grand_total_amount", String.valueOf(course_detail));
//                vidcategory.putExtra("user_id", SessionManager.USER_ID);
//                vidcategory.putExtra("sub_pack_id", "16");
//                vidcategory.putExtra("coupon_id", "6");
//                vidcategory.putExtra("refferal_user_id", "2");
//                vidcategory.putExtra("refferal_off", "10");
//                 startActivity(vidcategory);
            }
        });

        binding.applyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(getApplicationContext(), OfferActivity.class);
                    intent.putExtra("type", "cart");
                    intent.putExtra("amount", course_detail.getString("sub_pack_price"));
                    intent.putExtra("apply_on", "2");
                    intent.putExtra("apply_on_id", course_detail.getString("sub_pack_id"));
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        calculation();
        binding.removePromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.amountAfterCoupon = "";
                Constant.couponDiscount = "";
                calculation();
            }
        });
    }

    public void calculation(){
        if (!Constant.amountAfterCoupon.equals("")){
            binding.removePromo.setVisibility(View.VISIBLE);
            binding.applyPromo.setVisibility(View.GONE);
            binding.couponRL.setVisibility(View.VISIBLE);
            binding.orderPrice.setText("₹" + Constant.amountAfterCoupon);
            binding.txtPayablePrice.setText("₹" + Constant.amountAfterCoupon);
            binding.tvCouponDis.setText("₹" + Constant.couponDiscount);
            finalAmount = Constant.amountAfterCoupon;
        }else {
            try {
                binding.removePromo.setVisibility(View.GONE);
                binding.applyPromo.setVisibility(View.VISIBLE);
                binding.couponRL.setVisibility(View.GONE);
                binding.orderPrice.setText("₹" + course_detail.getString("sub_pack_price"));
                binding.txtPayablePrice.setText("₹" + course_detail.getString("sub_pack_price"));
                finalAmount = course_detail.getString("sub_pack_price");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void web_Open() {
        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getweburl(grand_total_amount,user_id,refferal_off,refferal_user_id,coupon_id,sub_pack_id,discount_amount,redeem_amount);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {
                pDialog.dismiss();
                String response = response_string.body();

//                Constant.logPrint("response", response);
//             Log.e("loading response",response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {

                            Constant.skip="yes";
                            Constant.fragments="main";

                            Toast.makeText(getApplicationContext(), "web successfully", Toast.LENGTH_SHORT).show();

                          String payment_url=jsonObject.getString("payment_url");
                            String porder_id=jsonObject.getString("porder_id");

                            Intent vidcategory = new Intent(getApplicationContext(), PayumoneyWebActivity.class);
                            vidcategory.putExtra("payment_url", payment_url);
                             vidcategory.putExtra("porder_id",porder_id);

                            vidcategory.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            vidcategory.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                          startActivity(vidcategory);
                            Constant.amountAfterCoupon = "";
                            Constant.couponDiscount = "";
                          finish();
                        } else {
                            Constant.setToast(CoursePurchaseActivity.this,"Loading failed!");

                           /* new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("Login failed! Please enter correct username or password.")
                                    .setNegativeButton("ok", null).show();*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                web_Open();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constant.amountAfterCoupon = "";
        Constant.couponDiscount = "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        calculation();
        Constant.adbEnabled(CoursePurchaseActivity.this);
        Constant.checkAdb(CoursePurchaseActivity.this);
    }
}