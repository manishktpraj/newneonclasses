/*
 * Copyright (c) Manish Garg
 * Android Developer
 * manish.h99group@gmail.com
 * 7610022611
 *
 *
 */

package codexo.neonclasses.ui.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity
public class DownloadFile {
    @NotNull
    @PrimaryKey(autoGenerate = false)
    private String id;
    @ColumnInfo
    private String downloadId;
    @ColumnInfo
    private String fileName;
    @ColumnInfo
    private String title;
    @ColumnInfo
    private String filepath;
    @ColumnInfo
    private String videoUrlOffline;
    @ColumnInfo
    private String filetype;
    @ColumnInfo
    private String imageUrlOffline;
    @ColumnInfo
    private String thumbPath;
    @ColumnInfo
    private String downloadStatus;
    @ColumnInfo
    private String percentage;
    @ColumnInfo
    private String timeremain;
    @ColumnInfo
    private String downloadspeed;
    @ColumnInfo
    private boolean isDownloadComplete=false;
    @ColumnInfo
    private long currentBytes=0;
    @ColumnInfo
    private long totalBytes=0;
    @ColumnInfo
    private long startTime=0;
    @ColumnInfo
    private long endTime=0;
    @NotNull
    public String getId() {
        return id;
    }

    public String getVideoUrlOffline() {
        return videoUrlOffline;
    }

    public void setVideoUrlOffline(String videoUrlOffline) {
        this.videoUrlOffline = videoUrlOffline;
    }

    public String getImageUrlOffline() {
        return imageUrlOffline;
    }

    public void setImageUrlOffline(String imageUrlOffline) {
        this.imageUrlOffline = imageUrlOffline;
    }
    public String getPercentage() {
        return percentage;
    }


    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }



    public String getFiletype() {
        return filetype;
    }
    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }


    public String getDownloadspeed() {
        return downloadspeed;
    }

    public void setDownloadspeed(String downloadspeed) {
        this.downloadspeed = downloadspeed;
    }


    public String getTimeremain() {
        return timeremain;
    }

    public void setTimeremain(String timeremain) {
        this.timeremain = timeremain;
    }

    public Long getStartTime() {
        return startTime;
    }
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    public Long getEndTime() {
        return endTime;
    }
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }


    public String getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(String downloadId) {
        this.downloadId = downloadId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    public String getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(String downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public boolean isDownloadComplete() {
        return isDownloadComplete;
    }

    public void setDownloadComplete(boolean downloadComplete) {
        isDownloadComplete = downloadComplete;
    }

    public long getCurrentBytes() {
        return currentBytes;
    }

    public void setCurrentBytes(long currentBytes) {
        this.currentBytes = currentBytes;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }
}
