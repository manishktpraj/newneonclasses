package codexo.neonclasses.ui.database;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityAllDownloadsBinding;
import codexo.neonclasses.databinding.ActivityDownloadsBinding;
import codexo.neonclasses.databinding.ItemDownloadedMaterialBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.DownloadsActivity;
import codexo.neonclasses.ui.download.StudyMaterialDownloadsFragment;
import codexo.neonclasses.ui.download.VideoDownloadsFragment;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.player.OfflineVideoPlayerActivity;

import static android.view.View.GONE;

public class AllDownloadsActivity extends AppCompatActivity {
    ActivityAllDownloadsBinding binding;
    List<File> listItem =new ArrayList<>();;
     List<File> item = new ArrayList<>();
    RecyclerAdapter1 recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAllDownloadsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setText("My Downloads");
        if(DownloadSupport.isStoragePermissionGranted(AllDownloadsActivity.this)){
            item=DownloadSupport.getListFiles(new File(DownloadSupport.getoldpath(getApplicationContext(),new SessionManager(getApplicationContext()))));
             if(item !=null && item.size()>0){
                 listItem =item;
//                 Constant.logPrint(listItem+"","listItemlistItemlistItemlistItem");
                binding.emptyTxt.setVisibility(GONE);
                recyclerAdapter = new RecyclerAdapter1(getApplicationContext(), item);
                binding.recycler.setAdapter(recyclerAdapter);

            }else{
                binding.emptyTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemDownloadedMaterialBinding binding;
        Context context;

        public RecyclerAdapter1(Context context,  List<File> listItem) {
            this.context = context;
         }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemDownloadedMaterialBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            holder.setIsRecyclable(false);
            holder.binding.speeder.setVisibility(GONE);
            holder.binding.progressBar.setVisibility(GONE);
            holder.binding.timer.setVisibility(GONE);
            holder.binding.viewfile.setImageResource(R.drawable.eye);
            holder.binding.viewfile.setVisibility(View.VISIBLE);
            File itemd =listItem.get(position);

            holder.binding.txtTitle.setText(itemd.getName());

            holder.binding.viewfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent videoplay = new Intent(getApplicationContext(), OfflineVideoPlayerActivity.class);
//                    Constant.logPrint("VIDEO_SAMPLEVIDEO_SAMPLE" ,"VIDEO_SAMPLE"+itemd.getAbsolutePath());

                    videoplay.putExtra("video_detail",itemd.getAbsolutePath());
                    startActivity(videoplay);
                }
            });

        }

        @Override
        public int getItemCount() {
            return listItem.size();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
            ItemDownloadedMaterialBinding binding;
            public RecyclerViewHolder(@NonNull ItemDownloadedMaterialBinding binding) {
                super(binding.getRoot());
                this.binding=binding;




            }
        }
    }

}