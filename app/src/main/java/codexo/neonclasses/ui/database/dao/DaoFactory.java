package codexo.neonclasses.ui.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import codexo.neonclasses.ui.database.entities.DownloadFile;


@Dao
public interface DaoFactory {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(DownloadFile table);

    @Query("Select * From DownloadFile WHERE  filetype= :type")
    LiveData<List<DownloadFile>> getAllDownloads(String type);

    @Query("Select * From DownloadFile WHERE id=:videoId ")
    public DownloadFile getDownloadFile(String videoId);

    @Query("UPDATE DownloadFile SET downloadStatus=:downloadStatus , isDownloadComplete=:isDownloads, starttime=:starttime WHERE downloadId = :downloadID")
    public void Update(String downloadStatus, boolean isDownloads, String downloadID,Long starttime);

    @Query("UPDATE DownloadFile SET currentBytes=:currentBytes , totalBytes=:totalBytes, percentage=:percent ,timeremain=:timed ,downloadspeed=:downloadspeed , downloadStatus=:status, endTime=:endTime WHERE downloadId = :downloadID AND filetype = :type")
    public void Updatewithper(long currentBytes, long totalBytes, String downloadID ,String percent,String timed,String downloadspeed,String status,String type,Long endTime);

   /* @Query("UPDATE DownloadFile SET currentBytes=:currentBytes , totalBytes=:totalBytes WHERE downloadId = :downloadID")
    public void Update(long currentBytes, long totalBytes, String downloadID);*/

    @Query("UPDATE DownloadFile SET videoUrlOffline=:videoUrl  WHERE downloadId = :downloadId")
    void Update(String videoUrl, String downloadId);

    @Query("UPDATE DownloadFile SET downloadId=:downloadId  WHERE id = :videoId")
    void UpdateDownload(String videoId, String downloadId);

    @Query("Delete from DownloadFile Where id = :videoId AND filetype=:type")
    void Delete (String videoId,String type);

    @Query("Select * From DownloadFile WHERE id=:id AND filetype=:type")
    LiveData<DownloadFile> getAllDownload(String id,String type);
}
