/*
 * Copyright (c)
 *  Ishant Sharma
 * Android Developer
 * ishant.sharma1947@gmail.com
 */

package codexo.neonclasses.ui.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.sqlite.db.SupportSQLiteDatabase;

import codexo.neonclasses.ui.database.dao.DaoFactory;
import codexo.neonclasses.ui.database.entities.DownloadFile;


@Database(entities = {DownloadFile.class}, version = 1, exportSchema = true)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    private static RoomDatabase instance;
    public abstract DaoFactory dao();
    private static Callback roomcallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

        }
    };

    public static synchronized RoomDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), RoomDatabase.class, "Neonclasses")
                    .setJournalMode(JournalMode.AUTOMATIC).allowMainThreadQueries()
                    .addCallback(roomcallback).fallbackToDestructiveMigration().build();
        }
        return instance;
    }

}