package codexo.neonclasses.ui.news;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityNewsBinding;
import codexo.neonclasses.databinding.ItemNewsBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.jwplayer.PlayerJW;
import codexo.neonclasses.ui.study.StudyMaterialFragment;
import codexo.neonclasses.ui.webview.WebActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class NewsActivity extends AppCompatActivity {

    private ActivityNewsBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray news_list =new JSONArray();
    String news_url ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNewsBinding.inflate(getLayoutInflater());
        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));
        setContentView(binding.getRoot());
        Constant.adbEnabled(NewsActivity.this);
        Constant.checkAdb(NewsActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());


        session = new SessionManager(getApplicationContext());
        pDialog = Constant.getProgressBar(NewsActivity.this);
        getnews();

    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemNewsBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                binding.txtDes.setText(item.getString("blog_title"));
                binding.txtDes.setTypeface(Constant.getsemiFonts(getApplicationContext()));


                binding.txtDate.setText(item.getString("blog_date"));
                binding.txtDate.setTypeface(Constant.getFonts(getApplicationContext()));


                String img = news_url+item.getString("blog_image");
                Constant.setImage(img,binding.image,getApplicationContext());

                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ///     BLOGDETAIL

                        try {
                            Intent idf =new Intent(NewsActivity.this, WebActivity.class);
                            idf.putExtra("title",item.getString("blog_title"));
                            idf.putExtra("url",Constant.BLOGDETAIL+""+item.getString("blog_slug"));
                            startActivity(idf);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemNewsBinding itemView) {
                super(itemView.getRoot());



            }
        }
    }


    private void getnews() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getnewsserver(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {
                            news_list = jsonObject.getJSONArray("results");
                            news_url = jsonObject.getString("blog_url");

                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(NewsActivity.this, news_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(NewsActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);




                        } else {
                            Constant.setToast(getApplicationContext(),notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getnews();
            }
        });
    }
    @Override
    public void onBackPressed(){
         this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(NewsActivity.this);
        Constant.checkAdb(NewsActivity.this);
    }
}
