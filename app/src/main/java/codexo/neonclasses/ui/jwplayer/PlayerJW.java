package codexo.neonclasses.ui.jwplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityPlayerJwBinding;
import codexo.neonclasses.ui.help.TicketHistoryActivity;

public class PlayerJW extends AppCompatActivity {
    ActivityPlayerJwBinding jwBinding;
   // private JWPlayer mPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jwBinding = ActivityPlayerJwBinding.inflate(getLayoutInflater());
        setContentView(jwBinding.getRoot());
        Constant.adbEnabled(PlayerJW.this);
        Constant.checkAdb(PlayerJW.this);
       /* new LicenseUtil().setLicenseKey(this, "bnO5H6T3vaTLceTLX62sXywiT+MjGniNtZFBIp5ybqXW7Nvwwkg8pA0HaOo+0SWE");

        mPlayer = jwBinding.jwplayer.getPlayer();



        PlaylistItem playlistItem = new PlaylistItem.Builder()
                .file("https://content.jwplatform.com/manifests/1sc0kL2N.m3u8")
                .build();
        List<PlaylistItem> playlist = new ArrayList<>();
        playlist.add(playlistItem);
        PlayerConfig config = new PlayerConfig.Builder()
                .playlist(playlist)
                .autostart(true)
                .build();

        mPlayer.setup(config);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(PlayerJW.this);
        Constant.checkAdb(PlayerJW.this);
    }
}