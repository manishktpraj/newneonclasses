package codexo.neonclasses.ui.purchase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import codexo.neonclasses.databinding.ActivityMyPurchaseDetailBinding;

public class MyPurchaseDetailActivity extends AppCompatActivity {
    ActivityMyPurchaseDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMyPurchaseDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }
}