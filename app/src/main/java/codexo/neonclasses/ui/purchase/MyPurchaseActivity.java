package codexo.neonclasses.ui.purchase;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityMyPurchaseBinding;
import codexo.neonclasses.databinding.ItemMyPurchaseBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.profile.MyProfileActivity;
import codexo.neonclasses.ui.videos.StudyMaterialTabFragment;
import codexo.neonclasses.ui.videos.VideoTabFragment;
import codexo.neonclasses.ui.videos.VideosFragment;
import retrofit2.Call;
import retrofit2.Callback;

public class MyPurchaseActivity extends AppCompatActivity {

    private ActivityMyPurchaseBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray mypurchaselist =new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMyPurchaseBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(MyPurchaseActivity.this);
        Constant.checkAdb(MyPurchaseActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setTypeface(Constant.getFontsBold(MyPurchaseActivity.this));
        session = new SessionManager(MyPurchaseActivity.this);
        pDialog = Constant.getProgressBar(MyPurchaseActivity.this);

        this.getpurchase();

    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemMyPurchaseBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemMyPurchaseBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            if(position==0)
            {
                binding.topspace.setVisibility(View.VISIBLE);
            }else{
                binding.topspace.setVisibility(View.GONE);
            }
            try {
                JSONObject item = listItem.getJSONObject(position);
                 binding.txtOrderId.setText("Order ID : "+item.getString("trans_id"));
                binding.txtDateEx.setVisibility(View.GONE);
                if(!item.getString("trans_datetime").equals("null")) {
                    String replaced = item.getString("trans_datetime").replaceAll("T"," ");
                    String replacedd = replaced.replace("+05:30","");
                    String[] replaceddd = replacedd.split(" ");
//                    Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replaceddd[0]);
                    binding.txtDate.setText("Date : "+replaceddd[0]);


                    String replaced_d = item.getString("trans_datetime").replaceAll("T"," ");
                    String replacedd_d = replaced_d.replace("+05:30","");
                    String[] replacedddd = replacedd_d.split(" ");
//                    Constant.logPrint("replacedreplacedreplacedreplacedreplacedreplaced",replacedddd[0]);
                    binding.txtDateEx.setText("Expiry Date : "+replacedddd[0]);


                }else{
                    binding.txtDate.setText("");
                }
                binding.txtDes.setText("");
                binding.txtDes.setVisibility(View.GONE);
                binding.txtPrice.setText("₹"+item.getString("trans_amt"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemMyPurchaseBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    private void getpurchase() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getpurchase(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");

                        if (message.equals("ok")) {
                            mypurchaselist = jsonObject.getJSONArray("results");
                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(MyPurchaseActivity.this, mypurchaselist);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(MyPurchaseActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);

                        } else {
                            Constant.setToast(MyPurchaseActivity.this,"Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getpurchase();
            }
        });
    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(MyPurchaseActivity.this);
        Constant.checkAdb(MyPurchaseActivity.this);
    }
}
