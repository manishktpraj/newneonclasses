package codexo.neonclasses.ui.notification;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityNotificationBinding;
import codexo.neonclasses.databinding.ItemNotificationBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.news.NewsActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class NotificationActivity extends AppCompatActivity {

    private ActivityNotificationBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray notification_list =new JSONArray();
    String notification_url ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNotificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Constant.adbEnabled(NotificationActivity.this);
        Constant.checkAdb(NotificationActivity.this);

        binding.txtHello.setTypeface(Constant.getFontsBold(getApplicationContext()));

        binding.imgBack.setOnClickListener(view -> finish());


        session = new SessionManager(getApplicationContext());
        pDialog = Constant.getProgressBar(NotificationActivity.this);
        getnotification();

    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemNotificationBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemNotificationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);

                 String vid_url = item.getString("message_image");
                Constant.setImage(vid_url,binding.image,getApplicationContext());
                binding.txtTitle.setText(item.getString("message_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(NotificationActivity.this));
                binding.txtDes.setText(item.getString("message_text"));
                binding.txtDes.setTypeface(Constant.getFontsBold(NotificationActivity.this));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemNotificationBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    private void getnotification() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getnotification(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {
                            notification_list = jsonObject.getJSONArray("results");

                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(NotificationActivity.this, notification_list);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);




                        } else {
                            Constant.setToast(getApplicationContext(),notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getnotification();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(NotificationActivity.this);
        Constant.checkAdb(NotificationActivity.this);
    }
}
