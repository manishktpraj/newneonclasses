package codexo.neonclasses.ui.study;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentStudyBinding;
import codexo.neonclasses.databinding.ItemStudyMaterialBinding;
import codexo.neonclasses.service.DownloadableServices;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.service.action.MultiDownload;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.cart.CartActivity;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.DownloadStatus;
import codexo.neonclasses.ui.download.DownloadsActivity;
import codexo.neonclasses.ui.download.YourService;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.home.HomeViewModel;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

import static codexo.neonclasses.Intro.MULTIPLE_PERMISSIONS;
import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class StudyMaterialFragment extends Fragment {
    String URL = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
    private HomeViewModel homeViewModel;
    private FragmentStudyBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray study_list = new JSONArray();
    JSONArray study_list_org = new JSONArray();
    JSONObject study_obj = new JSONObject();
    String study_url = "", titleItem="";
    private BroadcastReceiver mMessageReceiver;
    String[] permissions = new String[]{
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private MultiDownload mydownloads;
    private DownloadableServices mgr = null;
    private IntentFilter filter;
    RecyclerAdapter1 recyclerAdapter;
    private DatabaseRepository repository;
    Context context;
    private final OkHttpClient nclient = new OkHttpClient();
    private long startTime;
    private long endTime;
    private long fileSize;
    int kilobytePerSec = 0;
    double speed_data;
    private static int SPLASH_TIME_OUT = 1000;
    JSONArray jsonArray = new JSONArray();

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().registerReceiver(mMessageReceiver, filter);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentStudyBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        filter = new IntentFilter("DownloadedPdf");
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissions();
        }
        repository = new DatabaseRepository(getContext());
        session = new SessionManager(getContext());
        mydownloads = new MultiDownload(requireContext(), session);
        pDialog = Constant.getProgressBar(getContext());
        context = getContext();

        binding.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchLayout.setVisibility(View.VISIBLE);
                binding.titleLayout.setVisibility(View.GONE);
                //binding.recyclerStudy.setVisibility(View.GONE);

                binding.search.setEnabled(true);
                binding.search.setTextIsSelectable(true);
                binding.search.setFocusable(true);
                binding.search.setFocusableInTouchMode(true);
                binding.search.requestFocus();

 Constant.showkeyboard(getContext(),binding.search);

            }
        });
        binding.imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchLayout.setVisibility(View.GONE);
                binding.titleLayout.setVisibility(View.VISIBLE);
                //binding.recyclerStudy.setVisibility(View.VISIBLE);
                binding.search.getText().clear();
                Constant.hideKeyBoard(getContext(),binding.search);
            }
        });


        //adding a TextChangedListener
        //to call a method whenever there is some change on the EditText
        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if(editable.toString().length()>=3)
                {
                    filter(editable.toString());

                }else if(editable.toString().length()<=0){
                    study_list =new JSONArray();
                    study_list = study_list_org;
                    recyclerAdapter.notifyDataSetChanged();

                }
            }
        });

        getfreestudymaterial();
        binding.imgCart.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), CartActivity.class));
        });
        return root;
    }


    private void filter(String text) {

        JSONArray jsonarray = new JSONArray();

        for (int i = 0; i <= study_list_org.length(); i++) {

            try {
                JSONObject newJsonObject = study_list_org.getJSONObject(i);
                String courseTitle = newJsonObject.getString("pd_title");

                if (courseTitle.toLowerCase().contains(text.toLowerCase())) {

                    jsonarray.put(newJsonObject);
                 //   recyclerAdapter.filterList(jsonArray);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        study_list =new JSONArray();
        study_list = jsonarray;
        recyclerAdapter.notifyDataSetChanged();

    }


    @Override
    public void onStop() {
        super.onStop();
        ///  requireActivity().unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {

        ItemStudyMaterialBinding binding;
        Context context;
        JSONArray listItem;
        List<File> fileItem = new ArrayList<>();
        JSONArray jsonArray;

        public RecyclerAdapter1(Context context) {
            this.context = context;
         }

        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemStudyMaterialBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);
            return holder;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = study_list.getJSONObject(position);
                String img = study_url + item.getString("pd_image");
                Constant.setImage(img, binding.pdfImage, getContext());
                binding.txtTitle.setText(item.getString("pd_title"));
                titleItem = item.getString("pd_title");
                String fileName = item.getString("pd_title");


                String filePath = DownloadSupport.getPdfDirectory(getContext(), new SessionManager(context)) + "/" + fileName + ".pdf";
                File videofile = new File(filePath);


                if (videofile.exists()) {
                    binding.txtDownload.setText("View");
                    binding.txtDownload.setBackground(getResources().getDrawable(R.drawable.corner_button));

                } else {
                    binding.txtDownload.setText("Download");
                    binding.txtDownload.setBackground(getResources().getDrawable(R.drawable.corner_gray));
                }


                binding.txtDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (videofile.exists()) {
//                            Constant.logPrint(videofile.getAbsolutePath(), "videofile.getAbsolutePath()");
                            startActivity(new Intent(context, PdfViewer.class).putExtra("file", videofile.getAbsolutePath()));
                        } else {
                            customalert(item);
                        }
                    }
                });



       /*         if (DownloadSupport.isStoragePermissionGranted(requireActivity())) {
                    fileItem = DownloadSupport.getListFiles(new File(DownloadSupport.getPdfDirectory(requireContext(), new SessionManager(context))));
                }
                if (fileItem != null && fileItem.size() > 0) {
                    for (int i = 0; i < fileItem.size(); i++) {
                        String name = fileItem.get(i).getName();
                        if (name.equalsIgnoreCase(item.getString("pd_title") + ".pdf")) {
                            binding.txtDownload.setText("View");

                            int finalI = i;
                            binding.txtDownload.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(context, PdfViewer.class).putExtra("file", fileItem.get(finalI).getAbsolutePath()));
                                }
                            });
                            break;
                        } else {
                            binding.txtDownload.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    try {
                                        JSONObject item = listItem.getJSONObject(position);
                                        String pdfurl = item.getString("pd_file");
                                        URL = study_url + item.getString("pd_file");

                                        Constant.logPrint(URL, "URLURLURLURLURL");

                                        if (!pdfurl.equals("")) {

                                            Intent intent = new Intent(context, DownloadableServices.class);
                                            // add infos for the service which file to download and where to store
                                            intent.putExtra(DownloadableServices.FILENAME, item.getString("pd_title"));

                                            intent.putExtra(DownloadableServices.urlpath, URL);
                                            intent.putExtra(DownloadableServices.TYPE, "pdf");
                                            intent.putExtra(DownloadableServices.ID, item.getString("pd_id"));
                                            //  startActivity(intent);
                                            getActivity().startService(intent);

                                            Toast.makeText(requireContext(), "Downloading....", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Constant.setToast(getContext(), "Donwload Not Available");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });


                        }
                    }
                } else {
                    binding.txtDownload.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                JSONObject item = listItem.getJSONObject(position);
                                String pdfurl = item.getString("pd_file");
                                URL = study_url + item.getString("pd_file");

                                Constant.logPrint(URL, "URLURLURLURLURL");
                                if (!pdfurl.equals("")) {
                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            mydownloads = new MultiDownload(requireContext(), session);
                                            try {
                                                mydownloads.StartDownload(URL, item.getString("pd_title"), "pdf");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                  */
                /*  Intent intent = new Intent(context, DownloadableServices.class);
                                    // add infos for the service which file to download and where to store
                                    intent.putExtra(DownloadableServices.FILENAME, item.getString("pd_title"));

                                     intent.putExtra(DownloadableServices.urlpath, URL);
                                    intent.putExtra(DownloadableServices.TYPE, "pdf");
                                    intent.putExtra(DownloadableServices.ID, item.getString("pd_id"));
                                    //  startActivity(intent);
                                    getActivity().startService(intent);*/
                /*
                                    Toast.makeText(requireContext(), "Downloading....", Toast.LENGTH_SHORT).show();
                                } else {
                                    Constant.setToast(getContext(), "Donwload Not Available");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                }*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);


        }

        @Override
        public int getItemCount() {
            return study_list.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemStudyMaterialBinding itemView) {
                super(itemView.getRoot());
                binding = itemView;
            }
        }


    }

    private void getfreestudymaterial() {


        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getfreestudymaterial(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {
                            study_list = jsonObject.getJSONArray("results");
                            study_url = jsonObject.getString("study_url");
                            study_list_org =  jsonObject.getJSONArray("results");
                            recyclerAdapter = new RecyclerAdapter1(getContext());
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recyclerStudy.setLayoutManager(layoutManager);
                            binding.recyclerStudy.setAdapter(recyclerAdapter);
                        } else {
                            ////Constant.setToast(getContext(), notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getfreestudymaterial();
            }
        });
    }

    private void checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
        }
    }


    public void customalert(JSONObject item) {

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
        bottomSheetDialog.setContentView(R.layout.download_layout);
        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
        TextView txt_title = bottomSheetDialog.findViewById(R.id.txt_title);
        try {
            txt_title.setText(item.getString("pd_title"));
            setindatabase(item, bottomSheetDialog);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        tv_go_inside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canceldownload(bottomSheetDialog, item);

            }
        });


        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();
    }

    public void setindatabase(JSONObject item, BottomSheetDialog bottomSheetDialog) {
        String fileName = null;
        try {
            fileName = item.getString("pd_title");
            File videofile = new File(DownloadSupport.getPdfDirectory(getContext(), session) + fileName + ".pdf");
            String Livepath = study_url + item.getString("pd_file");
            DownloadFile downloadFile = new DownloadFile();
            downloadFile.setId(item.getString("pd_id"));
            downloadFile.setDownloadId(item.getString("pd_id"));
            downloadFile.setCurrentBytes(0);
            downloadFile.setTotalBytes(0);
            downloadFile.setFiletype("1");
            downloadFile.setFileName(fileName);
            downloadFile.setDownloadStatus(DownloadStatus.START);
            downloadFile.setFilepath(DownloadSupport.getPdfDirectory(getContext(), session));
            downloadFile.setThumbPath("");
            downloadFile.setVideoUrlOffline(Livepath);
            Long startTime = System.currentTimeMillis();
            downloadFile.setStartTime(startTime);


            repository.Insert(downloadFile);


            Intent intent = new Intent(getContext(), YourService.class);
            // add infos for the service which file to download and where to store
            intent.putExtra(YourService.FILENAME, downloadFile.getFileName());
            //  intent.putExtra(YourService.urlpath, url);
            intent.putExtra(YourService.urlpath, downloadFile.getVideoUrlOffline());
            intent.putExtra(YourService.TYPE, "1");
            intent.putExtra(YourService.VIDEO_ID, downloadFile.getId());
            context.startService(intent);

            TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);

            view_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, PdfViewer.class).putExtra("file", downloadFile.getFilepath() + "/" + downloadFile.getFileName() + ".pdf"));
                    recyclerAdapter.notifyDataSetChanged();
                    bottomSheetDialog.setCancelable(true);
                    bottomSheetDialog.dismiss();
                }
            });

            setobserver(bottomSheetDialog, item.getString("pd_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setobserver(BottomSheetDialog bottomSheetDialog, String downloadFile) {

        ProgressBar pg = bottomSheetDialog.findViewById(R.id.progressBar);
        TextView percent = bottomSheetDialog.findViewById(R.id.percent);
        TextView minuteleft = bottomSheetDialog.findViewById(R.id.minuteleft);
        TextView speed = bottomSheetDialog.findViewById(R.id.speed);
        TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);

        downloadInfo();

        repository.getDownloadTable(downloadFile, "1").observe(((MainActivity) context), new Observer<DownloadFile>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onChanged(DownloadFile file) {
                if (file != null) {
                    if (!file.isDownloadComplete()) {
                        downloadInfo();
                        view_file.setVisibility(View.GONE);
                        tv_go_inside.setVisibility(View.VISIBLE);
                        try {
                          /* Long endTime = System.currentTimeMillis();
                            final double timeTakenMills = Math.floor(endTime - file.getStartTime());  // time taken in milliseconds
                            final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                            final int kilobytePerSec = (int) Math.round(1024 / timeTakenInSecs);
                            final double speedddd = Math.round(file.getCurrentBytes() / timeTakenMills);
                            Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
                            Log.d(TAG, "Kb per sec: " + kilobytePerSec);
                            Log.d(TAG, "Download Speed: " + speedddd);
                            Log.d(TAG, "File size in kb: " + file.getCurrentBytes());*/
                            // update the UI with the speed test results


                            pg.setProgress((int) Integer.parseInt(file.getPercentage()));
                            percent.setText(file.getPercentage() + "%");
                            minuteleft.setText(file.getTimeremain() + " s left");
                            speed.setText(roundTwoDecimals(Double.parseDouble(kilobytePerSec + "")) + " KB/S");

                        } catch (Exception e) {

                        }
                    } else {
                        pg.setProgress((int) 100);
                        percent.setText(100 + "%");
                        minuteleft.setText(0 + " s left");
                        view_file.setVisibility(View.VISIBLE);
                        tv_go_inside.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    private void canceldownload(BottomSheetDialog bottomSheetDialog, JSONObject item) {
        String fileName = null;
        try {
            fileName = item.getString("pd_title");
            new File(DownloadSupport.getPdfDirectory(getContext(), session) + fileName + ".pdf").delete();
            repository.Delete(item.getString("pd_id"), "1");
            bottomSheetDialog.setCancelable(true);
            bottomSheetDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    Double roundTwoDecimals(Double vLongValue) {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        return Double.valueOf(twoDForm.format(vLongValue));
    }

    private void downloadInfo() {
//        Log.d(TAG, "downloadInfo: iN downloadInfo");
        Request request = new Request.Builder()
                .url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png") // replace image url
                .build();
        startTime = System.currentTimeMillis();
        nclient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
//                Log.d(TAG, "downloadInfo: iN failure");
                //check when device is connected to Router but there is no internet

            }

            @Override
            public void onResponse(okhttp3.Call call, Response response) throws IOException {
//                Log.d(TAG, "downloadInfo: iN success");
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }
                InputStream input = response.body().byteStream();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) != -1) {
                        bos.write(buffer);
                    }
                    byte[] docBuffer = bos.toByteArray();
                    fileSize = bos.size();
                } finally {
                    input.close();
                }
                endTime = System.currentTimeMillis();

                // calculate how long it took by subtracting endtime from starttime
                final double timeTakenMills = Math.floor(endTime - startTime);  // time taken in milliseconds
                final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                int ds = (int) Math.round(1024 / timeTakenInSecs);
                if (ds > 0) {
                    kilobytePerSec = ds / 1000;
                }
                speed_data = Math.round(fileSize / timeTakenMills);
//                Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
//                Log.d(TAG, "Kb per sec: " + kilobytePerSec);
//                Log.d(TAG, "Download Speed: " + speed_data);
//                Log.d(TAG, "File size in kb: " + fileSize);
                // update the UI with the speed test results
            }
        });
    }
}
