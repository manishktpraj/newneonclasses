package codexo.neonclasses.ui.home;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.Player;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.Splash;
import codexo.neonclasses.databinding.FragmentHomeBinding;
import codexo.neonclasses.databinding.ItemExpiryPackageListLayoutBinding;
import codexo.neonclasses.databinding.ItemFreeClassesBinding;
import codexo.neonclasses.databinding.ItemHomeLiveClassesBinding;
import codexo.neonclasses.databinding.ItemNeonBooksBinding;
import codexo.neonclasses.databinding.ItemPopularCoursesBinding;
import codexo.neonclasses.expandable.CatActivity;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.books.BookDetailActivity;
import codexo.neonclasses.ui.books.BooksActivity;
import codexo.neonclasses.ui.cart.CartActivity;
import codexo.neonclasses.ui.courses.CourseDetailActivity;
import codexo.neonclasses.ui.courses.CoursesActivity;
import codexo.neonclasses.ui.descriptive.Descriptive;
import codexo.neonclasses.ui.download.DownloadsActivity;
import codexo.neonclasses.ui.news.NewsActivity;
import codexo.neonclasses.ui.notification.NotificationActivity;
import codexo.neonclasses.ui.offer.OfferActivity;
import codexo.neonclasses.ui.player.PlayerActivity;
import codexo.neonclasses.ui.purchase.MyPurchaseActivity;
import codexo.neonclasses.ui.share.ShareActivity;
import codexo.neonclasses.ui.study.StudyMaterialFragment;
import codexo.neonclasses.ui.test.TestSeriesFragment;
import codexo.neonclasses.ui.videos.FreevideolistAvtivity;
import codexo.neonclasses.ui.videos.StudymaterialActivity;
import codexo.neonclasses.ui.videos.VideosFragment;
import codexo.neonclasses.ui.youtube.YoutubeActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    JSONArray slideritem = new JSONArray();
    JSONArray packageitem = new JSONArray();
    JSONArray free_video = new JSONArray();
    JSONArray books = new JSONArray();
    JSONArray liveVideo = new JSONArray();
    JSONArray expiry_package = new JSONArray();
    JSONObject promotional_slider = new JSONObject();
    List item = new ArrayList();
    RecyclerAdapter1 recyclerAdapter;
    String custommessage = "";
    String slider_url = "", package_url = "", free_url = "", book_url = "", alret_url = "";
    int alertPosition;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        session = new SessionManager(getContext());

        pDialog = Constant.getProgressBar(getContext());
        TextView user_name_home = binding.userNameHome;
        TextView txt_hello = binding.txtHello;
        user_name_home.setText(session.getFirstName());
        user_name_home.setTypeface(Constant.getFontsBold(getContext()));
        txt_hello.setTypeface(Constant.getFontsBold(getContext()));
        if (Constant.home.equals("0")) {
            getdashboard();
        } else {
            try {
                slideritem = Constant.alldashboarditem.getJSONArray("slider");
                slider_url = Constant.alldashboarditem.getString("slider_url");
                packageitem = Constant.alldashboarditem.getJSONArray("package");
                package_url = Constant.alldashboarditem.getString("package_url");
                free_video = Constant.alldashboarditem.getJSONArray("total_video");
                free_url = Constant.alldashboarditem.getString("video_url");
                liveVideo = Constant.alldashboarditem.getJSONArray("live");
                expiry_package = Constant.alldashboarditem.getJSONArray("expire_soon_package");

                books = Constant.alldashboarditem.getJSONArray("books");
                if (books.length() <= 0) {
                    binding.neonBookCol.setVisibility(View.GONE);
                }
//                Constant.logPrint(book_url + "", "booksbooksbooksbooksbooksbooksbooksbooksbooks");
                book_url = Constant.alldashboarditem.getString("book_url");
                init();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        binding.swipLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                    //dashboard();
                    getdashboard();
                    binding.swipLayout.setRefreshing(false);

                /*if (!isNetworkAvailable() == true) {
                    new AlertDialog.Builder(getContext())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Internet Connection Alert")
                            .setMessage("Please Check Your Internet Connection")
                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //finish();
                                }
                            }).show();

                    binding.swipLayout.setRefreshing(false);
                }*/
            }
        });
        binding.swipLayout.setColorSchemeColors(Color.BLACK);
        /*if (!isNetworkAvailable() == true) {
            new AlertDialog.Builder(getContext())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Internet Connection Alert")
                    .setMessage("Please Check Your Internet Connection")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //finish();
                        }
                    }).show();
        }*/

        return root;


    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {

                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {

                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {

                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void init() {

        binding.joinliveclass.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new VideosFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_video);
        });
        binding.freestudymaterialHome.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new StudyMaterialFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_study);
        });
        binding.testseriesHome.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new TestSeriesFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_test);
        });
        binding.bookHome.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), BooksActivity.class));

        });
        binding.neonBooksDbo.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), BooksActivity.class));

        });
        binding.viewallbook.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), BooksActivity.class));

        });
        binding.viewallcourse.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), CoursesActivity.class));

        });
        binding.viewallfreevideo.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), FreevideolistAvtivity.class));
        });
        binding.viewalllive.setOnClickListener(v -> {
            ((MainActivity) getActivity()).openFragment(new VideosFragment());
        });
        binding.newsHome.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), NewsActivity.class));
        });
        binding.testSeries.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new TestSeriesFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_test);
        });
        binding.videoCourses.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new VideosFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_video);
        });
        binding.studyMaterial.setOnClickListener(view -> {
            ((MainActivity) getActivity()).openFragment(new StudyMaterialFragment());
            ((MainActivity) getActivity()).setselected(R.id.navigation_study);
        });
        binding.newBlog.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), NewsActivity.class));
        });
        binding.cuurrentaffair.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), DownloadsActivity.class));
        });
        binding.offers.setOnClickListener(view -> {
            Intent intent = new Intent(requireContext(), OfferActivity.class);
            intent.putExtra("type", "Home");
            intent.putExtra("amount", "amount");
            intent.putExtra("apply_on", "apply_on");
            intent.putExtra("apply_on_id", "apply_on_id");
            startActivity(intent);
        });
        binding.desciptive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Descriptive.class));
            }
        });

        //// slideritem.add("image");
        SliderView sliderView = binding.imageSlider;
        SliderAdapter adapter = new SliderAdapter(getContext(), slideritem);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(2);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getContext()) {
                    private static final float SPEED = 1f;// Change this                value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }
                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        recyclerAdapter = new RecyclerAdapter1(getContext(), packageitem);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerCourses.setLayoutManager(layoutManager);
        binding.recyclerCourses.setAdapter(recyclerAdapter);

        RecyclerAdapter2 recyclerAdapter2 = new RecyclerAdapter2(getContext(), free_video);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getContext());
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerFreeVideos.setLayoutManager(layoutManager2);
        binding.recyclerFreeVideos.setAdapter(recyclerAdapter2);

        RecyclerAdapter3 recyclerAdapter3 = new RecyclerAdapter3(getContext(), books);
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(getContext());
        layoutManager3.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerBooks.setLayoutManager(layoutManager3);
        binding.recyclerBooks.setAdapter(recyclerAdapter3);
        if (liveVideo.length()<=0){
            binding.liveLayout.setVisibility(View.GONE);
            binding.recyclerLive.setVisibility(View.GONE);
        }else {
            binding.liveLayout.setVisibility(View.VISIBLE);
            binding.recyclerLive.setVisibility(View.VISIBLE);
            LiveClassAdapter liveClassAdapter = new LiveClassAdapter(getContext(), liveVideo);
            LinearLayoutManager livelayoutmanager = new LinearLayoutManager(getContext());
            livelayoutmanager.setOrientation(RecyclerView.VERTICAL);
            binding.recyclerLive.setLayoutManager(livelayoutmanager);
            binding.recyclerLive.setAdapter(liveClassAdapter);
        }

        if (expiry_package.length()<=0){
            binding.expiryPackageRecycler.setVisibility(View.GONE);
        }else {
            binding.expiryPackageRecycler.setVisibility(View.VISIBLE);
            ExpiryPackageAdapter expiryPackageAdapter = new ExpiryPackageAdapter(getContext(), expiry_package);
            LinearLayoutManager livelayoutmanager = new LinearLayoutManager(getContext());
            binding.expiryPackageRecycler.setLayoutManager(livelayoutmanager);
            binding.expiryPackageRecycler.setAdapter(expiryPackageAdapter);
        }
        ///   autoScroll(binding);

        binding.relUserImage.setOnClickListener(view -> {
            MainActivity.openDrawer();
        });

        binding.imgCart.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), CartActivity.class));
//            startActivity(new Intent(getContext(), CatActivity.class));
        });

        binding.imgNotification.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), NotificationActivity.class));
        });
        customalert(binding.getRoot());

    }

    public void autoScroll(FragmentHomeBinding bind) {
        final int speedScroll = 0;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                if (count == recyclerAdapter.getItemCount())
                    count = 0;
                if (count < recyclerAdapter.getItemCount()) {
                    bind.recyclerCourses.smoothScrollToPosition(++count);
                    handler.postDelayed(this, speedScroll);
                }
            }
        };
        handler.postDelayed(runnable, speedScroll);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    //custom alert start
    public void customalert(View view) {
        final Dialog builder = new Dialog(view.getContext());
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.custome_home_alert_layout, null);
        ImageView home_banner = dialogLayout.findViewById(R.id.home_banner);
        ImageView btn_yes = dialogLayout.findViewById(R.id.cancel_btn);
        if (!alret_url.equals("")) {
            Constant.setImage(slider_url + alret_url, home_banner, getContext());
            builder.setContentView(dialogLayout);
            builder.setCancelable(true);
            builder.show();
        }
        home_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nullImage = "";
                int alertPosition = 0;
                try {
                    alertPosition = Integer.parseInt(promotional_slider.getString("slider_redirect_to"));
                    switch (alertPosition) {
                        case 0:
                            ((MainActivity) getActivity()).openFragment(new TestSeriesFragment());
                            builder.dismiss();
                            break;
                        case 1:
                            startActivity(new Intent(getContext(), MyPurchaseActivity.class));
                            builder.dismiss();
                            break;
                        case 2:
                            //Constant.setToast(getContext(),"Coming Soon");
                            startActivity(new Intent(getContext(), Descriptive.class));
                            builder.dismiss();
                            break;
                        case 3:
                            ((MainActivity) getActivity()).openFragment(new VideosFragment());
                            builder.dismiss();
                            break;
                        case 4:
                            startActivity(new Intent(getContext(), CoursesActivity.class));
                            builder.dismiss();
                            break;
                        case 5:
                            startActivity(new Intent(getContext(), BooksActivity.class));
                            builder.dismiss();
                            break;
                        case 6:
                            startActivity(new Intent(getContext(), ShareActivity.class));
                            builder.dismiss();
                            break;
                        case 7:
                            ((MainActivity) getActivity()).openFragment(new StudyMaterialFragment());
                            builder.dismiss();
                            break;
                        case 8:
                            nullImage = promotional_slider.getString("slider_button_url");
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(nullImage));
                            startActivity(i);
                            builder.dismiss();
                            break;
                        default:
                            startActivity(new Intent(getContext(), NewsActivity.class));
                            builder.dismiss();
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
    }


    /**
     * Create these is separate files later as per your need
     */
    public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

        private Context context;
        private JSONArray mSliderItems;

        public SliderAdapter(Context context, JSONArray mSliderItems) {
            this.context = context;
            this.mSliderItems = mSliderItems;
        }

        public void renewItems(JSONArray sliderItems) {
            this.mSliderItems = sliderItems;
            notifyDataSetChanged();
        }

        @Override
        public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
            return new SliderAdapterVH(inflate);
        }

        @Override
        public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
            try {
                JSONObject jdobj = mSliderItems.getJSONObject(position);
                String img = slider_url + "" + jdobj.getString("slider_image");
                ////   Constant.logPrint(img,"imgimgimgimgimgimgimgimgimgimgimgimgimgimg");
                Glide.with(viewHolder.itemView)
                        .load(img)
                        .fitCenter()
                        .placeholder(R.drawable.poster)
                        .into(viewHolder.imageViewBackground);
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            openitem(jdobj.getString("slider_redirect_to"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getCount() {
            //slider view count could be dynamic size
            return mSliderItems.length();
        }

        class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

            View itemView;
            ImageView imageViewBackground;

            public SliderAdapterVH(View itemView) {
                super(itemView);
                imageViewBackground = itemView.findViewById(R.id.image);
                this.itemView = itemView;
            }
        }

    }


    private void openitem(String type) {
        String nullImage = "";
        switch (type) {
            case "0":

                ((MainActivity) getActivity()).openFragment(new TestSeriesFragment());
                ((MainActivity) getActivity()).setselected(R.id.navigation_test);
                break;
            case "1":
                startActivity(new Intent(getContext(), MyPurchaseActivity.class));
                break;
            case "2":
                //Constant.setToast(getContext(),"Coming Soon");
                startActivity(new Intent(getContext(), Descriptive.class));
                break;
            case "3":

                ((MainActivity) getActivity()).openFragment(new VideosFragment());
                //((MainActivity)getActivity()).setselected(R.id.navigation_video);
                break;
            case "4":
                startActivity(new Intent(getContext(), CoursesActivity.class));
                break;
            case "5":
                startActivity(new Intent(getContext(), BooksActivity.class));
                break;
            case "6":
                startActivity(new Intent(getContext(), ShareActivity.class));
                break;
            case "7":
                ((MainActivity) getActivity()).openFragment(new StudyMaterialFragment());
                //((MainActivity)getActivity()).setselected(R.id.navigation_video);
                break;
            case "8":
                try {
                    nullImage = promotional_slider.getString("slider_button_url");
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(nullImage));
                    startActivity(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                startActivity(new Intent(getContext(), NewsActivity.class));
                break;
        }
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemPopularCoursesBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemPopularCoursesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = package_url + item.getString("sub_pack_image");
                if (binding != null && !img.equals("")) {
                    //Constant.setImage(img,);
                    Glide.with(requireContext()).load(img).into(binding.image);
                }
                binding.txtTitle.setText(item.getString("sub_pack_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));


                binding.txtDiscountedPrice.setText("₹" + item.getString("sub_pack_price"));
                binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setText("₹" + item.getString("sub_actual_price"));
                binding.txtPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

                String disperc = Constant.getdiscount(item.getString("sub_actual_price"), item.getString("sub_pack_price"));
//                Constant.logPrint(disperc + "", "dispercdispercdispercdispercdisperc");
                if (!disperc.equals("")) {
                    binding.txtDiscount.setText(disperc + "%");
                    binding.txtDiscount.setTypeface(Constant.getFontsBold(getContext()));
                }

                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(position);
                            Intent vidcategory = new Intent(getContext(), CourseDetailActivity.class);
                            vidcategory.putExtra("course_detail", item + "");
                            vidcategory.putExtra("course_url", package_url + "");
                            startActivity(vidcategory);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemPopularCoursesBinding itemView) {
                super(itemView.getRoot());
                String s = "₹10000";
                itemView.txtPrice.setText(s, TextView.BufferType.SPANNABLE);
                Spannable spannable = (Spannable) itemView.txtPrice.getText();
                spannable.setSpan(STRIKE_THROUGH_SPAN, 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            }
        }
    }

    public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.RecyclerViewHolder> {
        ItemFreeClassesBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter2(Context context, JSONArray free_video) {
            this.context = context;
            this.listItem = free_video;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemFreeClassesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String vid_url = free_url + item.getString("vid_image");
                Constant.setImage(vid_url, binding.image, getContext());
                binding.txtTitle.setText(item.getString("vid_title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return free_video.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemFreeClassesBinding itemView) {
                super(itemView.getRoot());
                itemView.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            JSONObject item = listItem.getJSONObject(getAdapterPosition());
                            Intent resultIntent = new Intent(Intent.ACTION_VIEW);
                            resultIntent.setData(Uri.parse(item.getString("vid_url")));
                            getActivity().startActivity(resultIntent);
                           /* if(Constant.player_setting.equals("1")) {
                                JSONObject item = listItem.getJSONObject(getAdapterPosition());
                                Intent vidcategory = new Intent(getContext(), YoutubeActivity.class);
                                vidcategory.putExtra("video_detail", item + "");
                                startActivity(vidcategory);
                            }else    if(Constant.player_setting.equals("0")) {
                                JSONObject item = listItem.getJSONObject(getAdapterPosition());
                                Intent vidcategory = new Intent(getContext(), PlayerActivity.class);
                                vidcategory.putExtra("video_detail", item + "");
                                startActivity(vidcategory);
                            }else{
                                Constant.setToast(getContext(),"Please Wait while server updation running");
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    public class RecyclerAdapter3 extends RecyclerView.Adapter<RecyclerAdapter3.RecyclerViewHolder> {
        ItemNeonBooksBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter3(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemNeonBooksBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = book_url + item.getString("product_image");
                Constant.setImage(img, binding.image, getContext());
                binding.txtTitle.setText(item.getString("product_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));

                binding.txtDiscountedPrice.setText("₹" + item.getString("product_discount_price"));
                binding.txtDiscountedPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setText("₹" + item.getString("product_original_price"));
                binding.txtPrice.setTypeface(Constant.getFontsBold(getContext()));
                binding.txtPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

                String disperc = Constant.getdiscount(item.getString("product_original_price"), item.getString("product_discount_price"));
//                Constant.logPrint(disperc + "", "dispercdispercdispercdispercdisperc");
                if (!disperc.equals("")) {
                    binding.txtDiscount.setText(disperc + "%");
                    binding.txtDiscount.setTypeface(Constant.getFontsBold(getContext()));
                }

                String url = item.getString("product_affurl");
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!url.equals("")) {

                            Intent vidcategory = new Intent(getContext(), BookDetailActivity.class);
                            vidcategory.putExtra("bookdetail", item + "");
                            vidcategory.putExtra("book_url", book_url + "");
                            startActivity(vidcategory);
                            ///  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            ///  startActivity(Intent.createChooser(browserIntent, "Browse with"));

                            ////      startActivity(browserIntent);
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return books.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemNeonBooksBinding itemView) {
                super(itemView.getRoot());

                String s = "₹10000";
                itemView.txtPrice.setText(s, TextView.BufferType.SPANNABLE);
                Spannable spannable = (Spannable) itemView.txtPrice.getText();
                spannable.setSpan(STRIKE_THROUGH_SPAN, 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    public class LiveClassAdapter extends RecyclerView.Adapter<LiveClassAdapter.RecyclerViewHolder> {
        ItemHomeLiveClassesBinding binding;
        Context context;
        JSONArray listItem;

        public LiveClassAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemHomeLiveClassesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                String img = item.getString("vid_image");
                Constant.setImage(img, binding.vidIcon, getContext());
                binding.txtTitle.setText(item.getString("vid_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));
                Constant.setBlinkAnim(binding.liveIcon);

                //String url = item.getString("product_affurl");
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject item = listItem.getJSONObject(position);
                            Intent livedetail = new Intent(getContext(), YoutubeActivity.class);
                            livedetail.putExtra("video_detail", item + "");
                            startActivity(livedetail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ///  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        ///  startActivity(Intent.createChooser(browserIntent, "Browse with"));

                        ////      startActivity(browserIntent);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return liveVideo.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemHomeLiveClassesBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    public class ExpiryPackageAdapter extends RecyclerView.Adapter<ExpiryPackageAdapter.RecyclerViewHolder> {
        ItemExpiryPackageListLayoutBinding binding;
        Context context;
        JSONArray listItem;

        public ExpiryPackageAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemExpiryPackageListLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerViewHolder holder = new RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, @SuppressLint("RecyclerView") int position) {
            try {
                JSONObject neon_package = listItem.getJSONObject(position);
//                String upd_expiry_date = "2022-07-30";
                String upd_expiry_date = neon_package.getString("upd_expiry_date");


                if (!upd_expiry_date.equals("null")){
                    String replaced = upd_expiry_date.replaceAll("T"," ");
                    String[] replaceddd = replaced.split(" ");
                    String ordat = replaceddd[0];
                    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
                    Date Ordate = (Date) parser.parse(ordat);
                    DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
                    String packageexpDate = formatter.format(Ordate);

                    SimpleDateFormat dateFormatJoin = new SimpleDateFormat("yyyy-MM-dd");
                    Date endtime = dateFormatJoin.parse(upd_expiry_date);
                    Date pasTime = new Date();
                    long ptime = pasTime.getTime();
                    long expdata = endtime.getTime();
                    long presentday = TimeUnit.MILLISECONDS.toDays(ptime);
                    long expday = TimeUnit.MILLISECONDS.toDays(expdata);

                    if (presentday<=expday) {
                        binding.txtDes.setText("This Subscription is expiring on : " + packageexpDate);
                    }else {
                        binding.txtDes.setText("This Subscription is expired on : " + packageexpDate);
                    }
                    // binding.orderDate.setText("Order Date : "+order_date);
                    binding.txtDes.setVisibility(View.VISIBLE);
                }else {
                    binding.txtDes.setVisibility(View.GONE);
                }

                JSONObject item = neon_package.getJSONObject("neon_package");
                String img = item.getString("sub_pack_image");
                Constant.setImage(package_url+img, binding.vidIcon, getContext());
                binding.txtTitle.setText(item.getString("sub_pack_title"));
                binding.txtTitle.setTypeface(Constant.getFontsBold(getContext()));
                Constant.setBlinkAnim(binding.btnLayout);
                binding.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONObject neon_package = listItem.getJSONObject(position);
                            JSONObject item = neon_package.getJSONObject("neon_package");
                            Intent vidcategory = new Intent(getContext(), CourseDetailActivity.class);
                            vidcategory.putExtra("course_detail", item + "");
                            vidcategory.putExtra("course_url", package_url + "");
                            startActivity(vidcategory);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return expiry_package.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

            public RecyclerViewHolder(@NonNull ItemExpiryPackageListLayoutBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

    private void getdashboard() {

//        Constant.logPrint("Constant.fcm_token", Constant.fcm_token);
//        Constant.logPrint("Constant.device_id" + Constant.device_info, Constant.device_id);

        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().getdashboard(session.getUserId(), Constant.fcm_token, Constant.device_id, Constant.device_info);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        Constant.alldashboarditem = jsonObject;
                        Constant.home = "1";
                        if (message.equals("ok")) {
                            Constant.player_setting = jsonObject.getString("player_setting");
                            slideritem = jsonObject.getJSONArray("slider");
                            slider_url = jsonObject.getString("slider_url");
                            packageitem = jsonObject.getJSONArray("package");
                            package_url = jsonObject.getString("package_url");
                            free_video = jsonObject.getJSONArray("total_video");
                            free_url = jsonObject.getString("video_url");
                            books = jsonObject.getJSONArray("books");
                            liveVideo = jsonObject.getJSONArray("live");
                            expiry_package = jsonObject.getJSONArray("expire_soon_package");
//                            Constant.logPrint("packegeliveData",liveVideo.length()+"");
                            promotional_slider = jsonObject.getJSONObject("promotional_slider");
                            if (promotional_slider != null) {
                                alret_url = promotional_slider.getString("slider_image");
                            }

                            Constant.PSC_TEXT = jsonObject.getString("psc_title");
                            Constant.PSC_URL = jsonObject.getString("psc_url");
                            if (books.length() <= 0) {
                                binding.neonBookCol.setVisibility(View.GONE);
                            }
//                            Constant.logPrint(book_url + "", "booksbooksbooksbooksbooksbooksbooksbooksbooks");
                            book_url = jsonObject.getString("book_url");

                            Constant.CUSTOM_MESSAGE = jsonObject.getString("dashboard_message");
                            if (!Constant.CUSTOM_MESSAGE.equals("")) {
                                binding.customMessageData.loadData(Constant.CUSTOM_MESSAGE, "text/html", "UTF-8");
                                binding.custommessage.setVisibility(View.VISIBLE);
                            } else {
                                binding.custommessage.setVisibility(View.GONE);
                            }


                            init();
                        } else {
                            Constant.setToast(getContext(), "Server Not Responding");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getdashboard();
            }
        });
    }

    @Override
    public void onResume() {
        if (!Constant.CUSTOM_MESSAGE.equals("")) {
            binding.customMessageData.loadData(Constant.CUSTOM_MESSAGE, "text/html", "UTF-8");
            binding.custommessage.setVisibility(View.VISIBLE);
        } else {
            binding.custommessage.setVisibility(View.GONE);
        }
        super.onResume();
    }

}