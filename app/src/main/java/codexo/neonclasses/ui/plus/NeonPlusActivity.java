package codexo.neonclasses.ui.plus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityNeonPlusBinding;
import codexo.neonclasses.databinding.ItemTransactionBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.player.WebuiPlayer;
import codexo.neonclasses.ui.profile.MyProfileActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class NeonPlusActivity extends AppCompatActivity {

    private ActivityNeonPlusBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    String student_balance="0.00";
    JSONArray wallethistory =new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNeonPlusBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        session = new SessionManager(getApplicationContext());
        pDialog = Constant.getProgressBar(NeonPlusActivity.this);
        Constant.adbEnabled(NeonPlusActivity.this);
        Constant.checkAdb(NeonPlusActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        getwallet();
    }
    private void getwallet() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().neonplus(session.getUserId());

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                            student_balance =jsonObject.getString("user_balance");
                            binding.txtBal.setText("₹"+student_balance);
                            wallethistory =jsonObject.getJSONArray("result");
                            RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(NeonPlusActivity.this, wallethistory);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(NeonPlusActivity.this);
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);
                        } else {
                            Constant.setToast(NeonPlusActivity.this,notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                getwallet();
            }
        });
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemTransactionBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemTransactionBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                binding.txtTitle.setText(item.getString("wtrans_remark"));
                binding.txtTitle.setTypeface(Constant.getsemiFonts(getApplicationContext()));

                binding.txtDate.setText(item.getString("wtrans_date"));


                String strDate = item.getString("wtrans_date");





                binding.txtDate.setTypeface(Constant.getFonts(getApplicationContext()));
                if(item.getString("wtrans_type").equals("P")) {
                    binding.txtPrice.setTextColor(getResources().getColor(R.color.purple_700));
                    binding.txtPrice.setText("- ₹" + item.getString("wtrans_amt"));
                }else{
                    binding.txtPrice.setTextColor(getResources().getColor(R.color.teal_700));
                    binding.txtPrice.setText("+ ₹" + item.getString("wtrans_amt"));

                }
                binding.txtPrice.setTypeface(Constant.getsemiFonts(getApplicationContext()));



            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemTransactionBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
    @Override
    public void onBackPressed(){
         this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(NeonPlusActivity.this);
        Constant.checkAdb(NeonPlusActivity.this);
    }
}

