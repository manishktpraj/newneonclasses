package codexo.neonclasses.ui.download;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.databinding.FragmentDownloadedVideosBinding;
import codexo.neonclasses.databinding.ItemDownloadedMaterialBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.home.HomeViewModel;
import codexo.neonclasses.ui.player.OfflineVideoPlayerActivity;
import codexo.neonclasses.ui.videos.VideoPartListActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class VideoDownloadsFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentDownloadedVideosBinding binding;
    List<File> item = new ArrayList<>();
    List<File> images = new ArrayList<>();
    RecyclerAdapter1 recyclerAdapter;
    private DatabaseRepository repository;
    List<DownloadFile> tablesd;
    private FragmentDownloadedVideosBinding newbinding;
    private final OkHttpClient nclient = new OkHttpClient();
    private long startTime;
    private long endTime;
    private long fileSize;
    int kilobytePerSec=0;
    double speed_data;
    SessionManager session;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        repository=new DatabaseRepository(requireContext());

        binding = FragmentDownloadedVideosBinding.inflate(inflater, container, false);
        session = new SessionManager(getContext());

        newbinding =binding;
       /* if(DownloadSupport.isStoragePermissionGranted(requireActivity())){
            item=DownloadSupport.getListFiles(new File(DownloadSupport.getVideoDirectory(requireContext(),new SessionManager(requireContext()))));
            images=DownloadSupport.getListFiles(new File(DownloadSupport.getImageDirectory(requireContext(),new SessionManager(requireContext()))));
            if(item !=null && item.size()>0){
                binding.emptyTxt.setVisibility(View.GONE);
                recyclerAdapter = new RecyclerAdapter1(getContext(), item);
                binding.videosRecycler.setAdapter(recyclerAdapter);
            }else{
                binding.emptyTxt.setVisibility(View.VISIBLE);
                Toast.makeText(requireContext(), "No Videos Downloaded", Toast.LENGTH_SHORT).show();
            }
        }*/
        init();
        View root = binding.getRoot();

        return root;
    }

    private void init() {
        if(DownloadSupport.isStoragePermissionGranted(requireActivity())){
            repository.getDownloadTable("0").observe(requireActivity(), new Observer<List<DownloadFile>>() {
                @Override
                public void onChanged(List<DownloadFile> tables) {
                    if(tables!=null && tables.size()>0){
                        binding.emptyTxt.setVisibility(View.GONE);

                        if(recyclerAdapter==null) {
                            tablesd = new ArrayList<>();
                            tablesd = tables;
                            binding.emptyTxt.setVisibility(View.GONE);
                            recyclerAdapter = new RecyclerAdapter1(getContext(), tablesd);
                            binding.videosRecycler.setAdapter(recyclerAdapter);

                        }

                    }else {
                        tablesd = new ArrayList<>();
                        tablesd = tables;
                        binding.emptyTxt.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        ItemDownloadedMaterialBinding binding;
        Context context;
        List<DownloadFile> listItem =new ArrayList<>();;

        public RecyclerAdapter1(Context context, List<DownloadFile> listItem) {
            this.context = context;
            this.listItem = tablesd;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemDownloadedMaterialBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
           RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            if(position==0)
            {
                binding.topspace.setVisibility(View.VISIBLE);
            }else
            {
                binding.topspace.setVisibility(View.GONE);
            }

            /*holder.binding.viewfile.setImageResource(R.drawable.playbutton);
            holder.binding.viewfile.setMaxHeight(18);
            holder.binding.viewfile.setMaxWidth(42);*/
            holder.binding.viewfile.setVisibility(View.VISIBLE);


            holder.binding.speeder.setVisibility(View.GONE);
            holder.binding.timer.setVisibility(View.GONE);
            holder.binding.progressBar.setVisibility(View.GONE);

            DownloadFile item = tablesd.get(position);

            if(!item.isDownloadComplete())
            {

                binding.viewfile.getLayoutParams().height = 48;
                binding.viewfile.getLayoutParams().width = 48;
                binding.viewfile.setImageResource(R.drawable.downloadicon);

            }else{
                binding.viewfile.setImageResource(R.drawable.eye);

            }

                ///    binding.imgDownload.setImageResource(R.drawable.playbutton);
            holder.binding.playerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DownloadFile item = tablesd.get(position);
                    if(item.isDownloadComplete()) {
//                        Constant.logPrint(item.getFilepath()+"/"+item.getFileName(),"item.getFilepath()");
                        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
                        bottomSheetDialog.setContentView(R.layout.dialog_box);
                        LinearLayout deletelayout = bottomSheetDialog.findViewById(R.id.deletelayout);
                        LinearLayout viewlayout = bottomSheetDialog.findViewById(R.id.viewlayout);
                        TextView textView =bottomSheetDialog.findViewById(R.id.title_play);
                        textView.setText("Play");
                        viewlayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent videoplay = new Intent(getContext(), OfflineVideoPlayerActivity.class);
                                videoplay.putExtra("video_detail",item.getFilepath()+"/"+item.getFileName());
                                startActivity(videoplay);
                                bottomSheetDialog.dismiss();
                            }
                        });
                        deletelayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new File(item.getFilepath()+"/"+item.getFileName()).delete();
                                repository.Delete(item.getDownloadId(),"0");
                                bottomSheetDialog.dismiss();
                                listItem.remove(position);
                                ///   tablesd.remove(position);
                                setlivedata();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerAdapter = new RecyclerAdapter1(getContext(), tablesd);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                                        ////layoutManager.setReverseLayout(true);

                                        layoutManager.setOrientation(RecyclerView.VERTICAL);
                                        newbinding.videosRecycler.setLayoutManager(layoutManager);
                                        newbinding.videosRecycler.setAdapter(recyclerAdapter);
                                    }
                                }, 1000);
                                ////   recyclerAdapter.notifyDataSetChanged();
                           }
                        });
                        bottomSheetDialog.show();


                    }else {
//                        Constant.logPrint(item.getFilepath()+"/"+item.getFileName(),"item.getFilepath()");
                        BottomSheetDialog bottomSheetDialogs = new BottomSheetDialog(getContext());
                        bottomSheetDialogs.setContentView(R.layout.dialog_box);
                        LinearLayout deletelayout = bottomSheetDialogs.findViewById(R.id.deletelayout);
                        LinearLayout viewlayout = bottomSheetDialogs.findViewById(R.id.viewlayout);
                        TextView textView =bottomSheetDialogs.findViewById(R.id.title_play);
                        textView.setText("Resume Download");
                        viewlayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                customalert(item,position,bottomSheetDialogs);
                            }
                        });
                        deletelayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new File(item.getFilepath()+"/"+item.getFileName()).delete();
                                repository.Delete(item.getDownloadId(),"0");
                                bottomSheetDialogs.dismiss();
                                listItem.remove(position);
                                ///   tablesd.remove(position);
                                setlivedata();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerAdapter = new RecyclerAdapter1(getContext(), tablesd);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                                        ////layoutManager.setReverseLayout(true);

                                        layoutManager.setOrientation(RecyclerView.VERTICAL);
                                        newbinding.videosRecycler.setLayoutManager(layoutManager);
                                        newbinding.videosRecycler.setAdapter(recyclerAdapter);
                                    }
                                }, 1000);
                                ////   recyclerAdapter.notifyDataSetChanged();
                            }
                        });
                        bottomSheetDialogs.show();


                    }
                }
            });
            holder.binding.txtTitle.setText(item.getTitle());
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return listItem.size();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
            ItemDownloadedMaterialBinding binding;
            public RecyclerViewHolder(@NonNull ItemDownloadedMaterialBinding binding) {
                super(binding.getRoot());
                this.binding=binding;
            }
        }
    }


    public void  choosespeed()
    {

    }

    public void  setlivedata()
    {
        repository.getDownloadTable("0").observe(this, fruitlist -> {
            tablesd=fruitlist;
        });

    }




    public void customalert(DownloadFile item, Integer pos,BottomSheetDialog bottomSheetDialogs) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
        bottomSheetDialog.setContentView(R.layout.download_layout);
        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tv_go_inside=bottomSheetDialog.findViewById(R.id.tv_go_inside);
        TextView txt_title=bottomSheetDialog.findViewById(R.id.txt_title);
        tv_go_inside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canceldownload(bottomSheetDialog,item,pos);
            }
        });
        txt_title.setText(item.getFileName());
        setindatabase(item,bottomSheetDialog,pos,bottomSheetDialogs);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();

    }

    public void  setindatabase(DownloadFile item,BottomSheetDialog bottomSheetDialog,Integer pos,BottomSheetDialog bottomSheetDialogs)
    {
        String fileName = null;

        Intent intent = new Intent(getContext(), YourService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(YourService.FILENAME, item.getFileName());
        //  intent.putExtra(YourService.urlpath, url);
        intent.putExtra(YourService.urlpath, item.getVideoUrlOffline());
        intent.putExtra(YourService.TYPE, "0");
        intent.putExtra(YourService.VIDEO_ID, item.getId());
        getContext().startService(intent);

        TextView view_file=bottomSheetDialog.findViewById(R.id.view_file);

        view_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ////  startActivity(new Intent(getApplicationContext(), PdfViewer.class).putExtra("file", downloadFile.getFilepath()+"/"+downloadFile.getFileName()+".pdf"));
                Intent videoplay = new Intent(getContext(), OfflineVideoPlayerActivity.class);
                videoplay.putExtra("video_detail",item.getFilepath()+"/"+item.getFileName());
                startActivity(videoplay);
                recyclerAdapter.notifyDataSetChanged();
                    bottomSheetDialog.setCancelable(true);
                bottomSheetDialog.dismiss();
                bottomSheetDialogs.dismiss();
            }
        });

        setobserver(bottomSheetDialog,item.getId());

    }
    private static int SPLASH_TIME_OUT = 10000;

    private  void setobserver(BottomSheetDialog bottomSheetDialog, String downloadFile) {

        ProgressBar pg = bottomSheetDialog.findViewById(R.id.progressBar);
        TextView percent = bottomSheetDialog.findViewById(R.id.percent);
        TextView minuteleft = bottomSheetDialog.findViewById(R.id.minuteleft);
        TextView speed = bottomSheetDialog.findViewById(R.id.speed);
        TextView view_file = bottomSheetDialog.findViewById(R.id.view_file);
        TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
//        Constant.logPrint(downloadFile,"downloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFiledownloadFile");
        downloadInfo();
        repository.getDownloadTable(downloadFile,"0").observe((getActivity()), new Observer<DownloadFile>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onChanged(DownloadFile file) {
                if(file!=null){
                    if(!file.isDownloadComplete() ){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                downloadInfo();
                            }
                        }, SPLASH_TIME_OUT);

                        view_file.setVisibility(View.GONE);
                        tv_go_inside.setVisibility(View.VISIBLE);
                        try {
                       /*    Long endTime = System.currentTimeMillis();
                            // calculate how long it took by subtracting endtime from starttime
                            final double timeTakenMills = Math.floor(endTime - file.getStartTime());  // time taken in milliseconds
                            final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                            final int kilobytePerSec = (int) Math.round(1024 / timeTakenInSecs);
                            final double speede = Math.round(file.getTotalBytes() / timeTakenMills);
                            Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
                            Log.d(TAG, "Kb per sec: " + kilobytePerSec);
                            Log.d(TAG, "Download Speed: " + speede);
                            Log.d(TAG, "File size in kb: " + file.getTotalBytes());
*/
                            pg.setProgress((int) Integer.parseInt(file.getPercentage()));
                            percent.setText(file.getPercentage()+"%");
                            minuteleft.setText(file.getTimeremain()+" s left");
                            speed.setText(roundTwoDecimals(Double.parseDouble(kilobytePerSec+""))+" KB/S");

                        }catch (Exception e) {

                        }
                    }else{
                        pg.setProgress((int) 100);
                        percent.setText(100+"%");
                        minuteleft.setText(0+" s left");
                        view_file.setVisibility(View.VISIBLE);
                        tv_go_inside.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    private void canceldownload(BottomSheetDialog bottomSheetDialog,DownloadFile item ,Integer pos )
    {
        String fileName = null;
        fileName = item.getFileName();
        new File(DownloadSupport.getPdfDirectory(getContext(), session)+fileName+".mp4").delete();
        repository.Delete( item.getId(),"0");
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.dismiss();

    }

    Double roundTwoDecimals(Double vLongValue)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        return Double.valueOf(twoDForm.format(vLongValue));
    }
    private void downloadInfo() {
//        Log.d(TAG, "downloadInfo: iN downloadInfo");
        Request request = new Request.Builder()
                .url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png") // replace image url
                .build();
        startTime = System.currentTimeMillis();
        nclient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
//                Log.d(TAG, "downloadInfo: iN failure");
                //check when device is connected to Router but there is no internet

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
//                Log.d(TAG, "downloadInfo: iN success");
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }
                InputStream input = response.body().byteStream();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    while (input.read(buffer) != -1) {
                        bos.write(buffer);
                    }
                    byte[] docBuffer = bos.toByteArray();
                    fileSize = bos.size();
                } finally {
                    input.close();
                }
                endTime = System.currentTimeMillis();

                // calculate how long it took by subtracting endtime from starttime
                final double timeTakenMills = Math.floor(endTime - startTime);  // time taken in milliseconds
                final double timeTakenInSecs = timeTakenMills / 1000;  // divide by 1000 to get time in seconds
                int ds = (int) Math.round(1024 / timeTakenInSecs);
                if(ds>0) {
                    kilobytePerSec = ds/1000;
                }
                speed_data = Math.round(fileSize / timeTakenMills);
//                Log.d(TAG, "Time taken in secs: " + timeTakenInSecs);
//                Log.d(TAG, "Kb per sec: " + kilobytePerSec);
//                Log.d(TAG, "Download Speed: " + speed_data);
//                Log.d(TAG, "File size in kb: " + fileSize);
                // update the UI with the speed test results
            }
        });
    }
}
