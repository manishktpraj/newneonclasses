package codexo.neonclasses.ui.download;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.style.StrikethroughSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.BuildConfig;
import codexo.neonclasses.Constant;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentDownloadedMaterialBinding;
import codexo.neonclasses.databinding.ItemDownloadedMaterialBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.download.pdfviewer.PdfViewer;
import codexo.neonclasses.ui.home.HomeViewModel;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.videos.StudymaterialActivity;

public class StudyMaterialDownloadsFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentDownloadedMaterialBinding binding;
    List<File> item = new ArrayList<>();
    RecyclerAdapter1 recyclerAdapter;

    private DatabaseRepository repository;
    private List<DownloadFile> studymaterial_list = new ArrayList<>();
    NetworkCapabilities nc;
    int downSpeed=0;

    NetworkInfo netInfo;
    Boolean downloadstaart=false;
    List<DownloadFile> tablesd;
    private FragmentDownloadedMaterialBinding newbinding;
    Context contextd;
   ///// ListAdapter customAdapter;
    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        binding = FragmentDownloadedMaterialBinding.inflate(inflater, container, false);
        newbinding =binding;
        View root = binding.getRoot();
        repository=new DatabaseRepository(requireContext());
        ///recyclerAdapter=null;
        contextd = getContext();
        init();
        return root;
    }

    private void init() {
        if(DownloadSupport.isStoragePermissionGranted(requireActivity())){
            repository.getDownloadTable("1").observe(requireActivity(), new Observer<List<DownloadFile>>() {
                @Override
                public void onChanged(List<DownloadFile> tables) {
                    if(tables!=null && tables.size()>0){
                        binding.emptyTxt.setVisibility(View.GONE);

                        if(recyclerAdapter==null) {
                            tablesd = new ArrayList<>();
                            tablesd = tables;
                            recyclerAdapter = new RecyclerAdapter1(contextd, tables);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                            ////layoutManager.setReverseLayout(true);

                            layoutManager.setOrientation(RecyclerView.VERTICAL);
                            binding.recycler.setLayoutManager(layoutManager);
                            binding.recycler.setAdapter(recyclerAdapter);
                            binding.emptyTxt.setVisibility(View.GONE);


                            for(int i=0;i<tablesd.size();i++) {
                                DownloadFile item = tablesd.get(i);
                                if (item != null && !item.isDownloadComplete() && item.getDownloadStatus().equals(DownloadStatus.START)) {
                                    downloadstaart = true;


                                            Intent intent = new Intent(getContext(), YourService.class);
                                            // add infos for the service which file to download and where to store
                                            intent.putExtra(YourService.FILENAME, item.getFileName());
                                            //  intent.putExtra(YourService.urlpath, url);
                                            intent.putExtra(YourService.urlpath, item.getVideoUrlOffline());
                                            intent.putExtra(YourService.TYPE, "1");
                                            intent.putExtra(YourService.VIDEO_ID, item.getId());
                                            contextd.startService(intent);



                                }
                            }
// get data from the table by the ListAdapter


                        }else{

                        }
                    }else {
                        tablesd = new ArrayList<>();
                        tablesd = tables;
                        binding.recycler.setVisibility(View.GONE);
                        binding.emptyTxt.setVisibility(View.VISIBLE);
                        binding.emptyTxt.setText(getString(R.string.no_study_found));
                    }
                }
            });
        }
    }
    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void OpenPDFFile(String path,String name) {
        File pdfFile = new File(path,name);//File path
        if (pdfFile.exists()) //Checking for the file is exist or not
        {
            Uri paths = Uri.fromFile(pdfFile);
            Intent objIntent = new Intent(Intent.ACTION_VIEW);
            objIntent.setDataAndType(paths, "application/pdf");
            objIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(objIntent);//Staring the pdf viewer
        }
    }
    /**
     * Create these is separate files later as per your need
     */


  public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
         ItemDownloadedMaterialBinding binding;
         Context context;
         List<DownloadFile> listItem =new ArrayList<>();;

         public RecyclerAdapter1(Context context,  List<DownloadFile> listItem) {
             this.context = context;
             this.listItem = tablesd;
         }


         @NonNull
         @Override
         public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
             binding = ItemDownloadedMaterialBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
             RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

             return holder;
         }

         @Override
         public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
             if(position==0)
             {
              holder.binding.topspace.setVisibility(View.VISIBLE);
             }else
             {
             holder.binding.topspace.setVisibility(View.GONE);
             }

             DownloadFile item = tablesd.get(position);
             if(item.isDownloadComplete())
             {
                 holder.binding.viewfile.setVisibility(View.VISIBLE);
                 holder.binding.speeder.setVisibility(View.GONE);
                 holder.binding.timer.setVisibility(View.GONE);
                 holder.binding.progressBar.setVisibility(View.GONE);

             } else{
                 holder.binding.viewfile.setVisibility(View.GONE);
                 holder.binding.speeder.setVisibility(View.VISIBLE);
                 holder.binding.timer.setVisibility(View.VISIBLE);
                 holder.binding.progressBar.setVisibility(View.VISIBLE);

             }


//             Constant.logPrint(item.getFileName()+"","item.getFileName()item.getFileName()item.getFileName()");
             holder.binding.txtTitle.setText(item.getFileName());
//             Constant.logPrint(item.isDownloadComplete()+"","item.isDownloadComplete()item.isDownloadComplete()item.isDownloadComplete()item.isDownloadComplete()item.isDownloadComplete()");



             repository.getDownloadTable(item.getId(),"1").observe(((DownloadsActivity) context), new Observer<DownloadFile>() {
                 @RequiresApi(api = Build.VERSION_CODES.Q)
                 @Override
                 public void onChanged(DownloadFile file) {
                     if(file!=null){
                         if(!file.isDownloadComplete() ){
                             if(holder.binding.speeder.getVisibility()==View.GONE) {
                                 holder.binding.viewfile.setVisibility(View.GONE);
                                 holder.binding.speeder.setVisibility(View.VISIBLE);
                                 holder.binding.timer.setVisibility(View.VISIBLE);
                                 holder.binding.progressBar.setVisibility(View.VISIBLE);
                             }


                             try {
                                 if(item.getId().equals(file.getId()))
                                 {
                                     holder.binding.progressBar.setProgress((int) Integer.parseInt(file.getPercentage()));
                                     holder.binding.percent.setText(file.getPercentage()+"%");
                                     holder.binding.minuteleft.setText(file.getTimeremain()+" s left");
                                     holder.binding.speed.setText(file.getDownloadspeed()+" MB/S");
                                 }
                                 ///// long per = (file.getTotalBytes() * 100) / file.getCurrentBytes();


                               /*  if(Integer.parseInt(file.getPercentage())>=99) {
                                     setlivedata();
                                     recyclerAdapter.notifyDataSetChanged();
                                     holder.binding.speeder.setVisibility(View.GONE);
                                     holder.binding.timer.setVisibility(View.GONE);
                                     holder.binding.progressBar.setVisibility(View.GONE);
                                     holder.binding.viewfile.setVisibility(View.VISIBLE);
                                 }*/


                             }catch (Exception e){

                             }

                             /////binding.viewfile.setVisibility(View.GONE);

                         }else{

                             holder.binding.speeder.setVisibility(View.GONE);
                             holder.binding.timer.setVisibility(View.GONE);
                             holder.binding.progressBar.setVisibility(View.GONE);
                             holder.binding.viewfile.setVisibility(View.VISIBLE);

                         }
                     }
                 }
             });

             holder.binding.parent.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     DownloadFile item = tablesd.get(position);
                     if(item.isDownloadComplete()) {
//                         Constant.logPrint(item.getFilepath()+"/"+item.getFileName(),"item.getFilepath()");

                         final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
                         bottomSheetDialog.setContentView(R.layout.dialog_box);

                          LinearLayout deletelayout = bottomSheetDialog.findViewById(R.id.deletelayout);
                           LinearLayout viewlayout = bottomSheetDialog.findViewById(R.id.viewlayout);

                         viewlayout.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 startActivity(new Intent(context, PdfViewer.class).putExtra("file", item.getFilepath()+"/"+item.getFileName()+".pdf"));
                                 bottomSheetDialog.dismiss();
                             }
                         });

                         deletelayout.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 new File(item.getFilepath()+"/"+item.getFileName()+".pdf").delete();
                                 repository.Delete(item.getDownloadId(),"1");
                                 bottomSheetDialog.dismiss();

                                 listItem.remove(position);
                              ///   tablesd.remove(position);
                                 setlivedata();
                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {


                                         recyclerAdapter = new RecyclerAdapter1(getContext(), tablesd);
                                         LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                                         ////layoutManager.setReverseLayout(true);

                                         layoutManager.setOrientation(RecyclerView.VERTICAL);
                                         newbinding.recycler.setLayoutManager(layoutManager);
                                         newbinding.recycler.setAdapter(recyclerAdapter);
                                      }
                                 }, 1000);


                              ////   recyclerAdapter.notifyDataSetChanged();

                             }
                         });
                         bottomSheetDialog.show();


                     }
                 }
             });



 /*
            holder.binding.txtTitle.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {

                    PopupMenu popupMenu = new PopupMenu(getContext(), binding.playerButton);
                    popupMenu.inflate(R.menu.chooseactionpdf);
                    popupMenu.setGravity(Gravity.END);
                    ///       popupMenu.setGravity();
                     popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            // Toast message on menu item clicked

                            switch (menuItem.getItemId()) {
                                case R.id.x05: {
                         startActivity(new Intent(context, PdfViewer.class).putExtra("file",item.getAbsolutePath()));
                              ///      OpenPDFFile(item.getAbsolutePath(),item.getName());


        try{
                                        File pdfFile = new File(item.getAbsolutePath()+"/"+item.getName());  // -> filename = maven.pdf
                                        Uri path = Uri.fromFile(pdfFile);
                                        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
                                        pdfIntent.setDataAndType(path, "application/pdf");
                                        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(pdfIntent);
                                    }catch(ActivityNotFoundException e){
                                        Toast.makeText(getContext(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
                                    }*//**//*


           Uri pdfUri = Uri.parse(item.getAbsolutePath());
                                    Intent shareIntent = ShareCompat.IntentBuilder.from(requireActivity())
                                            .setText("Share PDF doc")
                                            .setType("application/pdf")
                                            .setStream(pdfUri )
                                            .getIntent()
                                            .setPackage("com.google.android.apps.docs");
                                    startActivity(shareIntent);*//**//*

     Intent intent = new Intent(Intent.ACTION_VIEW);
                                    Uri outputFileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", new File(item.getAbsolutePath()));
                                    intent.setDataAndType(outputFileUri, "application/pdf");
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    Intent in = Intent.createChooser(intent, "Open File");
                                    startActivity(in);*//**//*

                                    break;
                                }
                                case R.id.delete: {
                                    new File(listItem.get(position).getAbsolutePath()).delete();
                                    listItem=DownloadSupport.getListFiles(new File(DownloadSupport.getPdfDirectory(requireContext(),new SessionManager(requireContext()))));
                                    recyclerAdapter.notifyDataSetChanged();
                                    break;
                                }
                            }

                            // Showing the popup menu


                            return true;
                        }
                    });
                    // Showing the popup menu
                    popupMenu.show();


                }
            });
*/
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return tablesd.size();
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            private final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
            ItemDownloadedMaterialBinding binding;
            public RecyclerViewHolder(@NonNull ItemDownloadedMaterialBinding binding) {
                super(binding.getRoot());
                this.binding=binding;




            }
        }
    }

    public void  setlivedata()
    {
        repository.getDownloadTable("1").observe(this, fruitlist -> {
            tablesd=fruitlist;
        });

    }
}
