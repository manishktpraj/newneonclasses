package codexo.neonclasses.ui.download;


import android.app.Activity;
import android.app.DownloadManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.FileProvider;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;

import java.io.File;

import codexo.neonclasses.BuildConfig;
import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;


public class YourService extends IntentService  {
 //private DownloadNotification notification;
    private DatabaseRepository repository;
    private DownloadFile file;
    private Context context;
    private int result = Activity.RESULT_CANCELED;
    public static final String urlpath = "urlpath";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = "filepath";
    public static final String TYPE = "filetype";
    public static final String VIDEO_ID = "id";
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "Notification";
    String downloadId="";
     int id=0;
    File videofile=null;
    public YourService() {
        super("DownloadService");
    }
    SessionManager session;
    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {

        session =new SessionManager(getApplicationContext());
        repository=new DatabaseRepository(this);
   // notification = new DownloadNotification(this);
        context=this;
        downloadId=intent.getStringExtra(VIDEO_ID);
        DownloadFile fileDownload=repository.getFile(downloadId);
       try {
            String urlPath = intent.getStringExtra(urlpath);
            String fileName = intent.getStringExtra(FILENAME);
            String type = intent.getStringExtra(TYPE);
            File output = null;

           String file = null;
            if (type .equalsIgnoreCase("0")) {
                file = fileName;

                output=new File(DownloadSupport.getVideoDirectory(this,session));
                videofile = new File(DownloadSupport.getVideoDirectory(this,session)+ "/" + File.separator +file);
               // videofile.createNewFile();

            } else {
                file = fileName + ".pdf";
                output=new File(DownloadSupport.getPdfDirectory(this,session));
                videofile = new File(DownloadSupport.getPdfDirectory(this,session),file);
             //   videofile.createNewFile();

            }
               if(fileDownload==null || !fileDownload.isDownloadComplete()){
                   if (type .equals("1")) {
//                       Constant.logPrint(type,"typetypetypetypetype");

                     /*  DownloadFile downloadFile=new DownloadFile();
                       downloadFile.setId(downloadId);
                       downloadFile.setDownloadId(downloadId);
                       downloadFile.setCurrentBytes(0);
                       downloadFile.setTotalBytes(0);
                       downloadFile.setFileName(fileName);
                       downloadFile.setDownloadStatus(DownloadStatus.START);
                       downloadFile.setFilepath(DownloadSupport.getPdfDirectory(context,session));
                       downloadFile.setThumbPath(DownloadSupport.getImageDirectory(context,session));
                       downloadFile.setVideoUrlOffline(videofile.getAbsolutePath());
                       repository.Insert(downloadFile);*/
                   ////    showNotification(fileName,"Downloading Start",0);




                       AndroidNetworking.download(urlPath,output.getAbsolutePath(),file)
                               .setTag("downloadTest")
                               .setPriority(Priority.HIGH)
                               .build()
                               .setDownloadProgressListener(new DownloadProgressListener() {
                                   @RequiresApi(api = Build.VERSION_CODES.M)
                                   @Override
                                   public void onProgress(long bytesDownloaded, long totalBytes) {
                                       if(totalBytes>0) {
                                           String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                                           /////      showNotification(fileName,"Downloading : "+percent+" %",Integer.parseInt(percent));
                                           ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                                           NetworkInfo netInfo = cm.getActiveNetworkInfo();
                                           //should check null because in airplane mode it will be null
                                           NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
                                           String downSpeed = nc.getLinkDownstreamBandwidthKbps() + "";
                                           Long dd = Long.parseLong(downSpeed) / 1000;
                                           WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                                           WifiInfo w = wifi.getConnectionInfo();
                                           Integer ddd = w.getLinkSpeed();
                                           Long remaininbyte = totalBytes - bytesDownloaded;
                                           Long sec = remaininbyte / (dd);//get the sec from m/m/s

                                           Long endTime = System.currentTimeMillis();

                                           repository.Updatewithper(bytesDownloaded, totalBytes, intent.getStringExtra(VIDEO_ID), percent, sec + "", ddd + "", DownloadStatus.CONTINUE, "1", endTime);
                                       }
                                       ///    notification.updateNotification(context,percent, fileName, "Downloading :"+percent+"% ");
                                   }
                               })
                               .startDownload(new DownloadListener() {
                                   @Override
                                   public void onDownloadComplete() {
                                       result = Activity.RESULT_OK;
                                     Long  startTime = System.currentTimeMillis();

                                       ///   showNotification(fileName,"Downloaded: "+100 +"%",100);
                                       repository.Update(DownloadStatus.DOWNLOADED,true,downloadId,startTime);
                                       publishResults(videofile.getAbsolutePath(), result);
                                      // notification.DownloadComplete(fileName, videofile, context,downloadId);

                                   }
                                   @Override
                                   public void onError(ANError error) {
                                       result = Activity.RESULT_CANCELED;
                                       repository.Delete(downloadId,"1");
                                    ///   showNotification(fileName,"Downloading Failed",0);
                                       //notification.failDownloadNotification(context,downloadId);
                                       Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                                   }
                               });


                  // repository.UpdateDownloadId(VIDEO_ID,downloadId);
               }else{


                       AndroidNetworking.download(urlPath,output.getAbsolutePath(),file)
                               .setTag("downloadTest")
                               .setPriority(Priority.HIGH)
                               .build()
                               .setDownloadProgressListener(new DownloadProgressListener() {
                                   @RequiresApi(api = Build.VERSION_CODES.M)
                                   @Override
                                   public void onProgress(long bytesDownloaded, long totalBytes) {
                                       if(totalBytes>0) {

                                       String percent = String.valueOf((bytesDownloaded * 100) / totalBytes);
                                       /////      showNotification(fileName,"Downloading : "+percent+" %",Integer.parseInt(percent));
                                       ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                                       NetworkInfo netInfo = cm.getActiveNetworkInfo();
                                       //should check null because in airplane mode it will be null
                                       NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
                                       String downSpeed = nc.getLinkDownstreamBandwidthKbps() + "";
                                       Long dd = Long.parseLong(downSpeed) / 1000;

                                       Long remaininbyte = totalBytes - bytesDownloaded;
                                       Long sec = remaininbyte / (dd);//get the sec from m/m/s
                                       Long endTime = System.currentTimeMillis();


                                       repository.Updatewithper(bytesDownloaded, totalBytes, intent.getStringExtra(VIDEO_ID), percent, sec + "", dd + "", DownloadStatus.CONTINUE, "0", endTime);

                                       ///    notification.updateNotification(context,percent, fileName, "Downloading :"+percent+"% ");
                                   }
                                   }
                               })
                               .startDownload(new DownloadListener() {
                                   @Override
                                   public void onDownloadComplete() {
                                       result = Activity.RESULT_OK;
                                       ///   showNotification(fileName,"Downloaded: "+100 +"%",100);
                                       Long  startTime = System.currentTimeMillis();
                                       repository.Update(DownloadStatus.DOWNLOADED,true,downloadId,startTime);
                                       publishResults(videofile.getAbsolutePath(), result);
                                       // notification.DownloadComplete(fileName, videofile, context,downloadId);

                                   }
                                   @Override
                                   public void onError(ANError error) {
                                       result = Activity.RESULT_CANCELED;
                                       repository.Delete(downloadId,"0");
                                       ///   showNotification(fileName,"Downloading Failed",0);
                                       //notification.failDownloadNotification(context,downloadId);
                                       Toast.makeText(context, "Downloaded Failed", Toast.LENGTH_SHORT).show();

                                   }
                               });

                    /*   //ExternalDevices
                       DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                       //Uri uri = Uri.parse("http://www.example.com/myfile.mp3");
                       Uri uri = Uri.parse(urlPath);

                       DownloadManager.Request request = new DownloadManager.Request(uri);
                       request.setTitle(fileName);
                       request.setDescription("Downloading");
                       request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
                       request.setVisibleInDownloadsUi(false);
                       final Uri uril = (!videofile.getAbsolutePath().endsWith(".jpg") && Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) ?
                               FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", videofile) :
                               Uri.fromFile(videofile);
                       request.setDestinationUri(uril);
                       // request.setDestinationUri(Uri.parse(output.getAbsolutePath()));

                       downloadmanager.enqueue(request);*/
               }

           }else{


           }
        }catch (Exception e){
           result = Activity.RESULT_CANCELED;
        }
/*

        if (output.exists()) {
            output.delete();
        }

        InputStream stream = null;
        FileOutputStream fos = null;
        try {

            URL url = new URL(urlPath);
            stream = url.openConnection().getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            fos = new FileOutputStream(output.getPath());
            int next = -1;
            while ((next = reader.read()) != -1) {
                fos.write(next);
            }
            // successfully finished
            result = Activity.RESULT_OK;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/

    }

    private void publishResults(String outputPath, int result) {
     /*   Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(FILEPATH, outputPath);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);*/
    }
    public void showNotification(String title, String body,int progress) {
        Bitmap logo;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
            builder.setColor(getResources().getColor(R.color.black));
        } else {
            builder.setSmallIcon(android.R.drawable.stat_sys_download);
        }
        logo = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setContentTitle(title)
                .setContentText(body)
                .setLargeIcon(logo)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setColorized(true)
                .setAutoCancel(true)
                .setSound(null)
                .setNotificationSilent()
                .setProgress(100,progress,false)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setDefaults(Notification.BADGE_ICON_LARGE)
                .setLights(1, 1, 1)
                .setOngoing(false)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            channel.enableVibration(true);
            channel.setSound(null, null);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            if(progress==100){
                notificationManager.notify(Integer.parseInt(downloadId), builder.build());
                notificationManager.cancel(Integer.parseInt(downloadId));
            }else{
                notificationManager.notify(Integer.parseInt(downloadId), builder.build());
            }

        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            Notification notificationCompat = builder.build();
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);

            if(progress==100){
                managerCompat.notify(Integer.parseInt(downloadId), notificationCompat);
                notificationManager.cancel(Integer.parseInt(downloadId));
            }else{
                managerCompat.notify(Integer.parseInt(downloadId), notificationCompat);
            }
        }



    }


}