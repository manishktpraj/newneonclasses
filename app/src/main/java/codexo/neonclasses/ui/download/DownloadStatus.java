package codexo.neonclasses.ui.download;

public class DownloadStatus {
    public static String START="START";
    public static String PAUSE="PAUSE";
    public static String CONTINUE="CONTINUE";
    public static String FAILED="FAILED";
    public static String DOWNLOADED="COMPLETED";
}
