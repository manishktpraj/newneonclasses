package codexo.neonclasses.ui.download;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import codexo.neonclasses.Constant;
import codexo.neonclasses.databinding.ActivityDownloadsBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.database.AllDownloadsActivity;
import codexo.neonclasses.ui.database.DatabaseRepository;
import codexo.neonclasses.ui.database.entities.DownloadFile;
import codexo.neonclasses.ui.descriptive.Descriptive;

public class DownloadsActivity extends AppCompatActivity {

    private ActivityDownloadsBinding binding;
    public JSONObject download_detail =new JSONObject();
    SessionManager session;
    String type ="0";
    File videofile;
    private DatabaseRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDownloadsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(DownloadsActivity.this);
        Constant.checkAdb(DownloadsActivity.this);
        binding.imgBack.setOnClickListener(view -> finish());
        binding.txtHello.setText("My Downloads");
        binding.txtHello.setTypeface(Constant.getFontsBold(DownloadsActivity.this));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Videos"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Study Material"));
        session = new SessionManager(DownloadsActivity.this);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        repository=new DatabaseRepository(this);

        try {
          type =getIntent().getStringExtra("type");
            VideoDownloadsFragment ldf =new VideoDownloadsFragment();
            StudyMaterialDownloadsFragment ldfs =new StudyMaterialDownloadsFragment();
            Bundle args = new Bundle();
            Bundle argss = new Bundle();
            if(getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equals("0")) {
                download_detail = new JSONObject(getIntent().getStringExtra("download_detail"));
                args.putString("download_detail", download_detail+"");
            }
            if(getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equals("1")) {
                download_detail = new JSONObject(getIntent().getStringExtra("download_detail"));
//                Constant.logPrint(download_detail+"","download_detaildownload_detaildownload_detaildownload_detail");
                String fileName = download_detail.getString("pd_title");
                videofile = new File(DownloadSupport.getPdfDirectory(getApplicationContext(), session)+fileName+".pdf");
                String study_url = getIntent().getStringExtra("study_url");
                String Livepath = study_url+download_detail.getString("pd_file");
                argss.putString("download_detail", download_detail+"");
                DownloadFile downloadFile=new DownloadFile();
                downloadFile.setId(download_detail.getString("pd_id"));
                downloadFile.setDownloadId(download_detail.getString("pd_id"));
                downloadFile.setCurrentBytes(0);
                downloadFile.setTotalBytes(0);
                downloadFile.setFileName(fileName);
                downloadFile.setDownloadStatus(DownloadStatus.START);
                downloadFile.setFilepath(DownloadSupport.getPdfDirectory(getApplicationContext(), session));
                downloadFile.setThumbPath("");
                downloadFile.setVideoUrlOffline(Livepath);
                repository.Insert(downloadFile);
//                Constant.logPrint("downloadFiledownloadFile",downloadFile.getFileName()+"");
//                Constant.logPrint("fileNamefileNamefileNamefileNamefileNamefileName",fileName+"");
                Constant.setToast(DownloadsActivity.this,"fs");
            }
            ldf.setArguments(argss);
            adapter.addFragment(ldf, "Videos");


            ldfs.setArguments(args);
            adapter.addFragment(ldfs, "Study Material");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        binding.viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newin=new Intent(getApplicationContext(), AllDownloadsActivity.class);
                startActivity(newin);
            }
        });


        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        if(type!=null && type.equals("1")) {
            binding.viewPager.setCurrentItem(1);
        }

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onBackPressed(){
         this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(DownloadsActivity.this);
        Constant.checkAdb(DownloadsActivity.this);
    }
}
