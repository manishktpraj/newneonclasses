package codexo.neonclasses.ui.download.pdfviewer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.github.barteksc.pdfviewer.listener.OnTapListener;

import java.io.File;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.ActivityNewsBinding;
import codexo.neonclasses.databinding.ActivityPdfViewerBinding;
import codexo.neonclasses.service.action.DownloadSupport;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.descriptive.Descriptive;

public class PdfViewer extends AppCompatActivity {
private ActivityPdfViewerBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPdfViewerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.adbEnabled(PdfViewer.this);
        Constant.checkAdb(PdfViewer.this);
        File myFile = new File(getIntent().getStringExtra("file"));
//        Constant.logPrint(myFile+"","myFile");
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.txtHello.setText(basename(getIntent().getStringExtra("file")));
        binding.PDFView.fromFile(myFile)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .onDraw(new OnDrawListener() {
                    @Override
                    public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {

                    }
                }).onDrawAll(new OnDrawListener() {
            @Override
            public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {

            }
        }).onPageError(new OnPageErrorListener() {
            @Override
            public void onPageError(int page, Throwable t) {
                Toast.makeText(PdfViewer.this, "Error:"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }).onPageChange(new OnPageChangeListener() {
            @Override
            public void onPageChanged(int page, int pageCount) {
                Toast.makeText(PdfViewer.this, "Page : "+page, Toast.LENGTH_SHORT).show();
            }
        }).onTap(new OnTapListener() {
            @Override
            public boolean onTap(MotionEvent e) {
                return true;
            }
        }).onRender(new OnRenderListener() {
            @Override
            public void onInitiallyRendered(int nbPages, float pageWidth, float pageHeight) {
                binding.PDFView.fitToWidth();
            }
        }).enableAnnotationRendering(true)
                .invalidPageColor(Color.WHITE)
                .load();

        binding.menudata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), binding.menudata);
                popupMenu.inflate(R.menu.chooseactionprint);
                 ///       popupMenu.setGravity();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        // Toast message on menu item clicked

                        switch (menuItem.getItemId()) {
                            case R.id.x05: {
                                File myFile = new File(getIntent().getStringExtra("file"));


                                new Thread(new Runnable() {
                                    public void run() {
                                        Uri path = Uri.fromFile(myFile);
                                        try {
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setDataAndType(path, "application/pdf");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        } catch (ActivityNotFoundException e) {
                                       ////     tv_loading.setError("PDF Reader application is not installed in your device");
                                        }
                                    }
                                }).start();

                                break;
                            }
                            case R.id.delete: {
                                File myFile = new File(getIntent().getStringExtra("file"));

                                Uri uri = FileProvider.getUriForFile(getApplicationContext(), "codexo.neonclasses.provider", myFile);
                                Intent intent = ShareCompat.IntentBuilder.from(PdfViewer.this)
                                        .setType("application/pdf")
                                        .setStream(uri)
                                        .setChooserTitle("Choose bar")
                                        .createChooserIntent()
                                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                startActivity(intent);
                                  break;
                            }
                        }

                        // Showing the popup menu


                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();

            }
        });
    }

    public static String basename(String path) {
        String filename = path.substring(path.lastIndexOf('/') + 1);

        if (filename == null || filename.equalsIgnoreCase("")) {
            filename = "";
        }else{

            filename = filename.substring(0, filename.lastIndexOf('.'));


        }
        return filename;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(PdfViewer.this);
        Constant.checkAdb(PdfViewer.this);
    }
}