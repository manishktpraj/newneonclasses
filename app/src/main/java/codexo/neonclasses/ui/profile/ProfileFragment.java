package codexo.neonclasses.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import codexo.neonclasses.Constant;
import codexo.neonclasses.R;
import codexo.neonclasses.databinding.FragmentProfileBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.help.HelpActivity;
import codexo.neonclasses.ui.home.HomeViewModel;
import codexo.neonclasses.ui.offer.OfferActivity;
import codexo.neonclasses.ui.plus.NeonPlusActivity;
import codexo.neonclasses.ui.purchase.MyPurchaseActivity;
import codexo.neonclasses.ui.share.ShareActivity;
import codexo.neonclasses.ui.subscription.SubscriptionActivity;

public class ProfileFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentProfileBinding binding;
    SessionManager session;
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        session = new SessionManager(getContext());
        context =getContext();
        if(!session.getLastName().equals("null")) {
            binding.txtName.setText(session.getFirstName() + " " + session.getLastName());
        }else{
            binding.txtName.setText(session.getFirstName());
        }
        binding.txtNumber.setText(session.getUserPhone());


        binding.txtHelp.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), HelpActivity.class));
        });

        binding.txtPlus.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), NeonPlusActivity.class));
        });

        binding.txtMyProfile.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MyProfileActivity.class));
        });

        binding.txtMyPurchases.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MyPurchaseActivity.class));
        });

        binding.txtMySubs.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), SubscriptionActivity.class));
        });

        binding.txtOffer.setOnClickListener(view -> {
            Intent intent = new Intent(requireContext(), OfferActivity.class);
            intent.putExtra("type", "Home");
            intent.putExtra("amount", "amount");
            intent.putExtra("apply_on", "apply_on");
            intent.putExtra("apply_on_id", "apply_on_id");
            startActivity(intent);
        });

        binding.txtShare.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), ShareActivity.class));
        });
        binding.txtRate.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=codexo.neonclasses"));
            startActivity(browserIntent);
        });

        binding.txtSignOut.setOnClickListener(view -> {
            customalert(view);
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void customalert(View view) {
        final Dialog builder = new  Dialog(view.getContext());
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.custome_alert, null);
        TextView txt_dia =  dialogLayout.findViewById(R.id.txt_dia);
        txt_dia.setText("Do you realy want to logout ?");

        TextView btn_yes =  dialogLayout.findViewById(R.id.btn_yes);
        TextView btn_no =  dialogLayout.findViewById(R.id.btn_no);
        builder.setContentView(dialogLayout);
        builder.setCancelable(true);
        builder.show();

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.home="0";
                session.logoutUser();
                builder.dismiss();
                getActivity().finish();

            }
        });
    }
}
