package codexo.neonclasses.ui.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import codexo.neonclasses.Constant;
import codexo.neonclasses.Login;
import codexo.neonclasses.MainActivity;
import codexo.neonclasses.databinding.ActivityMyProfileBinding;
import codexo.neonclasses.session.SessionManager;
import codexo.neonclasses.ui.plus.NeonPlusActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class MyProfileActivity extends AppCompatActivity {

    private ActivityMyProfileBinding binding;
    SessionManager session;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMyProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.imgBack.setOnClickListener(view -> finish());
        session = new SessionManager(getApplicationContext());
        Constant.adbEnabled(MyProfileActivity.this);
        Constant.checkAdb(MyProfileActivity.this);
        binding.editFnLabel.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editFn.setText(session.getFirstName());
        binding.editFn.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editFn.setEnabled(false);

        binding.editEmailLabel.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editEmail.setText(session.getUserEmail());
        binding.editEmail.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editEmail.setEnabled(false);
        binding.editPhoneLabel.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editPhone.setText(session.getUserPhone());
        binding.editPhone.setTypeface(Constant.getsemiFonts(getApplicationContext()));
        binding.editPhone.setEnabled(false);
        pDialog = Constant.getProgressBar(MyProfileActivity.this);

        binding.updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String go_next = "yes";
                String username_string = binding.editFn.getText().toString();
                 if(username_string.equals(""))
                {
                    binding.editFn.setError("Please Enter Full Name");
                    go_next ="no";
                }

                if(go_next.equals("yes")) {
                    updateprofile(username_string);
                }
            }
        });
    }


    private void updateprofile(final String username_string) {


        if (!pDialog.isShowing()){
            pDialog.show();
        }
        Call<String> call = Constant.getUrl().updateprofile(session.getUserId(),username_string);

        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {


                            JSONObject localJSONObject2 = jsonObject.getJSONObject("results");
                            session.createLoginSession(localJSONObject2);
                            Constant.setToast(MyProfileActivity.this,notification);

                        } else {
                            Constant.setToast(MyProfileActivity.this,notification);

                           /* new AlertDialog.Builder(activity)
                                    .setCancelable(false)
                                    .setMessage("Login failed! Please enter correct username or password.")
                                    .setNegativeButton("ok", null).show();*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                updateprofile(username_string);
            }
        });
    }
    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(MyProfileActivity.this);
        Constant.checkAdb(MyProfileActivity.this);
    }
}