package codexo.neonclasses;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.exoplayer2.util.Log;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import codexo.neonclasses.databinding.ActivityChangepasswordBinding;
import retrofit2.Call;
import retrofit2.Callback;

public class Changepassword extends AppCompatActivity {
    AppCompatActivity activity;
    TextView firsttext,secondtext,sign_up;
    TextInputLayout password,repassword;
    TextInputEditText default_edit_text,redefault_edit_text;
    JSONObject student_data;
    ProgressDialog pDialog;
    String newpass="",confpass="";
    String student_id="0";
    ActivityChangepasswordBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        Toolbar toolbar = findViewById(R.id.toolbarr);
        activity = Changepassword.this;
        Constant.setToolbar(activity, toolbar);
        pDialog = Constant.getProgressBar(Changepassword.this);
        Constant.adbEnabled(Changepassword.this);
        Constant.checkAdb(Changepassword.this);
        firsttext =findViewById(R.id.firsttext);
        secondtext =findViewById(R.id.secondtext);
        sign_up =findViewById(R.id.sign_up);
        password= findViewById(R.id.password);
        repassword= findViewById(R.id.repassword);
        default_edit_text =findViewById(R.id.default_edit_text);
        redefault_edit_text =findViewById(R.id.redefault_edit_text);
        default_edit_text.setTypeface(Constant.getFonts(getApplicationContext()));
        redefault_edit_text.setTypeface(Constant.getFonts(getApplicationContext()));
        default_edit_text.setEnabled(true);
        default_edit_text.setTextIsSelectable(true);
        default_edit_text.setFocusable(true);
        default_edit_text.setFocusableInTouchMode(true);
        default_edit_text.requestFocus();
        firsttext.setTypeface(Constant.getFontsBold(getApplicationContext()));
        secondtext.setTypeface(Constant.getFonts(getApplicationContext()));
        sign_up.setTypeface(Constant.getsemiFonts(getApplicationContext()));


        try {
            student_data = new JSONObject(getIntent().getStringExtra("results"));
            student_id = student_data.getString("student_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent iLogin = new Intent(getApplicationContext(), Intro.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(iLogin);
                finish();*/
                String go_next = "yes";
                newpass = default_edit_text.getText().toString();
                confpass = redefault_edit_text.getText().toString();
                if(newpass.equals(""))
                {
                    default_edit_text.setError("Please Enter New Password");
                    go_next ="no";
                }
                if(confpass.equals(""))
                {
                    redefault_edit_text.setError("Please Re-enter Password");
                    go_next ="no";
                }

                if(!confpass.equals(newpass))
                {
                     redefault_edit_text.setError("Confirm Password does not match");
                    go_next ="no";


                }

                if(go_next.equals("yes")) {
                    changepassword();
                }
            }
        });
    }

    private void changepassword() {


        if (!pDialog.isShowing()){
            pDialog.show();
        }

        Call<String> call = Constant.getUrl().changepassword(student_id,newpass);


        call.clone().enqueue(new Callback<String>() {

            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response_string) {

                pDialog.dismiss();

                String response = response_string.body();

//                Constant.logPrint("response", response);

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String message = jsonObject.getString("message");
                        String notification = jsonObject.getString("notification");

                        if (message.equals("ok")) {

                            Constant.setToast(Changepassword.this,notification);
                            Intent i = new Intent(activity, Intro.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                             activity.startActivity(i);
                           finish();

                        } else {
                            Constant.setToast(Changepassword.this,notification);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                changepassword();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.adbEnabled(Changepassword.this);
        Constant.checkAdb(Changepassword.this);
    }
}